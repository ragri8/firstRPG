from core.Life_Entities import *
from core.Monster import Monster
from core.Playable_Character import PlayableCharacter
from objects.old.Equipment_Sheet import *


monsterweapon = ("claws", 2, 4, 1)


Player1 = PlayableCharacter(level=1, const=10, strength=10, intel=10, agility=10, defense=10, luck=6, gold=20, spritetag="player_spr_1")
Player1.personnalID(name="Deziar", race="Lizard-men", gender="male", fightclass="Warrior")
Player1.setweapon(rapier1)
Player1.setequipment(mequipment1)
Player1.setequipment(mequipment2)

Player2 = PlayableCharacter(level=1, const=10, strength=10, intel=10, agility=10, defense=10, luck=6, gold=15, spritetag="player_spr_2")
Player2.personnalID(name="Antho", race="Human", gender="male", fightclass="Tank")
Player2.setweapon(axe1)
Player2.setequipment(hequipment1)

Player3 = PlayableCharacter(level=1, const=10, strength=10, intel=10, agility=10, defense=10, luck=6, gold=15, spritetag="player_spr_3")
Player3.personnalID(name="Geneviève", race="Elf", gender="female", fightclass="Spellcaster")
Player3.setweapon(staff1)
Player3.setequipment(lequipment1)
Player3.setequipment(lequipment2)
# Player3.setspell(StatusSpell(*hspell1))
# Player3.setspell(StatusSpell(*hspell2))

Team = PlayerParty()
Team.addMember(Player1)
Team.addMember(Player2)
Team.addMember(Player3)


Monster1 = Monster(*wolf1)
Monster2 = Monster(*wolf1)
Monster3 = Monster(*wolf2)
Monster4 = Monster(*rat1)
Monster5 = Monster(*rat1)
Monster6 = Monster(*scarecrow)
Monster7 = Monster(*rat1)
Monster8 = Monster(*rat1)
Monster9 = Monster(*wolf1)
Monster9.boss()
Monster10 = Monster(*wolf1)
Monster11 = Monster(*wolf1)

MonsterTeam1 = [Monster4, Monster5]
MonsterTeam2 = [Monster6, Monster7, Monster8]
MonsterTeam3 = [Monster1, Monster2, Monster3]
MonsterTeam4 = [Monster11, Monster9, Monster10]

# print(Player1.weapon)
# Player1.weapon.display()
# print(Player2.weapon)
# Player2.weapon.display()
#Player1.weapon.refine(Player1)
Player1.setitems(hitem)
Player2.setitems(ritem1)
Player2.setitems(ritem1)
Player2.setitems(ritem1)
Player2.setitems(hitem)
Player2.setitems(ritem2)
Player2.setitems(ritem3)

# for i in Team:
#    i.givexp(50)

#Player1.display()
#Player2.display()
#Player3.display()
print("\n")
