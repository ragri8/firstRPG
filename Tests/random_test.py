from copy import copy, deepcopy
from core.Combat_Mechanics.Spell import *
chat = "groos gros chat"


class Obj:
    def __init__(self):
        self.param1 = [1]
        self.param2 = 2

    def changeParam1(self, value):
        self.param1[0] = value

    def copy(self):
        return deepcopy(self)

    def display(self):
        print(self.param1)
        print(self.param2)


chat_type = type(chat)

print(chat_type)

spell = OffensiveSpell("boule de poil", 10, 12, 3, 2, 10, "fire")

print(spell.type())

print("\n\n")

test_list = ["Hello", "darkness", "my", "old", "friend"]

for i in test_list.__reversed__():
    print(i)
    if i == "darkness":
        test_list.remove(i)

print("\n")


obj1 = Obj()

obj2 = obj1.copy()

obj2.param1.append(42)

obj1.display()

obj2.display()


print("\n\nNew tests:\n")

print("Size of [1, 2]: {}".format(len([1, 2])))
print("Size of [1, 2, ]: {}".format(len([1, 2, ])))
