from core.Handlers.Menu_Handler import MenuHandler
from core.Life_Entities import EnemyParty, PlayerParty
from core.Playable_Character import PlayableCharacter
from core.Monster import Monster
from objects.Armor_Sheet import *
from objects.Weapon_Sheet import *
from objects.Item_Sheet import *
from objects.Monsters_Sheet import wolf1, wolf2, wolf3, bear1, bear2, bat1, rat1, rat2, mimic1, bandit1, bandit2,\
                                    bandit3, bandit4
from objects.Texture_Sheet import grass_back
from core.Skill_Unlock import *

Player1 = PlayableCharacter(level=1, const=10, strength=10, intel=10, agility=10, defense=0, luck=5,
                            gold=20, spritetag="player_spr_1")
Player1.personnalID(name="Deziar", race="Lizard-men", gender="male", fightclass="Warrior")
Player1.newSetItem(sword1)
Player1.newSetItem(sword2)
Player1.newSetItem(mequipment1)
Player1.newSetItem(mequipment2)
Player1.newSetItem(mequipment3)
Player1.newSetWeapon(sword1)
Player1.newSetEquipment(mequipment1)
Player1.newSetEquipment(mequipment2)
Player1.newSetEquipment(mequipment3)
Player1.newSetItem(lifeBottle)
Player1.newSetItem(apple)
Player1.newSetItem(apple)

Player2 = PlayableCharacter(level=1, const=10, strength=10, intel=10, agility=10, defense=0, luck=5,
                            gold=15, spritetag="player_spr_2")
Player2.personnalID(name="Antho", race="Human", gender="male", fightclass="Tank")
Player2.newSetItem(axe1)
Player2.newSetItem(hequipment1)
Player2.newSetItem(hequipment4)
Player2.newSetWeapon(axe1)
Player2.newSetEquipment(hequipment1)
Player2.newSetEquipment(hequipment4)
Player1.newSetItem(lifeBottle)
Player2.newSetItem(apple)
Player2.newSetItem(apple)

Player3 = PlayableCharacter(level=1, const=10, strength=10, intel=10, agility=10, defense=0, luck=5,
                            gold=15, spritetag="player_spr_3")
Player3.personnalID(name="Geneviève", race="Elf", gender="female", fightclass="Spellcaster")
Player3.newSetItem(staff1)
Player3.newSetItem(lequipment1)
Player3.newSetItem(lequipment2)
Player3.newSetItem(lequipment3)
Player3.newSetWeapon(staff1)
Player3.newSetEquipment(lequipment1)
Player3.newSetEquipment(lequipment2)
Player3.newSetEquipment(lequipment3)
Player3.newSetItem(apple)
Player3.newSetItem(apple)

team = PlayerParty()
team.addMember(Player1)
team.addMember(Player2)
team.addMember(Player3)

for member in team.members:
    member.organise_items()

for member in team.members:
    print(member.giveXp(50))
    print(member.giveXp(100))
    print(member.giveXp(200))
    print(member.giveXp(300))
    print(member.giveXp(500))
    print(member.giveXp(800))
    print(member.giveXp(1250))
for index1 in range(30):
    team.members[2].fightStatus.spellUsed("fire")
    team.members[2].fightStatus.spellUsed("electric")
    team.members[2].fightStatus.spellUsed("healing")
    team.members[2].fightStatus.spellUsed("earth")
    team.members[2].fightStatus.spellUsed("ice")
    team.members[2].fightStatus.spellUsed("buff")
    team.members[2].fightStatus.spellUsed("fire")
    team.members[2].fightStatus.spellUsed("electric")
    team.members[2].fightStatus.spellUsed("healing")
    team.members[2].fightStatus.spellUsed("earth")
    team.members[2].fightStatus.spellUsed("ice")
    team.members[2].fightStatus.spellUsed("buff")
    team.members[2].fightStatus.spellUsed("fire")
    team.members[2].fightStatus.spellUsed("electric")
    team.members[2].fightStatus.spellUsed("healing")
    team.members[2].fightStatus.spellUsed("earth")
    team.members[2].fightStatus.spellUsed("ice")
    team.members[2].fightStatus.spellUsed("buff")
    team.members[2].spellLevelUp()

if team.members[2].name == "Geneviève":
    team.members[2].spellPoint = 500

    skill_tree = team.members[0].fightclass.skill_tree
    if skill_tree.spells[0].checkPrerequite(team.members[2]):
        if skill_tree.spells[0].setSkill(team.members[2]):
            print("{} spell added".format(skill_tree.spells[0].skill.name))
        else:
            print("Unexpected failure")
    else:
        print("Doesn't respect prerequite!")

    if skill_tree.spells[1].checkPrerequite(team.members[2]):
        if skill_tree.spells[1].setSkill(team.members[2]):
            print("{} spell added".format(skill_tree.spells[1].skill.name))
        else:
            print("Unexpected failure")
    else:
        print("Doesn't respect prerequite!")

    if skill_tree.spells[2].checkPrerequite(team.members[2]):
        if skill_tree.spells[2].setSkill(team.members[2]):
            print("{} spell added".format(skill_tree.spells[2].skill.name))
        else:
            print("Unexpected failure")
    else:
        print("Doesn't respect prerequite!")

    if skill_tree.spells[3].checkPrerequite(team.members[2]):
        if skill_tree.spells[3].setSkill(team.members[2]):
            print("{} spell added".format(skill_tree.spells[3].skill.name))
        else:
            print("Unexpected failure")
    else:
        print("Doesn't respect prerequite!")

    if skill_tree.spells[4].checkPrerequite(team.members[2]):
        if skill_tree.spells[4].setSkill(team.members[2]):
            print("{} spell added".format(skill_tree.spells[4].skill.name))
        else:
            print("Unexpected failure")
    else:
        print("Doesn't respect prerequite!")

    if skill_tree.spells[5].checkPrerequite(team.members[2]):
        if skill_tree.spells[5].setSkill(team.members[2]):
            print("{} spell added".format(skill_tree.spells[5].skill.name))
        else:
            print("Unexpected failure")
    else:
        print("Doesn't respect prerequite!")

    if skill_tree.spells[6].checkPrerequite(team.members[2]):
        if skill_tree.spells[6].setSkill(team.members[2]):
            print("{} spell added".format(skill_tree.spells[6].skill.name))
        else:
            print("Unexpected failure")
    else:
        print("Doesn't respect prerequite!")

    if skill_tree.spells[7].checkPrerequite(team.members[2]):
        if skill_tree.spells[7].setSkill(team.members[2]):
            print("{} spell added".format(skill_tree.spells[7].skill.name))
        else:
            print("Unexpected failure")
    else:
        print("Doesn't respect prerequite!")

    if skill_tree.spells[8].checkPrerequite(team.members[2]):
        if skill_tree.spells[8].setSkill(team.members[2]):
            print("{} spell added".format(skill_tree.spells[3].skill.name))
        else:
            print("Unexpected failure")
    else:
        print("Doesn't respect prerequite!")

    if skill_tree.spells[9].checkPrerequite(team.members[2]):
        if skill_tree.spells[9].setSkill(team.members[2]):
            print("{} spell added".format(skill_tree.spells[9].skill.name))
        else:
            print("Unexpected failure")
    else:
        print("Doesn't respect prerequite!")

    if skill_tree.spells[10].checkPrerequite(team.members[2]):
        if skill_tree.spells[10].setSkill(team.members[2]):
            print("{} spell added".format(skill_tree.spells[10].skill.name))
        else:
            print("Unexpected failure")
    else:
        print("Doesn't respect prerequite!")

print("Spell list:")
for fighter in team.members:
    print("{}'s spells:".format(fighter))
    for spell in fighter.spellList:
        print(spell.name)

print("Point left: {}".format(team.members[2].spellPoint))

team.members[0].hpleft -= 25

menu = MenuHandler(team)
menu.setup()
