from core.Handlers.Fight_Handler import FightHandler
from core.Life_Entities import EnemyParty, PlayerParty
from core.Monster import Monster
from core.Playable_Character import PlayableCharacter
from objects.Armor_Sheet import *
from objects.Weapon_Sheet import *
from objects.Item_Sheet import *
from objects.Monsters_Sheet import wolf1, wolf2, wolf3, bear1, bear2, bat1, rat1, rat2, mimic1, bandit1, bandit2,\
                                    bandit3, bandit4
from objects.Texture_Sheet import grass_back
from core.Skill_Unlock import *

Player1 = PlayableCharacter(level=1, const=10, strength=10, intel=10, agility=10, defense=0, luck=5,
                            gold=20, spritetag="player_spr_1")
Player1.personnalID(name="Deziar", race="Lizard-men", gender="male", fightclass="Warrior")
Player1.newSetWeapon(sword1)
Player1.newSetEquipment(mequipment1)
Player1.newSetEquipment(mequipment2)
Player1.newSetEquipment(mequipment3)
Player1.newSetItem(lifeBottle)
Player1.newSetItem(apple)
Player1.newSetItem(apple)

Player2 = PlayableCharacter(level=1, const=10, strength=10, intel=10, agility=10, defense=0, luck=5,
                            gold=15, spritetag="player_spr_2")
Player2.personnalID(name="Antho", race="Human", gender="male", fightclass="Tank")
Player2.newSetWeapon(axe1)
Player2.newSetEquipment(hequipment1)
Player2.newSetEquipment(hequipment4)
Player1.newSetItem(lifeBottle)
Player2.newSetItem(apple)
Player2.newSetItem(apple)

Player3 = PlayableCharacter(level=1, const=10, strength=10, intel=10, agility=10, defense=0, luck=5,
                            gold=15, spritetag="player_spr_3")
Player3.personnalID(name="Geneviève", race="Elf", gender="female", fightclass="Spellcaster")
Player3.newSetWeapon(staff1)
Player3.newSetEquipment(lequipment1)
Player3.newSetEquipment(lequipment2)
Player3.newSetEquipment(lequipment3)
Player3.newSetItem(apple)
Player3.newSetItem(apple)

team = PlayerParty()
team.addMember(Player1)
team.addMember(Player2)
team.addMember(Player3)

for index in range(50):
    team.members[0].fightStatus.weaponUsed(team.members[0])
    team.members[0].weaponLevelUp()
    
for member in team.members:
    print(member.giveXp(50))
    print(member.giveXp(100))
#    print(member.giveXp(150))
#    print(member.giveXp(100))


if team.members[0].name == "Deziar":
    skill_tree = team.members[0].fightclass.skill_tree
    if skill_tree.skills[0].checkPrerequite(team.members[0]):
        skill_tree.skills[0].setSkill(team.members[0])
    else:
        print("Doesn't respect prerequite!")

if team.members[2].name == "Geneviève":
    skill_tree = team.members[0].fightclass.skill_tree
    if skill_tree.spells[0].checkPrerequite(team.members[2]):
        skill_tree.spells[0].setSkill(team.members[2])
    else:
        print("Doesn't respect prerequite!")
    if skill_tree.spells[1].checkPrerequite(team.members[2]):
        skill_tree.spells[1].setSkill(team.members[2])
    else:
        print("Doesn't respect prerequite!")

monster1 = Monster(*wolf1)
monster2 = Monster(*wolf2)
enemy_party = EnemyParty()
enemy_party.addMember(monster1)
enemy_party.addMember(monster2)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


monster3 = Monster(*wolf1)
enemy_party = EnemyParty()
enemy_party.addMember(monster3)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


monster4 = Monster(*wolf1)
monster5 = Monster(*rat1)
enemy_party = EnemyParty()
enemy_party.addMember(monster4)
enemy_party.addMember(monster5)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


monster6 = Monster(*rat2)
enemy_party = EnemyParty()
enemy_party.addMember(monster6)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


monster7 = Monster(*rat1)
monster8 = Monster(*bat1)
monster9 = Monster(*rat1)
enemy_party = EnemyParty()
enemy_party.addMember(monster7)
enemy_party.addMember(monster8)
enemy_party.addMember(monster9)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


monster10 = Monster(*wolf2)
enemy_party = EnemyParty()
enemy_party.addMember(monster10)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


monster11 = Monster(*wolf2)
monster12 = Monster(*rat2)
enemy_party = EnemyParty()
enemy_party.addMember(monster12)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


monster13 = Monster(*wolf1)
monster14 = Monster(*wolf1)
monster15 = Monster(*wolf1)
enemy_party = EnemyParty()
enemy_party.addMember(monster13)
enemy_party.addMember(monster14)
enemy_party.addMember(monster15)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


monster16 = Monster(*wolf2)
monster17 = Monster(*wolf2)
monster18 = Monster(*wolf2)
enemy_party = EnemyParty()
enemy_party.addMember(monster16)
enemy_party.addMember(monster17)
enemy_party.addMember(monster18)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


Player1.newSetEquipment(mequipment5)
Player1.newSetWeapon(sword2)
Player2.newSetEquipment(hequipment5)
Player2.newSetWeapon(axe2)
Player3.newSetEquipment(lequipment5)
Player3.newSetWeapon(staff2)
Player2.newSetItem(lightManaPotion)
Player3.newSetItem(apple)


monster19 = Monster(*bear1)
enemy_party = EnemyParty()
enemy_party.addMember(monster19)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


for member in team.members:
    print(member.giveXp(50))
    print(member.giveXp(100))
    print(member.giveXp(200))
    print(member.giveXp(130))


monster20 = Monster(*bear1)
monster21 = Monster(*wolf2)
enemy_party = EnemyParty()
enemy_party.addMember(monster20)
enemy_party.addMember(monster21)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


monster22 = Monster(*rat2)
monster23 = Monster(*bear1)
monster24 = Monster(*rat2)
enemy_party = EnemyParty()
enemy_party.addMember(monster22)
enemy_party.addMember(monster23)
enemy_party.addMember(monster24)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


monster25 = Monster(*bat1)
monster26 = Monster(*wolf2)
monster27 = Monster(*bat1)
enemy_party = EnemyParty()
enemy_party.addMember(monster25)
enemy_party.addMember(monster26)
enemy_party.addMember(monster27)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


monster28 = Monster(*wolf3)
monster28.boss(4, 4)
monster29 = Monster(*wolf2)
monster29.elite()
monster30 = Monster(*wolf2)
monster30.elite()
enemy_party = EnemyParty()
enemy_party.addMember(monster29)
enemy_party.addMember(monster28)
enemy_party.addMember(monster30)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


monster31 = Monster(*bear2)
monster32 = Monster(*bear2)
enemy_party = EnemyParty()
enemy_party.addMember(monster31)
enemy_party.addMember(monster32)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


Player1.newSetEquipment(mequipment6)
Player2.newSetEquipment(hequipment6)
Player3.newSetEquipment(lequipment6)
Player1.newSetEquipment(mequipment7)
Player2.newSetEquipment(hequipment7)
Player3.newSetEquipment(lequipment7)


monster33 = Monster(*mimic1)
enemy_party = EnemyParty()
enemy_party.addMember(monster33)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


for member in team.members:
    print(member.giveXp(200))
    print(member.giveXp(300))
    print(member.giveXp(400))


monster34 = Monster(*bandit1)
monster35 = Monster(*bandit2)
enemy_party = EnemyParty()
enemy_party.addMember(monster34)
enemy_party.addMember(monster35)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)


for index1 in range(12):
    team.members[2].fightStatus.spellUsed("fire")
    team.members[2].fightStatus.spellUsed("electric")
    team.members[2].fightStatus.spellUsed("healing")
    team.members[2].fightStatus.spellUsed("buff")
    team.members[2].fightStatus.spellUsed("fire")
    team.members[2].fightStatus.spellUsed("electric")
    team.members[2].fightStatus.spellUsed("healing")
    team.members[2].fightStatus.spellUsed("buff")
    team.members[2].fightStatus.spellUsed("fire")
    team.members[2].fightStatus.spellUsed("electric")
    team.members[2].fightStatus.spellUsed("healing")
    team.members[2].fightStatus.spellUsed("buff")
    team.members[2].spellLevelUp()
for index2 in range(21):
    team.members[2].fightStatus.lightArmor.hitTaken()
    team.members[0].fightStatus.weaponUsed(team.members[0])
    team.members[0].fightStatus.mediumArmor.hitTaken()
    team.members[1].fightStatus.heavyArmor.hitTaken()
    team.members[1].fightStatus.weaponUsed(team.members[1])
    team.members[2].fightStatus.lightArmor.hitTaken()
    team.members[0].fightStatus.weaponUsed(team.members[0])
    team.members[0].fightStatus.mediumArmor.hitTaken()
    team.members[1].fightStatus.heavyArmor.hitTaken()
    team.members[1].fightStatus.weaponUsed(team.members[1])
    team.members[2].fightStatus.lightArmor.hitTaken()
    team.members[0].fightStatus.weaponUsed(team.members[0])
    team.members[0].fightStatus.mediumArmor.hitTaken()
    team.members[1].fightStatus.heavyArmor.hitTaken()
    team.members[1].fightStatus.weaponUsed(team.members[1])
    team.members[0].weaponLevelUp()
    team.members[1].weaponLevelUp()
    team.members[0].armorLevelUp()
    team.members[1].armorLevelUp()
    team.members[2].armorLevelUp()


for member in team.members:
    print(member.giveXp(200))
    print(member.giveXp(300))
    print(member.giveXp(400))


monster36 = Monster(*bandit3)
monster37 = Monster(*bandit4)
monster38 = Monster(*bandit3)
enemy_party = EnemyParty()
enemy_party.addMember(monster36)
enemy_party.addMember(monster37)
enemy_party.addMember(monster38)
fight = FightHandler(team, enemy_party)
fight.setup(grass_back)

print("Total xp: {}".format(team.members[0].xp))

quit()
