import objects.Sprite_Sheet as spr
#
# Fighting class
# content: (name, classbonus: (const, strength, intel, agility, defense, luck), level bonus: (const, strength,
#           intel, agility, defense, luck), turnRecovery: (healthR, manaR, staminaR))
warriorclass = ("Warrior", 5, 6, 2, 5, 3, 0,
                3.45, 2.4, 1.2, 2.1, 1.7, 0.2,
                0, 0, 3)
spellcasterclass = ("Spellcaster", 3, 2, 7, 4, 3, 0,
                    2.85, 1.2, 2.7, 1.7, 1.5, 0.225,
                    0, 1, 1)
tankclass = ("Tank", 9, 4, 1, 2, 4, 0,
             4.85, 1.6, 1.1, 1.5, 2.1, 0.2,
             0, 0, 2)
rangerclass = ("Ranger", 4, 3, 3, 7, 2, 1,
               2.7, 1.5, 1.4, 2.4, 1.4, 0.25,
               0, 1, 2)


# Monster
# content: (level, const, strength, intel, agility, luck, gold, xp, pdef, mdef, fireresist, iceresist,
# earthresist, electricresist race, name, attacklist, spawn prob, spritelist, size)
wolf1 = (1.1, 9, 12, 10, 15, 6, 2, 3, 6, 4, -20, 0, 0, 0, "Wolf", "Young wolf", [claw1], 50, spr.wolf1, 60)
wolf2 = (1.9, 14, 15, 12, 18, 6, 4, 7, 6, 6, -20, 0, 0, 0, "Wolf", "Wild wolf", [claw1, bite1], 30, spr.wolf2, 60)
wolf3 = (3, 26, 18, 15, 22, 6, 10, 19, 8, 8, -20, 0, 0, 0, "Wolf", "Alpha wolf", [claw1, bite2], 10, spr.wolf3, 60)
wolf4 = (4.4, 40, 25, 18, 25, 4, 22, 48, 14, 10, -20, 0, 0, 0, "Wolf", "hunting wolf", [claw2, bite2], 10, spr.wolf4, 60)  # incomplete
wolf5 = (6.4, 80, 30, 21, 32, 5, 80, 154, 16, 12, -20, 0, 0, 0, "Wolf", "giant wolf", [claw2, bite3], 8, spr.wolf5, 80)  # incomplete
bear1 = (2.2, 21, 20, 12, 16, 5, 10, 9, 8, 6, -20, 0, -10, 0, "Bear", "Wild bear", [claw1, bite1], 24, spr.bear1, 80)
bear2 = (3.2, 38, 23, 15, 18, 5, 12, 23, 10, 8, -20, 0, 0, 0, "Bear", "Cave bear", [claw1, bite2], 16, spr.bear2, 80)
bear3 = (4, 50, 26, 16, 20, 5, 15, 38, 12, 8, -20, 0, 0, 0, "Bear", "Grizzly ", [claw2, bite3], 10, spr.bear3, 80)
bear4 = (7.9, 150, 35, 23, 30, 5, 130, 276, 18, 14, -20, 0, 0, 0, "Bear", "giant bear", [claw2, bite3], 0, spr.bear4, 100)
scarecrow = (1.5, 10, 10, 10, 22, 6, 3, 5, 6, 6, -25, 0, 75, -20, "Scarecrow", "Scarecrow", [peck], 40, spr.scarecrow, 50)
bat1 = (2.2, 13, 14, 12, 24, 6, 5, 10, 6, 6, 0, 0, 75, -30, "Bat", "Bat", [bite1], 50, spr.bat1, 60)
bat2 = (7.6, 60, 25, 30, 42, 4, 75, 199, 10, 18, 75, -40, 75, -15, "Bat", "firebat", [bite3], 0, spr.bat2)  # incomplete
bat3 = (9.9, 85, 30, 34, 50, 4, 180, 422, 12, 20, -40, 75, 75, -15, "Bat", "ice bat", [bite3], 0, spr.bat3)  # incomplete
rat1 = (1, 8, 10, 8, 16, 5, 1, 2, 6, 4, -10, 0, 0, 0, "Rat", "Big rat", [claw1, bite1], 60, spr.rat1, 60)
rat2 = (1.9, 15, 16, 12, 18, 4, 4, 7, 8, 4, -10, 0, 0, 0, "Rat", "Cave rat", [claw1, bite1], 60, spr.rat2, 60)
boar = (2, 20, 15, 10, 15, 5, 3, 6, 4, 4, -10, 0, 0, 0, "Boar", "Wild boar", [bite1], 0, spr.boar, 60)  # incomplete
spider1 = (2, 21, 14, 14, 16, 5, 8, 8, 8, 8, -25, 0, 0, 0, "Spider", "Cave spider", [bite1], 30, spr.spider1, 60)
spider2 = (2.4, 25, 14, 15, 18, 6, 9, 12, 8, 12, -25, 0, 0, 0, "Spider", "Poisonous spider", [bite1, pbite], 20, spr.spider2, 60)
spider3 = (5.9, 90, 26, 24, 22, 5, 35, 93, 12, 16, -25, 0, 0, 0, "Spider", "tarantula", [pbite3, sting2, frostbite], 15, spr.spider3, 60)
spider4 = (5.9, 60, 25, 21, 32, 4, 28, 100, 10, 14, -25, 0, 0, 0, "Spider", "jumping spider", [bite2, sting2], 30, spr.spider4, 60)
mimic1 = (4.4, 48, 24, 20, 20, 10, 50, 50, 16, 10, 20, 20, 20, 20, "Mimic", "Mimic", [charge1, magic1, magic2, magic3], 6, spr.mimic1, 80)
mimic2 = (7.4, 95, 30, 25, 27, 10, 200, 200, 24, 20, 25, 25, 25, 25, "Mimic", "Big mimic", [charge2, magic1, magic3, magic4], 6, spr.mimic2, 80)
mimic3 = (11.7, 180, 38, 34, 35, 12, 900, 750, 30, 24, 25, 25, 25, 25, "Mimic", "Savage mimic", [charge2, magic7, magic8], 6, spr.mimic3, 80)
orc1 = (6.3, 100, 28, 18, 25, 8, 55, 122, 20, 12, 0, 0, 0, 0, "Orc", "Orc warrior", [], 10, spr.orc1)  # incomplete

# incomplete monsters:
troll1 = (8.9, 175, 45, 18, 25, 5, 120, 317, 30, 24, -20, 20, -20, 0, "Troll", "Troll", [charge1, smash1, magic5], 0, spr.troll1)  # incomplete
troll2 = (12.2, 250, 55, 26, 30, 6, 150, 788, 36, 28, -20, 20, -20, 0, "Troll", "Hunting troll", [charge2, smash2, magic5], 0, spr.troll2)  # incomplete
chimera1 = (20.4, 500, 50, 55, 45, 5, 1275, 3735, 32, 36, 0, 0, -20, 0, "Chimera", "chimera", [], 0, spr.chimera1)  # incomplete
bandit1 = (6, 50, 28, 20, 32, 6, 40, 104, 16, 12, 0, 0, 0, 0, "Human", "Bandit", [esword1, esword2], 40, spr.bandit1, 60)
bandit2 = (6.4, 65, 32, 20, 29, 6, 48, 127, 14, 18, 0, 0, 0, 0, "Human", "Thief", [emace1, emace2], 30, spr.bandit2, 60)
bandit3 = (7.5, 80, 32, 25, 30, 8, 60, 192, 18, 16, 0, 0, 0, 0, "Human", "Outlaw", [eaxe1, eaxe2], 18, spr.bandit3, 60)
bandit4 = (8.7, 120, 35, 25, 34, 8, 125, 358, 24, 20, 0, 0, 0, 0, "Human", "Bandit leader", [eaxe1, eaxe2, magic6, magic7], 4, spr.bandit4, 60)
werewolf1 = (11, "werewolf")  # incomplete
werewolf2 = (13, "werewolf alpha")  # incomplete
insect1 = (5.9, 40, 42, 16, 25, 4, 35, 100, 10, 6, -10, 0, -30, 0, "Insect", "Giant manta", [slash1, slash2], 20, spr.insect1, 60)
insect2 = (4.3, 40, 18, 22, 24, 8, 14, 45, 8, 14, 50, -20, -20, 0, "Insect", "fire beetle", [spit1, bite2], 40, spr.insect2, 60)
insect3 = (4.5, 52, 28, 16, 23, 5, 16, 50, 8, 8, 0, 0, 50, -20, "Insect", "killer bee", [sting1, psting], 30, spr.insect3, 60)
insect4 = (4.8, 62, 25, 20, 22, 4, 42, 59, 14, 14, 10, 0, -20, 0, "Insect", "giant ant", [bite2, spit1], 10, spr.insect4, 60)
plant = (5.6, 90, 30, 30, 19, 5, 30, 89, 3, 2, -40, 0, 20, 0, "Plant", "Carnivorous plant", [bite2, spit2], 16, spr.plant, 60)
scorpio = (6.5, 70, 36, 26, 23, 4, 50, 132, 28, 20, 20, 20, 0, -20, "Insect", "Scorpio", [sting1, psting], 15, spr.scorpio, 60)
dragon1 = (12, 200, 46, 40, 40, 5, 500, 1231, 28, 20, 30, 0, 0, -20, "Dragon", "youngling dragon", [bite4, claw3, firebreath], 10, spr.dragon1, 150)
dragon2 = (16.5, 350, 58, 50, 44, 5, 975, 3200, 35, 25, 30, 0, 20, -20, "Dragon", "great dragon", [bite4, claw3, firebreath, firebreath2], 5, spr.dragon2, 150)
dragon3 = (22.1, 600, 70, 65, 50, 5, 1500, 7750, 46, 33, 30, 0, 20, -20, "Dragon", "emerald dragon", [bite4, claw3, firebreath2], 5, spr.dragon3, 150)  # incomplete
dragon4 = (26.6, 700, 72, 75, 55, 5, 1800, 14000, 65, 42, 40, 10, 30, -20, "Dragon", "Wyvern", [bite4, firebreath2], 5, spr.dragon4, 150)  # incomplete
goblin1 = (11.2, 250, 40, 20, 35, 5, 385, 1180, 28, 24, 0, 0, 0, 0, "Goblin", "Goblin", [], 0, spr.goblin1)  # incomplete
goblin2 = (11.9, 180, 55, 20, 40, 5, 470, 1450, 22, 16, 0, 0, 0, 0, "Goblin", "Big goblin", [], 0, spr.goblin2)  # incomplete
undead1 = (6.4, 125, 35, 18, 20, 5, 80, 200, 20, 22, -20, 0, 0, 0, "Undead", "Walking dead", [pbite2], 60, spr.undead1)  # incomplete
undead2 = (7.2, 150, 38, 18, 21, 5, 100, 290, 22, 26, -20, 0, 0, 0, "Undead", "Brainiac", [pbite2], 40, spr.undead2)  # incomplete
undead3 = (8.4, 180, 40, 18, 24, 5, 150, 465, 24, 28, -20, 0, 0, 0, "Undead", "Putrific corpse", [pbite2], 25, spr.undead3)
necromancer = (9.1, 120, 25, 50, 30, 8, 210, 760, 16, 24, 0, 0, 0, -30, "Human", "Necromancer", [], 8, spr.necromancer)
skeleton1 = (8.3, 150, 30, 20, 30, 5, 135, 445, 12, 16, 20, 0, -20, 0, "Skeleton", "Archer skeleton", [], 25, spr.skeleton1)
skeleton2 = (9.4, 170, 40, 23, 32, 5, 200, 666, 14, 16, 20, 0, -20, 0, "Skeleton", "Skeleton fighter", [], 30, spr.skeleton2)
skeleton3 = (10.9, 200, 45, 25, 35, 5, 275, 1080, 14, 18, 20, 0, -20, 0, "Skeleton", "Skeleton champion", [], 10, spr.skeleton3)
cockatrice = (8.4, 60, 30, 40, 25, 3, 150, 400, 26, 26, 0, 0, 0, 0, "Cockatrice", "Cockatrice", [], 0, spr.cockatrice)
golem1 = (10, 200, 40, 28, 30, 5, 300, 1020, 40, 20, 0, 0, 50, 0, "Golem", "Clay golem", [], 0, spr.golem1)
golem2 = (11, 225, 42, 28, 32, 5, 340, 1385, 45, 24, 0, 0, 50, 0, "Golem", "Stone golem", [], 0, spr.golem2)
golem3 = (12.1, 250, 45, 28, 34, 5, 395, 1900, 50, 28, -25, 70, 0, 0, "Golem", "Snow golem", [], 0, spr.golem3)
golem4 = (13.2, 280, 48, 28, 36, 5, 525, 2510, 55, 30, 80, -25, 0, 0, "Golem", "Lava golem", [], 0, spr.golem4)
golem5 = (16, 600, 55, 30, 38, 5, 1125, 6630, 64, 36, 30, 30, 30, -10, "Golem", "Quartz golem", [], 0, spr.golem5)
cyclop = (14, 400, 55, 25, 32, 5, 600, 2475, 30, 24, 0, 0, 0, 0, "Cyclop", "Cyclop", [], 0, spr.cyclop)
tech1 = (18.2, 480, 52, 30, 45, 6, 975, 5900, 40, 30, 0, 0, 0, -10, "Tech machine", "Tech spider", [], 0, spr.tech1)
tech2 = (18.5, 560, 58, 30, 40, 5, 1050, 6250, 42, 32, 0, 0, 0, -10, "Tech machine", "Steamfighter", [], 0, spr.tech2)
tech3 = (19.5, 340, 62, 48, 55, 5, 1280, 8500, 34, 26, 0, 0, 0, -10, "Tech machine", "Steam blazer", [], 0, spr.tech3)
soldier1 = (13.5, 450, 65, 35, 50, 5, 2200, 11225, 60, 46, 0, 0, 0, 0, "Human", "Corrupted soldier", [], 0, spr.soldier1)
soldier2 = (13.5, 525, 55, 35, 55, 5, 2400, 12650, 64, 48, 0, 0, 0, 0, "Human", "Savage soldier", [], 0, spr.soldier2)
angel = (16.5, 500, 50, 60, 54, 4, 2500, 14450, 40, 54, -20, 0, 50, -20, "Dark angel", [], 0, spr.angel)
fighter1 = (18, 150, 60, 40, 54, 8, 1200, 6325, 75, 70, 0, 0, 0, 0, "Hero slayer", [], 0, spr.fighter)
spellcaster = (18, 100, 40, 70, 45, 8, 950, 4575, 50, 50, 0, 0, 0, -20, "dark mage", [], 0, spr.spellcaster)
demon1 = (17, 650, 56, 52, 44, 4, 2300, 12250, 30, 58, 50, -30, 0, 0, "Demon", "Demonic knight", [], 0, spr.demon1)
demon2 = (16.4, 750, 60, 54, 47, 4, 2875, 17000, 32, 60, 50, -30, 0, 0, "Demon", "Demonic soul", [], 0, spr.demon2)
ghost1 = (13.5, 195, 30, 45, 35, 4, 350, 1685, 60, 20, 0, 0, 80, -30, "Ghost", "Lost soul", [], 0, spr.ghost1)
ghost2 = (14, 220, 30, 52, 38, 4, 480, 2650, 68, 24, 0, 0, 80, -30, "Ghost", "Spectral fighter", [], 0, spr.ghost2)

# boss:
boss1 = (8, 280, 40, 35, 30, 5, 400, 1000, 26, 14, -30, 0, 0, 0, "spider", "Spider Queen (boss)", [spit3, sting2, bite3], 0)
boss2 = (13, "Vargo the Necromancer")
boss3 = (20, "Steam guardian")
boss4 = (18, "demon", "Demon Lord")

# monster biomes
forest = [wolf1, wolf2, wolf3, bear1, bear3, scarecrow, rat1]
dungeon = [wolf3, bear3, spider2, bat1, mimic1, mimic2, mimic3, orc1]
cave = [wolf1, bear2, bear3, bat1, spider1, spider2, orc1]
allb = [wolf1, wolf2, wolf3, bear1, bear2, bear3, scarecrow, bat1, rat1, rat2, spider1, spider2, mimic1, mimic2, mimic3,
        insect1, insect2, insect3, insect4, plant, scorpio, dragon1, dragon2, bandit1, bandit2, bandit3, bandit4]

# monsterTypeList:
wildCreatureList = ["Wolf", "Bear", "Spider", "Rat", "Bat"]
intelligentCreatureList = ["Human", "Dragon", "Demon", "Angel", "Ghost"]
