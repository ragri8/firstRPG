from core.Skill_Tree import Ability_Node, Spell_Node, requirements
from core.Skill_Tree import Skill_Tree
from core.Skill_Unlock import Skill_Unlock, Spell_Unlock
from core.Combat_Mechanics.Ability import *
from objects.Spell_Sheet import *

# abilities

# ability1 = Ability_Node("Power attack", PowerAttack(), 2, requirements(level=3, weapon=2), [])
# ability2 = Ability_Node("Spin attack", SpinAttack(), 2, requirements(level=4))
# ability3 = Ability_Node("Charge attack", ChargedAttack(), 3, requirements(level=5, weapon=3), ["Power attack"])
# ability4 = Ability_Node("Shield bash", ShieldBash(), 2, requirements(level=4), [])
# ability5 = Ability_Node("Focus spirit", FocusSpirit(), 1, requirements(), [])
# ability6 = Ability_Node("Focus body", FocusBody(), 1, requirements(), [])
# ability7 = Ability_Node("Rainning strike", RainningStrike(), 3, requirements(level=6, weapon=4), ["Spin attack"])
# ability8 = Ability_Node("Thrust", Thrust(), 2, requirements(level=6, weapon=3), [])
# ability9 = Ability_Node("Heavy thrust", HeavyThrust, 3, requirements(level=9, weapon=5), ["Thrust", "Power attack"])

# spells
# spell1 = Spell_Node("fireball", fireball, 1, requirements(), [])
# spell2 = Spell_Node("frost", frost, 1, requirements(), [])
# spell3 = Spell_Node("heat wave", heat_wave, 2, requirements(fire=2), ["fireball"])


ability1 = Skill_Unlock(PowerAttack(), [], {"level": 2, "weapon": 2}, 3)
ability2 = Skill_Unlock(SpinAttack(), [], {"level": 4}, 3)
ability3 = Skill_Unlock(ChargedAttack(), ["Power attack"], {"level": 5, "weapon": 3}, 4)
ability4 = Skill_Unlock(ShieldBash(), [], {"level": 4}, 3)
ability5 = Skill_Unlock(FocusSpirit(), 1, {}, 1)
ability6 = Skill_Unlock(FocusBody(), [], {}, 1)
ability7 = Skill_Unlock(RainningStrike(), ["Spin attack"], {"level": 6, "weapon": 4}, 4)
ability8 = Skill_Unlock(Thrust(), [], {"level": 6, "weapon": 3}, 3)
ability9 = Skill_Unlock(HeavyThrust(), ["Thrust", "Power attack"], {"level": 9, "weapon": 5}, 4)

# spells
spell1 = Spell_Unlock(fireball, [], {}, 2)
spell2 = Spell_Unlock(frost, [], {}, 2)
spell3 = Spell_Unlock(spark, [], {"level": 2}, 2)
spell4 = Spell_Unlock(quake, [], {"level": 3}, 2)
spell5 = Spell_Unlock(first_aid, [], {}, 2)
spell6 = Spell_Unlock(recovery, [], {}, 1)
spell7 = Spell_Unlock(fierce_heart, [], {"level": 4}, 2)
spell8 = Spell_Unlock(critical_slash, [], {"level": 4, "buff": 1}, 2)
spell9 = Spell_Unlock(heat_wave, ["fireball"], {"level": 5, "fire": 2}, 3)
spell10 = Spell_Unlock(north_wind, ["frost"], {"level": 5, "ice": 2}, 3)
spell11 = Spell_Unlock(plasma, ["spark"], {"level": 6, "electric": 2}, 3)
spell12 = Spell_Unlock(rock_spike, ["quake"], {"level": 6, "earth": 2}, 3)
spell13 = Spell_Unlock(guard, ["fierce heart"], {"level": 5, "buff": 2}, 3)
spell14 = Spell_Unlock(healing_aura, ["first aid"], {"level": 7, "healing": 2}, 3)
spell15 = Spell_Unlock(recovery_aura, [], {"level": 8, "healing": 1}, 2)
spell16 = Spell_Unlock(thunderstorm, ["plasma"], {"level": 10, "electric": 4}, 4)
spell17 = Spell_Unlock(eruption, ["heat wave"], {"level": 10, "fire": 4}, 4)
spell18 = Spell_Unlock(earthquake, ["rock spike"], {"level": 11, "earth": 4}, 4)
spell19 = Spell_Unlock(ice_cannon, ["north wind"], {"level": 11, "ice": 4}, 4)
spell20 = Spell_Unlock(close_wound, ["first aid"], {"level": 12, "healing": 4}, 4)
spell21 = Spell_Unlock(high_impact, ["fierce heart"], {"level": 13, "buff": 4}, 4)
spell22 = Spell_Unlock(protection, ["guard"], {"level": 14, "buff": 4}, 4)
spell23 = Spell_Unlock(resurrection, ["close wound"], {"level": 15, "healing": 5}, 5)


# skill trees
skill_tree = Skill_Tree([ability1, ability2, ability3, ability4, ability5, ability6, ability7, ability8, ability9],
                        [spell1, spell2, spell3, spell4, spell5, spell6, spell7, spell8, spell9, spell10, spell11,
                         spell12, spell13, spell14, spell15, spell16, spell17, spell18, spell19, spell20, spell21],
                        [])
