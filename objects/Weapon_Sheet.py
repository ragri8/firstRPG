from core.Weapon import Weapon
from objects.Weapon_Text_Sheet import *

# weapon types effect:
# swords: Bleeding; based on agility, have a small chance to inflict minor damage each turn
#       (equal weapon's base attack?), mostly light and medium, some heavy
# maces: Stunning; with strength, give a chance to stun an enemy on his next turn
#       mostly medium and heavy
# rapier: Counter; Based on agility, deal a counter attack instead of dodging the enemy (weaker attack)
# mostly light and medium
# axes: Critical strike x3; Critical strike dealed with an axe deal 3 time the damage instead of 2
# spears: Armor piercing; Determined on luck, have a chance to ignore enemy's armor
# bows: Multiple target; Based on strength, an arrow can go througth multiple enemies at once
#                                              (lose effectiveness after each additional foes)

# todo: add a speed level or weight, affecting the number of strikes of an attack,
# todo ...different weight for the same weapon, choosing between speed and raw damage
# todo: every weapon needs rebalancing with the speed
# todo: more weapons, that's it

# speed chart:
# light weapons: speed = 1.5
# medium weapons: speed = 1.3
# heavy weapons: speed = 1
# for base damage, they are inversely proportionnal to the speed
# ex: light sword base damage = 10 => same level heavy axe base damage = 15

sword1 = Weapon("Light sword", 4, 4, 1, "sword", 1, 50, 1.4, description=sword1_text)  # dmg = 5-8
sword2 = Weapon("Soldier's blade", 7, 4, 1, "sword", 3, 120, 1.2, description=sword2_text)  # dmg = 8-11
sword3 = Weapon("Twisted blade", 9, 6, 1, "sword", 6, 350, 1.2, description=sword3_text)  # dmg = 10-15
sword4 = Weapon("Knight's sword", 10, 4, 2, "sword", 9, 850, 1.1, description=sword4_text)  # dmg = 12-18
sword5 = Weapon("Blood sword", 12, 4, 3, "sword", 12, 1875, 1.3, description=sword5_text)  # dmg = 15-24
sword6 = Weapon("Blade of the zephhyr", 17, 4, 3, "sword", 15)  # dmg = 20-32
sword7 = Weapon("Emerald sword", 22, 4, 4, "sword", 18)  # dmg = 26-38
sword8 = Weapon("Dragonslasher", 27, 4, 5, "sword", 21)  # dmg = 32-47, level 21
sword9 = Weapon("Blade of the gambler", 8, 20, 2, "sword", 17)  # dmg = 10-48
# longsword(medium), greatsword(heavy), short sword(light)
# categories: dagger(1.4-1.8), short sword(1.3-1.4), long sword(1.2-1.3), greatsword(1.1)
# iron dagger, ceremonial dagger, exquisite dagger, 

swords = [sword1, sword2, sword3, sword4, sword5, sword6, sword7, sword8, sword9]


rapier1 = Weapon("Rapier", 3, 4, 1, "rapier", 1, 60, 1.5, description=rapier1_text)  # dmg = 4-7, level 1
rapier2 = Weapon("Balanced rapier", 5, 4, 1, "rapier", 3, 135, 1.5, description=rapier2_text)  # dmg = 6-9, level 3
rapier3 = Weapon("Fine rapier", 6, 4, 2, "rapier", 6, 380, 1.3, description=rapier3_text)  # dmg = 8-14, level 6
rapier4 = Weapon("Swift blade", 7, 4, 3, "rapier", 9, 900, 1.3, description=rapier4_text)  # dmg = 10-19, level 9
rapier5 = Weapon("Lightning blade", 8, 4, 4, "rapier", 12, 2125, 1.4, description=rapier5_text)  # dmg = 12-24, level 12
rapier6 = Weapon("Exquisite rapier", 14, 6, 3, "rapier", 15, 4800, 1.5, description=rapier6_text)  # dmg = 17-32, level 15
rapier7 = Weapon("Lightning striker", 18, 4, 4, "rapier", 18, 7500, 2, description=rapier7_text)  # dmg = 22-34, level 18
# katana(medium)

rapiers = [rapier1, rapier2, rapier3, rapier4, rapier5, rapier6, rapier7]


axe1 = Weapon("wood cutter axe", 5, 4, 1, "axe", 1, 50, 1.1, description=axe1_text)  # dmg = 6-9, level 1
axe2 = Weapon("Fighter axe", 8, 4, 1, "axe", 3, 120, 1.2, description=axe2_text)  # dmg = 9-12, level 3
axe3 = Weapon("Battle axe", 9, 4, 1, "axe", 6, 340, 1.2, description=axe3_text)  # dmg = 10-13, level 6
axe4 = Weapon("Heavy axe", 12, 6, 1, "axe", 9, 825, 1, description=axe4_text)  # dmg = 13-18, level 9
axe5 = Weapon("Great axe", 16, 4, 2, "axe", 12, 1700, 1.1, description=axe5_text)  # dmg = 18-24, level 12
axe6 = Weapon("Fury axe", 20, 4, 2, "axe", 15, 1.3, description=axe6_text)  # dmg = 22-28, level 15
axe7 = Weapon("Battleground smasher", 25, 4, 3, "axe", 18)  # dmg = 28-37, level 18
# light axe(medium), rusty great axe (hvy), giant axe(hvy)

axes = [axe1, axe2, axe3, axe4, axe5, axe6, axe7]


mace1 = Weapon("Small mace", 4, 4, 1, "mace", 1, 50, 1.2, description=mace1_text)  # dmg = 5-8
mace2 = Weapon("Battle mace", 4, 6, 1, "mace", 2, 100, 1.1, description=mace2_text)  # dmg = 5-10
mace3 = Weapon("Morning star", 6, 4, 2, "mace", 5, 300, 1.2, description=mace3_text)  # dmg = 8-14
mace4 = Weapon("War hammer", 11, 6, 2, "mace", 10, 1050, 1, description=mace4_text)  # dmg = 13-23
mace5 = Weapon("Heavy mace", 18, 6, 2, "mace", 13, 2250, 0.8, description=mace5_text)  # dmg = 20-30
mace6 = Weapon("Spiked mace", 16, 6, 3, "mace", 15, 4450, 1.2, description=mace6_text)  # dmg = 19-34
mace7 = Weapon("mace of destruction", 24, 4, 3, "mace", 17)  # dmg = 27-36
mace8 = Weapon("Skullcrusher", 30, 4, 4, "mace", 20)  # dmg = 34-44
# ()
# categories: morning star (1.3-1.4), warhammer(1.2-1.3), mace(1.1)

maces = [mace1, mace2, mace3, mace4, mace5, mace6, mace7, mace8]


bow1 = ("Short bow", 2, "bow")
bow2 = ("Ranger's bow", 4, "bow")
bow3 = ("Hunting bow", 5, "bow")
bow4 = ("Long bow", 8, "bow")
bow5 = ("Bow of the hunter", 11, "bow")
bow6 = ("Imperial bow", 14, "bow")
bow7 = ("Hero's bow", 18, "bow")
bow8 = ("The Predator", 21, 8, 3, "bow")  # dmg = 24-45
# wooden bow(light), iron bow(medium), skeleton bow(light), amazon bow(medium)

bows = [bow1, bow2, bow3, bow4, bow5, bow6, bow7, bow8]


spear1 = Weapon("Hunting spear", 3, 4, 1, "spear", 1, 45, 1.3, description=spear1_text)  # dmg = 4-7
spear2 = ("Soldier's spear", "spear")
spear3 = ("mounted lance", "spear")
spear4 = ("Long spear", "spear")
spear5 = ("Silver spear", "spear")
spear6 = ("", "spear")
spear7 = ("War spear", "spear")
# iron lance(hvy),

spears = [spear1, spear2, spear3, spear4, spear5, spear6, spear7]


staff1 = Weapon("Staff", 3, 4, 1, "staff", 1, 65, 1.2, description=staff1_text)  # dmg = 4-7
staff2 = Weapon("Magic staff", 5, 4, 1, "staff", 3, 150, 1.2, description=staff2_text)  # dmg = 6-9
staff3 = Weapon("Great wand", 7, 4, 1, "staff", 6, 400, 1.4, description=staff3_text)  # dmg = 8-11
staff4 = Weapon("Witcher staff", 9, 4, 2, "staff", 9, 1000, 1.3, description=staff4_text)  # dmg = 11-17
staff5 = Weapon("Great scepter", 13, 4, 3, "staff", 12, 2250, 1.1, description=staff5_text)  # dmg = 16-25
staff6 = Weapon("Ancien staff", 18, 4, 3, "staff", 16)  # dmg = 21-30
staff7 = Weapon("Staff of the unicorn", 23, 4, 3, "staff", 20)  # dmg = 26-35
# magic wand(light), phoenix staff(medium), scepter(hvy)
# categories: wand(1.4-1.5), staff(1.2-1.3), scepter/long staff(1.1)

staffs = [staff1, staff2, staff3, staff4, staff5, staff6, staff7]