from core.Combat_Mechanics.Ability import CustomEnemyAction, MagicEnemyAction, EnemyAttack, EnemyPowerAttack
from core.Combat_Mechanics.Status_Linker import Status_Linker
from core.Combat_Mechanics.Status_Effect import *


bleedingEffect1 = BleedingEffect(3, 2)
bleedingEffect2 = BleedingEffect(6, 2)
fireEffect1 = BurningEffect(4, 2)
fireEffect2 = BurningEffect(7, 2)
freezeEffect1 = FreezeEffect(2)
stunEffect = StunningEffect(2)
poisonEffect1 = PoisonEffect(7, 8)
poisonEffect2 = PoisonEffect(10, 8)

# (name, baseattack, damagedice, nbrdmgdice, frequency=3, cost=3, nbrtarget="1", damage_type,
#       target_group="Enemy", actionLinker=None)
#     action_STatus(Status_Effect, bonusDurationDice, probability)
claw1 = CustomEnemyAction("claws", 8, 2, 1, 16, 3, "SP", "1", "pattack", "Enemy",
                          Status_Linker(bleedingEffect1, 3, 4))  # single target, weak
claw2 = CustomEnemyAction("sharp claws", 6, 2, 1, 4, 3, "SP", "all", "pattack", "Enemy",
                          Status_Linker(bleedingEffect1, 2, 4))  # mult. target, weak
claw3 = CustomEnemyAction("sharp claws", 9, 4, 1, 4, 3, "SP", "all", "pattack", "Enemy",
                          Status_Linker(bleedingEffect1, 3, 4))  # multi target, intermediate
bite1 = CustomEnemyAction("bite", 8, 2, 1, 8, 3, "SP", "1", "pattack", "Enemy")  # single target, weak
bite2 = CustomEnemyAction("bite", 11, 4, 1, 7, 3, "SP", "1", "pattack", "Enemy")  # single target, intermediate
bite3 = CustomEnemyAction("bite", 16, 4, 1, 6, 3, "SP", "1", "pattack", "Enemy")  # single target, high
bite4 = CustomEnemyAction("bite", 22, 6, 1, 5, 3, "SP", "1", "pattack", "Enemy")  # single target, dragon bite
peck = CustomEnemyAction("drill peck", 6, 2, 1, 10, 3, "SP", "1", "pattack", "Enemy")  # single target, weak, for flying enemies
charge1 = CustomEnemyAction("charge", 8, 2, 2, 8, 3, "SP", "1", "pattack", "Enemy",
                            Status_Linker(stunEffect, 0, 2))  # single target, intermediate
charge2 = CustomEnemyAction("charge", 8, 6, 1, 8, 3, "SP", "1", "pattack", "Enemy",
                            Status_Linker(stunEffect, 0, 2))  # single target, intermediate
smash1 = CustomEnemyAction("smash", 8, 6, 1, 4, 3, "SP", "1", "pattack", "Enemy")
smash2 = CustomEnemyAction("smash", 15, 4, 2, 4, 3, "SP", "1", "pattack", "Enemy")
sting1 = CustomEnemyAction("sting", 6, 4, 1, 8, 3, "SP", "1", "pattack", "Enemy")
sting2 = CustomEnemyAction("sting", 8, 6, 1, 8, 3, "SP", "1", "pattack", "Enemy")
slash1 = CustomEnemyAction("razor blade", 12, 6, 1, 5, 3, "SP", "1", "pattack", "Enemy",
                           Status_Linker(bleedingEffect2, 3, 5))
slash2 = CustomEnemyAction("Razor spin", 6, 6, 1, 2, 3, "SP", "all", "pattack", "Enemy",
                           Status_Linker(bleedingEffect2, 3, 3))
firebite = CustomEnemyAction("firebite", 8, 4, 1, 3, 5, "SP", "1", "fire", "Enemy",
                             Status_Linker(fireEffect1, 3, 35))
frostbite = CustomEnemyAction("frostbite", 8, 4, 1, 3, 6, "SP", "1", "ice", "Enemy",
                              Status_Linker(freezeEffect1, 0, 18))
pbite = CustomEnemyAction("Poisonus bite", 4, 4, 1, 3, 5, "1", "poison", "Enemy",
                          Status_Linker(poisonEffect1, 6, 8))  # single target, can cause poison
pbite2 = CustomEnemyAction("Infected bite", 10, 6, 1, 4, 5, "1", "poison", "Enemy",
                           Status_Linker(poisonEffect1, 6, 10))
pbite3 = CustomEnemyAction("Poisonus bite", 10, 4, 1, 3, 7, "1", "poison", "Enemy",
                           Status_Linker(poisonEffect1, 6, 12))
psting = CustomEnemyAction("Poison sting", 6, 4, 1, 3, 3, "1", "poison", "Enemy",
                           Status_Linker(poisonEffect1, 6, 8))  # single target, can cause poison

weaponAttack = EnemyAttack()
powerAttack = EnemyPowerAttack()

# magic attack
magic1 = MagicEnemyAction("Spark", 5, 6, 1, 4, 5, "1", "electric", "Enemy",
                          status=None)
magic2 = MagicEnemyAction("fireball", 6, 0, 0, 5, 3, "1", "fire", "Enemy",
                          Status_Linker(fireEffect1, 3, 4))
magic3 = MagicEnemyAction("earthquake", 7, 4, 1, 2, 3, "all", "earth", "Enemy",
                          status=None)
magic4 = MagicEnemyAction("plasma", 12, 6, 1, 2, 5, "1", "electric", "Enemy",
                          status=None)
magic6 = MagicEnemyAction("north wind", 10, 6, 1, 4, 12, "al", "ice", "Enemy",
                          Status_Linker(freezeEffect1, 0, 4))
magic7 = MagicEnemyAction("rock spike", 17, 4, 2, 2, 7, "1", "earth", "Enemy",
                          status=None)
magic8 = MagicEnemyAction("tunderstorm", 10, 4, 2, 2, 10, "all", "electric", "Enemy",
                          status=None)
magic9 = MagicEnemyAction("flare", 9, 4, 2, 4, 5, "1", "fire", "Enemy",
                          Status_Linker(fireEffect1, 3, 4))
spit1 = MagicEnemyAction("Firespit", 6, 4, 2, 5, 5, "1", "fire", "Enemy",
                         Status_Linker(fireEffect1, 3, 4))  # single target, fire element
spit2 = MagicEnemyAction("Poison spit", 2, 4, 1, 2, 4, "1", "poison", "Enemy",
                         Status_Linker(poisonEffect1, 6, 25))
spit3 = MagicEnemyAction("Poison spit", 7, 4, 2, 2, 6, "1", "poison", "Enemy",
                         Status_Linker(poisonEffect2, 6, 40))
firebreath = MagicEnemyAction("Firebreath", 25, 4, 2, 3, 12, "1", "fire", "Enemy",  # single target, powerfull
                              status=Status_Linker(fireEffect1, 8, 4))
firebreath2 = MagicEnemyAction("Inferno", 16, 4, 2, 2, 3, "all", "fire", "Enemy",
                               status=Status_Linker(fireEffect1, 8, 4))
