import pygame

grass_tile = pygame.image.load("../texture_file/grass.png")
default_tile = pygame.image.load("../texture_file/default.png")
water_tile = pygame.image.load("../texture_file/water.png")
wood_tile = pygame.image.load("../texture_file/wood.png")

water_grass_tile_1 = pygame.image.load("../texture_file/water_grass_1.png")
water_grass_tile_2 = pygame.image.load("../texture_file/water_grass_2.png")
water_grass_tile_3 = pygame.image.load("../texture_file/water_grass_3.png")
water_grass_tile_4 = pygame.image.load("../texture_file/water_grass_4.png")
water_grass_tile_5 = pygame.image.load("../texture_file/water_grass_5.png")
water_grass_tile_6 = pygame.image.load("../texture_file/water_grass_6.png")
water_grass_tile_7 = pygame.image.load("../texture_file/water_grass_7.png")
water_grass_tile_8 = pygame.image.load("../texture_file/water_grass_8.png")
water_grass_tile_9 = pygame.image.load("../texture_file/water_grass_9.png")
water_grass_tile_10 = pygame.image.load("../texture_file/water_grass_10.png")
water_grass_tile_11 = pygame.image.load("../texture_file/water_grass_11.png")
water_grass_tile_12 = pygame.image.load("../texture_file/water_grass_12.png")

rock_wall_wood_tile_1 = pygame.image.load("../texture_file/rock_wall_wood_1.png")
rock_wall_wood_tile_2 = pygame.image.load("../texture_file/rock_wall_wood_2.png")
rock_wall_wood_tile_3 = pygame.image.load("../texture_file/rock_wall_wood_3.png")
metal_grid_tile_1 = pygame.image.load("../texture_file/metal_grid_1.png")
metal_grid_tile_2 = pygame.image.load("../texture_file/metal_grid_2.png")
counter_tile_1 = pygame.image.load("../texture_file/wood_counter_1.png")
counter_tile_2 = pygame.image.load("../texture_file/wood_counter_2.png")
counter_tile_3 = pygame.image.load("../texture_file/wood_counter_3.png")
counter_tile_4 = pygame.image.load("../texture_file/wood_counter_4.png")

dirt_tile = pygame.image.load("../texture_file/dirt.png")
dirt_grass_tile_1 = pygame.image.load("../texture_file/dirt_grass_1.png")
dirt_grass_tile_2 = pygame.image.load("../texture_file/dirt_grass_2.png")
dirt_grass_tile_3 = pygame.image.load("../texture_file/dirt_grass_3.png")
dirt_grass_tile_4 = pygame.image.load("../texture_file/dirt_grass_4.png")
dirt_grass_tile_5 = pygame.image.load("../texture_file/dirt_grass_5.png")
dirt_grass_tile_6 = pygame.image.load("../texture_file/dirt_grass_6.png")
dirt_grass_tile_7 = pygame.image.load("../texture_file/dirt_grass_7.png")
dirt_grass_tile_8 = pygame.image.load("../texture_file/dirt_grass_8.png")
dirt_grass_tile_9 = pygame.image.load("../texture_file/dirt_grass_9.png")
dirt_grass_tile_10 = pygame.image.load("../texture_file/dirt_grass_10.png")
dirt_grass_tile_11 = pygame.image.load("../texture_file/dirt_grass_11.png")
dirt_grass_tile_12 = pygame.image.load("../texture_file/dirt_grass_12.png")

two_ground_tile = pygame.image.load("../texture_file/ground_leveled.png")
upper_cliff_tile = pygame.image.load("../texture_file/cliff_1.png")
lower_cliff_tile = pygame.image.load("../texture_file/cliff_bottom_1.png")

cliff_tile_1 = pygame.image.load("../texture_file/grassy_cliff_1.png")
cliff_tile_2 = pygame.image.load("../texture_file/grassy_cliff_2.png")
cliff_tile_3 = pygame.image.load("../texture_file/grassy_cliff_3.png")
cliff_tile_4 = pygame.image.load("../texture_file/grassy_cliff_4.png")
cliff_tile_5 = pygame.image.load("../texture_file/grassy_cliff_5.png")
cliff_tile_6 = pygame.image.load("../texture_file/grassy_cliff_6.png")
cliff_tile_7 = pygame.image.load("../texture_file/grassy_cliff_7.png")
cliff_tile_8 = pygame.image.load("../texture_file/grassy_cliff_8.png")
cliff_tile_9 = pygame.image.load("../texture_file/grassy_cliff_9.png")
cliff_tile_10 = pygame.image.load("../texture_file/grassy_cliff_10.png")

trunk_1 = pygame.image.load("../texture_file/base_tree.png")
leaves_1 = pygame.image.load("../texture_file/leaves.png")
chest_1 = pygame.image.load("../texture_file/chest_close.png")
chest_2 = pygame.image.load("../texture_file/chest_open.png")
bridge_1 = pygame.image.load("../texture_file/bridge_1.png")
bridge_2 = pygame.image.load("../texture_file/bridge_2.png")
bridge_3 = pygame.image.load("../texture_file/bridge_3.png")
bridge_4 = pygame.image.load("../texture_file/bridge_4.png")
bridge_5 = pygame.image.load("../texture_file/bridge_5.png")
bridge_6 = pygame.image.load("../texture_file/bridge_6.png")
bridge_7 = pygame.image.load("../texture_file/bridge_7.png")
bridge_8 = pygame.image.load("../texture_file/bridge_8.png")
bridge_9 = pygame.image.load("../texture_file/bridge_9.png")
bridge_10 = pygame.image.load("../texture_file/bridge_10.png")
bridge_11 = pygame.image.load("../texture_file/bridge_11.png")
bridge_12 = pygame.image.load("../texture_file/bridge_12.png")

sprite1 = pygame.image.load("../sprites/character_sprite/Isaac.png")
sprite1_1 = pygame.image.load("../sprites/character_sprite/Isaac_1.png")
sprite1_2 = pygame.image.load("../sprites/character_sprite/Isaac_2.png")
sprite1_3 = pygame.image.load("../sprites/character_sprite/Isaac_3.png")
sprite1_4 = pygame.image.load("../sprites/character_sprite/Isaac_4.png")
sprite1_5 = pygame.image.load("../sprites/character_sprite/Isaac_5.png")
sprite1_6 = pygame.image.load("../sprites/character_sprite/Isaac_6.png")
sprite1_7 = pygame.image.load("../sprites/character_sprite/Isaac_7.png")
sprite1_8 = pygame.image.load("../sprites/character_sprite/Isaac_8.png")
sprite1_9 = pygame.image.load("../sprites/character_sprite/Isaac_9.png")
sprite1_10 = pygame.image.load("../sprites/character_sprite/Isaac_10.png")
sprite1_11 = pygame.image.load("../sprites/character_sprite/Isaac_11.png")
sprite1_12 = pygame.image.load("../sprites/character_sprite/Isaac_12.png")
sprite2 = pygame.image.load("../sprites/character_sprite/Garet.png")
sprite3 = pygame.image.load("../sprites/character_sprite/npc_2.png")

boss_sprite_1 = pygame.image.load("../sprites/enemy_sprite/wolf_sprite_2_map.png")
boss_sprite_2 = pygame.image.load("../sprites/enemy_sprite/bear3_2_map.png")
boss_sprite_3 = pygame.image.load("../sprites/enemy_sprite/bandit_map.png")
boss_sprite_4 = pygame.image.load("../sprites/enemy_sprite/dragon_sprite.png")
default_map_sprite = pygame.image.load("../sprites/enemy_sprite/default_sprite_map.png")

coliseum_back = pygame.image.load("../map_background/Battleback_coliseum.png")
grass_back = pygame.image.load("../map_background/Grass_Battle.png")
frame = pygame.image.load("../texture_file/frame.png")

texture_tags = {"a0": default_tile, "aa": grass_tile, "ab": water_tile, "ac": wood_tile, "ad": water_grass_tile_1, "ae": water_grass_tile_2,
                "af": water_grass_tile_3, "ag": water_grass_tile_4, "ah": water_grass_tile_5, "ai": water_grass_tile_6,
                "aj": water_grass_tile_7, "ak": water_grass_tile_8, "al": water_grass_tile_9, "am": water_grass_tile_10,
                "an": water_grass_tile_11, "ao": water_grass_tile_12, "ap": rock_wall_wood_tile_1,
                "aq": rock_wall_wood_tile_2, "ar": rock_wall_wood_tile_3, "as": metal_grid_tile_1, "at": metal_grid_tile_2,
                "au": counter_tile_1, "av": counter_tile_2, "aw": counter_tile_3, "ax": counter_tile_4, "ay": dirt_tile,
                "az": dirt_grass_tile_1, "aA": dirt_grass_tile_2, "aB": dirt_grass_tile_3, "aC": dirt_grass_tile_4,
                "aD": dirt_grass_tile_5, "aE": dirt_grass_tile_6, "aF": dirt_grass_tile_7, "aG": dirt_grass_tile_8,
                "aH": dirt_grass_tile_9, "aI": dirt_grass_tile_10, "aJ": dirt_grass_tile_11, "aK": dirt_grass_tile_12,
                "aL": two_ground_tile, "aM": upper_cliff_tile, "aN": lower_cliff_tile, "aO": cliff_tile_1, "aP": cliff_tile_2,
                "aQ": cliff_tile_3, "aR": cliff_tile_4, "aS": cliff_tile_5, "aT": cliff_tile_6, "aU": cliff_tile_7,
                "aV": cliff_tile_8, "aW": cliff_tile_9, "aX": cliff_tile_10, "a1": wood_tile, "a2": wood_tile}

texture_list = [default_tile, grass_tile, water_tile, wood_tile, water_grass_tile_1, water_grass_tile_2,
                water_grass_tile_3, water_grass_tile_4, water_grass_tile_5, water_grass_tile_6,
                water_grass_tile_7, water_grass_tile_8, water_grass_tile_9, water_grass_tile_10,
                water_grass_tile_11, water_grass_tile_12, rock_wall_wood_tile_1,
                rock_wall_wood_tile_2, rock_wall_wood_tile_3, metal_grid_tile_1, metal_grid_tile_2,
                counter_tile_1, counter_tile_2, counter_tile_3, counter_tile_4, dirt_tile,
                dirt_grass_tile_1, dirt_grass_tile_2, dirt_grass_tile_3, dirt_grass_tile_4,
                dirt_grass_tile_5, dirt_grass_tile_6, dirt_grass_tile_7, dirt_grass_tile_8,
                dirt_grass_tile_9, dirt_grass_tile_10, dirt_grass_tile_11, dirt_grass_tile_12,
                two_ground_tile, upper_cliff_tile, lower_cliff_tile, cliff_tile_1, cliff_tile_2,
                cliff_tile_3, cliff_tile_4, cliff_tile_5, cliff_tile_6, cliff_tile_7,
                cliff_tile_8, cliff_tile_9, cliff_tile_10, wood_tile, wood_tile]

object_tags = {"6001": trunk_1, "5500": bridge_1, "5501": bridge_2, "5502": bridge_3, "5503": bridge_4, "5504": bridge_5,
               "5505": bridge_6, "5506": bridge_7, "5507": bridge_8, "5508": bridge_9, "5509": bridge_10,
               "5510": bridge_11, "5511": bridge_12}

boss_tags = {"7001": boss_sprite_1, "7002": default_map_sprite, "7003": boss_sprite_2, "7004": boss_sprite_3,
             "7005": boss_sprite_4}


map_tags = {"aa": coliseum_back, "ab": grass_back}
