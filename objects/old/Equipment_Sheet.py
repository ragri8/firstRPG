# Equipment armor:
# tier 1:  level 1-4

lequipment1 = ("Leather chestplate", 3, 2, "Body", "light", 1, 40, 0.06)
lequipment2 = ("Leather helmet", 2, 1, "Head", "light", 1, 20, 0.025)
lequipment3 = ("Leather gauntlet", 1, 1, "Hand", "light", 1, 15, 0.02)
lequipment4 = ("short shield", 2, 2, "Shield", "light", 1, 35, 0.045)

mequipment1 = ("Breastplate armor", 4, 3, "Body", "medium", 1, 45, 0.12)
mequipment2 = ("Iron circlet", 2, 2, "Head", "medium", 1, 24, 0.05)
mequipment3 = ("Leather plate gauntlet", 2, 1, "Hand", "medium", 1, 18, 0.04)
mequipment4 = ("Medium shield", 3, 3, "Shield", "medium", 1, 40, 0.09)

hequipment1 = ("Steel armor", 5, 3, "Body", "heavy", 1, 50, 0.2)
hequipment2 = ("Steel helmet", 3, 2, "Head", "heavy", 1, 28, 0.09)
hequipment3 = ("Steel gauntlet", 2, 2, "Hand", "heavy", 1, 23, 0.06)
hequipment4 = ("Heavy shield", 4, 4, "Shield", "heavy", 1, 44, 0.15)

equipmentT1 = [lequipment1, lequipment2, lequipment3, lequipment4, mequipment1, mequipment2, mequipment3, mequipment4,
               hequipment1, hequipment2, hequipment3, hequipment4]

# tier 2:  level 5-8

lequipment5 = ("Padded armor", 5, 5, "Body", "light", 5, 150)
lequipment6 = ("Padded helmet", 3, 3, "Head", "light", 5, 80)
lequipment7 = ("Padded gauntlet", 3, 2, "Hand", "light", 5, 60)
lequipment8 = ("Light steel shield", 5, 4, "Shield", "light", 5, 120)

mequipment5 = ("Chainmail", 7, 6, "Body", "medium", 5, 165)
mequipment6 = ("Steel circlet", 4, 4, "Head", "medium", 5, 90)
mequipment7 = ("Vambrace", 4, 3, "Hand", "medium", 5, 70)
mequipment8 = ("Steel shield", 6, 5, "Shield", "medium", 5, 130)

hequipment5 = ("Banded mail armor", 9, 8, "Body", "heavy", 5, 180)
hequipment6 = ("Mail helmet", 5, 5, "Head", "heavy", 5, 100)
hequipment7 = ("Banded mail gauntlet", 5, 4, "Hand", "heavy", 5, 80)
hequipment8 = ("Heavy steel shield", 7, 7, "Shield", "heavy", 5, 145)

equipmentT2 = [lequipment5, lequipment6, lequipment7, lequipment8, mequipment5, mequipment6, mequipment7, mequipment8,
               hequipment5, hequipment6, hequipment7, hequipment8]

# tier 3:  level 9-12

lequipment9 = ("Light nordic armor", 8, 7, "Body", "light", 9, 350)
lequipment10 = ("Light nordic helmet", 5, 5, "Head", "light", 9, 180)
lequipment11 = ("Light nordic gauntlet", 4, 4, "Hand", "light", 9, 145)
lequipment12 = ("Light nordic shield", 7, 6, "Shield", "light", 9, 300)

mequipment9 = ("Celtic leather plate armor", 11, 10, "Body", "medium", 9, 380)
mequipment10 = ("Celtic helmet", 6, 6, "Head", "medium", 9, 195)
mequipment11 = ("Celtic gauntlet", 5, 5, "Hand", "medium", 9, 160)
mequipment12 = ("Celtic shield", 9, 8, "Shield", "medium", 9, 325)

hequipment9 = ("Heavy nordic armor", 13, 12, "Body", "heavy", 9, 420)
hequipment10 = ("Heavy nordic helmet", 7, 7, "Head", "heavy", 9, 210)
hequipment11 = ("Heavy nordic gauntlet", 6, 6, "Hand", "heavy", 9, 170)
hequipment12 = ("Heavy nordic shield", 11, 10, "Shield", "heavy", 9, 350)

equipmentT3 = [lequipment9, lequipment10, lequipment11, lequipment12, mequipment9, mequipment10, mequipment11,
               mequipment12, hequipment9, hequipment10, hequipment11, hequipment12]


# tier 4:  level 13-16

mequipment13 = ("brigandine", 7, 6)


# tier 5:  level 17-20

lequipment17 = ("Elfic armor", 7, 9, "Body", "light")
lequipment18 = ("Elfic helmet", 5, 4, "Head", "light")
lequipment19 = ("Elfic gauntlet", "Hand", "light")
lequipment20 = ("Elfic shield", "Shield", "light")

mequipment17 = ("Dragonscale mail", 9, 12, "Body", "medium")
mequipment18 = ("Dragonscale helmet", "Head", "medium")
mequipment19 = ("Dragonscale gauntlet", "Hand", "medium")
mequipment20 = ("Dragonscale shield", "Shield", "medium")

hequipment17 = ("Wyvern bone armor", 14, 15, "Body", "heavy")
hequipment18 = ("Wyvern bone helmet", "Head", "heavy")
hequipment19 = ("Wyvern bone gauntlet", "Hand", "heavy")
hequipment20 = ("Wyvern bone shield", "Shield", "heavy")

equipmentT5 = [lequipment17, lequipment18, lequipment19, lequipment20, mequipment17, mequipment18, mequipment19,
               mequipment20, hequipment17, hequipment18, hequipment19, hequipment20]

# special armors


# other gear part

other1 = ("cape", 0, 0)

complete_armor_list = [*equipmentT1, *equipmentT2, *equipmentT3]
