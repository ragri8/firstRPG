apple_text = "Restore a short amount of hp"
lightHealPotion_text = "Restore a small amount of hp"
lightStamPotion_text = "Restore a small amount of stamina"
lightManaPotion_text = "Restore a small amount of mana"
healPotion_text = "Restore a medium amount of hp"
stamPotion_text = "Restore a medium amount of stamina"
manaPotion_text = "Restore a medium amount of mana"
strongHealPotion_text = "Restore a large amount of hp"
strongStamPotion_text = "Restore a large amount of stamina"
strongManaPotion_text = "Restore a large amount of mana"

# special items:
lifeBottle_text = "Bring back to life a fainted ally"
