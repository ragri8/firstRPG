from core.Combat_Mechanics.Spell import OffensiveSpell, HealingSpell, RecoverySpell, StatusSpell, ResurectionSpell
from core.Combat_Mechanics.Status_Linker import Status_Linker
from core.Combat_Mechanics.Status_Effect import *
from objects.Spell_text_sheet import *

# Magic spells:

# Offensive spells:
# format: (name, basic damage, damage dice size, number of damage dice, targetGroupSize,
#          manacost, element type, duration dice, status prob)


# 1st tier spell  level 1 to 5
fireball = OffensiveSpell("fireball", 6, 0, 0, "1", 4, "fire",
                          status=Status_Linker(BurningEffect(3), 1, 10), description=fireball_text)  # level 1
frost = OffensiveSpell("frost", 5, 2, 1, "1", 5, "ice",
                       status=Status_Linker(FreezeEffect(3), 2, 10), description=frost_text)  # level 3
quake = OffensiveSpell("quake", 2, 2, 1, "all", 8, "earth",
                       status=None, description=quake_text)  # level 4
spark = OffensiveSpell("spark", 5, 2, 1, "1", 5, "electric",
                       status=None, description=spark_text)  # level 5


# 2nd tier spell  level 6 to 10
heat_wave = OffensiveSpell("heat wave", 3, 2, 1, "all", 11, "fire",
                           status=Status_Linker(BurningEffect(3), 2, 20), description=heat_wave_text)  # level 6
north_wind = OffensiveSpell("north wind", 5, 2, 1, "all", 10, "ice",
                            status=Status_Linker(FreezeEffect(3), 2, 10), description=north_wind_text)  # level 7
plasma = OffensiveSpell("plasma", 11, 4, 1, "1", 7, "electric",
                        status=None, description=plasma_text)  # level 8
rock_spike = OffensiveSpell("rock spike", 16, 4, 1, "1", 7, "earth",
                            status=None, description=rock_spike_text)  # level 10

# 3rd tier spell  level 11 to 15
thunderstorm = OffensiveSpell("thunderstorm", 9, 4, 1, "all", 18, "electric",
                              status=None, description=thunderstorm_text)  # level 12
eruption = OffensiveSpell("eruption", 15, 4, 2, "1", 11, "fire",
                          status=Status_Linker(BurningEffect(6), 2, 12), description=eruption_text)  # level 13
earthquake = OffensiveSpell("earthquake", 11, 4, 1, "all", 17, "earth",
                            status=None, description=earthquake_text)  # level 14
ice_cannon = OffensiveSpell("ice cannon", 17, 4, 2, "1", 10, "ice",
                            status=Status_Linker(FreezeEffect(3), 2, 10), description=ice_cannon_text)  # level 15

# 4th tier spell  level 16 to 20
inferno = OffensiveSpell("inferno", 12, 6, 1, "all", 21, "fire",
                         status=Status_Linker(BurningEffect(7), 2, 10), description=inferno_text)  # level 16
ground_dasher = OffensiveSpell("ground dasher", 24, 4, 2, "1", 13, "earth",
                               status=None, description=ground_dasher_text)  # level 17
glacier = OffensiveSpell("glacier", 14, 4, 2, "all", 22, "ice",
                         status=Status_Linker(FreezeEffect(4), 2, 15))  # level 18
plasma_cannon = OffensiveSpell("plasma cannon", 21, 4, 2, "1", 13, "electric",
                               status=None, description=plasma_cannon_text)  # level 19

# more spell names:
# Fire spell: Brazier, Flames, Scorch, Fire breath, Torch
# Ice spell: Frostbite, Ice spike, Freeze, Blizzard, Snowstorm
# Earth spell: Fissure, Stalagmite, Sandstorm, Rock Throw
# Electric spell: Thunderbolt, Shocktrap, Lightning strike, Shockwave
#

# Healing spells:

first_aid = HealingSpell("first aid", 8, 4, 1, "1", 4, "healing",
                         description=first_aid_text)  # level 2, heal 9-12 HP

healing_aura = HealingSpell("healing aura", 5, 4, 1, "all", 8, "healing",
                            description=healing_aura_text)  # level 6, heal 6-9 HP

close_wound = HealingSpell("close wound", 28, 6, 2, "1", 7, "healing",
                           description=close_wound_text)  # level 9, heal 30-40 HP

fairy_fountain = HealingSpell("fairy fountain", 24, 8, 1, "all", 18, "healing",
                              description=fairy_fountain_text)  # level 15, heal 25-32 HP

massive_heal = HealingSpell("massive heal", 60, 8, 2, "1", 13, "healing",
                            description=massive_heal_text)  # level 18, heal 62-76 HP

recovery = RecoverySpell("recovery", "1", 3, description=recovery_text)
# Eliminate negative effect on an ally or himself

recovery_aura = RecoverySpell("recovery aura", "all", 6, status=Status_Linker(HealingEffect(5, 3), 2, 15),
                              description=recovery_aura_text)
# Eliminate negative effect on an ally or himself, probability of applying a healing effect over time

# Status spells

fierce_heart = StatusSpell("fierce heart", "Party", "1", 5, "strength buff",
                           status=Status_Linker(AttackBuff(2), 3, 100))
# level 4, +10% target.strength + caster.intel//5, single target, max: tempStrength/strength = 150%, 6-8 turns
critical_slash = StatusSpell("critical slash", "Party", "1", 6, "critical buff",
                             status=Status_Linker(CriticalBuff(30), 0, 100))
# level 5, + caster.intel*2/3 of luck for 3 to 6 turns, single target, not stackable
guard = StatusSpell("guard", "Party", "1", 5, "defense buff",
                    status=Status_Linker(DefenseBuff(4), 0, 0))
# level 8, +(caster.intel // 10) pdefense & mdefense, single target, max: tempDefenses - defenses = level - 2
fairy_blessing = StatusSpell("fairy blessing", "Party", "all", 10, "heal",
                             status=Status_Linker(HealingEffect(3), 0, 0))
# level 11, heal (caster.intel // 10) per turn
high_impact = StatusSpell("high impact", "Party", "all", 13, "strength buff",
                          status=Status_Linker(AttackBuff(4), 0, 0))
# level 12, +10% target.strength + caster.intel//5, multiple target, max: tempStrength/strength = 150%
protection = StatusSpell("protection", "Party", "all", 14, "defense buff",
                         status=Status_Linker(DefenseBuff(3), 0, 0))
# level 14, +3 + (caster.intel // 10) pdefense & mdefense, multiple target
resurrection = ResurectionSpell("resurrection", "1", 20)
# level 16, bring back to life an ally
magic_shield = StatusSpell("Magic shield", "Party", "all", 7, "absorb",
                           status=Status_Linker(AbsorbBuff(4), 6, 100))
# level 8, add 5 + intel/3 of block, 2-3 turns, single target

# spellList = [fireball, frost, quake, spark, heat_wave, north_wind, plasma, rock_spike, thunderstorm, eruption,
#             earthquake, ice_cannon, inferno, ground_dasher, ]
spellList = [fireball, first_aid, recovery, frost, quake, fierce_heart, spark, critical_slash,
             heat_wave, healing_aura, north_wind, plasma, guard, close_wound, rock_spike,
             thunderstorm, high_impact, eruption, earthquake, protection, ice_cannon,
             fairy_fountain, inferno, ground_dasher, glacier, massive_heal, plasma_cannon]
