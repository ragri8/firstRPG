from core.Item import ReviveItem, RecoveryItem
from objects.Item_Text_Sheet import *


# items
# recovery items:
# format: (name, statmodif, stattype, description)

apple = RecoveryItem("apple", 15, "HP", 10, apple_text)
lightHealPotion = RecoveryItem("light health potion", 35, "HP", 20, lightHealPotion_text)
lightStamPotion = RecoveryItem("light stamina potion", 30, "SP", 25, lightStamPotion_text)
lightManaPotion = RecoveryItem("light mana potion", 30, "MP", 30, lightManaPotion_text)
healPotion = RecoveryItem("health potion", 60, "HP", 75, healPotion_text)
stamPotion = RecoveryItem("stamina potion", 70, "SP", 90, stamPotion_text)
manaPotion = RecoveryItem("mana potion", 60, "MP", 100, manaPotion_text)
strongHealPotion = RecoveryItem("strong health potion", 120, "HP", 75, strongHealPotion_text)
strongStamPotion = RecoveryItem("strong stamina potion", 100, "SP", 90, strongStamPotion_text)
strongManaPotion = RecoveryItem("strong mana potion", 100, "MP", 100, strongManaPotion_text)

# special items:
lifeBottle = ReviveItem("life bottle", 350, lifeBottle_text)  # bring back to life an ally
