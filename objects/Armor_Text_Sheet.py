# Equipment armor:
# tier 1:  level 1-4

lequipment1_text = "A lightweight armor"
lequipment2_text = ""
lequipment3_text = ""
lequipment4_text = ""

mequipment1_text = ""
mequipment2_text = ""
mequipment3_text = ""
mequipment4_text = ""

hequipment1_text = "A heavy yet durable armor"
hequipment2_text = ""
hequipment3_text = ""
hequipment4_text = ""

# tier 2:  level 5-8

lequipment5_text = ""
lequipment6_text = ""
lequipment7_text = ""
lequipment8_text = ""

mequipment5_text = ""
mequipment6_text = ""
mequipment7_text = ""
mequipment8_text = ""

hequipment5_text = ""
hequipment6_text = ""
hequipment7_text = ""
hequipment8_text = ""

# tier 3:  level 9-12

lequipment9_text = ""
lequipment10_text = ""
lequipment11_text = ""
lequipment12_text = ""

mequipment9_text = ""
mequipment10_text = ""
mequipment11_text = ""
mequipment12_text = ""

hequipment9_text = ""
hequipment10_text = ""
hequipment11_text = ""
hequipment12_text = ""

# tier 4:  level 13-16

mequipment13_text = ""


# tier 5:  level 17-20

lequipment17_text = ""
lequipment18_text = ""
lequipment19_text = ""
lequipment20_text = ""

mequipment17_text = ""
mequipment18_text = ""
mequipment19_text = ""
mequipment20_text = ""

hequipment17_text = ""
hequipment18_text = ""
hequipment19_text = ""
hequipment20_text = ""


# other gear part

other1_text = ""
