from core.Equipment_and_Other import HeadGear, ChestGear, ArmGear, Shield
from objects.Armor_Text_Sheet import *

# todo: add boots

# Equipment armor:
# tier 1:  level 1-4

lequipment1 = ChestGear("Leather chestplate", 4, 3, "Body", "light", 1, 40, 0.06, lequipment1_text)
lequipment2 = HeadGear("Leather helmet", 3, 2, "Head", "light", 1, 20, 0.025)
lequipment3 = ArmGear("Leather gauntlet", 2, 2, "Hand", "light", 1, 15, 0.02)
lequipment4 = Shield("short shield", 3, 3, "Shield", "light", 1, 35, 0.045)

mequipment1 = ChestGear("Breastplate armor", 5, 4, "Body", "medium", 1, 45, 0.12)
mequipment2 = HeadGear("Iron circlet", 3, 3, "Head", "medium", 1, 24, 0.05)
mequipment3 = ArmGear("Leather plate gauntlet", 3, 2, "Hand", "medium", 1, 18, 0.04)
mequipment4 = Shield("Medium shield", 4, 4, "Shield", "medium", 1, 40, 0.09)

hequipment1 = ChestGear("Steel armor", 6, 4, "Body", "heavy", 1, 50, 0.2)
hequipment2 = HeadGear("Steel helmet", 4, 3, "Head", "heavy", 1, 28, 0.09)
hequipment3 = ArmGear("Steel gauntlet", 3, 3, "Hand", "heavy", 1, 23, 0.06)
hequipment4 = Shield("Heavy shield", 5, 5, "Shield", "heavy", 1, 44, 0.15)

equipmentT1 = [lequipment1, lequipment2, lequipment3, lequipment4, mequipment1, mequipment2, mequipment3, mequipment4,
               hequipment1, hequipment2, hequipment3, hequipment4]

# tier 2:  level 5-8

lequipment5 = ChestGear("Padded armor", 6, 6, "Body", "light", 5, 150, 0.06)
lequipment6 = HeadGear("Padded helmet", 4, 4, "Head", "light", 5, 80, 0.025)
lequipment7 = ArmGear("Padded gauntlet", 4, 3, "Hand", "light", 5, 60, 0.02)
lequipment8 = Shield("Light steel shield", 6, 5, "Shield", "light", 5, 120, 0.045)

mequipment5 = ChestGear("Chainmail", 8, 7, "Body", "medium", 5, 165, 0.12)
mequipment6 = HeadGear("Steel circlet", 5, 5, "Head", "medium", 5, 90, 0.05)
mequipment7 = ArmGear("Vambrace", 5, 4, "Hand", "medium", 5, 70, 0.04)
mequipment8 = Shield("Steel shield", 7, 6, "Shield", "medium", 5, 130, 0.09)

hequipment5 = ChestGear("Banded mail armor", 10, 9, "Body", "heavy", 5, 180, 0.2)
hequipment6 = HeadGear("Mail helmet", 6, 6, "Head", "heavy", 5, 100, 0.09)
hequipment7 = ArmGear("Banded mail gauntlet", 6, 5, "Hand", "heavy", 5, 80, 0.06)
hequipment8 = Shield("Heavy steel shield", 8, 8, "Shield", "heavy", 5, 145, 0.15)

equipmentT2 = [lequipment5, lequipment6, lequipment7, lequipment8, mequipment5, mequipment6, mequipment7, mequipment8,
               hequipment5, hequipment6, hequipment7, hequipment8]

# tier 3:  level 9-12

lequipment9 = ChestGear("Light nordic armor", 9, 8, "Body", "light", 9, 450, 0.06)
lequipment10 = HeadGear("Light nordic helmet", 6, 6, "Head", "light", 9, 1220, 0.025)
lequipment11 = ArmGear("Light nordic gauntlet", 5, 5, "Hand", "light", 9, 180, 0.02)
lequipment12 = Shield("Light nordic shield", 8, 7, "Shield", "light", 9, 400, 0.045)

mequipment9 = ChestGear("Celtic leather plate armor", 11, 11, "Body", "medium", 9, 480, 0.12)
mequipment10 = HeadGear("Celtic helmet", 7, 7, "Head", "medium", 9, 245, 0.05)
mequipment11 = ArmGear("Celtic gauntlet", 6, 6, "Hand", "medium", 9, 200, 0.04)
mequipment12 = Shield("Celtic shield", 10, 9, "Shield", "medium", 9, 420, 0.09)

hequipment9 = ChestGear("Heavy nordic armor", 14, 13, "Body", "heavy", 9, 550, 0.2)
hequipment10 = HeadGear("Heavy nordic helmet", 8, 8, "Head", "heavy", 9, 260, 0.09)
hequipment11 = ArmGear("Heavy nordic gauntlet", 7, 7, "Hand", "heavy", 9, 225, 0.06)
hequipment12 = Shield("Heavy nordic shield", 12, 11, "Shield", "heavy", 9, 480, 0.15)

equipmentT3 = [lequipment9, lequipment10, lequipment11, lequipment12, mequipment9, mequipment10, mequipment11,
               mequipment12, hequipment9, hequipment10, hequipment11, hequipment12]


# tier 4:  level 13-16

mequipment13 = ("brigandine", 7, 6)


# tier 5:  level 17-20

lequipment17 = ("Elfic armor", 15, 17, "Body", "light")
lequipment18 = ("Elfic helmet", 8, 9, "Head", "light")
lequipment19 = ("Elfic gauntlet", 7, 8, "Hand", "light")
lequipment20 = ("Elfic shield", 13, 14, "Shield", "light")

mequipment17 = ("Dragonscale mail", 18, 19, "Body", "medium")
mequipment18 = ("Dragonscale helmet", 9, 10, "Head", "medium")
mequipment19 = ("Dragonscale gauntlet", 8, 9, "Hand", "medium")
mequipment20 = ("Dragonscale shield", 15, 16, "Shield", "medium")

hequipment17 = ("Wyvern bone armor", 21, 22, "Body", "heavy")
hequipment18 = ("Wyvern bone helmet", 10, 11, "Head", "heavy")
hequipment19 = ("Wyvern bone gauntlet", 9, 10, "Hand", "heavy")
hequipment20 = ("Wyvern bone shield", 18, 19, "Shield", "heavy")

equipmentT5 = [lequipment17, lequipment18, lequipment19, lequipment20, mequipment17, mequipment18, mequipment19,
               mequipment20, hequipment17, hequipment18, hequipment19, hequipment20]

# special armors


# other gear part

other1 = ("cape", 0, 0)