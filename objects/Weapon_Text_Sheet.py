sword1_text = "A lightweight sword, used by beginners"
sword2_text = "A sword mostly used by town's guard"
sword3_text = "The shape of this blade make incredible damage"
sword4_text = "The knight of the Kingdom have adopted this sword of its versatility"
sword5_text = "A cursed weapon thirsty of blood"
sword6_text = ""
sword7_text = ""
sword8_text = "A relic said to have slain hundreds of dragons"
sword9_text = "Do you feel lucky? If so, this is your weapon of choice!"
# longsword(medium), greatsword(heavy), short sword(light)
# categories: dagger(1.4-1.8), short sword(1.3-1.4), long sword(1.2-1.3), greatsword(1.1)
# iron dagger, ceremonial dagger, exquisite dagger,


rapier1_text = "A simple rapier"
rapier2_text = "A balanced weapon easy to handle"
rapier3_text = "One of the finest sword ever made"
rapier4_text = ""
rapier5_text = ""
rapier6_text = ""
rapier7_text = "Embeded with the speed of light"
# katana(medium)


axe1_text = "Good to chop logs and goblin"
axe2_text = "A good axe made for fight"
axe3_text = "A great weapon on the battleground"
axe4_text = "An heavy weapon with powerful smash"
axe5_text = ""
axe6_text = ""
axe7_text = ""
# light axe(medium), rusty great axe (hvy), giant axe(hvy)


mace1_text = "A one handed mace"
mace2_text = ""
mace3_text = ""
mace4_text = "Can crush a sturdy shield in a single swing"
mace5_text = ""
mace6_text = ""
mace7_text = "Cause fear in the heart of its opponents"
mace8_text = "The name explain by itself"
# ()
# categories: morning star (1.3-1.4), warhammer(1.2-1.3), mace(1.1)


bow1_text = ""
bow2_text = ""
bow3_text = ""
bow4_text = ""
bow5_text = ""
bow6_text = ""
bow7_text = ""
bow8_text = ""
# wooden bow(light), iron bow(medium), skeleton bow(light), amazon bow(medium)


spear1_text = "A spear mostly used to hunt wild animals"
spear2_text = ""
spear3_text = "Used by royal cavalry"
spear4_text = ""
spear5_text = ""
spear6_text = ""
spear7_text = ""
# iron lance(hvy),


staff1_text = "A wooden staff with magic power"
staff2_text = "A staff for adept spellcaster"
staff3_text = ""
staff4_text = ""
staff5_text = ""
staff6_text = "This old staff is as much powerful as it is old"
staff7_text = "Made with a horn of unicorn, its power is infinite"
# magic wand(light), phoenix staff(medium), scepter(hvy)
# categories: wand(1.4-1.5), staff(1.2-1.3), scepter/long staff(1.1)