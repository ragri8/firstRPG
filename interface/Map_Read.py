import os.path


def file_read(map_number):
    map_code = ""
    map_name = "../map/map_{}.txt".format(map_number)
    map_file = open(map_name, "r")
    for line in map_file:
        map_code += line[:-1]
    map_file.close()
    return map_code


def mapfile_save(p_map, map_number):
    size_x = str(p_map.size_x)
    size_x = (4 - len(size_x))*"0" + size_x
    size_y = str(p_map.size_y)
    size_y = (4 - len(size_y))*"0" + size_y
    biome = str(p_map.biomecode)
    biome = (2 - len(biome))*"0" + biome
    minLevel = str(p_map.minLevel)
    minLevel = (2 - len(minLevel))*"0" + minLevel
    maxLevel = str(p_map.maxLevel)
    maxLevel = (2 - len(maxLevel)) * "0" + maxLevel
    background = str(p_map.fightBackground)
    background = (2 - len(background))*"0" + background
    start_x = str(p_map.start_x)
    start_x = (4 - len(start_x)) * "0" + start_x
    start_y = str(p_map.start_y)
    start_y = (4 - len(start_y)) * "0" + start_y
    mapcode = str(p_map.mapcode)
    interactionList = ""
    for i in p_map.mapinteraction:
        interactionList += str(i[0]) + str(i[1])
    print("Interaction list: \n{}\n".format(interactionList))
    complete_code = size_x + size_y + "\n" + biome + minLevel + maxLevel + background + "\n" + start_x + start_y
    for i in range(int(size_y)):
        complete_code += "\n" + mapcode[int(size_x)*i: int(size_x)*(i+1)]
    for i in range(0, len(interactionList), 16):
        if i+16 <= len(interactionList):
            complete_code += "\n" + interactionList[i: i + 16]
        elif i+8 == len(interactionList):
            complete_code += "\n" + interactionList[i: i + 8]
    complete_code += "\n"
    print(complete_code)
    map_file = "map/map_{}.txt".format(map_number)
    newFile = open(map_file, "w")
    newFile.write(complete_code)
    newFile.close()


def add_zeros(value, size=2):
    return (size - len(value))*"0" + value
