from interface.Pygame_Basic import *
from interface.menu.Menu_Factory import Menu_Factory
from core.Global import menu_mode


class MenuInterface:
    def __init__(self, party):
        self.player_party = party
        self.menu_boxes = [Menu_Factory.createMainMenu()]
        self.select = 0  # indicate real selection index from every...
        self.menu_select = None
        self.menu_select_index = 0
        self.listener = None
        self.end_menu = False
        self.cancel_flag = False
        self.execution_flag = False
        self.prev_page_flag = False
        self.next_page_flag = False
        self.test = True

    def display(self):
        uniform_background(menu_blue)
        for box in self.menu_boxes:
            box.display()

    def changeMode(self, mode, team, arg=None):
        self.menu_select = mode
        menu = Menu_Factory.createMenu(mode, team, arg)
        self.menu_boxes.append(menu)

    def sendResponse(self, index, sub_index=0):  # handle syst. event: decide to call the listener or not using flags
        self.listener(index, sub_index, self.cancel_flag)
        self.cancel_flag = False
        self.execution_flag = False

    def loop(self):
        while not self.end_menu:
            current_box = self.menu_boxes[-1]
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.end_menu = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        current_box.changeSelect(y=-1)
                        print("try to move up")
                    elif event.key == pygame.K_DOWN:
                        print("try to move down")
                        current_box.changeSelect(y=1)
                    elif event.key == pygame.K_LEFT:
                        current_box.changeSelect(x=-1)
                    elif event.key == pygame.K_RIGHT:
                        current_box.changeSelect(x=1)
                    elif event.key in [pygame.K_RETURN, pygame.K_SPACE]:
                        self.selection()
                    elif event.key == pygame.K_x:
                        current_box.select_y = 0
                        self.cancel_flag = True
                        self.listener(0, current_box.select_x, self.cancel_flag)
                        self.cancel_flag = False
                    elif event.key == pygame.K_o:
                        for fighter in self.player_party.members:
                            print("{}'s items:".format(fighter.name))
                            for item in fighter.getItems():
                                print("-{}".format(item.name))
                            if fighter.weapon is not None:
                                print("Weapon equipped: {}".format(fighter.weapon))
                            else:
                                print("No weapon equipped")
                            print("Set equipment:")
                            for equipment in fighter.equipment:
                                print("-{}".format(equipment.name))
                            fighter.organise_items()
            self.select = current_box.select_y
            self.display()
            pygame.display.update()
            clock.tick(fps)

    def selection(self):
        current_box = self.menu_boxes[-1]
        select_y = current_box.select_y
        if current_box.hasCancel and select_y == current_box.max_y:
            self.cancel_flag = True
        self.sendResponse(select_y, current_box.select_x)

    def getCurrentMode(self):
        return self.menu_boxes[-1].mode

    def getCurrentSelect(self):
        return self.menu_boxes[-1].select_y

    def getMainMode(self):
        return self.menu_boxes[1].mode

    def getSubMode(self):
        return self.menu_boxes[2].mode

    def getMainChoices(self):
        return self.menu_boxes[1].getXSelect(), self.menu_boxes[1].getYSelect()

    def getSubChoice(self):
        if len(self.menu_boxes) > 2:
            return self.menu_boxes[2].getYSelect()
        else:
            return None

    def goToSubMenu(self):
        for box in range(len(self.menu_boxes) - 2):
            self.menu_boxes.pop(-1)

    def isMain(self):
        if len(self.menu_boxes) == 1:
            return True
        else:
            return False

    def isChoiceEnabled(self):
        return self.menu_boxes[-1].isChoiceEnabled()
