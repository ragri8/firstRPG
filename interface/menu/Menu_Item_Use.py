from interface.menu.Custom_Box import *


class Menu_Item_Use(Custom_Box):
    def __init__(self, fighter, itemIndex):
        super().__init__(max_x=0, max_y=5, mode=Menu_Mode.ITEMUSE)
        self.fighter = fighter
        self.item = self.fighter.getItems()[itemIndex]
        self.choices = ["Equip", "Unequip", "Use", "Give", "Toss", "Cancel"]
        self.disabledChoices = self.getDisabledChoices(self.item)

    def changeSelect(self, x=0, y=0):
        super().changeSelect(x, y)
        print("x and y values: ({}, {})".format(self.select_x, self.select_y))

    def display(self):
        left = thin_border * 2 + display_width * 3 // 4
        top = thin_border
        width = display_width // 4 - thin_border * 3
        height = display_height * 3 // 5 - thin_border * 2
        custom_box_display(left, top, width, height, menu_blue)
        selection_box(left + border, top + menu_text_size * 2, self.select_y, display_width * 3 // 16)
        custom_text_display_enabled(self.choices, self.disabledChoices, left + border,
                                    top + menu_text_size * 2)

    def getDisabledChoices(self, item):
        disabled = []
        if item.item_type() in ["Equipment", "Weapon"]:
            if self.fighter.isEquipped(item):
                disabled.append(False)
                disabled.append(True)
            else:
                disabled.append(True)
                disabled.append(False)
        else:
            disabled.append(False)
            disabled.append(False)
        disabled.append(item.isUsable())
        disabled.append(not self.fighter.isEquipped(item))
        disabled.append(item.isTossable() and not self.fighter.isEquipped(item))
        disabled.append(True)
        return disabled

    def isChoiceEnabled(self):
        return self.disabledChoices[self.select_y]
