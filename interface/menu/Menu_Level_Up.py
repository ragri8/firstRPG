from interface.menu.Custom_Box import *


class Menu_Level_Up(Custom_Box):
    def __init__(self, party):
        super().__init__(max_x=len(party.members)-1, max_y=3, mode=Menu_Mode.FIGHTER)
        self.party = party

    def display(self):
        left = thin_border * 2 + display_width // 4
        top = thin_border
        width = display_width * 3 // 4 - thin_border * 3
        height = display_height - thin_border * 2
        custom_box_display(left, top, width, height, menu_blue)
        fighter = self.party.members[self.select_x]
        statsList = fighter.displayLevelUpInfo()
        selection_box(display_width * 3 // 8, border + menu_margin_size * 8, self.select_y, display_width // 4)
        if fighter.isStatPointLeft():
            custom_text_display(["+1"], display_width * 9 // 16, border + menu_margin_size * (8 + self.select_y))
        custom_text_display(statsList, display_width * 3 // 8, border)
