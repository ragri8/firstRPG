from interface.menu.Main_Menu import Main_Menu
from interface.menu.Menu_Fighter import Menu_Fighter
from interface.menu.Menu_Spell import Menu_Spell
from interface.menu.Menu_Item import Menu_Item
from interface.menu.Menu_Target import Menu_Target
from interface.menu.Menu_Item_Use import Menu_Item_Use
from interface.menu.Menu_Level_Up import Menu_Level_Up
from interface.menu.Menu_Option import Menu_Option
from interface.menu.Menu_Skills import Menu_Skills
from core.Global import Menu_Mode


class Menu_Factory:
    @staticmethod
    def createMenu(mode, team=None, arg=None):
        menu_box = None
        if mode == Menu_Mode.FIGHTER:
            menu_box = Menu_Fighter(team)
        elif mode == Menu_Mode.SPELL:
            menu_box = Menu_Spell(team)
        elif mode == Menu_Mode.ITEM:
            menu_box = Menu_Item(team)
        elif mode == Menu_Mode.LEVELUP:
            menu_box = Menu_Level_Up(team)
        elif mode == Menu_Mode.OPTION:
            menu_box = Menu_Option(team)
        elif mode == Menu_Mode.CANCEL:
            return None
        elif mode == Menu_Mode.TARGET:
            menu_box = Menu_Target(team, arg)
        elif mode == Menu_Mode.CONFIRM:
            pass  # menu_box = Menu_Confirm(team) todo: what to do with this?
        elif mode == Menu_Mode.ITEMUSE:
            fighterIndex = arg[0]
            itemIndex = arg[1]
            menu_box = Menu_Item_Use(team.members[fighterIndex], itemIndex)
        elif mode == Menu_Mode.SKILLS:
            menu_box = Menu_Skills
        else:
            print(mode)
            raise TypeError
        return menu_box

    @staticmethod
    def createMainMenu():
        return Main_Menu()
