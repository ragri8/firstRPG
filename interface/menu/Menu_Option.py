from interface.menu.Custom_Box import *


class Menu_Option(Custom_Box):
    def __init__(self, party):
        super().__init__(max_x=len(party.members)-1, max_y=0, mode=Menu_Mode.FIGHTER)
        self.party = party

    def display(self):
        left = thin_border * 2 + display_width // 4
        top = thin_border
        width = display_width * 3 // 4 - thin_border * 3
        height = display_height - thin_border * 2
        custom_box_display(left, top, width, height, menu_blue)
        fighter = self.party.members[self.select_x]
