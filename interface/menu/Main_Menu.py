from interface.menu.Custom_Box import *


class Main_Menu(Custom_Box):
    def __init__(self):
        super().__init__(max_x=0, max_y=5, mode=None)
        self.choiceList = menu_mode[:]
        self.hasCancel = True

    def display(self):
        custom_box_display(thin_border, thin_border, display_width // 4, display_height - thin_border * 2, menu_blue)
        custom_selection([], self.choiceList, thin_border * 3, thin_border * 4, self.select_y, 24)
