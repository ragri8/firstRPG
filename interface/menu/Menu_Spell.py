from interface.menu.Custom_Box import *


class Menu_Spell(Custom_Box):
    def __init__(self, party):
        super().__init__(max_x=len(party.members) - 1, max_y=0, mode=Menu_Mode.SPELL)
        self.party = party
        self.max_y = len(party.members[self.select_x].spellList)

    def changeSelect(self, x=0, y=0):
        super().changeSelect(x, y)
        print("x and y values: ({}, {})".format(self.select_x, self.select_y))
        self.max_y = len(self.party.members[self.select_x].spellList)
        if self.max_y != 0:
            self.max_y -= 1

    def display(self):
        left = thin_border * 2 + display_width // 4
        top = thin_border
        width = display_width * 3 // 4 - thin_border * 3
        height = display_height * 3 // 5 - thin_border * 2
        custom_box_display(left, top, width, height, menu_blue)

        fighter = self.party.members[self.select_x]
        custom_text_display([fighter.name], display_width * 5 // 16, border)
        mana_text = "Mana: {}/{}".format(fighter.manaleft, fighter.maxmana)
        custom_text_display([mana_text], display_width * 11 // 16, border)
        spellsName = fighter.getSpellsName()
        spellsCost = fighter.getSpellsCost()
        spellsUsable = fighter.getUsableSpells()
        custom_text_display(["Spells"], display_width * 5 // 16,
                            border + menu_text_size * 2)
        custom_text_display(["Cost"], display_width * 11 // 16,
                            border + menu_text_size * 2)
        if len(spellsName) != 0:
            self.display_choice(spellsName, spellsCost, spellsUsable)
            self.display_description(fighter, left, width, height + thin_border * 2)

    def display_choice(self, spellsName, spellsCost, spellsUsable):
        size = len(spellsName)
        adjustment, current = self.set_list_display(size)
        custom_text_display(["Spells"], display_width * 5 // 16,
                            border + menu_text_size * 2)
        custom_text_display(["Cost"], display_width * 11 // 16,
                            border + menu_text_size * 2)
        corrected_names = spellsName[adjustment:adjustment + 8]
        corrected_costs = spellsCost[adjustment:adjustment + 8]
        corrected_usable = spellsUsable[adjustment:adjustment+8]
        selection_box(display_width * 5 // 16, border + menu_text_size * 3, current, display_width * 7 // 16)
        custom_text_display_enabled(corrected_names, corrected_usable, display_width * 5 // 16,
                                    border + menu_text_size * 3)
        custom_text_display_enabled(corrected_costs, corrected_usable, display_width * 11 // 16,
                                    border + menu_text_size * 3)

    def display_description(self, fighter, left, width, top):
        height = display_height - top - thin_border
        custom_box_display(left, top, width, height, menu_blue)
        description = fighter.spellList[self.select_y].getDescription()
        custom_text_display(description, display_width * 5 // 16,
                            thin_border + top)
