from interface.menu.Custom_Box import *


class Menu_Item(Custom_Box):
    def __init__(self, party):
        super().__init__(max_x=len(party.members) - 1, max_y=0, mode=Menu_Mode.ITEM)
        self.party = party
        self.max_y = len(party.members[self.select_x].items)

    def update(self):
        self.select_y = 0
        self.max_y = len(self.party.members[self.select_x].items)

    def changeSelect(self, x=0, y=0):
        super().changeSelect(x, y)
        print("x and y values: ({}, {})".format(self.select_x, self.select_y))
        self.max_y = len(self.party.members[self.select_x].items)
        if self.max_y != 0:
            self.max_y -= 1

    def display(self):
        left = thin_border * 2 + display_width // 4
        top = thin_border
        width = display_width * 3 // 4 - thin_border * 3
        height = display_height * 3 // 5 - thin_border * 2
        custom_box_display(left, top, width, height, menu_blue)

        fighter = self.party.members[self.select_x]
        custom_text_display([fighter.name], display_width * 5 // 16, border)

        items = fighter.getItems()
        itemsName = []
        itemsCost = []
        for item in items:
            itemsName.append(item.name)
            itemsCost.append("{}".format(item.price))
        if len(itemsName) != 0:
            self.display_choice(itemsName, itemsCost)
            self.display_description(fighter, left, width, height + thin_border * 2)

    def display_choice(self, itemsName, itemsCost):
        size = len(itemsName)
        adjustment, current = self.set_list_display(size)
        custom_text_display(["Items"], display_width * 6 // 16,
                            border + menu_text_size * 2)
        custom_text_display(["Cost"], display_width * 11 // 16,
                            border + menu_text_size * 2)
        corrected_names = itemsName[adjustment:adjustment + 8]
        corrected_costs = itemsCost[adjustment:adjustment + 8]
        selection_box(display_width * 5 // 16, border + menu_text_size * 3, current, display_width * 7 // 16)
        custom_text_display(corrected_names, display_width * 5 // 16, border + menu_text_size * 3)
        custom_text_display(corrected_costs, display_width * 11 // 16, border + menu_text_size * 3)

    def display_description(self, fighter, left, width, top):
        height = display_height - top - thin_border
        custom_box_display(left, top, width, height, menu_blue)
        description = fighter.items[self.select_y].getDescription()
        custom_text_display(description, display_width * 5 // 16,
                            thin_border + top)
