from interface.Pygame_Basic import *
from core.Global import menu_mode, Menu_Mode


class Custom_Box:
    def __init__(self, max_x, max_y, mode):
        self.select_x = 0
        self.select_y = 0
        self.max_x = max_x
        self.max_y = max_y
        self.hasCancel = False
        self.mode = mode

    def update(self):
        pass

    def display(self):
        pass

    def set_list_display(self, size):
        adjustment = 0
        current = self.select_y
        if size <= 8:
            return adjustment, current
        else:
            if self.select_y > 6:
                if self.select_y == size - 1:
                    current = 7
                    adjustment = size - 8
                else:
                    current = 6
                    adjustment = self.select_y - 6
        return adjustment, current

    def changeSelect(self, x=0, y=0):
        if x != 0 <= self.select_x + x <= self.max_x:
            self.select_x += x
            self.select_y = 0
            print("max_y: {}".format(self.max_y))
        if 0 <= self.select_y + y <= self.max_y:
            self.select_y += y
            print("y value: {}".format(self.select_y))

    def getXSelect(self):
        return self.select_x

    def getYSelect(self):
        return self.select_y

    def isChoiceEnabled(self):
        return True
