from interface.menu.Custom_Box import *


class Menu_Target(Custom_Box):
    def __init__(self, party, size=1):
        super().__init__(max_x=0, max_y=len(party.members) - 1, mode=Menu_Mode.TARGET)
        self.party = party
        self.size = size

    def changeSelect(self, x=0, y=0):
        super().changeSelect(x, y)
        print("x and y values: ({}, {})".format(self.select_x, self.select_y))

    def display(self):
        left = thin_border * 2 + display_width // 4
        top = display_height * 3 // 5
        width = display_width * 3 // 4 - thin_border * 3
        height = display_height * 2 // 5 - thin_border
        custom_box_display(left, top, width, height, menu_blue)
        custom_box_display(left, top, width, height, menu_blue)
        selection_box(display_width * 5 // 16, thin_border+top+text_margin,
                      self.select_y, display_width * 3 // 16)
        if self.size != "1":
            custom_text_display(["-All"], display_width * 5 // 16,
                                thin_border + top)
        fighters = []
        for fighter in self.party.members:
            fighters.append("-{}".format(fighter.name))
        custom_text_display(["Choose a target:"], display_width * 5 // 16,
                            thin_border + top)
        custom_text_display(fighters, display_width * 5 // 16,
                            thin_border + top+text_margin)
