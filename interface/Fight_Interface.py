from interface.DisplayBox import *


class FightInterface:
    def __init__(self, player_party, enemy_party, background, fight_type="normal"):
        self.player_party = player_party
        self.enemy_party = enemy_party
        self.fight_type = fight_type
        self.background = background
        self.display_box = DisplayBox()
        self.page = 0
        self.cursor = 0  # indicate which element selected in choice page
        self.max = 0
        self.choice_size = 0
        self.select = 0  # indicate real selection index from every...
        self.listener = None
        self.end_fight = False
        self.cancel_flag = False
        self.prev_page_flag = False
        self.next_page_flag = False

    #def setDisplay(self, header, choices):
    #    self.main_menu_box.setDisplay(header, choices)

    def changePage(self, change):
        self.display_box.changePage(change)

    def display(self):
        gameDisplay.blit(self.background, (0, 0))
        self.displaySprites()
        self.display_box.display()

    def displaySprites(self):
        totalSize = -20
        partySize = len(self.player_party.members)
        positionList = []
        totalSize += 60*partySize
        x = int(display_width / 8)
        y = int((display_height - totalSize) / 2 - 160)
        for i in range(partySize):
            x_position = int(x + abs((partySize - 1) / 2 - i) * 80)
            positionList.append((x_position, y))
            y += 70
            self.displaySprite(self.player_party.members[i], x_position, y)
            self.displayStats(i, self.player_party.members[i])
        # enemy display
        totalSize = -20
        enemyPartySize = len(self.enemy_party.members)
        positionList = []
        totalSize += 60 * enemyPartySize
        x = int(5 * display_width / 8)
        y = int((display_height - totalSize) / 2 - 120)
        for i in range(enemyPartySize):
            x_position = int(x + abs((enemyPartySize - 1) / 2 - i) * 80)
            positionList.append((x_position, y))
            y += 70
            if self.enemy_party.members[i].hpleft > 0:
                self.displaySprite(self.enemy_party.members[i], x_position, y)
                self.stat_bar(self.enemy_party.members[i], "hp", x_position+10, y-10)

    @staticmethod
    def displaySprite(fighter, x, y):
        sprite = fighter.sprite.getSprite(fighter.spriteCondition)
        gameDisplay.blit(sprite, (x, y))

    # temp method display, strongly need to be more clean
    def displayStats(self, fighter_index, fighter):
        statList = [str(fighter)]
        statList.append("Hp {}/{}".format(fighter.hpleft, fighter.maxhp))
        statList.append("Sp {}/{}".format(fighter.stamleft, fighter.maxstam))
        statList.append("Mp {}/{}".format(fighter.manaleft, fighter.maxmana))
        iterator = 0
        pygame.draw.rect(gameDisplay, black,
                         (border + fighter_index * (stat_width + thin_border) - thin_border, 15, stat_width, 85))
        pygame.draw.rect(gameDisplay, white, (border + fighter_index * (stat_width + thin_border),
                                              15 + thin_border, stat_width - thin_border * 2, 85 - thin_border * 2))
        self.stat_bar(fighter, "hp", border + fighter_index * (stat_width + thin_border) + thin_border, 45, 2)
        self.stat_bar(fighter, "sp", border + fighter_index * (stat_width + thin_border) + thin_border, 64, 2)
        self.stat_bar(fighter, "mp", border + fighter_index * (stat_width + thin_border) + thin_border, 83, 2)
        for j in range(len(statList)):
            if j == 0:
                TextSurf, TextRect = text_objects(statList[j], largeText)
                side = 0
            else:
                TextSurf, TextRect = text_objects(statList[j], smallText)
                side = 110
            TextRect = (border + fighter_index * (stat_width + thin_border) + thin_border + side,
                        20 + thin_border + iterator)
            iterator += text_size + 1
            gameDisplay.blit(TextSurf, TextRect)

    def playerPosition(self):
        totalSize = -12
        partySize = len(self.player_party.members)
        positionList = []
        for i in self.player_party.members:
            totalSize += 70
        x = int(display_width / 8)
        y = (display_height - totalSize) / 2 - 60
        for i in range(partySize):
            x_position = x + abs((partySize - 1) / 2 - i) * 80
            positionList.append((x_position, y))
            y += 80
        return positionList

    @staticmethod
    def stat_bar(p_fighter, stat, x, y, size=1):
        if stat == "hp":
            pointLeft = int((p_fighter.hpleft / p_fighter.maxhp) * 50 * size)
            if p_fighter.hpleft <= 0:
                pointLeft = -1
        elif stat == "sp":
            pointLeft = int((p_fighter.stamleft / p_fighter.maxstam) * 50 * size)
        else:
            pointLeft = int((p_fighter.manaleft / p_fighter.maxmana) * 50 * size)
        if pointLeft != -1:
            pygame.draw.rect(gameDisplay, black, (x, y, 50 * size, 5))
        if pointLeft >= 30 * size:
            color = green
        elif 30 * size > pointLeft >= 18 * size:
            color = yellow
        elif 18 * size > pointLeft >= 8:
            pigment = int(pointLeft / 18 * 200 / size)
            color = (240, pigment, 0)
        elif 8 * size > pointLeft >= 0:
            color = red
        if pointLeft >= 0:
            pygame.draw.rect(gameDisplay, black, (x, y, 50 * size, 5))
            pygame.draw.rect(gameDisplay, color, (x, y, pointLeft, 5))

    # call to the Fight_Handler listener, send the current selected item index and the cancel flag
    # the cancel flag means the ability is canceled and to roll back to the previous state
    def sendResponse(self):  # handle syst. event: decide to call the listener or not using flags
        self.listener(self.display_box.getSelect(), self.cancel_flag)
        self.cancel_flag = False

    def loop(self):
        while not self.end_fight:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.end_fight = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        if self.display_box.select == 0:
                            continue
                        else:
                            self.display_box.select -= 1
                    if event.key == pygame.K_DOWN:
                        if self.display_box.select >= self.display_box.max:
                            continue
                        else:
                            self.display_box.select += 1
                    if event.key in [pygame.K_RETURN, pygame.K_SPACE]:
                        self.selection()
                    if event.key == pygame.K_o:
                        for fighter in self.player_party.members:
                            fighter.fight_display()
                        for fighter in self.enemy_party.members:
                            fighter.fight_display()
            self.display()
            pygame.display.update()
            clock.tick(fps)

    def startMusic(self):
        if self.fight_type == "boss":
            pygame.mixer.music.load("../music/boss_theme.wav")
        else:
            pygame.mixer.music.load("../music/battle_theme.wav")
        pygame.mixer.music.play(-1)

    @staticmethod
    def victoryMusic():
        pygame.mixer.music.load("../music/victory_theme.wav")
        pygame.mixer.music.play(-1)

    def selection(self):
        if self.display_box.selectionSize() > 0:
            if self.display_box.getDisabledState():
                return None
            selected = self.display_box.getSelected()
            if selected == "-Cancel":
                self.cancel_flag = True
                self.sendResponse()
            elif selected == "-Next page":
                self.display_box.changePage(1)
            elif selected == "-Previous page":
                self.display_box.changePage(-1)
            else:
                self.sendResponse()
        else:
            if self.display_box.line_display < len(self.display_box.headerText):
                self.display_box.updateLinesDisplay()
            else:
                self.sendResponse()

    # update content for the displayBox object with new parameters
    # param [in] type_info: useless, might be removed
    def update(self, type_info, h_text, c_text, disabled):  # Update display box text
        #print("update called")
        cancel = True
        if type_info == "no cancel":
            cancel = False
        self.display_box.resetPage()
        #print(c_text)
        #print(disabled)
        self.display_box.setDisplay(h_text, c_text, disabled, cancel)
        self.max = self.display_box.max
