import sys
import pygame

pygame.init()
display_width = 1020  # normally at 1020, 1080 before that
display_height = 600  # normally at 600
dialog_height = 400
stat_width = 200
border = 40
thin_border = 4
fps = 30
tile_size = 60
x_division = [0, 60, 120, 180, 240, 300, 360, 420, 480, 540, 600, 660, 720, 780, 840, 900]
y_division = [0, 60, 120, 180, 240, 300, 360, 420, 480]
starting_x = (display_width * 0.45)
starting_y = (display_height * 0.7)
black = (0, 0, 0)
grey = (205, 205, 205)
dark_grey = (110, 110, 110)
white = (255, 255, 255)
green = (0, 225, 0)
red = (255, 0, 0)
yellow = (255, 255, 0)
pale_blue = (125, 150, 245)
menu_blue = (75, 120, 225)
blue = (0, 0, 255)
text_size = 18
text_margin = text_size + 4
small_text_size = 12
menu_text_size = 24
menu_margin_size = menu_text_size + 1
menuText = pygame.font.Font("freesansbold.ttf", menu_text_size)
largeText = pygame.font.Font("freesansbold.ttf", text_size)
smallText = pygame.font.Font("freesansbold.ttf", small_text_size)
gameDisplay = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption("First RPG")
clock = pygame.time.Clock()
blank = pygame.image.load("../texture_file/blank.png")


# Create an object to generate text to display
# param [in] text: string object to display on a single line
# param [in] font: text police and text size object generated with pygame.font.Font
# param [in] color: tuple of color in RGB format
# param [out] textSurface: text as an image
# param [out] textSurface.get_rect(): size parameter of the textSurface image
def text_objects(text, font, color=black):
    textSurface = font.render(text, True, color)
    return textSurface, textSurface.get_rect()


# Display on the UI a multiline text
# param [in] textList: List of string to display, each element as a different line
# param [in] height: height of the text box to start
# param [in] line: number of the last line to display in the textList, if line > 7, only display the 7 last ones
# param [in] color: Text color in a RGB tuple format, black by default
def displayDialog(textList, height, line, color=black):
    iterator = 0
    for i in range(0, line):
        if line - i > 7:
            continue
        TextSurf, TextRect = text_objects(textList[i], largeText, color)
        TextRect = (80, height+iterator)
        iterator += text_size + 1
        gameDisplay.blit(TextSurf, TextRect)


# Display on the UI a multiline text
# param [in] textList: List of string to display, each element as a different line, size always < 6
# param [in] disabledTextList: list of boolean, determine which choices are disabled
# param [in] skipMargin: Size of the gap to skip, if different text are displayed before
# param [in] height: height of the text box to start
def displayChoice(textList, disabledTextList, skipMargin, height):
    iterator = skipMargin
    for index in range(len(textList)):
        if disabledTextList[index]:
            TextSurf, TextRect = text_objects(textList[index], largeText, dark_grey)
        else:
            TextSurf, TextRect = text_objects(textList[index], largeText)
        TextRect = (80, height+iterator)
        iterator += text_size + 1
        gameDisplay.blit(TextSurf, TextRect)


# Display of a large text box with a selection box on the UI
# Used to display option for the user to choose
# param [in] dialogList: List of string to display, each element as a different line
# param [in] choiceList: List of string to display, each element as a different line, each string is a possible
#                       option for the user to choose
# param [in] focusBox: integer for the focused option in choiceList
# param [in] dialogSize: Size of the gap to skip, depend on the len of dialogList
def big_display(dialogList, choiceList, focusBox, dialogSize):
    out_left = border
    out_top = dialog_height
    out_width = display_width - border*2
    out_height = display_height-dialog_height-30
    custom_box_display(out_left, out_top, out_width, out_height, black)
    in_left = border + thin_border
    in_top = dialog_height + thin_border
    in_width = display_width - 2 * (border + thin_border)
    in_height = display_height - dialog_height - 30 - 2 * thin_border
    custom_box_display(in_left, in_top, in_width, in_height, white)
    box_left = 80
    box_top = dialog_height + 12 + focusBox*(text_size + 1) + dialogSize
    box_width = 300
    box_height = text_size + 2
    custom_box_display(box_left, box_top, box_width, box_height, grey)
    displayDialog(dialogList, dialog_height+12, len(dialogList))
    displayChoice(choiceList, dialogSize, dialog_height+12)


# Display of a large text box on the UI
# can display a set amount of lines at the same time
# param [in] dialogList: List of string to display, each element as a different line
# param [in] line: number of the last line to display in the textList, if line > 7, only display the 7 last ones
def small_display(dialogList, line):
    out_left = border
    out_top = dialog_height
    out_width = display_width - border * 2
    out_height = display_height - dialog_height - 30
    custom_box_display(out_left, out_top, out_width, out_height, black)
    in_left = border + thin_border
    in_top = dialog_height + thin_border
    in_width = display_width - 2 * (border + thin_border)
    in_height = display_height - dialog_height - 30 - 2 * thin_border
    custom_box_display(in_left, in_top, in_width, in_height, white)
    displayDialog(dialogList, dialog_height+12, line)


def uniform_background(color=menu_blue):
    pygame.draw.rect(gameDisplay, color, (0, 0, display_width, display_height))


def custom_box_display(left, top, width, height, color=menu_blue):
    pygame.draw.rect(gameDisplay, black, (left, top, width, height))
    pygame.draw.rect(gameDisplay, color, (left+thin_border, top+thin_border, width-thin_border*2, height-thin_border*2))


def custom_selection(text, choices, left, top, select, c_text_size=18):
    margin = c_text_size + 1
    text_height = len(text) * margin
    for index in range(len(text)):
        TextSurf, TextRect = text_objects(text[index], largeText)
        TextRect = (left+4, top + margin * (index + 1))
        gameDisplay.blit(TextSurf, TextRect)
    for index in range(len(choices)):
        TextSurf, TextRect = text_objects(choices[index], largeText)
        TextRect = (left, top + margin*(index+1)+text_height)
        if index == select:
            pygame.draw.rect(gameDisplay, pale_blue,
                             (left, top + margin * (index + 1)-4, 200, c_text_size + 2))
        gameDisplay.blit(TextSurf, TextRect)


def selection_box(left, top, select, width=200, c_text_size=24):
    margin = c_text_size + 1
    pygame.draw.rect(gameDisplay, pale_blue,
                     (left, top + margin * (select + 1) - 4, width, c_text_size + 2))


def custom_selection_display(text, choices, select, left, top, c_text_size=24):
    margin = c_text_size + 1
    text_height = len(text) * margin
    for index in range(len(text)):
        TextSurf, TextRect = text_objects(text[index], pygame.font.Font("freesansbold.ttf", c_text_size))
        TextRect = (left + 4, top + margin * (index + 1))
        gameDisplay.blit(TextSurf, TextRect)
    for index in range(len(choices)):
        TextSurf, TextRect = text_objects("-{}".format(choices[index]),
                                          pygame.font.Font("freesansbold.ttf", c_text_size))
        TextRect = (left, top + margin * (index + 1) + text_height)
        if index == select:
            pygame.draw.rect(gameDisplay, pale_blue,
                             (left, top + margin * (index + 1) - 1, 200, c_text_size + 3))
        gameDisplay.blit(TextSurf, TextRect)


def custom_text_display(text, left, top, c_text_size=24):
    margin = c_text_size + 1
    for index in range(len(text)):
        TextSurf, TextRect = text_objects(text[index], largeText)
        TextRect = (left + 4, top + margin * (index + 1))
        gameDisplay.blit(TextSurf, TextRect)


def custom_text_display_enabled(text, enabled, left, top, c_text_size=24):
    margin = c_text_size + 1
    for index in range(len(text)):
        color = black
        if not enabled[index]:
            color = dark_grey
        TextSurf, TextRect = text_objects(text[index], largeText, color)
        TextRect = (left + 4, top + margin * (index + 1))
        gameDisplay.blit(TextSurf, TextRect)
