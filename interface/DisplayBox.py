from interface.Pygame_Basic import *


class DisplayBox:
    def __init__(self):
        self.headerText = []
        self.choiceText = []
        self.disabledText = []
        self.choices = []
        self.disabled = []
        self.line_display = 1
        self.select = 0
        self.page = 0
        self.page_max = 0
        self.max = 0

    def resetPage(self):
        self.page = 0

    # todo: rearrange disabled to work with many display style
    def setDisplay(self, header, choices, disabled, cancel=True):
        self.headerText.clear()
        self.choiceText.clear()
        self.disabledText.clear()
        self.headerText = header
        self.line_display = 1
        self.choices = choices
        self.disabled = disabled
        self.page_max = (len(self.choices) - 1) // 4
        if not cancel:
            self.max = len(choices)-1
            self.choiceText = choices
            self.disabledText = disabled
            self.select = 0
        else:
            if len(choices) == 0:
                self.max = 0
                self.line_display = 1
            elif len(choices) < 5:
                self.max = len(choices)
                self.choiceText = choices
                self.disabledText = disabled
                self.select = 0
            else:
                self.max = 5
                self.setPage()
            self.choiceText.append("-Cancel")
            self.disabledText.append(False)

    def setPage(self):
        self.select = 0
        self.choiceText = self.choices[0:4]
        self.choiceText.append("-Next page")
        self.disabledText = self.disabled[0:4]
        self.disabledText.append(False)

    def changePage(self, change):
        if 0 <= self.page + change <= self.page_max:
            self.page += change
            self.choiceText.clear()
            if self.page == 0:
                self.max = 5
                self.setPage()
                self.choiceText.append("-Cancel")
                self.disabledText.append(False)
            else:
                self.select = 1
                self.choiceText.append("-Previous page")
                self.disabledText = [False]
                if len(self.choices) - self.page * 4 >= 4:
                    self.choiceText = self.choiceText + self.choices[4*self.page:4*(self.page+1)]
                    self.disabledText = self.disabledText + self.disabled[4 * self.page:4 * (self.page + 1)]
                    self.max = 5
                    if len(self.choices) > 4*(self.page + 1):
                        self.choiceText.append("-Next page")
                        self.disabledText.append(False)
                        self.max += 1
                else:
                    size = len(self.choices) - self.page * 4
                    self.choiceText = self.choiceText + self.choices[4*self.page:4*self.page + size]
                    self.max = size + 1
                self.choiceText.append("-Cancel")
                self.disabledText.append(False)

    def display(self):
        pygame.draw.rect(gameDisplay, black,
                         (border, dialog_height, display_width - border * 2, display_height - dialog_height - 30))
        pygame.draw.rect(gameDisplay, white,
                         (border + thin_border, dialog_height + thin_border, display_width - (border + thin_border) * 2,
                          display_height - dialog_height - 30 - thin_border * 2))
        if len(self.choiceText) > 0:
            pygame.draw.rect(gameDisplay, grey,
                             (80, dialog_height + 12 + (self.select+1) * (text_size + 1) + len(self.headerText),
                              300, text_size + 2))
            displayDialog(self.headerText, dialog_height + 12, len(self.headerText))
            displayChoice(self.choiceText, self.disabledText, len(self.headerText), dialog_height + 12 + text_size)
        else:
            temp_text = self.headerText[0:self.line_display]
            displayDialog(temp_text, dialog_height + 12, len(temp_text))  # update displayDialog to have possibility to show specific number of lines

    def addDisplay(self, line):
        self.headerText.append(line)

    def updateLinesDisplay(self):
        self.line_display += 1

    # Return True if a choice has been made or if te last ability is canceled.
    # Return False if the page is changed, but charge the new one
    def selection(self):
        if self.choiceText[self.select] == "-Next page":
            self.changePage(1)
            return False
        elif self.choiceText[self.select] == "-Previous page":
            self.changePage(-1)
            return False
        elif self.choiceText[self.select] == "-Cancel":
            return True
        else:
            return True

    def selectionSize(self):
        return self.max

    def getDisabledState(self):
        return self.disabledText[self.select]

    def getSelected(self):
        return self.choiceText[self.select]

    # Return True if a choice has been made, False if the last ability is canceled
    def newState(self):
        if self.choiceText[self.select] == "-Cancel":
            return False
        else:
            return True

    def getSelect(self):
        if self.page > 0:
            return self.select + self.page * 4 - 1
        else:
            return self.select
