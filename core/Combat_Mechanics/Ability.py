from core.Equipment_and_Other import dice
from core.Global import moveList
from core.Combat_Mechanics.Status_Linker import Status_Linker, Shield_Linker, Charge_Linker
from core.Combat_Mechanics.Status_Effect import StunningEffect, ChargingEffect


# Ability class is a class object used to store ability that can be performed in combat, as simple attack,
# or spellcasting, and include a more complex one called "Ability", which can have various effects
class Ability:
    def __init__(self, name, ability_type, damage_type, cost, cost_type, targetGroup, targetGroupSize, cooldown=0):
        self.name = name
        self.ability_type = ability_type
        self.damage_type = damage_type
        self.cost = cost
        self.cost_type = cost_type
        self.targetGroup = targetGroup
        self.targetGroupSize = targetGroupSize
        self.statusLinker = None
        self.cooldown = cooldown
        self.currentCD = 0

    def __str__(self):
        return self.name

    def cost(self):
        return self.cost

    def getTargetGroup(self):
        return self.targetGroup

    def getGroupSize(self):
        return self.targetGroupSize

    def actionTarget(self):
        return self.targetGroup, self.targetGroupSize

    def actionCast(self, user, target, critical=False):
        self.currentCD = self.cooldown
        #print("Cooldown: {}".format(self.currentCD))

    def isDisabled(self):
        if self.currentCD > 0:
            return True
        return False

    def landBonus(self, fighter):
        pass

    @staticmethod
    def isCriticalLanded(user):
        result = dice(100) > 100 - user.tempCritical
        #if result:
        #    print("Critical success")
        return result

    def applyAction(self, target, hit_point):
        target.takeDamage(hit_point)

    def extraEffect(self, user, target):
        pass

    def strike(self, user, target):
        numberStrike = 1 + user.tempAgility * user.offensiveSpeed // (target.tempAgility / target.defensiveSpeed)
        additionalStrike = (user.tempAgility * user.offensiveSpeed %
                            (target.tempAgility / target.defensiveSpeed) /
                            (target.tempAgility / target.defensiveSpeed)) * 100
        if additionalStrike > dice(100):
            numberStrike += 1
        numberStrike = int(numberStrike)
        #print("Number of strike: {}".format(numberStrike))
        return numberStrike

    @staticmethod
    def damageType():
        return "pattack"

    def setCost(self, new_cost):
        self.cost = new_cost

    @staticmethod
    def type():
        return "<ability>"

    @staticmethod
    def skillType():
        return "active"

    def addWeaponStatus(self, weapon, fighter):
        self.statusLinker = Status_Linker.addWeaponStatus(weapon, fighter)

    def removeWeaponStatus(self):
        self.statusLinker = None


# Basic attack: damage based on the weapon and multiplied by the number of strike,
#               but enemy resistance is calculated on each strike
class Attack(Ability):
    def __init__(self):
        super().__init__("attack", "weapon based", "pattack", 3, "SP", "Enemy", "1", 0)

    def actionCast(self, user, target, critical=False):
        damage = 0
        for strike in range(self.strike(user, target)):
            temp_damage = user.weapon.baseattack
            for i in range(user.weapon.nbrdmgdice):
                temp_damage += dice(user.weapon.damagedice)
            temp_damage += (user.tempStrength - 10) // 2
            if critical:
                temp_damage *= 1.5
            if strike != 0:
                temp_damage *= 0.60
            temp_damage *= user.tempWeaponBonus
            temp_damage = int(temp_damage)
            #print("Damage before resistance: {}".format(temp_damage))
            temp_damage = target.damageResist(temp_damage, self.damage_type)
            #print("Strike damage: {}".format(temp_damage))
            damage += temp_damage
        return damage

    def landBonus(self, fighter):
        fighter.fightStatus.weaponUsed(fighter)


# Power attack: like a basic attack, but with a single strike and multiplied by 2, with one resistance call
class PowerAttack(Ability):
    def __init__(self):
        super().__init__("power attack", "weapon based", "pattack", 8, "SP", "Enemy", "1", 0)

    def actionCast(self, user, target, critical=False):
        damage = user.weapon.baseattack + (user.tempStrength - 5) // 2
        for i in range(user.weapon.nbrdmgdice):
            damage += dice(user.weapon.damagedice)
        damage *= 1.75
        if critical:
            damage *= 1.5
        damage *= user.tempWeaponBonus
        damage = int(damage)
        damage = target.damageResist(damage, self.damage_type)
        return damage

    def landBonus(self, fighter):
        fighter.fightStatus.weaponUsed(fighter)


# Spin attack: like basic attack, damage all enemies at once with an additional strike on each,
#              but the first strike has the same reduction as the other ones
class SpinAttack(Ability):
    def __init__(self):
        super().__init__("spin attack", "weapon based", "pattack", 9, "SP", "Enemy", "all", 0)

    def actionCast(self, user, target, critical=False):
        damage = 0
        for strike in range(self.strike(user, target)):
            temp_damage = user.weapon.baseattack + (user.tempStrength - 10) // 2
            for i in range(user.weapon.nbrdmgdice):
                temp_damage += dice(user.weapon.damagedice)
            if critical:
                temp_damage *= 1.5
            temp_damage *= 0.6
            temp_damage = int(temp_damage)
            temp_damage = target.damageResist(temp_damage, self.damage_type)
            #print("Strike damage: {}".format(temp_damage))
            damage += temp_damage
        return damage

    def strike(self, user, target):
        numberStrike = super().strike(user, target)
        numberStrike += 1
        #print("Real number of strike: {}".format(numberStrike))
        return numberStrike

    def landBonus(self, fighter):
        fighter.fightStatus.weaponUsed(fighter)


# todo: check if translation makes sense or not
class RainningStrike(Ability):
    def __init__(self):
        super().__init__("rainnig strike", "weapon based", "pattack", 7, "SP", "Enemy", "1", 2)
        self.piercing = 0.1

    def actionCast(self, user, target, critical=False):
        super().actionCast(user, target, critical)
        damage = 0
        for strike in range(self.strike(user, target)):
            temp_damage = user.weapon.baseattack
            for i in range(user.weapon.nbrdmgdice):
                temp_damage += dice(user.weapon.damagedice)
            temp_damage += (user.tempStrength - 10) // 2
            if critical:
                temp_damage *= 1.5
            temp_damage *= 0.60
            temp_damage *= user.tempWeaponBonus
            temp_damage = int(temp_damage)
            #print("Damage before resistance: {}".format(temp_damage))
            temp_damage = target.damageResist(temp_damage, self.damage_type, self.piercing)
            #print("Strike damage: {}".format(temp_damage))
            damage += temp_damage
        return damage

    def strike(self, user, target):
        numberStrike = super().strike(user, target) * 2
        numberStrike += 2
        #print("Real number of strike: {}".format(numberStrike))
        return numberStrike

    def landBonus(self, fighter):
        fighter.fightStatus.weaponUsed(fighter)


class ChargedAttack(Ability):
    def __init__(self):
        super().__init__("charge strike", "charge", "pattack", 10, "SP", "Enemy", "1", 4)
        self.statusLinker = None

    def actionCast(self, user, target, critical=False):
        super().actionCast(user, target, critical)
        charge = ChargingEffect(target, ChargeStrike())
        charge.action.statusLinker = Status_Linker.addWeaponStatus(user.weapon, user)
        user.passiveStatusEffects.append(charge)
        user.tempChargingStatus = True
        return 0


class ChargeStrike(Ability):
    def __init__(self):
        super().__init__("charged strike", "weapon based", "pattack", 0, "SP", "Enemy", "1")

    def actionCast(self, user, target, critical=False):
        damage = user.weapon.baseattack + user.tempStrength // 2
        for i in range(user.weapon.nbrdmgdice):
            damage += dice(user.weapon.damagedice)
        damage *= 2.75
        if critical:
            damage *= 1.5
        damage *= user.tempWeaponBonus
        damage = int(damage)
        damage = target.damageResist(damage, self.damage_type)
        return damage

    def landBonus(self, fighter):
        fighter.fightStatus.weaponUsed(fighter)


class ShieldBash(Ability):
    def __init__(self):
        super().__init__("shield bash", "shield based", "pattack", 6, "SP", "Enemy", "1", 2)
        self.statusLinker = Shield_Linker(StunningEffect(2), 0, 0)

    def actionCast(self, user, target, critical=False):
        super().actionCast(user, target, critical)
        damage = 0
        for equipment in user.equipment:
            if equipment.gearSet() == "Shield":
                damage += 3 * equipment.pdefense
        damage += (user.tempStrength - 5) // 2
        if critical:
            damage *= 1.5
        damage = int(damage)
        damage = target.damageResist(damage, self.damage_type)
        if damage == 0:
            damage = 1
        return damage

    def landBonus(self, fighter):
        pass


# only for sword, rapier and lance; deals normal damage with a single strike, but ignore 40% of armor or more
class Thrust(Ability):
    def __init__(self):
        super().__init__("Thrust", "weapon based", "pattack", 5, "SP", "Enemy", "1")
        self.piercing = 0.4

    def actionCast(self, user, target, critical=False):
        damage = user.weapon.baseattack + (user.tempStrength - 10) // 2
        for i in range(user.weapon.nbrdmgdice):
            damage += dice(user.weapon.damagedice)
        if critical:
            damage *= 1.5
        damage *= user.tempWeaponBonus
        damage = int(damage)
        damage = target.damageResist(damage, self.damage_type, self.piercing)
        return damage

    def strike(self, user, target):
        return super().strike(user, target)

    def landBonus(self, fighter):
        fighter.fightStatus.weaponUsed(fighter)


# only for sword, rapier and lance; upgraded version of Thrust, with a damage multiplier of 1.2,
#       and ignore 70% of armor
#       cooldown: 2 turns
class HeavyThrust(Ability):
    def __init__(self):
        super().__init__("heavy thrust", "weapon based", "pattack", 10, "SP", "Enemy", "1", 2)
        self.piercing = 0.7

    def actionCast(self, user, target, critical=False):
        super().actionCast(user, target, critical)
        damage = user.weapon.baseattack + (user.tempStrength - 10) // 2
        for i in range(user.weapon.nbrdmgdice):
            damage += dice(user.weapon.damagedice)
        damage *= 1.2
        if critical:
            damage *= 1.5
        damage *= user.tempWeaponBonus
        damage = int(damage)
        damage = target.damageResist(damage, self.damage_type, self.piercing)
        return damage

    def landBonus(self, fighter):
        fighter.fightStatus.weaponUsed(fighter)


# only for maces, deals single strike damage, but damage enemy stamina
class Smash(Ability):
    def __init__(self):
        super().__init__("Smash", "weapon based", "pattack", 5, "SP", "Enemy", "1")

    def actionCast(self, user, target, critical=False):
        damage = user.weapon.baseattack + (user.tempStrength - 10) // 2
        for i in range(user.weapon.nbrdmgdice):
            damage += dice(user.weapon.damagedice)
        damage *= 1.4
        if critical:
            damage *= 1.5
        damage *= user.tempWeaponBonus
        damage = int(damage)
        damage = target.damageResist(damage, self.damage_type)
        return damage

    def strike(self, user, target):
        return super().strike(user, target)

    def landBonus(self, fighter):
        fighter.fightStatus.weaponUsed(fighter)

    def applyAction(self, target, hit_point):
        super().applyAction(target, hit_point)
        target.stamleft -= 5


class FocusSpirit(Ability):
    def __init__(self):
        super().__init__("focus spirit", "recovery ability", "mana", 0, "MP", "Personal", "1")

    def actionCast(self, user, target, critical=False):
        return target.fightclass.manaRecovery+3+dice(4)

    def applyAction(self, target, hit_point):
        target.manaleft += hit_point
        if target.manaleft > target.maxmana:
            target.manaleft = target.maxmana


class FocusBody(Ability):
    def __init__(self):
        super().__init__("focus body", "recovery ability", "stamina", 0, "MP", "Personal", "1")

    def actionCast(self, user, target, critical=False):
        return target.fightclass.staminaRecovery + 3 + dice(4)

    def applyAction(self, target, hit_point):
        target.stamleft += hit_point
        if target.stamleft > target.maxstam:
            target.stamleft = target.maxstam


# ability created only to wrap item use, contains a method to remove the item of the user's pocket after use
class ItemUse(Ability):
    def __init__(self, item, ability_type, targetGroup):
        super().__init__(str(item), ability_type, "pdamage", 0, "SP", targetGroup, "1")
        self.item = item 

    def actionCast(self, user, target, critical=False):
        self.consumeItem(user)

    def consumeItem(self, user):
        user.removeItem(self.item)

    def applyAction(self, target, hit_point):
        self.item.use(target, hit_point)

    @staticmethod
    def type():
        return "<item consumme>"


class RecoveryItemUse(ItemUse):
    def __init__(self, item):
        super().__init__(item, "Recovery item", "Party")
        self.recovery_type = item.stattype

    def actionCast(self, user, target, critical=False):
        if self.item.search() == "Recovery item":
            hitpoint = self.item.effectValue(target)
        else:
            raise TypeError
        user.removeItem(self.item)
        return hitpoint


class ReviveItemUse(ItemUse):
    def __init__(self, item):
        super().__init__(item, "Revive item", "Fallen")

    def actionCast(self, user, target, critical=False):
        if self.item.search() == "Revive item":
            hitpoint = self.item.effectValue(target)
        else:
            raise TypeError
        user.removeItem(self.item)
        return hitpoint


class DefendCast(Ability):
    def __init__(self):
        super().__init__("Defend", None, "Defend", 0, "SP", "Personal", "1")
        self.targetGroup = "Personal"

    def actionCast(self, user, target, critical=False):
        target.tempDefendingStatus = True

    def applyAction(self, target, hit_point):
        pass

    @staticmethod
    def type():
        return "<defend>"


# Escape cast: wrap the escape mechanic,
class EscapeCast(Ability):
    def __init__(self):
        super().__init__("Escape", None, "Escape", 0, "SP", "Personal", "1")
        self.isSuccessful = False
        self.enemy_leader = None

    def actionCast(self, user, target, critical=False):
        result = dice(12)+user.level-7
        if self.enemy_leader.enemytype == "elite":
            result -= 2
        if result > self.enemy_leader.level:
            self.isSuccessful = True
        return 0

    @staticmethod
    def type():
        return "<escape>"


class EnemyAction(Ability):
    def __init__(self, name, ability_type, damage_type, cost, cost_type, targetGroup, targetGroupSize, frequency):
        super().__init__(name, ability_type, damage_type, cost, cost_type, targetGroup, targetGroupSize)
        self.frequency = frequency

    def getFrequency(self):
        return self.frequency

    def applyAction(self, target, hit_point):
        target.takeDamage(hit_point)

    @staticmethod
    def type():
        return "<ability>"


class EnemyAttack(EnemyAction):
    def __init__(self):
        super().__init__("Attack", "Weapon based", "pattack", 3, "SP", "Enemy", "1", 20)

    def actionCast(self, user, target, critical=False):
        damage = 0
        for strike in range(self.strike(user, target)):
            temp_damage = user.weapon.baseattack
            for i in range(user.weapon.nbrdmgdice):
                temp_damage += dice(user.weapon.damagedice)
            temp_damage += (user.tempStrength - 10) // 2
            if critical:
                temp_damage *= 1.5
            if strike != 0:
                temp_damage *= 0.60
            temp_damage = int(temp_damage)
            #print("Damage before resistance: {}".format(temp_damage))
            temp_damage = target.damageResist(temp_damage, self.damage_type)
            #print("Strike damage: {}".format(temp_damage))
            damage += temp_damage
        return damage

    @staticmethod
    def getMovetype():
        return moveList[0]


# Power attack: like a basic attack, but with a single strike and multiplied by 3, with one resistance call
class EnemyPowerAttack(EnemyAction):
    def __init__(self):
        super().__init__("Power attack", "Weapon based", "pattack", 8, "SP", "Enemy", "1", 5)

    def actionCast(self, user, target, critical=False):
        damage = user.weapon.baseattack + (user.tempStrength - 10) // 2
        for i in range(user.weapon.nbrdmgdice):
            damage += dice(user.weapon.damagedice)
        damage *= 2
        if critical:
            damage *= 1.5
        damage = int(damage)
        #print("Damage before resistance: {}".format(damage))
        damage = target.damageResist(damage, self.damage_type)
        #print("Strike damage: {}".format(damage))
        return damage

    @staticmethod
    def getMovetype():
        return moveList[0]


class CustomEnemyAction(EnemyAction):
    def __init__(self, name, baseattack, damagedice, nbrdmgdice, frequency=3, cost=3, cost_type="SP", nbrtarget="1",
                 damage_type="pattack", target_group="Enemy", status=None):
        super().__init__(name, "None", damage_type, cost, cost_type, target_group, nbrtarget, frequency)
        self.base_attack = baseattack
        self.damage_dice = damagedice
        self.nbr_damage_dice = nbrdmgdice
        self.statusLinker = status

    def actionCast(self, user, target, critical=False):
        damage = 0
        #print("Enemy attack:")
        for strike in range(self.strike(user, target)):
            #print("Strike {}".format(strike+1))
            temp_damage = self.base_attack + (user.tempStrength - 10) // 2
            for die in range(self.nbr_damage_dice):
                temp_damage += dice(self.damage_dice)
            if critical:
                temp_damage *= 1.5
            if strike != 0:
                temp_damage *= 0.6
            temp_damage = int(temp_damage)
            #print("Damage before resistance: {}".format(temp_damage))
            #print("Target's armor: {}".format(target.tempPDefense))
            temp_damage = target.damageResist(temp_damage, self.damage_type)
            damage += temp_damage
        #print("Total damage: {}".format(damage))
        return damage

    @staticmethod
    def getMovetype():
        return moveList[0]


class MagicEnemyAction(EnemyAction):
    def __init__(self, name, baseattack, damagedice, nbrdmgdice, frequency=3, cost=3, nbrtarget="1",
                 damage_type="None", target_group="Enemy", status=None):
        super().__init__(name, damage_type, "None", cost, "MP", target_group, nbrtarget, frequency)
        self.base_attack = baseattack
        self.damage_dice = damagedice
        self.nbr_damage_dice = nbrdmgdice
        self.statusLinker = status

    def actionCast(self, user, target, critical=False):
        damage = self.base_attack + (user.tempIntel - 10) // 2
        for die in range(self.nbr_damage_dice):
            damage += dice(self.damage_dice)
        if critical:
            damage *= 1.5
        damage = int(damage)
        damage = target.damageResist(damage, self.damage_type)
        self.applyMagic()
        return damage

    @staticmethod
    def search():
        return "OffensiveSpell"

    def strike(self, user, target):
        numberStrike = 1 + user.tempAgility * user.offensiveSpeed // (target.tempAgility / target.defensiveSpeed)
        additionalStrike = (user.tempAgility * user.offensiveSpeed %
                            (target.tempAgility / target.defensiveSpeed) /
                            (target.tempAgility / target.defensiveSpeed)) * 100
        if additionalStrike > dice(100):
            numberStrike += 1
        numberStrike = int(numberStrike)
        #print("Number of strike: {}".format(numberStrike))
        return numberStrike

    def applyMagic(self):
        pass

    @staticmethod
    def type():
        return "<spell>"

    @staticmethod
    def getMovetype():
        return moveList[1]
