from core.Equipment_and_Other import dice
from core.Combat_Mechanics.Status_Effect import *


class Status_Linker:
    def __init__(self, status, duration_dice, probability):
        self.status = status
        self.probability = probability
        self.duration_dice = duration_dice

    def isEffective(self, user, target):
        result = dice(100)
        probability = self.probability+user.tempLuck
        if probability > 100:
            probability = 100
        print("Probability of success: {}%".format(probability))
        if result > 100 - self.probability - user.tempLuck:
            print("Successful throw")
            return True
        return False

    def getStatusEffect(self, target, bonus=0):
        duration = self.status.duration + dice(self.duration_dice)
        new_status = self.status.copy()
        new_status.duration = duration
        print("Bonus value: {}".format(bonus))
        new_status.effect_point = new_status.getStatusEffectValue(target, bonus)
        return new_status

    @staticmethod
    def addWeaponStatus(weapon, fighter):
        statusLinker = None
        print("Weapon type: {}".format(weapon.wpntype))
        bonus = fighter.luck
        if weapon.wpntype == "sword":
            status = BleedingEffect(weapon.baseattack)
            statusLinker = Status_Linker(status, 3, bonus)
        elif weapon.wpntype == "rapier":
            statusLinker = None
        elif weapon.wpntype == "axe":
            statusLinker = None
        elif weapon.wpntype == "mace":
            status = StunningEffect(2)
            statusLinker = Status_Linker(status, 0, bonus)
        elif weapon.wpntype == "spear":
            statusLinker = None
        elif weapon.wpntype == "bow":
            statusLinker = None
        return statusLinker

    @staticmethod
    def type():
        return "status linker"


class Shield_Linker(Status_Linker):
    def __init__(self, status, duration_dice, probability):
        super().__init__(status, duration_dice, probability)

    def isEffective(self, user, target):
        userBonus = user.tempStrength
        for equipment in user.equipment:
            if equipment.gearSet() == "Shield":
                userBonus += equipment.pdefense * 3 + 50
                break
        targetBonus = target.tempStrength + target.tempPDefense
        print("Chance of success: ({} - {}) / 100".format(userBonus, targetBonus))
        dicethrow = dice(100)
        print("Result: {}".format(dicethrow))
        isEffective = 100 - (userBonus - targetBonus) <= dicethrow
        return isEffective

    @staticmethod
    def type():
        return "shield linker"


class Charge_Linker(Status_Linker):
    def __init__(self, status, duration_dice, probability):
        super().__init__(status, duration_dice, probability)

    def isEffective(self, user, target):
        return True

    def getAction(self):
        return self.status.action

    def getTarget(self):
        return self.status.target

    @staticmethod
    def type():
        return "charge linker"


# special category of Status linker, where Status is linked to specific gear and is active in fight
#           as long as the fighter wear it
# Can hold passive type (buffs) and active type (healing, etc) status
class Equipment_Status(Status_Linker):
    def __init__(self, status, probability, duration_dice):
        pass
