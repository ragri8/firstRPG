from core.Combat_Mechanics.Ability import RecoveryItemUse, ReviveItemUse, DefendCast, EscapeCast


moveList = ["Attack", "Cast spell", "Use item", "Defend", "Flee"]

# probably going to erase the class entirely


class ActionFactory:
    def __init__(self):
        pass

    @staticmethod
    def createAction(action_type, reqObject=None):
        action = None
        if action_type == moveList[0]:
            pass
        elif action_type == moveList[1]:
            raise ValueError
        elif action_type == moveList[2]:
            if reqObject.type() != "<item>":
                raise TypeError
            else:
                action = ActionFactory.createItemAction(reqObject)
        elif action_type == moveList[3]:
            action = DefendCast()
        elif action_type == moveList[4]:
            action = EscapeCast()
        else:
            raise TypeError
        return action

    @staticmethod
    def createItemAction(item):
        if item.search() in ["Recovery item", "HP"]:
            print("recovery item")
            return RecoveryItemUse(item)
        elif item.search() == "Revive item":
            return ReviveItemUse(item)
        else:
            raise TypeError("Error, the item can't be consummed in battle")
