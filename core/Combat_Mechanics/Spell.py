from core.Combat_Mechanics.Ability import Ability
from core.Equipment_and_Other import dice


class SpellClass(Ability):
    def __init__(self, spellname, beffect=0, effectdice=0, nbrdice=0, targetGroupSize="1", manacost=1,
                 ability_type="spell", targetGroup="Enemy", status=None, description=""):
        super().__init__(spellname, ability_type, "mattack", manacost, "MP", targetGroup, targetGroupSize)
        self.beffect = beffect
        self.effectdice = effectdice
        self.nbrdice = nbrdice
        self.element = None
        self.description = description
        self.statusLinker = status

    def __str__(self):
        return self.name

    @staticmethod
    def search():
        return "Spell"

    @staticmethod
    def type():
        return "<spell>"

    def applyAction(self, target, hit_point):
        pass

    def actionCast(self, user, target, critical=False):
        pass

    def addWeaponStatus(self, weapon, fighter):
        pass

    def removeWeaponStatus(self):
        pass

    def isUsableInMenu(self):
        return False

    def getDescription(self):
        return self.description


class OffensiveSpell(SpellClass):
    def __init__(self, spellname, beffect=0, effectdice=0, nbrdice=0, targetGroupSize="1", manacost=3, element="fire",
                 status=None, description=""):
        super().__init__(spellname, beffect=beffect, effectdice=effectdice, nbrdice=nbrdice,
                         targetGroupSize=targetGroupSize, manacost=manacost, targetGroup="Enemy",
                         status=status, description=description)
        self.typeEffect = element
        self.element = element

    @staticmethod
    def description(descript):
        print("spell effect: \n", descript)

    @staticmethod
    def search():
        return "OffensiveSpell"

    def actionCast(self, user, target, critical=False):
        damage = 0
        if user.weapon.wpntype == "staff":
            damage += user.weapon.baseattack
        for strike in range(self.strike(user, target)):
            temp_damage = self.beffect + (user.tempIntel - 10) // 2
            for i in range(self.nbrdice):
                temp_damage += dice(self.effectdice)
            temp_damage *= 2 / (strike + 2)
            print("Temp damage: {}".format(temp_damage))
            damage += temp_damage
        if critical:
            damage *= 1.5
        print("Damage before spell bonus: {}".format(damage))
        damage *= user.fightStatus.spellBonus(self)
        damage = int(damage)
        print("Before resist: {}".format(damage))
        damage = target.damageResist(damage, self.element)
        print("After resist: {}".format(damage))
        return damage

    def strike(self, user, target):
        numberStrike = super().strike(user, target)
        return numberStrike

    def applyAction(self, target, hit_point):
        target.takeDamage(hit_point)

    def landBonus(self, fighter):
        fighter.fightStatus.spellUsed(self.element)

    def getDescription(self):
        descritpion = [super().getDescription()]
        if self.nbrdice > 0:
            descritpion.append("Damage output: {}-{}".format(self.beffect+self.nbrdice,
                                                             self.beffect+self.nbrdice*self.effectdice))
        else:
            descritpion.append("Damage output: {}".format(self.beffect))
        return descritpion


class HealingSpell(SpellClass):
    def __init__(self, spellname, beffect=0, effectdice=0, nbrdice=0,
                 targetGroupSize="1", manacost=3, typeEffect="healing", targetGroup="Party", description=""):
        super().__init__(spellname, beffect=beffect, effectdice=effectdice, nbrdice=nbrdice, targetGroup=targetGroup,
                         targetGroupSize=targetGroupSize, manacost=manacost, description=description)
        self.element = typeEffect
        self.typeEffect = typeEffect

    @staticmethod
    def search():
        return "HealingSpell"

    def actionCast(self, user, target, critical=False):
        if self.element == "healing":
            heal_point = self.beffect
            heal_point += int((user.intel - 10) // 2)
            if user.weapon.wpntype == "staff":
                heal_point += user.weapon.baseattack
            for i in range(self.nbrdice):
                heal_point += dice(self.effectdice)
            if critical:
                heal_point = int(1.5 * heal_point)
            heal_point *= user.fightStatus.spellBonus(self)
            heal_point = int(heal_point)
            if target.hpleft + heal_point > target.maxhp:
                heal_point = target.maxhp - target.hpleft
            for status in target.activeStatusEffects:
                if status.name == "bleeding" and status.duration > 0:
                    status.duration = 0
                    status.spell_cured = True
        elif self.element == "recovery":
            heal_point = 0
            for status in target.activeStatusEffects:
                if status.name in ["poison", "burning", "freezing", "paralysis"]:
                    status.duration = 0
                    status.spell_cured = True
        else:
            heal_point = 0
        return heal_point

    def applyAction(self, target, hit_point):
        target.heal(hit_point)

    def landBonus(self, fighter):
        fighter.fightStatus.spellUsed("healing")

    def isUsableInMenu(self):
        return True

    def getDescription(self):
        description = [super().getDescription()]
        if self.nbrdice > 0:
            description.append("Healing effect: {}-{}".format(self.beffect+self.nbrdice,
                                                              self.beffect+self.nbrdice*self.effectdice))
        else:
            "Healing effect: {}".format(self.beffect)
        return description


class ResurectionSpell(HealingSpell):
    def __init__(self, spellname, targetGroupSize="1", manacost=20, description=""):
        super().__init__(spellname, beffect=0, effectdice=0, nbrdice=0, targetGroupSize=targetGroupSize,
                         manacost=manacost, typeEffect="healing", targetGroup="fallen", description=description)

    def actionCast(self, user, target, critical=False):
        if critical:
            health_point = target.maxhp
        else:
            health_point = target.maxhp // 2
        return health_point

    def getDescription(self):
        return [super().getDescription()]


class RecoverySpell(SpellClass):
    def __init__(self, spellname, targetGroupSize="1", manacost=3,
                 typeEffect="status effect", status=None, description=""):
        super().__init__(spellname, beffect=0, effectdice=0,  nbrdice=0, targetGroup="Party",
                         targetGroupSize=targetGroupSize, manacost=manacost, status=status, description=description)
        self. element = "status ailment"
        self.typeEffect = typeEffect

    @staticmethod
    def search():
        return "RecoverySpell"

    def actionCast(self, user, target, critical=False):
        for status in target.activeStatusEffects:
            if status.impact == "negative" and status.duration > 0:
                status.duration = 0
                status.spell_cured = True
        return 0

    def getDescription(self):
        return [super().getDescription()]


class OldStatusSpell(SpellClass):  # unclear difference with BuffSpell (check target group)
    def __init__(self, spellname, beffect=4, effectdice=0, nbrdice=0, targetGroupSize="1", manacost=3,
                 typeEffect="strength buff", duration_dice=3,  probability=20, status=None, description=""):
        super().__init__(spellname, beffect=beffect, effectdice=effectdice, nbrdice=nbrdice, targetGroup="Party",
                         targetGroupSize=targetGroupSize, manacost=manacost, description=description)
        self. element = "status"
        self.typeEffect = typeEffect
        self.statusLinker = status

    @staticmethod
    def search():
        return "StatusSpell"

    def landBonus(self, fighter):
        fighter.fightStatus.spellUsed("buff")


class StatusSpell(SpellClass):  # unclear difference with BuffSpell (check target group)
    def __init__(self, spellname, targetGroup="Party", targetGroupSize="1",
                 manacost=3, typeEffect="strength buff", status=None, description=""):
        super().__init__(spellname, beffect=0, effectdice=0, nbrdice=0, targetGroup=targetGroup,
                         targetGroupSize=targetGroupSize, manacost=manacost, description=description)
        self. element = "status"
        self.typeEffect = typeEffect
        self.statusLinker = status

    @staticmethod
    def search():
        return "StatusSpell"

    def landBonus(self, fighter):
        fighter.fightStatus.spellUsed("buff")


# New status class todo: complete it
# create a damage aura around the target, deals magic damage to enemy getting close
class AuraSpell(StatusSpell):
    def __init__(self, spellname, targetGroup="Party", targetGroupSize="1",
                 manacost=3, element="fire", status=None, description=""):
        super().__init__(spellname, targetGroup=targetGroup, targetGroupSize=targetGroupSize,
                         manacost=manacost, status=status, description=description)
        self.element = element


class BuffSpell(SpellClass):  # unclear difference with StatusSpell (check target group)
    def __init__(self, spellname, beffect=4, effectdice=0, nbrdice=0, targetGroupSize="1",
                 manacost=3, typeeffect="strength buff", targetGroup="Party", description=""):
        super().__init__(spellname, beffect=beffect, effectdice=effectdice, nbrdice=nbrdice, targetGroup=targetGroup,
                         targetGroupSize=targetGroupSize, manacost=manacost, description=description)
        self. element = "status"
        self.typeeffect = typeeffect

    @staticmethod
    def search():
        return "BuffSpell"
