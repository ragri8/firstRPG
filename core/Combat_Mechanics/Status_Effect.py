from copy import deepcopy


# abstract class;
# Status_Effect object are use to handle fighter's affliction, buff and debuff during a fight
# Most status effects have a limited duration, decrementing on each turn
class Status_Effect:
    def __init__(self, name, effect_type, effect_point, duration, impact_type, text):
        self.name = name
        self.effect_point = effect_point
        self.effect_type = effect_type
        self.duration = duration
        self.impact = impact_type
        self.afflict_text = text

    def isAffected(self):
        if self.duration > 0:
            return True
        return False

    def apply(self, fighter):
        return 0

    def statusCast(self, target):
        pass

    def intelBonus(self, caster):
        pass

    def copy(self):
        return deepcopy(self)

    def __str__(self):
        return self.name


# Active type effects are affecting the fighter on each turn at a precise moment
# Active effects include stun, poison, healing, elemental effect, etc.
class Active_Effect(Status_Effect):
    def __init__(self, name, effect_type, effect_point, duration, impact_type, text):
        super().__init__(name, effect_type, effect_point, duration, impact_type, text)
        self.appliance = "pre turn"
        self.spell_cured = False

    @staticmethod
    def search():
        return "active"

    def applyStatus(self, target, hit_point):
        pass

    def getStatusEffectValue(self, target, bonus):
        return self.effect_point


# Passive type effects are affecting the fighter's stats on whoever turn
# passive effects include stat buffs and debuffs,
class Passive_Effect(Status_Effect):
    def __init__(self, name, effect_type, effect_point, duration, impact_type, text):
        super().__init__(name, effect_type, effect_point, duration, impact_type, text)

    @staticmethod
    def search():
        return "passive"

    def removeEffect(self, target):
        pass

    def statusCast(self, target):
        return self.effect_point

    def getStatusEffectValue(self, target, bonus):
        return self.effect_point


# a passive effect with positive impact on the character
class Buff_Effect(Passive_Effect):
    def __init__(self, name, effect_type, effect_point, duration, text):
        super().__init__(name, effect_type, effect_point, duration, "positive", text)

    def intelBonus(self, caster):
        return caster.tempIntel // 5


# a passive effect with negative impact on the character
class Debuff_Effect(Passive_Effect):
    def __init__(self, name, effect_type, effect_point, duration, text):
        super().__init__(name, effect_type, effect_point, duration, "negative", text)

    def intelBonus(self, caster):
        return caster.tempIntel // 5


# active effect, deals damage on a turn basis, until duration hit 0
class Damage_Effect(Active_Effect):
    def __init__(self, name, effect_point, duration, text):
        super().__init__(name, "hp damage", effect_point, duration, "negative", text)

    def statusCast(self, target):
        pass

    def applyStatus(self, target, hit_point):
        target.takeDamage(hit_point)


# damage effect, caused by blades and sharp attacks, can be healed by any healing spell
class BleedingEffect(Damage_Effect):
    def __init__(self, effect_point=5, duration=2):
        super().__init__("bleeding", effect_point, duration, "bleeding effect")

    def statusCast(self, target):
        return self.effect_point

    def apply(self, fighter):
        temp_hp = fighter.hpleft
        fighter.hpleft -= self.effect_point
        if fighter.hpleft < 0:
            fighter.hpleft = 0
        return temp_hp - fighter.hpleft


class StunningEffect(Active_Effect):
    def __init__(self, duration=2):
        super().__init__("stunning", "stun", 0, duration, "negative", "stunned")
        self.appliance = "pre turn"


class ChargingEffect(Active_Effect):
    def __init__(self, target, action, duration=2):
        super().__init__("charging", "stun", 0, 2, "positive", "charging")
        self.appliance = "pre turn"
        self.target = target
        self.action = action


class FreezeEffect(Debuff_Effect):
    def __init__(self, duration=4):
        super().__init__("freezing", "agility", 0, duration, "frozen")
        self.appliance = "pre turn"

    def getStatusEffectValue(self, target, bonus):
        if target.tempAgility < int(0.75 * target.agility):
            bonus = 0
        elif target.tempAgility - bonus < int(0.75 * target.agility):
            bonus = target.tempAgility - int(0.75 * target.agility)
        self.effect_point = bonus
        return bonus

    def applyStatus(self, target, bonus):
        target.tempAgility -= bonus

    def removeEffect(self, target):
        target.tempAgility = int(target.agility)


class BurningEffect(Damage_Effect):
    def __init__(self, effect_point=3, duration=2):
        super().__init__("burning", effect_point, duration, "burned")

    def statusCast(self, target):
        damage = self.effect_point * (1 - (target.tempFireResist / 100))
        return int(damage)

    def intelBonus(self, caster):
        return caster.tempIntel // 5


class PoisonEffect(Damage_Effect):
    def __init__(self, effect_point=6, duration=20):
        super().__init__("poison", effect_point, duration, "poisoned")

    def statusCast(self, target):
        damage = self.effect_point
        return int(damage)

    def intelBonus(self, caster):
        return caster.tempIntel // 4


class HealingEffect(Active_Effect):
    def __init__(self, effect_point=5, duration=3):
        super().__init__("healing", "heal", effect_point, duration, "positive", "blessed")
        self.appliance = "pre turn"

    def statusCast(self, target):
        heal_point = self.effect_point
        if self.effect_point < target.maxhp - target.hpleft:
            heal_point = target.maxhp - target.hpleft
        return heal_point

    def intelBonus(self, caster):
        return caster.tempIntel // 5


class AbsorbBuff(Buff_Effect):
    def __init__(self, effect_point=10, duration=1):
        super().__init__("absorb shield", "shield", effect_point, duration, "magic shield raised")

    def statusCast(self, target):
        bonus = self.effect_point
        return bonus

    def getStatusEffectValue(self, target, bonus):
        bonus += self.effect_point
        bonus -= target.tempAbsorb
        bonus = int(bonus)
        self.effect_point = bonus
        return bonus

    def applyStatus(self, target, bonus):
        target.tempAbsorb += bonus

    def removeEffect(self, target):
        target.tempAbsorb -= self.effect_point

    def absorb(self, damage):
        if damage > self.effect_point:
            damage -= self.effect_point
            self.effect_point = 0
        else:
            self.effect_point -= damage
            damage = 0
        if self.effect_point == 0:
            self.duration = 0
        return damage

    def intelBonus(self, caster):
        return caster.intel // 4


class AttackBuff(Buff_Effect):
    def __init__(self, effect_point=2, duration=5):
        super().__init__("strength boost", "strength", effect_point, duration, "strength raised")

    def statusCast(self, target):
        bonus = self.effect_point
        return bonus

    def getStatusEffectValue(self, target, bonus):
        bonus += self.effect_point
        if target.tempStrength + bonus > int(1.5 * target.strength):
            bonus = int(1.5 * target.strength) - target.tempStrength
        self.effect_point = bonus
        return bonus

    def applyStatus(self, target, bonus):
        target.tempStrength += bonus

    def removeEffect(self, target):
        target.tempStrength = int(target.strength)

    def intelBonus(self, caster):
        return caster.tempIntel // 5


class DefenseBuff(Buff_Effect):
    def __init__(self, effect_point=4, duration=5):
        super().__init__("defense boost", "pdefense", effect_point, duration, "defense raised")

    def statusCast(self, target):
        bonus = self.effect_point
        return bonus

    def getStatusEffectValue(self, target, bonus):
        bonus += self.effect_point
        if target.tempPDefense + bonus > int(1.5 * target.pdefense):
            bonus = int(1.5 * target.pdefense) - target.tempPDefense
        self.effect_point = bonus
        return bonus

    def applyStatus(self, target, bonus):
        target.tempPDefense += bonus

    def removeEffect(self, target):
        target.tempPDefense = int(target.pdefense)

    def intelBonus(self, caster):
        return caster.tempIntel // 5


class ResistanceBuff(Buff_Effect):
    def __init__(self, effect_point=4, duration=5):
        super().__init__("resistance boost", "mdefense", effect_point, duration, "magic resistance raised")

    def statusCast(self, target):
        bonus = self.effect_point
        return bonus

    def getStatusEffectValue(self, target, bonus):
        bonus += self.effect_point
        if target.tempMDefense + bonus > int(1.5 * target.mdefense):
            bonus = int(1.5 * target.mdefense) - target.tempMDefense
        self.effect_point = bonus
        return bonus

    def applyStatus(self, target, bonus):
        target.tempMDefense += bonus

    def removeEffect(self, target):
        target.tempMDefense = int(target.mdefense)

    def intelBonus(self, caster):
        return caster.tempIntel // 5


class AgilityBuff(Buff_Effect):
    def __init__(self, effect_point=4, duration=5):
        super().__init__("agility boost", "agility", effect_point, duration, "agility raised")

    def statusCast(self, target):
        bonus = self.effect_point
        #if target.tempAgility + bonus > int(1.5 * target.agility):
        #    bonus = int(1.5 * target.agility) - target.tempAgility
        #self.effect_point = bonus
        return bonus

    def getStatusEffectValue(self, target, bonus):
        bonus += self.effect_point
        if target.tempAgility + bonus > int(1.5 * target.agility):
            bonus = int(1.5 * target.agility) - target.tempAgility
        self.effect_point = bonus
        return bonus

    def applyStatus(self, target, bonus):
        target.tempAgility += bonus

    def removeEffect(self, target):
        target.tempAgility = int(target.agility)


class CriticalBuff(Buff_Effect):
    def __init__(self, effect_point=40, duration=2):
        super().__init__("critical boost", "critical", effect_point, duration, "critical chances raised")

    def statusCast(self, target):
        bonus = self.effect_point
        #if target.tempCritical > int(target.tempLuck):
        #    bonus = int(target.tempLuck - target.tempCritical) + bonus
        #self.effect_point = bonus
        return bonus

    def getStatusEffectValue(self, target, bonus):
        bonus += self.effect_point
        if target.tempCritical > int(target.tempLuck):
            bonus = int(target.tempLuck - target.tempCritical) + bonus
        self.effect_point = bonus
        return bonus

    def applyStatus(self, target, bonus):
        target.tempCritical += bonus

    def removeEffect(self, target):
        target.tempCritical = int(target.tempLuck)


class AttackDebuff(Debuff_Effect):
    def __init__(self, effect_point=4, duration=5):
        super().__init__("strength debuff", "strength", effect_point, duration, "strength lowered")

    def statusCast(self, target):
        bonus = self.effect_point
        #if target.tempStrength - bonus < int(0.6 * target.strength):
        #    bonus = target.tempStrength - int(0.6 * target.strength)
        #self.effect_point = bonus
        return bonus

    def getStatusEffectValue(self, target, bonus):
        bonus += self.effect_point
        if target.tempStrength - bonus < int(0.6 * target.strength):
            bonus = target.tempStrength - int(0.6 * target.strength)
        self.effect_point = bonus
        return bonus

    def applyStatus(self, target, bonus):
        target.tempStrength -= bonus

    def removeEffect(self, target):
        target.tempStrength = int(target.strength)


class DefenseDebuff(Debuff_Effect):
    def __init__(self, effect_point=4, duration=5):
        super().__init__("defense debuff", "pdefense", effect_point, duration, "defense lowered")

    def statusCast(self, target):
        bonus = self.effect_point
        #if target.tempPDefense - bonus < int(0.6 * target.pdefense):
        #    bonus = target.tempPDefense - int(0.6 * target.pdefense)
        #self.effect_point = bonus
        return bonus

    def getStatusEffectValue(self, target, bonus):
        bonus += self.effect_point
        if target.tempPDefense - bonus < int(0.6 * target.pdefense):
            bonus = target.tempPDefense - int(0.6 * target.pdefense)
        self.effect_point = bonus
        return bonus

    def applyStatus(self, target, bonus):
        target.tempPDefense -= bonus

    def removeEffect(self, target):
        target.tempPDefense = int(target.pdefense)


class ResistanceDebuff(Debuff_Effect):
    def __init__(self, effect_point=4, duration=5):
        super().__init__("resistance debuff", "mdefense", effect_point, duration, "magic resistance lowered")

    def statusCast(self, target):
        bonus = self.effect_point
        #if target.tempMDefense - bonus < int(0.6 * target.mdefense):
        #    bonus = target.tempMDefense - int(0.6 * target.mdefense)
        #self.effect_point = bonus
        return bonus

    def getStatusEffectValue(self, target, bonus):
        bonus += self.effect_point
        if target.tempMDefense - bonus < int(0.6 * target.mdefense):
            bonus = target.tempMDefense - int(0.6 * target.mdefense)
        self.effect_point = bonus
        return bonus

    def applyStatus(self, target, bonus):
        target.tempMDefense -= bonus

    def removeEffect(self, target):
        target.tempMDefense = int(target.mdefense)


class AgilityDebuff(Debuff_Effect):
    def __init__(self, effect_point=4, duration=5):
        super().__init__("agility debuff", "agility", effect_point, duration, "agility lowered")

    def statusCast(self, target):
        bonus = self.effect_point
        #if target.tempAgility - bonus < int(0.6 * target.agility):
        #    bonus = target.tempAgility - int(0.6 * target.agility)
        #self.effect_point = bonus
        return bonus

    def getStatusEffectValue(self, target, bonus):
        bonus += self.effect_point
        if target.tempAgility - bonus < int(0.6 * target.agility):
            bonus = target.tempAgility - int(0.6 * target.agility)
        self.effect_point = bonus
        return bonus

    def applyStatus(self, target, bonus):
        target.tempAgility -= bonus

    def removeEffect(self, target):
        target.tempAgility = int(target.agility)
