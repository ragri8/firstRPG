from core.Equipment_and_Other import dice
from core.Combat_Mechanics.Ability import Ability, DefendCast


def chooseAction(enemy):
    if enemy.enemytype == "Human":
        action = humanoidAction(enemy)
    elif enemy.enemytype in ["Wolf", "Bear", "Scarecrow", "Bat", "Rat", "Boar"]:
        action = monsterAction(enemy)
    else:
        action = monsterAction(enemy)
    if not isLanding(action, enemy):
        action = DefendCast()
    return action


def isLanding(action, fighter):
    is_landing = True
    if action.cost_type in ["SP", "sp"]:
        if action.cost > fighter.stamleft:
            fighter.stamleft += fighter.maxstam * 0.5 + dice(int(fighter.maxstam * 0.5 - fighter.stamleft))
            is_landing = False
    elif action.cost_type in ["MP", "mp"]:
        if action.cost > fighter.manaleft:
            fighter.manaleft += fighter.maxmana * 0.5 + dice(int(fighter.maxmana * 0.5 - fighter.manaleft))
            is_landing = False
    return is_landing


def chooseTarget(enemy, target_list, group_size):
    if enemy.enemytype == "Human":
        targets = humanoidTarget(target_list, group_size)
    elif enemy.enemytype in ["Wolf", "Bear", "Scarecrow", "Bat", "Rat", "Boar"]:
        targets = monsterTarget(target_list, group_size)
    else:
        targets = monsterTarget(target_list, group_size)
    return targets


def monsterAction(enemy):
    total_frequency = 0
    for move in enemy.moveset:
        print("{} frequency: {}".format(str(move), move.getFrequency()))
        total_frequency += move.getFrequency()
    print("Total chance: {}".format(total_frequency))
    prob_choice = dice(total_frequency)
    print("Result: {}".format(prob_choice))
    action = None
    for move in enemy.moveset:
        if prob_choice <= move.getFrequency():
            action = move
            print("{} is choosen".format(str(move)))
            break
        else:
            prob_choice -= move.getFrequency()
            print("{} is not choosen".format(str(move)))
    if action is None:
        movestring = ", ".join(enemy.moveset)
        error = ("No move has been selected\n"
                 "Current variables used:\n"
                 "Enemy: {}, type of object: {}\n"
                 "Move list: {}".format(enemy, type(enemy), movestring))
        raise ValueError(error)
    return action


def monsterTarget(target_list, group_size):
    targets = []
    if len(target_list) > 0:
        if group_size == "1":
            targets.append(target_list[dice(len(target_list)) - 1])
        else:
            targets = target_list
    else:
        error = "The targeting list is empty"
        raise ValueError(error)
    return targets


def humanoidAction(enemy):
    total_frequency = 0
    for move in enemy.moveset:
        total_frequency += move.getFrequency()
    prob_choice = dice(total_frequency)
    action = None
    for move in enemy.moveset:
        if prob_choice <= move.getFrequency():
            action = move
        else:
            prob_choice -= move.getFrequency()
    if action is None:
        movestring = ", ".join(enemy.moveset)
        error = ("No move has been selected\n"
                 "Current variables used:\n"
                 "Enemy: {}, type of object: {}\n"
                 "Move list: {}".format(enemy, type(enemy), movestring))
        raise ValueError(error)
    return action


def humanoidTarget(target_list, group_size):
    targets = []
    if len(target_list) > 0:
        if group_size == "1":
            targets.append(target_list[dice(len(target_list)) - 1])
        else:
            targets = target_list
    return targets
