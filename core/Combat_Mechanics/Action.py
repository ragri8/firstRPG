from core.Combat_Mechanics.AI import *
from core.Global import moveList
from core.Combat_Mechanics.Ability import DefendCast


# todo: rename Moveset to Action and Action to Ability
class Action:
    def __init__(self, fighter):
        self.fighter = fighter
        self.damages = []
        self.moveType = ""
        self.ability = None
        self.targets = []
        self.textSize = 0
        self.critical = False
        self.extra = []

    def setMove(self, select):
        self.moveType = select

    def setAction(self, action):
        if action.type() in ["<spell>", "<item consumme>", "<ability>", "<defend>", "<escape>"]:
            self.ability = action
        else:
            print("Error, {} is not a valid Action type".format(action.type()))

    def setTargets(self, targetList):
        for target in targetList:
            self.targets.append(target)

    def actionCast(self):
        if self.hasEnoughPoint():
            self.ability.landBonus(self.fighter)
            self.critical = self.ability.isCriticalLanded(self.fighter)
            for target in self.targets:
                self.damages.append(self.ability.actionCast(self.fighter, target, self.critical))
                if self.ability.statusLinker is not None:
                    if self.ability.statusLinker.isEffective(self.fighter, target):
                        bonus = 0
                        if self.ability.ability_type == "spell":
                            bonus = self.ability.statusLinker.status.intelBonus(self.fighter)
                            #print("Base spell effect_point: {}".format(self.ability.statusLinker.status.effect_point))
                        self.extra.append(self.ability.statusLinker.getStatusEffect(target, bonus))
                    else:
                        self.extra.append(None)
                else:
                    self.extra.append(None)
        else:
            for target in self.targets:
                self.damages.append(0)
                self.extra.append(None)
                #print("{} has failed its attack".format(self.fighter))

    def applyAction(self):
        for index in range(len(self.targets)):
            self.ability.applyAction(self.targets[index], self.damages[index])
            if self.extra[index] is not None:
                self.addStatus(self.targets[index], self.extra[index])

    @staticmethod
    def addStatus(target, status):
        isPresent = False
        if status.search() == "active":
            for oldStatus in target.activeStatusEffects:
                if status.name == oldStatus.name:
                    isPresent = True
                    oldStatus.duration = max(status.duration, oldStatus.duration)
            if not isPresent:
                target.activeStatusEffects.append(status)
        elif status.search() == "passive":
            for oldStatus in target.passiveStatusEffects:
                if status.name == oldStatus.name:
                    isPresent = True
                    oldStatus.duration = max(status.duration, oldStatus.duration)  # todo: cumulative on buff and debuff
                    bonus = status.statusCast(target)
                    status.applyStatus(target, bonus)
            if not isPresent:
                target.passiveStatusEffects.append(status)
                bonus = status.statusCast(target)
                status.applyStatus(target, bonus)  # todo: be less disgusting

    def hasEnoughPoint(self):
        isEnough = True
        if self.ability.cost_type == "SP":
            if self.fighter.stamleft < self.ability.cost:
                isEnough = False
            else:
                self.fighter.stamleft -= self.ability.cost
        elif self.ability.cost_type == "MP":
            if self.fighter.manaleft < self.ability.cost:
                isEnough = False
            else:
                self.fighter.manaleft -= self.ability.cost
        elif self.ability.cost_type == "HP":
            if self.fighter.hpleft < self.ability.cost:
                isEnough = False
            else:
                self.fighter.hpleft -= self.ability.cost
        return isEnough


class EnemyAction(Action):
    def __init__(self, fighter):
        super().__init__(fighter)

    def setAttack(self):
        # chooseAction(self.fighter, self)
        self.setAction(chooseAction(self.fighter))
        if self.ability.type() == "<ability>":
            self.setMove(moveList[0])
        elif self.ability.type() == "<spell>":
            self.setMove(moveList[1])
        elif self.ability.type() == "<item consumme>":
            self.setMove(moveList[2])
        elif self.ability.type() == "<defend>":
            self.setMove(moveList[3])
        else:
            raise TypeError("No valid ability is selected")


class StatusAction(Action):
    def __init__(self, fighter):
        self.fighter = fighter
        self.effect_points = []
        self.triggered = []
        self.isParalyzed = False
        self.isDialog = False

    def actionCast(self):
        for status in self.fighter.activeStatusEffects:
            if status.duration > 0:
                self.isDialog = True
                if status.effect_type == "stun":
                    self.effect_points.append(0)
                    if status.isAffected():
                        self.isParalyzed = True
                elif status.effect_type == "hp damage":
                    self.effect_points.append(status.statusCast(self.fighter))
            elif status.duration == 0:
                self.effect_points.append(0)
                self.isDialog = True

    def applyAction(self):
        for index in range(len(self.fighter.activeStatusEffects)):
            self.fighter.activeStatusEffects[index].applyStatus(self.fighter, self.effect_points[index])

    def newTurn(self):
        for status in self.fighter.activeStatusEffects.__reversed__():
            status.duration -= 1
            if status.duration == -1:
                self.fighter.activeStatusEffects.remove(status)
        for status in self.fighter.passiveStatusEffects.__reversed__():
            status.duration -= 1
            if status.duration == -1:
                status.removeEffect(self.fighter)
                self.fighter.passiveStatusEffects.remove(status)


class ChargeAction(Action):
    def __init__(self, fighter):
        super().__init__(fighter)
        self.getAction()

    def getAction(self):
        for status in self.fighter.passiveStatusEffects:
            if status.name == "charging":
                if status.target.hpleft > 0:
                    self.ability = status.action
                    self.targets.append(status.target)
                    self.setMove(moveList[0])
                else:
                    self.ability = DefendCast()
                    self.targets.append(self.fighter)
                    self.setMove(moveList[3])
                self.fighter.passiveStatusEffects.remove(status)
                break
