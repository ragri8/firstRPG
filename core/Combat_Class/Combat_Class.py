from objects.Skill_Sheet import *

levelchart = [40, 120, 270, 540, 1000, 1800, 3000, 7800, 12200, 18600, 28000, 41000, 60000, 85000, 120000,
                           170000, 230000, 320000, 430000, 590000, 790000, 1050000, 1380000, 1820000, 2400000, 3010000,
                           4000000, 5150000]

# New possible xp calculation per level
#
#  1,   2,   3,   4,    5,    6,    7,    8,     9,    10,    11,    12,    13,    14,     15 previous level
#  1,   3,   6,  10,   15,   21,   28,   36,    45,    55,    66,    78,    91,   105,    120
#  1,   4,  10,  20,   35,   56,   84,  120,   165,   220,   286,   364,   455,   560,    680
# 40, 160, 400, 800, 1400, 2240, 3360, 4800,  6600,  8800, 11440, 14560, 18200, 22400,  27200 result per level
# -------------------------------------------------------------------------------------------
# 40, 120, 270, 540, 1000, 1800, 3000, 7800, 12200, 18600, 28000, 41000, 60000, 85000, 120000 actual method


#     16,     17,     18,     19,     20,     21,      22,      23,      24    previous level
#    136,    153,    171,    190,    210,    231,     253,     276,     300
#    816,    969,   1140,   1330,   1540,   1771,    2024,    2300,    2600
#  32640,  38760,  45600,  53200,  61600,  70840,   80960,   92000,  106000  result per level
# -------------------------------------------------------------------------------------------
# 170000, 230000, 320000, 430000, 590000, 790000, 1050000, 1380000, 1820000, 2400000 actual method

class CombatClass:
    def __init__(self, name="CombatClass", const=0, strength=0, intel=0, agility=0, defense=0, luck=0, pack1=0.0,
                 pack2=0.0, pack3=0., pack4=0., pack5=0., pack6=0., hrecovery=0, mrecovery=0, srecovery=0):
        self.name = name
        self.bconst = const
        self.bstrength = strength
        self.bintel = intel
        self.bagility = agility
        self.bdefense = defense
        self.bluck = luck
        self.lvlpack = (pack1, pack2, pack3, pack4, pack5, pack6)
        self.healthRecovery = hrecovery
        self.manaRecovery = mrecovery
        self.staminaRecovery = srecovery
        self.skill_tree = skill_tree.copy()

    # todo: recalculate statuslinker in each ability to improve probabilities of succcess with luck
    def levelingup(self, player, const=1, strength=1, intel=1, agility=1, defense=1, luck=0.25):
        dialoglist = []
        oldstat = player.const
        player.const += const
        if (oldstat // 1) < (player.const // 1):
            dialog = "  Gained +{} constitution!".format(int(player.const // 1 - oldstat // 1))
            dialoglist.append(dialog)
        oldstat = player.strength
        player.strength += strength
        if (oldstat // 1) < (player.strength // 1):
            dialog = "  Gained +{} strength!".format(int(player.strength // 1 - oldstat // 1))
            dialoglist.append(dialog)
        oldstat = player.intel
        player.intel += intel
        if (oldstat // 1) < (player.intel // 1):
            dialog = "  Gained +{} intelligence!".format(int(player.intel // 1 - oldstat // 1))
            dialoglist.append(dialog)
        oldstat = player.agility
        player.agility += agility
        if (oldstat // 1) < (player.agility // 1):
            dialog = "  Gained +{} agility!".format(int(player.agility // 1 - oldstat // 1))
            dialoglist.append(dialog)
        oldstat = player.defense
        player.defense += defense
        if (oldstat // 1) < (player.defense // 1):
            dialog = "  Gained +{} defense!".format(int(player.defense // 1 - oldstat // 1))
            dialoglist.append(dialog)
        oldstat = player.luck
        player.luck += luck
        if (oldstat // 1) < (player.luck // 1):
            dialog = "  Gained +{} luck!".format(int(player.luck // 1 - oldstat // 1))
            dialoglist.append(dialog)
        oldstat = player.maxhp
        player.maxhp = int(player.const * 2)
        if (oldstat // 1) < (player.maxhp // 1):
            dialog = "  Gained +{} health points!".format(player.maxhp // 1 - oldstat // 1)
            dialoglist.append(dialog)
        player.hpleft = player.maxhp
        oldstat = player.maxmana
        player.maxmana = int(player.intel * 2)
        if (oldstat // 1) < (player.maxmana // 1):
            dialog = "  Gained +{} mana points!".format(player.maxmana - oldstat)
            dialoglist.append(dialog)
        player.manaleft = player.maxmana
        oldstat = player.maxstam
        player.maxstam = int(10 + 5 * (player.const / 15) * (player.strength / 10))
        if (oldstat // 1) < (player.maxstam // 1):
            dialog = "  Gained +{} stamina points!".format(player.maxstam // 1 - oldstat // 1)
            dialoglist.append(dialog)
        player.stamleft = player.maxstam
        player.level += 1
        self.levelupskills(player)
        self.levelupspells(player)
        return dialoglist

    def turnRecovery(self, p_player, p_amount=1, p_amountMana=1):
        p_player.hpleft += self.healthRecovery * p_amount
        if p_player.hpleft > p_player.maxhp:
            p_player.hpleft = p_player.maxhp
        p_player.stamleft += self.staminaRecovery * p_amount
        if p_player.stamleft > p_player.maxstam:
            p_player.stamleft = p_player.maxstam
        p_player.manaleft += self.manaRecovery * p_amountMana
        if p_player.manaleft > p_player.maxmana:
            p_player.manaleft = p_player.maxmana

    def levelupskills(self, p_player):
        pass

    def levelupspells(self, player):
        pass

    def classbonus(self, player, loaded=False):
        player.const += self.bconst
        player.strength += self.bstrength
        player.intel += self.bintel
        player.agility += self.bagility
        player.defense += self.bdefense
        player.luck += self.bluck
        player.maxhp = int(player.const * 2)
        player.hpleft = player.maxhp
        player.maxmana = int(player.intel * 2)
        player.manaleft = player.maxmana
        player.maxstam = int(10 + 5 * (player.const / 15) * (player.strength / 10))
        player.stamleft = player.maxstam
        if not loaded:
            self.levelupskills(player)
            self.levelupspells(player)

    def __str__(self):
        return self.name
