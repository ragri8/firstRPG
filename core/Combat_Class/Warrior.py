from core.Combat_Class.Combat_Class import *
from core.Combat_Mechanics.Ability import *
from objects.Skill_Sheet import Skill_Tree


class Warrior(CombatClass):  # 5, 6, 2, 5, 3, 0, 3.45, 2.4, 1.2, 2.1, 1.7, 0.2, 0, 0, 3
    def __init__(self):
        super().__init__("Warrior", 5, 6, 2, 5, 3, 0, 3.45, 2.4, 1.2, 2.1, 1.7, 0.2, 0, 0, 3)

    def levelingup(self, player, const=1, strength=1, intel=1, agility=1, defense=1, luck=0.25):
        player.skillPoint += 4
        player.spellPoint += 1
        return super().levelingup(player, *self.lvlpack)

    def levelupskills(self, p_player):
        dialogList = []
        if True:
            return dialogList
        level = p_player.level
        lvlList = [3, 5]
        skillList = [PowerAttack(), ChargedAttack()]
        if level in lvlList:
            indexSkill = lvlList.index(level)
            nbrSkill = lvlList.count(level)
            for i in range(nbrSkill):
                skill = skillList[indexSkill + i]
                p_player.addAbilitySkill(skill)
                dialog = "Learned a new skill: {}!".format(str(skill))
                print(dialog)
                dialogList.append(dialog)
        return dialogList

    def levelupspells(self, player):
        dialogList = []
        lvl = player.level
        lvllist = []
        spelllist = []
        if lvl in lvllist:
            indexspell = lvllist.index(lvl)
            nbrspell = lvllist.count(lvl)
            for i in range(nbrspell):
                spell = spelllist[indexspell + i]
                player.setspell(spell)
                dialog = "Learned a new spell: {}!".format(spell)
                print(dialog)
                dialogList.append(dialog)
        return dialogList
