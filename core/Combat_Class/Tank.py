from core.Combat_Class.Combat_Class import *
from core.Combat_Mechanics.Ability import *
from objects.Spell_Sheet import spellList, magic_shield


class Tank(CombatClass):  # "Tank", 9, 4, 1, 2, 4, 0, 4.85, 1.6, 1.1, 1.5, 2.1, 0.2, 0, 0, 2
    def __init__(self):
        super().__init__("Tank", 9, 4, 1, 2, 4, 0, 4.85, 1.6, 1.1, 1.5, 2.1, 0.2, 0, 0, 2)

    def levelingup(self, player, const=1, strength=1, intel=1, agility=1, defense=1, luck=0.25):
        return super().levelingup(player, *self.lvlpack)

    def levelupskills(self, p_player):
        dialogList = []
        if True:
            return dialogList
        level = p_player.level
        lvlList = []#3, 5]
        skillList = [ShieldBash(), PowerAttack()]
        if level in lvlList:
            indexSkill = lvlList.index(level)
            nbrSkill = lvlList.count(level)
            for i in range(nbrSkill):
                skill = skillList[indexSkill + i]
                p_player.addAbilitySkill(skill)
                dialog = "Learned a new skill: {}!".format(str(skill))
                print(dialog)
                dialogList.append(dialog)
        return dialogList

    def levelupspells(self, player):
        dialogList = []
        lvl = player.level
        lvllist = []#3]
        spelllist = [] #  magic_shield]
        if lvl in lvllist:
            indexspell = lvllist.index(lvl)
            nbrspell = lvllist.count(lvl)
            for i in range(nbrspell):
                spell = spelllist[indexspell + i]
                player.setspell(spell)
                dialog = "Learned a new spell: {}!".format(spell)
                print(dialog)
                dialogList.append(dialog)
        return dialogList
