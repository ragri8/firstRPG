from core.Combat_Class.Combat_Class import *
from objects.Spell_Sheet import spellList


class Spellcaster(CombatClass):  # "Spellcaster", 3, 2, 7, 4, 3, 0, 2.85, 1.2, 2.7, 1.7, 1.5, 0.225, 0, 1, 1
    def __init__(self):
        super().__init__("Spellcaster", 3, 2, 7, 4, 3, 0, 2.85, 1.2, 2.7, 1.7, 1.5, 0.225, 0, 1, 1)

    def levelingup(self, player, const=1, strength=1, intel=1, agility=1, defense=1, luck=0.25):
        player.skillPoint += 2
        player.spellPoint += 3
        return super().levelingup(player, *self.lvlpack)

    def levelupspells(self, player):
        dialogList = []
        lvl = player.level
        lvllist = [3, 4, 4, 4, 4, 4, 6, 7, 8, 8, 9, 10, 12, 12, 13, 13, 14, 15, 15, 16, 17, 18, 18, 19, 19]
        #lvllist = [1, 2, 3, 4, 4, 5, 5, 6, 6, 7, 8, 8, 9, 10, 12, 12, 13, 13, 14, 15, 15, 16, 17, 18, 18, 19, 19] # todo remettre "fierce heart au lvl 4

        #spelllist = ["fireball", "first aid", "recovery", "frost", "quake", "fierce heart", "spark", "critical slash",
        #             "heat wave", "healing aura", "north wind", "plasma", "guard", "close wound", "rock spike",
        #             "thunderstorm", "high impact", "eruption", "earthquake", "protection", "ice cannon",
        #             "fairy fountain", "inferno", "ground dasher", "glacier", "massive heal", "plasma cannon"]
        #spelllist = [OffensiveSpell(*ospell1), HealingSpell(*hspell1), StatusSpell(*sspell1), OffensiveSpell(*ospell2),
        #             OffensiveSpell(*ospell3), StatusSpell(*sspell2), OffensiveSpell(*ospell4), StatusSpell(*sspell8), OffensiveSpell(*ospell5),
        #             HealingSpell(*hspell2), OffensiveSpell(*ospell6), OffensiveSpell(*ospell7), StatusSpell(*sspell3),
        #             HealingSpell(*hspell3), OffensiveSpell(*ospell8), OffensiveSpell(*ospell9), StatusSpell(*sspell5),
        #             OffensiveSpell(*ospell10), OffensiveSpell(*ospell11), StatusSpell(*sspell6), OffensiveSpell(*ospell12),
        #             HealingSpell(*hspell4), OffensiveSpell(*ospell13), OffensiveSpell(*ospell14),
        #             OffensiveSpell(*ospell15), HealingSpell(*hspell5), OffensiveSpell(*ospell16)]
        #if lvl in lvllist:
        #    indexspell = lvllist.index(lvl)
        #    nbrspell = lvllist.count(lvl)
        #    for i in range(nbrspell):
        #        # spellName = spelllist[indexspell + i]
        #        # spell = ActionFactory.createSpell(spellName)
        #        spell = spellList[indexspell + i]
        #        player.setspell(spell)
        #        dialog = "Learned a new spell: {}!".format(spell)
        #        print(dialog)
        #        dialogList.append(dialog)
        return dialogList

# Spell gain on levelup:
# fireball: level 1 OS
# first aid: level 2 HS
# recovery: level 3 OS
# splash: level 4
# quake: level 4
# fierce heart: level 5
# spark: level 5
# critical slash: level 6
# heat wave: level 6
# healing aura: level 7
# north wind: level 8
# plasma: level 8
# guard: level 9
# close wound: level 10
# rock spike: level 12
# thunderstorm: level 12
# high impact: level 13
# eruption: level 13
# earthquake: level 14
# protection: level 15
# water cannon: level 15
# fairy fountain: level 16
# inferno: level 17
# ground dasher: level 18
# deluge: level 18
# massive heal: level 19
# plasma cannon: level 19
