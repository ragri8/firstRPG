from core.Combat_Class.Combat_Class import *
from core.Combat_Mechanics.Ability import *


class Ranger(CombatClass):  # ("Ranger", 4, 3, 3, 7, 2, 1, 2.7, 1.5, 1.4, 2.4, 1.4, 0.25, 0, 1, 2)
    def __init__(self):
        super().__init__("Ranger", 4, 3, 3, 7, 2, 1, 2.7, 1.5, 1.4, 2.4, 1.4, 0.25, 0, 1, 2)

    def levelingup(self, player, const=1, strength=1, intel=1, agility=1, defense=1, luck=0.25):
        return super().levelingup(player, *self.lvlpack)

    def levelupskills(self, p_player):
        dialogList = []
        level = p_player.level
        lvlList = [7]
        skillList = [PowerAttack()]
        if level in lvlList:
            indexSkill = lvlList.index(level)
            nbrSkill = lvlList.count(level)
            for i in range(nbrSkill):
                skill = skillList[indexSkill + i]
                p_player.addAttackAbility(skill)
                dialog = "Learned a new skill: {}!".format(str(skill))
                print(dialog)
                dialogList.append(dialog)
        return dialogList

    def levelupspells(self, player):
        dialogList = []
        lvl = player.level
        lvllist = []
        spelllist = []
        if lvl in lvllist:
            indexspell = lvllist.index(lvl)
            nbrspell = lvllist.count(lvl)
            for i in range(nbrspell):
                spell = spelllist[indexspell + i]
                player.setspell(spell)
                dialog = "Learned a new spell: {}!".format(spell)
                print(dialog)
                dialogList.append(dialog)
        return dialogList
