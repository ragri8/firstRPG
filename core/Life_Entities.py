from math import tanh

from core.Combat_Mechanics.Ability import *
from core.Equipment_and_Other import HeadGear, ChestGear, ArmGear, Shield
from core.Item import RecoveryItem, ReviveItem, QuestItem
from core.Weapon import Weapon
from objects.Monsters_Sheet import forest


# Base class for all fighting living entities
# Never used directly, methods only called by children class
class LifeEntity:
    # Initiate stats of a character and temp stats which can be modified in battle
    def __init__(self, level=1, const=10, strength=0, intel=0, agility=10, defense=10, fireresist=0,
                 iceresist=0, earthresist=0, electricresist=0, poisonresist=0, luck=0, gold=0, gender="None"):
        self.level = level
        self.const = const
        self.maxhp = int(self.const * 2)
        self.hpleft = self.maxhp
        self.strength = strength
        self.maxstam = int(10 + 5 * (self.const / 15) * (self.strength / 10))
        self.stamleft = self.maxstam
        self.intel = intel
        self.maxmana = int(self.intel * 2)
        self.manaleft = self.maxmana
        self.agility = agility
        self.defense = defense
        self.fireresist = fireresist
        self.iceresist = iceresist
        self.earthresist = earthresist
        self.electricresist = electricresist
        self.poisonresist = poisonresist  # todo: add poison resist
        self.luck = luck
        self.weapon = None
        self.weaponSecondHand = None
        self.equipment = []
        self.gold = gold
        self.activeStatusEffects = []
        self.passiveStatusEffects = []
        self.gender = gender
        self.race = "none"
        self.pdefense = defense
        self.mdefense = defense
        self.items = []
        self.usableItems = []
        self.allItems = []
        self.moveset = []
        self.spellList = []
        self.passives = []
        self.tempStrength = strength
        self.tempIntel = intel
        self.tempAgility = agility
        self.tempLuck = luck
        self.tempCritical = int(self.luck)
        self.tempAbsorb = 0
        self.tempPDefense = self.pdefense
        self.tempMDefense = self.mdefense
        self.tempFireResist = 0
        self.tempIceResist = 0
        self.tempEarthResist = 0
        self.tempElectricResist = 0
        self.tempPoisonResist = 0
        self.tempWeaponBonus = 1
        self.offensiveSpeed = 1
        self.defensiveSpeed = 1
        self.name = None
        self.tempDefendingStatus = False
        self.tempChargingStatus = False
        self.tempArmorType = None

    def updateSubStat(self):
        oldHp = self.maxhp
        oldStam = self.maxstam
        oldMana = self.maxmana
        self.maxhp = int(self.const * 2)
        self.hpleft += self.maxhp - oldHp
        self.maxstam = int(10 + 5 * (self.const / 15) * (self.strength / 10))
        self.stamleft += self.maxstam - oldStam
        self.maxmana = int(self.intel * 2)
        self.manaleft += self.maxmana - oldMana


    # reset temp stat for new fights
    # offensiveSpeed is used as a weapon strike multiplicator based on weapon speed. Not using shield add 20% bonus
    def initTempStats(self):
        self.tempStrength = int(self.strength)
        self.tempIntel = int(self.intel)
        self.tempAgility = int(self.agility)
        self.tempLuck = int(self.luck)
        self.tempCritical = int(self.luck)
        self.tempAbsorb = 0
        self.tempPDefense = int(self.pdefense)
        self.tempMDefense = int(self.mdefense)
        self.tempFireResist = self.fireresist
        self.tempIceResist = self.iceresist
        self.tempEarthResist = self.earthresist
        self.tempElectricResist = self.electricresist
        self.tempPoisonResist = self.poisonresist
        self.tempDefendingStatus = False
        self.tempChargingStatus = False
        self.tempWeaponBonus = 1
        if self.weapon is not None:
            self.offensiveSpeed = self.weapon.speed
            for equipment in self.equipment:
                if equipment.gearSet() == "Shield":
                    self.offensiveSpeed += 0.2  # 20% bonus for no shield
        else:
            self.offensiveSpeed = 1

    # apply new stats after the life entity is created
    def modifstat(self, level=1, const=10, strength=0, intel=0, agility=10, defense=10, luck=0):
        self.level = level
        self.const = const
        self.maxhp = int(self.const * 2)
        self.hpleft = self.maxhp
        self.strength = strength
        self.maxstam = int(10 + 5 * (self.const / 15) * (self.strength / 10))
        self.stamleft = self.maxstam
        self.intel = intel
        self.maxmana = int(self.intel * 2)
        self.manaleft = self.maxmana
        self.agility = agility
        self.defense = defense
        self.luck = luck

    # algorithm of dodging probability
    # executed before applying any damage to anyone
    # param [in]: attacker; enemy attacking self entity
    # return: bool of dodging success or failure
    def dodge(self, attacker):
        if (90 + 10 * tanh(1.5 * (attacker.agility - self.agility) / attacker.agility)) < dice(100):
            status = True
        else:
            status = False
        return status

    # General damage reduction algorithm
    # For all types of damage, plus elemental reduction
    # For pdef & mdef, call iterative function damage_reduction
    # param [in] p_damage: full damage received
    # param [in] p_damage_type: type of damage or elemental type for elemental spell
    # param [] piercing: piercing armor factor, which reduce armor rate reduction
    # return: Damage after defensive reduction
    def damageResist(self, p_damage, p_damageType, piercing=0):
        if p_damageType in ["pattack", "cattack"]:
            p_damage = self.damage_reduction(self.tempPDefense, p_damage, piercing)
        if p_damageType == "mattack":
            p_damage = self.damage_reduction(self.tempMDefense, p_damage)
        elif p_damageType == "fire":
            p_damage = self.damage_reduction(self.tempMDefense, p_damage)
            p_damage *= 1 - (self.tempFireResist / 100)
        elif p_damageType == "ice":
            p_damage = self.damage_reduction(self.tempMDefense, p_damage)
            p_damage *= 1 - (self.tempIceResist / 100)
        elif p_damageType == "earth":
            p_damage = self.damage_reduction(self.tempMDefense, p_damage)
            p_damage *= 1 - (self.tempEarthResist / 100)
        elif p_damageType == "electric":
            p_damage = self.damage_reduction(self.tempMDefense, p_damage)
            p_damage *= 1 - (self.tempElectricResist / 100)
        elif p_damageType == "poison":
            p_damage = self.damage_reduction(self.tempMDefense, p_damage)
            p_damage *= 1 - (self.tempPoisonResist / 100)
        if self.tempDefendingStatus:
            p_damage //= 2
        elif p_damageType == "cattack":
            p_damage //= 2
        if p_damageType == "cattack" and p_damage < 1:
            p_damage = 1
        if p_damage < 0:
            p_damage = 0
        p_damage = int(p_damage)
        if self.tempAbsorb != 0:
            if self.tempAbsorb > p_damage:
                self.tempAbsorb -= p_damage
                p_damage = 0
            else:
                p_damage -= self.tempAbsorb
                self.tempAbsorb = 0
        return p_damage

    # Iterative reduction damage algorithm
    # Reduce damage relatively of tempDefense value,
    # reducing at most 1/5 of damage per iteration
    # each new iteration minimizing the effect of remaining defense points
    # damage become null if armor go to 300% of damage value
    # param [in] tempDfense: defense point value
    # param [in] p_damage: initial damage value
    # param [] piercing: piercing armor factor, which reduce armor rate reduction
    # return: damage after applying complete defense reduction
    @staticmethod
    def damage_reduction(tempDefense, p_damage, piercing=0):
        tempDefense *= 1 - piercing
        final_damage = p_damage
        for i in range(1, 5):
            if tempDefense / i <= p_damage / 5:
                final_damage -= tempDefense / i
                break
            else:
                final_damage -= p_damage / 5
                tempDefense -= p_damage * i / 5
        final_damage = round(final_damage)
        #print("strike damage: {}".format(final_damage))
        return final_damage

    def takeDamage(self, damage):
        if self.tempAbsorb != 0:
            pass
        self.hpleft -= damage
        if self.hpleft < 0:
            self.hpleft = 0

    def heal(self, health_point):
        self.hpleft += health_point
        if self.hpleft > self.maxhp:
            self.hpleft = self.maxhp

    # old weapon set,  check for dependencies before deletion
    def setweapon(self, p_weapon):
        if self.weapon is not None:
            self.unsetWeapon()
        self.weapon = Weapon(*p_weapon)
        self.items.append(self.weapon)

    # Weapon active set to the user
    # param [in] p_weapon: A weapon type item in the user inventory
    def newSetWeapon(self, weapon):
        self.unsetWeapon()
        self.weapon = weapon
        self.addAbilityWeaponStatus()

    # For every weapon based Ability, add a status effect depending the weapon equipped
    # Needs to be called on weapon set and on level up to update luck bonus
    def addAbilityWeaponStatus(self):
        for ability in self.moveset:
            if ability.ability_type == "weapon based":
                ability.statusLinker = ability.addWeaponStatus(self.weapon, self)

    # unused function, for later use only
    def newSetWeaponSecondHand(self, p_weapon):
        for i in self.equipment:
            if i.gearSet() == "Shield":
                print("Cannot hold 2 weapons and a shield")
                return None
        self.weaponSecondHand = self.items[p_weapon]

    # Unset active weapon
    def unsetWeapon(self):
        self.weapon = None
        for ability in self.moveset:
            if ability.ability_type == "weapon based":
                ability.statusLinker = None

    def setequipment(self, equipment):
        gearType = equipment[3]
        for i in range(len(self.equipment)):
            if gearType == self.equipment[i].gearSet():
                self.unsetEquipment(self.equipment[i])
                break
        if gearType == "Head":
            self.equipment.append(HeadGear(*equipment))
        elif gearType == "Body":
            self.equipment.append(ChestGear(*equipment))
        elif gearType == "Hand":
            self.equipment.append(ArmGear(*equipment))
        elif gearType == "Shield":
            self.equipment.append(Shield(*equipment))
        self.items.append(self.equipment[-1])
        self.setDefense()

    # Set a new gear piece, including weapon
    # There could be only one piece of each gear set
    # param [in] p_equipment: A piece of gear already own in the inventory
    def newSetEquipment(self, equipment):
        if equipment.item_type() == "Weapon":
            self.newSetWeapon(equipment)
            return None
        for i in range(len(self.equipment)):
            if equipment.gearSet() == self.equipment[i].gearSet():
                self.unsetEquipment(self.equipment[i])
                break
        self.equipment.append(equipment)
        self.setDefense()

    def isEquipped(self, equipment):
        if equipment == self.weapon:
            return True
        elif equipment in self.equipment:
            return True
        else:
            return False

    # Reset pdef & mdef with natural def and with equipment bonus
    def setDefense(self):
        self.pdefense = 0  # self.defense
        self.mdefense = 0  # self.defense
        for i in self.equipment:
            self.pdefense += i.pdefense
            self.mdefense += i.mdefense

    # Unset a piece of armor
    # param [in] p_equipment: Piece of gear already setted
    def unsetEquipment(self, equipment):
        print("Weapon to unset:")
        print("Name: {}".format(equipment.name))
        print("Type: {}".format(equipment.item_type()))
        if equipment.item_type() == "Weapon":
            self.unsetWeapon()
            print("Unset weapon")
        else:
            self.equipment.remove(equipment)
            self.setDefense()

    # old item set,  check for dependencies before deletion
    def setitems(self, p_item):
        if p_item[0] == "recovery":
            self.items.append(RecoveryItem(*p_item))
            self.usableItems.append(self.items[-1])
        elif p_item[0] == "revive":
            self.items.append(ReviveItem(*p_item))
            self.usableItems.append(self.items[-1])
        else:
            self.items.append(QuestItem(*p_item))

    # Remove an item from the inventory
    # param [in] item: An item present in the inventory
    def removeItem(self, item):
        self.items.remove(item)
        if item in self.usableItems:
            self.usableItems.remove(item)

    # Function to reorganise more logically
    # Equipped item first, starting from weapon, to gear(Head, body, hand, shield)
    # end with unequiped weapons and gears and lastly usable items and others
    def organise_items(self):
        item_list = self.items
        self.items = []
        if self.weapon is not None:
            self.items.append(self.weapon)
            item_list.remove(self.weapon)
        if len(self.equipment) > 0:
            for i in self.equipment:
                if i.gearSet() == "Head":
                    self.items.append(i)
                    break
            for i in self.equipment:
                if i.gearSet() == "Body":
                    self.items.append(i)
                    break
            for i in self.equipment:
                if i.gearSet() == "Hand":
                    self.items.append(i)
                    break
            for i in self.equipment:
                if i.gearSet() == "Shield":
                    self.items.append(i)
                    break
            for i in self.equipment:
                item_list.remove(i)
        for i in item_list:
            if i.item_type() in ["Weapon", "Equipment"]:
                self.items.append(i)
        for i in item_list:
            if i.item_type() in ["Recovery item", "Healing item"]:
                self.items.append(i)
        for i in item_list:
            if i.item_type() not in ["Weapon", "Equipment", "Recovery item"]:
                self.items.append(i)

    # Add in the inventory new item
    # param [in] p_item: a new item already initialized
    def newSetItem(self, item):
        self.items.append(item)
        if item.search() in ["Recovery item", "Healing item", "Revive item"]:
            self.usableItems.append(item)

    # Transfer an item from a player to an other in the same party
    # param [in] p_item: An item to transfer from the self player
    # param [in] other_player: an other player to receive the item
    def giveItem(self, p_item, other_player):
        if p_item not in self.equipment and self.weapon != p_item != self.weaponSecondHand:
            self.removeItem(p_item)
            other_player.newSetItem(p_item)

    # Function to use the benefict effect of a usable item
    # Used items are removed from the inventory
    # param [in] target: player for who the effect is applied
    # param [in] item: a usable item in the user inventory
    # return a dialog, used for the interface
    def useItem(self, target, item):
        dialogList = item.effect(self, target)
        self.removeItem(item)
        return dialogList

    def setspell(self, spell):
        self.spellList.append(spell)

    def reduceCoolDown(self):
        for skill in self.moveset:
            if skill.currentCD > 0:
                skill.currentCD -= 1
        for spell in self.spellList:
            if spell.currentCD > 0:
                spell.currentCD -= 1

    def display(self):
        print("Level: ", self.level)
        print("HP: ", self.hpleft, "/", self.maxhp)
        print("Mana: ", self.manaleft, "/", self.maxmana)
        print("Stamina: ", self.stamleft, "/", self.maxstam)
        print("Constitution: ", int(self.const // 1))
        print("Strength: ", int(self.strength // 1))
        print("Intelligence: ", int(self.intel // 1))
        print("Agility: ", int(self.agility // 1))
        print("Defense: ", int(self.defense // 1))
        print("Luck: ", int(self.luck // 1))
        if self.weapon is not None:
            print("Weapon:")
            if self.weapon.bonus != 0:
                print(self.weapon.__str__(), " + {}".format(self.weapon.bonus))
            else:
                print(self.weapon.__str__())
        if len(self.equipment) != 0:
            print("Equipment: ")
            for i in self.equipment:
                print("-", i)
        if len(self.items) != 0:
            print("Items: ")
            for i in self.items:
                print("-", i)
        print("Gold: ", self.gold)

    def fight_display(self):
        print("Level:               ", self.level)
        print("HP:             ", self.hpleft, "/", self.maxhp)
        print("Mana:           ", self.manaleft, "/", self.maxmana)
        print("Stamina:        ", self.stamleft, "/", self.maxstam)
        print("Constitution:        ", int(self.const // 1))
        print("Strength:            ", int(self.tempStrength // 1))
        print("Intelligence:        ", int(self.tempIntel // 1))
        print("Agility:             ", int(self.tempAgility // 1))
        print("Physic defense:      ", int(self.tempPDefense // 1))
        print("Magic defense:       ", int(self.tempMDefense // 1))
        print("Luck:                ", int(self.tempLuck // 1))
        print("Fire resistance:     ", int(self.tempFireResist))
        print("Ice resistance:      ", int(self.tempIceResist))
        print("Earth resistance:    ", int(self.tempEarthResist))
        print("Electric resistance: ", int(self.tempElectricResist))
        print("Poison resistance:   ", int(self.tempPoisonResist))
        if self.weapon is not None:
            print("Weapon:      {}".format(self.weapon.__str__()))
        if len(self.equipment) != 0:
            print("Equipment: ")
            for i in self.equipment:
                print("         -", i)
        if len(self.items) != 0:
            print("Items: ")
            for i in self.items:
                if i not in self.equipment:
                    print("         -", i)

    def shortdisplay(self):
        print("\nName: ", self.name)
        print("Level: ", self.level)
        print("HP: ", self.hpleft, "/", self.maxhp)
        print("Mana: ", self.manaleft, "/", self.maxmana)
        print("Stamina: ", self.stamleft, "/", self.maxstam)

    def nameset(self, name):
        self.name = name

    # Return player name
    def __str__(self):
        return self.name

    def __call__(self, *args, **kwargs):
        return self.name

    # return the type as a string
    def search(self):
        return "LifeEntity"

    # return a possessive pronoun of the character gender
    def article(self):
        if self.gender == "male":
            return "his"
        elif self.gender == "female":
            return "her"
        else:
            return "its"

    # return a possessive pronoun of the character gender
    def pronoun(self):
        if self.gender == "male":
            return "himself"
        elif self.gender == "female":
            return "herself"
        else:
            return "itself"

    def shortstat(self):
        stat = "hp: {}/{}\nmp: {}/{}\nsp: {}/{}".format(self.hpleft, self.maxhp, self.manaleft,
                                                        self.maxmana, self.stamleft, self.maxstam)
        return stat

    def statusEffect(self):
        pass

    def getSpellsName(self):
        spellsName = []
        for spell in self.spellList:
            spellsName.append(spell.name)
        return spellsName

    def getSpellsCost(self):
        spellsCost = []
        for spell in self.spellList:
            spellsCost.append("{}".format(spell.cost))
        return spellsCost

    def getUsableSpells(self):
        usableSpells = []
        for spell in self.spellList:
            usableSpells.append(spell.isUsableInMenu() and spell.cost <= self.manaleft)
        return usableSpells

    def getItems(self):
        return self.items


class Party:
    def __init__(self):
        self.members = []

    def clearMembers(self):
        self.members.clear()

    def addMember(self, member):
        self.members.append(member)

    def removeMember(self, member):
        self.members.remove(member)


# Class for handling characters as a group
# Manage game advancement with a fight count, list of opened chest and monster slained
class PlayerParty(Party):
    def __init__(self):
        super(PlayerParty, self).__init__()
        self.gold = 100  # temporary, just for testing
        self.fight_encountered = 0
        self.opened_chest = []
        self.boss_slained = []

    # Method call after each chest opening
    # Add the chest code to the list
    # Disable the possibility of opening a chest more than once in a game
    def chest_opening(self, code):
        self.opened_chest.append(code)

    # Verify if the chest as already been opened in the opened_chest list
    def check_chest(self, code):
        for i in self.opened_chest:
            if i == code:
                return True
        return False

    # Method call after each boss slained, NOT IMPLEMENTED
    # Add the boss code to the list
    # Disable the possibility of encountering the same boss more than once in a game
    def defeating_boss(self, code):
        self.boss_slained.append(code)

    # Verify if the chest as already been opened in the opened_chest list
    def check_boss(self, code):
        for i in self.boss_slained:
            if i == code:
                return True
        return False


class EnemyParty(Party):
    def __init__(self, party_type="Normal"):
        super(EnemyParty, self).__init__()
        self.party_type = party_type
        self.leader = "None"

    def setup(self):
        self.leader = str(self.defineLeader())
        self.nameIssues()

    def defineLeader(self):
        temp_leader = self.members[0]
        for fighter in self.members[1:]:
            if fighter.level > temp_leader.level and temp_leader.enemytype == fighter.enemytype:
                temp_leader = fighter
            elif fighter.enemytype == "boss" != temp_leader.enemytype:
                temp_leader = fighter
            elif fighter.enemytype == "elite" and temp_leader.enemytype == "normal":
                temp_leader = fighter
        self.party_type = temp_leader.enemytype
        return temp_leader

    # manage any duplicated enemy names by adding a suffix number after them
    def nameIssues(self):
        for i in range(len(self.members)):
            indexList = [i]
            for j in range(len(self.members)):
                if i == j:
                    continue
                if str(self.members[i]) == str(self.members[j]):
                    indexList.append(j)
            if len(indexList) > 1:
                iterator = 0
                for j in indexList:
                    iterator += 1
                    newName = str(self.members[j]) + " " + str(iterator)
                    self.members[j].nameset(newName)
                return self.nameIssues()


# todo: delete this function
# Random creation of enemy party with range stat
# param [in] nbrmax: Max number of enemy in the enemy party
# param [in] lvlmin & lvlmax: level range for generated enemies
# param [in] biome: Source of enemies, each enemy can come from different biomes
# return randomized enemy party
#def enemyrdmnbr(nbrmax=3, lvlmin=1, lvlmax=2, biome=forest):
#    enemypartysize = dice(nbrmax)
#    enemyparty = []
#    for i in range(enemypartysize):
#        monster = randomenemygenerator(lvlmin=lvlmin, lvlmax=lvlmax, biome=biome)
#        enemyparty.append(monster)
#    return enemyparty


# todo: delete this function
# sub-method, generate a single random enemy
# param [in] lvlmin & lvlmax: level range for generated enemy
# param [in] biome: Source of enemies, each enemy can come from different biomes
# return randomized enemy
#def randomenemygenerator(lvlmin=1, lvlmax=2, biome=forest):
#    problist = [0]
#    for i in biome:
#        if lvlmin <= i[0] <= lvlmax:
#            problist.append(i[17] + problist[-1])
#        else:
#            problist.append(problist[-1])
#    dicerslt = dice(problist[-1])
#    for i in range(len(biome)):
#        if dicerslt <= problist[i + 1]:
#            monster1 = Monster(*(biome[i]))
#            return monster1
