class Item_Class:
    def __init__(self, name, description=""):
        self.name = name
        self.description = description

    def __str__(self):
        return self.name

    def item_type(self):
        return "None"

    def search(self):
        return "None"

    def type(self):
        return "<Item_object>"

    def getDescription(self):
        return self.description

    def isTossable(self):
        return True

    def isUsable(self):
        return False


class UsableItem(Item_Class):
    def __init__(self, itemType, name, gold=0, description=""):
        super().__init__(name, description)
        self.itemType = itemType
        self.price = gold

    def effect(self, user, target):
        return "{} is using {} on {}".format(user, self.name, target)

    def effectValue(self, target):
        pass

    def use(self, target, hit_point):
        pass

    def type(self):
        return "<item>"

    def search(self):
        return "Usable item"

    def isUsable(self):
        return True


class ReviveItem(UsableItem):
    def __init__(self, name, gold=0, description=""):
        super().__init__(itemType="revive", name=name, gold=gold, description=description)

    def effect(self, user, target):
        dialogList = []
        dialog = super().effect(user, target)
        dialogList.append(dialog)
        if self.name == "life bottle":
            target.hpleft = target.maxhp
            dialog = "{} is back!".format(target)
            print(dialog)
            dialogList.append(dialog)
        return dialogList

    def effectValue(self, target):
        recovery_point = target.maxhp
        return recovery_point

    def use(self, target, recovery_point):
        if self.name == "life bottle":
            target.hpleft += recovery_point

    def search(self):
        return "Revive item"

    def getDescription(self):
        return [self.description]


class RecoveryItem(UsableItem):
    def __init__(self, name, statmodif, stattype, gold=0, description=""):
        super().__init__(itemType="recovery", name=name, gold=gold, description=description)
        self.statmodif = statmodif
        self.stattype = stattype

    def effect(self, user, target):
        dialog = super().effect(user, target)
        if self.stattype in ["life recovery", "HP"]:
            target.hpleft += self.statmodif
            if target.hpleft > target.maxhp:
                target.hpleft = target.maxhp
        if self.stattype in ["mana recovery", "MP"]:
            target.manaleft += self.statmodif
            if target.manaleft > target.maxmana:
                target.manaleft = target.maxmana
        if self.stattype in ["stamina recovery", "SP"]:
            target.stamleft += self.statmodif
            if target.stamleft > target.maxstam:
                target.stamleft = target.maxstam
        return [dialog]

    def effectValue(self, target):
        recovery_point = 0
        if self.stattype in ["life recovery", "HP"]:
            if self.statmodif <= target.maxhp - target.hpleft:
                recovery_point = self.statmodif
            else:
                recovery_point = target.maxhp - target.hpleft
        elif self.stattype in ["mana recovery", "MP"]:
            if self.statmodif <= target.maxmana - target.manaleft:
                recovery_point = self.statmodif
            else:
                recovery_point = target.maxmana - target.manaleft
        elif self.stattype in ["stamina recovery", "SP"]:
            if self.statmodif <= target.maxstam - target.stamleft:
                recovery_point = self.statmodif
            else:
                recovery_point = target.maxstam - target.stamleft
        return recovery_point

    def use(self, target, recovery_point):
        if self.stattype in ["life recovery", "HP"]:
            target.hpleft += recovery_point
        elif self.stattype in ["mana recovery", "MP"]:
            target.manaleft += recovery_point
        elif self.stattype in ["stamina recovery", "SP"]:
            target.stamleft += recovery_point

    def search(self):
        return "Recovery item"

    def getDescription(self):
        return [self.description]


class QuestItem(Item_Class):
    def __init__(self, name, description=""):
        super().__init__(name=name, description=description)

    def item_type(self):
        return "Quest item"

    def isTossable(self):
        return False

    def getDescription(self):
        return [self.description]
