from core.Global import selectPhase
from core.Handlers.Handlers import Handler
from core.Combat_Mechanics.Action import Action, EnemyAction, StatusAction, ChargeAction
from core.Combat_Mechanics.AI import *
from core.Combat_Mechanics.ActionFactory import ActionFactory
from dialogs.Fight_Dialog import *
from interface.Fight_Interface import FightInterface


# handler class, handle all the battle mechanics
# instanciate the FightInterface object, from which comes user's command
class FightHandler(Handler):
    def __init__(self, player_party, enemy_team):
        super().__init__()
        self.player_party = player_party
        self.enemy_team = enemy_team
        self.fight_interface = None
        self.turnOrder = self.setTurnOrder()
        self.turn_party = None
        self.phase = selectPhase[0]
        self.action = None
        self.select = 0
        self.is_select_list_empty = False
        self.page = 0
        self.escape = False

    # After creating a FightHandler, set a background and the last fight spec
    # before throwing the fight loop
    def setup(self, background):
        for fighter in [*self.player_party.members, *self.enemy_team.members]:
            fighter.initTempStats()
        self.enemy_team.setup()
        self.loadInterface(background)
        self.phase = selectPhase[8]
        self.setInterfaceListener()
        self.updateDisplay()
        self.fight_interface.startMusic()
        self.fight_interface.loop()

    # create FightInterface object with the background element
    # param[in] background: loaded image to display on the background of the fight
    # param[in] fight_type: "normal", "elite" or "boss", rank of the enemy team
    #                       which determine the fight theme played
    def loadInterface(self, background, fight_type="normal"):
        self.fight_interface = FightInterface(self.player_party, self.enemy_team, background, fight_type)

    # set order in which each fighter play its turn, depending on its agility
    def setTurnOrder(self):
        order = []
        for i in self.player_party.members:
            order.append(i)
        for i in self.enemy_team.members:
            order.append(i)
        for i in range(len(order)):
            actual = order[i]
            temp = i
            for j in range(i, len(order)):
                if order[j].agility > order[temp].agility:
                    temp = j
            order[i] = order[temp]
            order[temp] = actual
        return order

    # select next fighter's turn and set the status turm
    # if the status turn has nothing special, go for the player fighter's ability turn
    # modify self.turnOrder by pushing the actual fighter to the end of the list
    def newTurn(self):
        if self.endCondition():
            pass
        else:
            temp_fighter = self.turnOrder[0]
            print("{}'s strength: {}".format(temp_fighter, temp_fighter.tempStrength))
            self.turnOrder.pop(0)
            self.turnOrder.append(temp_fighter)
            temp_fighter.tempDefendingStatus = False
            temp_fighter.reduceCoolDown()
            if temp_fighter.hpleft <= 0:
                self.newTurn()
            else:
                if temp_fighter.search() == "Player":
                    self.turnRecovery(temp_fighter)
                self.statusTurn(temp_fighter)
                if not self.action.isDialog:
                    self.actionTurn(temp_fighter)

    # set the turn pahse to status turn and create the condition for it
    # always done in the begining of each fighter's turn
    def statusTurn(self, fighter):
        self.phase = selectPhase[9]
        self.action = StatusAction(fighter)
        self.action.newTurn()
        self.action.actionCast()

    # start the real fighter turn where the fighter choose an ability to do
    # begin right after statusTurn() if there is no statusEffect change
    def actionTurn(self, fighter):
        if fighter.search() == "Player":
            self.turn_party = "Player"
            if fighter.tempChargingStatus:
                self.action = ChargeAction(fighter)
                self.action.actionCast()
                self.phase = selectPhase[3]
                fighter.tempChargingStatus = False
            else:
                self.action = Action(fighter)
                self.phase = selectPhase[0]
        elif fighter.search() == "Monster":
            self.action = EnemyAction(fighter)
            self.turn_party = "Enemy"
            self.enemyActionCast()
            self.phase = selectPhase[3]

    # called to update fightInterface display infos
    def updateDisplay(self):
        if self.phase == selectPhase[0]:  # move
            self.movePhase()
        elif self.phase == selectPhase[1]:  # ability
            self.actionPhase()
        elif self.phase == selectPhase[2]:  # target
            self.targetPhase()
        elif self.phase == selectPhase[3]:  # resume
            self.resumePhase()
        elif self.phase == selectPhase[5]:  # victory
            self.winPhase()
        elif self.phase == selectPhase[6]:  # defeat
            self.losePhase()
        elif self.phase == selectPhase[8]:  # intro
            self.introPhase()
        elif self.phase == selectPhase[9]:  # pre turn
            self.preturnPhase()

    # sub-function called by updateDisplay to send intro dialog to Fight_Interface
    def introPhase(self):
        h_text = intro_dialog(self.enemy_team)
        self.fight_interface.update("no cancel", h_text, [], [])

    # sub-function called by updateDisplay to send infos before the begining of the move phase if the fighter
    # is afflicted with an active status effect
    def preturnPhase(self):
        h_text = preturn_dialog(self.action.fighter, self.action)
        self.fight_interface.update("no cancel", h_text, [], [])

    # sub-function called by updateDisplay to send infos when on the "move" phase
    def movePhase(self):
        h_text = player_header_dialog(self.action.fighter)
        c_text = player_choice_dialog()
        disabledChoice = self.disabledChoices(c_text)
        self.fight_interface.update("no cancel", h_text, c_text, disabledChoice)

    # sub-function called by updateDisplay to send infos when on the "ability" phase
    def actionPhase(self):
        c_text = player_choice_move_dialog(self.action.fighter, self.action.moveType)
        # if ability category is empty
        if len(c_text) == 0 and self.action.moveType in [moveList[0], moveList[1], moveList[2]]:
            self.is_select_list_empty = True
            h_text = player_header_no_move_dialog(self.action.moveType)
            self.fight_interface.update("no cancel", h_text, [], [])
        else:  # if not
            self.is_select_list_empty = False
            h_text = player_header_move_dialog(self.action.fighter, self.action.moveType)
            disabledChoice = self.disabledChoices(c_text)
            self.fight_interface.update("cancel", h_text, c_text, disabledChoice)

    # sub-function called by updateDisplay to send infos when on the "target" phase
    def targetPhase(self):
        if self.action.moveType == moveList[4] and self.enemy_team.party_type == "boss":
            self.is_select_list_empty = True
            h_text = no_escape_dialog()
            self.fight_interface.update("no cancel", h_text, [], [])
        targetList = self.getTargetGroup()
        if len(targetList) > 0:
            self.is_select_list_empty = False
            group_size = self.action.ability.getGroupSize()
            group_type = self.action.ability.getTargetGroup()
            h_text = player_header_target_dialog()
            c_text = player_choice_target_dialog(targetList, group_size, group_type)
            disabledChoice = self.disabledChoices(c_text)
            self.fight_interface.update("cancel", h_text, c_text, disabledChoice)
        else:
            self.is_select_list_empty = True
            h_text = player_header_no_target_dialog()
            self.fight_interface.update("no cancel", h_text, [], [])

    # sub-function called by updateDisplay to send infos when on the "resume" phase
    def resumePhase(self):
        h_text = action_cast_dialog(self.action.fighter, self.action.targets, self.action)
        self.fight_interface.update("no cancel", h_text, [], [])

    # sub-function called by updateDisplay to send texts when on the enemy phase
    def enemyPhase(self):
        h_text = action_cast_dialog(self.action.fighter, self.action.targets, self.action)
        self.fight_interface.update("no cancel", h_text, [], [])

    # sub-function called by updateDisplay to send infos when on the victory phase
    def winPhase(self):
        text = self.rewards()
        h_text = win_dialog(text, self.enemy_team)
        self.fight_interface.victoryMusic()
        self.fight_interface.update("no cancel", h_text, [], [])

    # sub-function called by updateDisplay to send infos on the losing phase
    def losePhase(self):
        h_text = lose_dialog()
        self.fight_interface.update("no cancel", h_text, [], [])

    # sub-function called by updateDisplay to send infos when on escape phase
    def escapePhase(self):
        h_text = escape_dialog(self.action.ability.escape)
        self.fight_interface.update("no cancel", h_text, [], [])

    def disabledChoices(self, choiceList):
        disabled = []
        if len(choiceList) != 0:
            if self.phase == selectPhase[1]:
                if self.action.moveType == moveList[0]:
                    for move in self.action.fighter.moveset:
                        disabled.append(move.isDisabled())
                elif self.action.moveType == moveList[1]:
                    for spell in self.action.fighter.spellList:
                        disabled.append(spell.isDisabled())
                elif self.action.moveType == moveList[2]:
                    for item in self.action.fighter.usableItems:
                        disabled.append(False)
                else:
                    print("Error, shouldn't get here")
            else:
                for choice in choiceList:
                    disabled.append(False)
        return disabled

    # return the selected Action object from the fighter
    # depend on self.select, the object index, and the action type, selected in previous phase
    def getAction(self):
        action = None
        if self.action.moveType == moveList[0]:
            action = self.action.fighter.moveset[self.select]
        elif self.action.moveType == moveList[1]:
            action = self.action.fighter.spellList[self.select]
        elif self.action.moveType == moveList[2]:
            item = self.action.fighter.usableItems[self.select]
            action = ActionFactory.createAction(self.action.moveType, item)
        elif self.action.moveType in [moveList[3], moveList[4]]:
            action = ActionFactory.createAction(self.action.moveType)
        return action

    # return a list of target using the index parameter if its single,
    # or the complete list of available targets if the ability target size is maxed
    def setTargets(self, target_index):
        target = []
        targetGroup, targetSize = self.action.ability.actionTarget()
        target_list = self.getTargetGroup()
        if targetSize == "1":
            target.append(target_list[target_index])
        elif targetSize == "all":
            target = target_list
        return target

    # return a list of all potential target of a fighter's ability for a player's character
    def getTargetGroup(self):
        targets = []
        target_group = self.action.ability.targetGroup
        if target_group == "Enemy":
            for member in self.enemy_team.members:
                if member.hpleft > 0:
                    targets.append(member)
        elif target_group == "Party":
            for member in self.player_party.members:
                if member.hpleft > 0:
                    targets.append(member)
        elif target_group == "Fallen":
            for member in self.player_party.members:
                if member.hpleft <= 0:
                    targets.append(member)
        elif target_group == "Personal":
            targets.append(self.action.fighter)
        return targets

    # return a list of all potential target of a fighter's ability for an enemy character
    def getEnemyTargetGroup(self):
        targets = []
        target_group = self.action.ability.targetGroup
        if target_group == "Party":
            for member in self.enemy_team.members:
                if member.hpleft > 0:
                    targets.append(member)
        elif target_group == "Enemy":
            for member in self.player_party.members:
                if member.hpleft > 0:
                    targets.append(member)
        elif target_group == "Fallen":
            for member in self.enemy_team.members:
                if member.hpleft <= 0:
                    targets.append(member)
        elif target_group == "Personal":
            targets.append(self.action.fighter)
        return targets

    # select ability depending on the phase on response reception (update setAction() function)
    def handleResponse(self, index, cancel_flag):
        if self.escape:  # check if an escape has been confirmed
            self.fight_interface.end_fight = True
        if cancel_flag or self.is_select_list_empty:  # check for a cancelation, if so go back from the previous step
            self.is_select_list_empty = False
            if self.phase == selectPhase[1]:
                self.phase = selectPhase[0]
            elif self.phase == selectPhase[2]:
                if self.action.moveType in [moveList[3], moveList[4]]:  # defend or flee
                    self.phase = selectPhase[0]
                else:
                    self.phase = selectPhase[1]
        else:  # if not, link to the next step
            self.select = index
            if self.phase == selectPhase[0]:  # from move phase...
                self.action.setMove(moveList[self.select])
                if self.select in [0, 1, 2]:
                    self.phase = selectPhase[1]  # ...to ability phase
                else:
                    if self.select in [3, 4]:
                        action = self.getAction()
                        self.action.setAction(action)
                        if self.select == 4:
                            self.action.ability.enemy_leader = self.enemy_team.defineLeader()
                        self.phase = selectPhase[2]  # ...to target phase
            elif self.phase == selectPhase[1]:  # from ability phase...
                action = self.getAction()
                self.action.setAction(action)
                self.phase = selectPhase[2]  # ...to target phase
            elif self.phase == selectPhase[2]:  # from target phase...
                self.action.setTargets(self.setTargets(index))  # select target and apply ability
                self.action.actionCast()
                if self.action.moveType == moveList[4]:
                    self.escape = self.action.ability.isSuccessful
                self.phase = selectPhase[3]  # ...to resume phase
            elif self.phase == selectPhase[3]:  # from resume phase...
                self.action.applyAction()
                if self.escape:
                    self.phase = selectPhase[7]  # ...to fleeing phase
                else:
                    self.newTurn()  # ... to newTurn() result
            elif self.phase in [selectPhase[5], selectPhase[6]]:  # from victory or defeat phase
                self.fight_interface.end_fight = True  # set end_fight to True to stop the fight loop
            elif self.phase == selectPhase[7]:  # from fleeing phase
                self.fight_interface.end_fight = True  # same for victory and defeat, end the fight
            elif self.phase == selectPhase[8]:  # from intro phase...
                self.newTurn()  # ...to newTurn() result
            elif self.phase == selectPhase[9]:  # from preturn phase...
                self.action.applyAction()
                if self.action.isParalyzed or self.action.fighter.hpleft <= 0:
                    self.newTurn()  # ... start a new turn if this one is finished
                else:
                    self.actionTurn(self.action.fighter)  # ...or enter in it
        print("Actual phase: {}".format(self.phase))
        if not self.fight_interface.end_fight:
            self.updateDisplay()

    # function called in each end of turn for natural stat recovery
    @staticmethod
    def turnRecovery(fighter):
        if fighter.fightclass is not None:
            fighter.hpleft += fighter.fightclass.healthRecovery
            if fighter.hpleft > fighter.maxhp:
                fighter.hpleft = fighter.maxhp
            fighter.manaleft += fighter.fightclass.manaRecovery
            if fighter.manaleft > fighter.maxmana:
                fighter.manaleft = fighter.maxmana
            fighter.stamleft += fighter.fightclass.staminaRecovery
            if fighter.stamleft > fighter.maxstam:
                fighter.stamleft = fighter.maxstam

    # set a listener to fight_interface to send to handleResponse a call at each event
    def setInterfaceListener(self):  # append lambda function
        self.fight_interface.listener = lambda index, cancel_flag: self.handleResponse(index, cancel_flag)

    def enemyActionCast(self):
        self.action.setAttack()
        target_list = self.getEnemyTargetGroup()
        self.action.setTargets(chooseTarget(self.action.fighter, target_list, self.action.ability.getGroupSize()))
        self.action.actionCast()

    # check fight condition
    # if team party or enemy party has all fainted, update phase
    # return True if one condition is respected
    #   to prevent main fight loop to continue
    def endCondition(self):
        win_condition = True
        lost_condition = True
        for fighter in self.enemy_team.members:
            if fighter.hpleft > 0:
                win_condition = False
        for fighter in self.player_party.members:
            if fighter.hpleft > 0:
                lost_condition = False
        if win_condition:
            self.phase = selectPhase[5]
        if lost_condition:
            self.phase = selectPhase[6]
        return win_condition or lost_condition

    # gives reward to the party for winning the fight
    # return text to display on screen
    #       -experience point gained
    #       -level up text
    #       -gold gained
    def rewards(self):
        print("\nReward")
        self.endRecovery()
        text = []
        reward = 0
        experience = 0
        for enemy in self.enemy_team.members:
            reward += enemy.gold
            experience += enemy.xp
        text.append("Party received {} experience points".format(experience))
        for fighter in self.player_party.members:
            if fighter.hpleft > 0:
                dialogs = fighter.giveXp(experience)
                for line in dialogs:
                    text.append(line)
                dialogs1 = fighter.weaponLevelUp()
                dialogs2 = fighter.armorLevelUp()
                dialogs3 = fighter.spellLevelUp()
                for line in dialogs1:
                    text.append(line)
                for line in dialogs2:
                    text.append(line)
                for line in dialogs3:
                    text.append(line)
        self.player_party.gold += int(reward)
        text.append("Party received {} gold".format(int(reward)))
        return text

    # Gives recovery point to each fighter at the end of the fight
    # Called only if the party is victorious
    def endRecovery(self):
        for fighter in self.player_party.members:
            if fighter.fightclass is not None:
                fighter.hpleft += fighter.fightclass.healthRecovery
                if fighter.hpleft > fighter.maxhp:
                    fighter.hpleft = fighter.maxhp
                fighter.manaleft += fighter.fightclass.manaRecovery + 3
                if fighter.manaleft > fighter.maxmana:
                    fighter.manaleft = fighter.maxmana
                fighter.stamleft += fighter.fightclass.staminaRecovery + 3
                if fighter.stamleft > fighter.maxstam:
                    fighter.stamleft = fighter.maxstam
            for status in fighter.passiveStatusEffects:
                fighter.passiveStatusEffects.remove(status)
            for status in fighter.activeStatusEffects:
                fighter.activeStatusEffects.remove(status)
