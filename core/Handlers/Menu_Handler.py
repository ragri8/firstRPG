from core.Handlers.Handlers import *
from interface.Menu_Interface import MenuInterface
from core.Global import Menu_Mode


# todo: menu layers, option selection, switching layer function, etc
class MenuHandler(Handler):
    def __init__(self, team):
        super().__init__()
        self.team = team
        self.menu_interface = MenuInterface(team)
        self.index = 0
        self.mode = None
        self.sub_mode = None
        self.fighter = team.members[0]

    def setup(self):
        self.setInterfaceListener()
        self.menu_interface.loop()

    def handleResponse(self, index, sub_index, cancel_flag):
        print("Response received: {}".format(index))
        self.fighter = self.team.members[sub_index]
        if self.mode is None:
            if cancel_flag:
                self.menu_interface.end_menu = True
            else:
                self.mode = Menu_Mode.getMode(index)
                if self.mode != Menu_Mode.CANCEL:
                    self.menu_interface.changeMode(self.mode, self.team)
                    print("{} mode selected".format(self.mode))
                else:
                    self.menu_interface.end_menu = True  # todo: probably useless
        elif cancel_flag:
            self.menu_interface.menu_boxes.pop(-1)
            self.mode = self.menu_interface.getCurrentMode()
        elif self.mode == Menu_Mode.SPELL:
            if self.isSpellUsable(index):
                self.mode = Menu_Mode.TARGET
                targetGroupSize = self.getSpellTargetSize(index)
                self.menu_interface.changeMode(self.mode, self.team, targetGroupSize)
        elif self.mode == Menu_Mode.TARGET:
            self.execute(index)
            self.menu_interface.goToSubMenu()
            self.mode = self.menu_interface.getMainMode()
        elif self.mode == Menu_Mode.ITEM:
            self.mode = Menu_Mode.ITEMUSE
            indexes = self.menu_interface.getMainChoices()
            self.fighter = self.team.members[indexes[0]]
            self.menu_interface.changeMode(self.mode, self.team, indexes)
        elif self.mode == Menu_Mode.ITEMUSE:
            self.executeItem(index)
        elif self.mode == Menu_Mode.LEVELUP:
            if index == 4:
                self.mode = Menu_Mode.SKILLS
                self.menu_interface.changeMode(self.mode, self.team)
            else:
                self.addStatPoint()

    # set a listener to fight_interface to send to handleResponse a call at each event
    def setInterfaceListener(self):  # append lambda function
        self.menu_interface.listener = lambda index, sub_index, cancel_flag: \
            self.handleResponse(index, sub_index, cancel_flag)

    def execute(self, index):
        main_mode = self.menu_interface.getMainMode()
        if main_mode == Menu_Mode.SPELL:
            userIndex, spellIndex = self.menu_interface.getMainChoices()
            user = self.getUser(userIndex)
            spell = self.getSpell(user, spellIndex)
            target = self.team.members[index]
            self.useSpell(user, target, spell)
        elif main_mode == Menu_Mode.ITEM:
            sub_mode = self.menu_interface.getSubMode()
            if sub_mode == Menu_Mode.ITEMUSE:
                userIndex, itemIndex = self.menu_interface.getMainChoices()
                user = self.getUser(userIndex)
                item = self.getItem(user, itemIndex)
                target = self.team.members[index]
                if self.menu_interface.getSubChoice() == 2:
                    self.useItem(user, target, item)
                elif self.menu_interface.getSubChoice() == 3:
                    self.giveItem(user, target, item)
        elif main_mode == Menu_Mode.OPTION:
            pass

    def getUser(self, userIndex):
        return self.team.members[userIndex]

    def getSpell(self, user, spellIndex):
        return user.spellList[spellIndex]

    def executeItem(self, index):
        if self.menu_interface.isChoiceEnabled():
            fighterIndex, itemIndex = self.menu_interface.getMainChoices()
            print("Indexes: {} & {}".format(fighterIndex, itemIndex))
            fighter = self.team.members[fighterIndex]
            item = fighter.items[itemIndex]
            if index == 0:
                fighter.newSetEquipment(item)
                print("Equipped")
                self.previousMode()
            elif index == 1:
                fighter.unsetEquipment(item)
                self.previousMode()
            elif index in [2, 3]:
                self.mode = Menu_Mode.TARGET
                self.menu_interface.changeMode(self.mode, self.team, "1")
            elif index == 4:
                pass
            elif index == 5:
                self.previousMode()
        else:
            return None

    def addStatPoint(self):
        fighterIndex, statIndex = self.menu_interface.getMainChoices()
        fighter = self.team.members[fighterIndex]
        if fighter.isStatPointLeft():
            stat = self.getStatFromIndex(statIndex)
            fighter.useStatPoint(stat)

    def getStatFromIndex(self, index):
        if index == 0:
            return "Constitution"
        elif index == 1:
            return "Strength"
        elif index == 2:
            return "Intelligence"
        elif index == 3:
            return "Agility"
        else:
            raise Exception

    def getItem(self, user, itemIndex):
        return user.items[itemIndex]

    def selectSpell(self):
        pass

    def isSpellUsable(self, index):
        usableSpells = self.fighter.getUsableSpells()
        return usableSpells[index]

    def getSpellTargetSize(self, index):
        spell = self.fighter.spellList[index]
        return spell.targetGroupSize

    def previousMode(self):
        self.menu_interface.menu_boxes.pop(-1)
        self.mode = self.menu_interface.getCurrentMode()

    def useSpell(self, user, target, spell):
        print("Spell has been used")  # todo: implement spell use

    def useItem(self, user, target, item):
        user.useItem(target, item)
        print("Item has been used")  # todo: implement item use
        self.menu_interface.menu_boxes[1].update()

    def giveItem(self, user, target, item):
        user.giveItem(item, target)
        print("Item has been given")
        self.menu_interface.menu_boxes[1].update()
