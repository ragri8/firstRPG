import pygame

SpriteDisplay = ["waiting", "attacking", "taking damage"]


class Sprite:
    def __init__(self, spriteList):
        self.spriteList = spriteList
        if len(self.spriteList) != 3:
            if len(self.spriteList) == 0:
                self.spriteList.append("default_sprite.png")
            for i in range(3-len(self.spriteList)):
                self.spriteList.append(self.spriteList[0])
        self.loadedSprites = []
        self.spriteCondition = "waiting"
        self.nextSpriteCondition = "waiting"

    def load(self):
        for i in self.spriteList:
            sprite = pygame.image.load(i)
            self.loadedSprites.append(sprite)

    def waiting(self, position=0):
        if position == 0:
            return self.loadedSprites[0]

    def takingDamage(self):
        return self.loadedSprites[1]

    def attacking(self, position=0):
        return self.loadedSprites[2]

    def blocking(self):
        return self.waiting()

    def getSprite(self, display_type):
        if display_type == SpriteDisplay[0]:
            sprite = self.waiting()
        elif display_type == SpriteDisplay[1]:
            sprite = self.attacking()
        elif display_type == SpriteDisplay[2]:
            sprite = self.takingDamage()
        else:
            sprite = self.waiting()
        return sprite


class EnemySprite(Sprite):
    def __init__(self, spriteList):
        newList = []
        for i in spriteList:
            newList.append("../sprites/enemy_sprite/{}".format(i))
        super().__init__(spriteList=newList)


class PlayerSprite(Sprite):
    def __init__(self, spriteList):
        newList = []
        for i in spriteList:
            newList.append("../sprites/character_sprite/{}".format(i))
        super().__init__(spriteList=newList)
