from interface.Pygame_Basic import *


class GameMap:
    def _init__(self, mapcode, map_number=1):  # new_attribute added, have to add it when called in pygame_main_game.py
        self.map_number = map_number
        self.size_x = int(mapcode[0: 4])
        self.size_y = int(mapcode[4: 8])
        self.biomecode = mapcode[8: 10]
        self.biome = self.getBiome(int(mapcode[8: 10]))
        self.minLevel = int(mapcode[10: 12])
        self.maxLevel = int(mapcode[12: 14])
        self.fightBackground = mapcode[14: 16]
        self.start_x = int(mapcode[16: 20])
        self.start_y = int(mapcode[20: 24])
        self.mapcode = mapcode[24:(self.size_x*self.size_y+24)*2]
        self.mapaccess = self.tileAccess()
        self.map_boss = []
        self.object_list = []
        self.special_list = []
        self.chest_list = []
        self.mapinteraction = self.interactive_tile(mapcode[(24 + self.size_x * self.size_y * 2):])
        self.mapnpc = self.npc_location(self.mapinteraction)
        print("mapcode:")
        print("Size: {}x{}".format(self.size_x, self.size_y))
        print("Data:")
        for i in range(self.size_y):
            print(self.mapcode[i * self.size_x*2: (i + 1) * self.size_x*2])
        print("Tile access:")
        for i in range(self.size_y-1, -1, -1):
            print(self.mapaccess[i * self.size_x: (i + 1) * self.size_x])

    def getBiome(self, code):
        if code == 0:
            return "all biome"

    def setMapcode(self, code):
        mapcodeList = []
        for i in range(0, len(code), 4):
            mapcodeList.append(int(code[i: i+4]))
        return mapcodeList

    def tileAccess(self):
        pass

    def display_terrain_grid(self, player_x, player_y):
        for x in range(-60, display_width + 60, tile_size):
            for y in range(-60, display_height + 60, tile_size):
                tile, position = self.getTile(x, y, player_x, player_y)
                if tile is None:
                    continue
                gameDisplay.blit(tile, (x - (player_x % tile_size), y + (player_y % tile_size)))
        for i in self.mapnpc:
            self.display_npc(i, player_x, player_y)
        for i in self.map_boss:
            self.display_boss(i, player_x, player_y)
        for i in self.chest_list:
            i.display_chest(player_x, player_y, self)
