from core.Combat_Class.Combat_Class import levelchart
from core.Combat_Class.Ranger import Ranger
from core.Combat_Class.SpellCaster import Spellcaster
from core.Combat_Class.Tank import Tank
from core.Combat_Class.Warrior import Warrior
from core.Combat_Mechanics.Ability import Attack
from core.FightStatus import PlayerStatus
from core.Life_Entities import LifeEntity
from core.Sprite_Class import PlayerSprite
from objects import Sprite_Sheet as spr
from objects.Skill_Sheet import skill_tree


# The playableCharacter class is the children class use for all user controlled character
# Needs a second function (self.personnalID())to initialize completely the character
# param [in]: all Parent parameter for initialisation
class PlayableCharacter(LifeEntity):
    def __init__(self, level=1, const=10, strength=0, intel=0, agility=10,
                 defense=10, luck=0, gold=0, xp=0, spritetag="default_spr"):
        super().__init__(level=level, const=const, strength=strength, intel=intel,
                         agility=agility, defense=defense, luck=luck, gold=gold)
        self.xp = xp
        self.moveset = [Attack()]
        self.name = None
        self.gender = None
        self.race = None
        self.fightclass = None
        self.fightStatus = PlayerStatus(self)
        self.skillPoint = 0
        self.spellPoint = 0
        self.statPoint = 0
        self.skillTree = skill_tree
        self.spriteTag = spritetag
        self.sprite = PlayerSprite(spr.player_sprite_tags.get(spritetag))
        self.sprite.load()
        self.spriteCondition = "waiting"
        self.nextSpriteCondition = "waiting"
        self.tempWeaponBonus = 1

    # Overwriten method, call super() and adds bonus for weapon type and gear type
    def initTempStats(self):
        self.setDefense()
        super().initTempStats()
        self.tempWeaponBonus = self.fightStatus.weaponBonus(self)
        self.tempArmorType = self.armorCheck()
        self.armorLevelBonus()

    # Verify if a type(light, medium, heavy) set is worn and return the value
    # if mixed, return "mixed" type
    # also set up defensiveSpeed, a parameter used in battle, affecting agility quotient when defending
    # the smallest its value, the less an opponent can strike the fighter
    def armorCheck(self):
        armorType = "None"
        self.defensiveSpeed = 0.75
        for i in self.equipment:
            self.defensiveSpeed += i.weight
            if armorType == "None":
                armorType = i.armorClass
            elif armorType == i.armorClass:
                continue
            else:
                armorType = "mixed"
        return armorType

    # Add bonus to armor if armorType isn't mixed or None and at least 3 parts are worn
    def armorLevelBonus(self):
        if len(self.equipment) >= 3 and self.tempArmorType in ["light", "medium", "heavy"]:
            self.tempPDefense += self.fightStatus.armorBonus(self.tempArmorType)
            self.tempMDefense += self.fightStatus.armorBonus(self.tempArmorType)

    # Set fighter class
    # param [in] fightclass: string of the warrior class
    # return warrior class object
    def fightClassSelection(self, fightclass):
        if fightclass == "Warrior":
            return Warrior()
        elif fightclass == "Spellcaster":
            self.spellPoint += 2
            return Spellcaster()
        elif fightclass == "Tank":
            return Tank()
        elif fightclass == "Ranger":
            return Ranger()

    # Set personnal infos for the character
    # Normally called only once
    def personnalID(self, race="human", gender="male", name="John", fightclass="Warrior", loaded=False):
        self.name = name
        self.gender = gender
        self.race = race
        self.fightclass = self.fightClassSelection(fightclass)
        self.fightclass.classbonus(self, loaded)

    # Set a sprite object for the character
    # param [in] spritetag: key value to get a sprite list in player_sprite_tags
    def setSprites(self, spritetag):
        self.sprite = PlayerSprite(spr.player_sprite_tags.get(spritetag))

    # Method to add xp to the character
    # also use to return dialogs for display in interface
    def giveXp(self, xpreward):
        dialogList = []
        self.xp += xpreward
        if levelchart[self.level - 1] <= self.xp:
            dialog = "{} gained a new level!".format(self)
            dialogList.append(dialog)
            dialogs1 = self.fightclass.levelingup(self)
            self.statPoint += 2
            self.addAbilityWeaponStatus()
            for i in dialogs1:
                dialogList.append(i)
        return dialogList

    # Overwrite method after adding the hit to fight statistics
    def damageResist(self, p_damage, p_damageType, piercing=0):
        if self.tempArmorType == "light":
            self.fightStatus.lightArmor.hitTaken()
        elif self.tempArmorType == "medium":
            self.fightStatus.mediumArmor.hitTaken()
        elif self.tempArmorType == "heavy":
            self.fightStatus.heavyArmor.hitTaken()
        return super().damageResist(p_damage=p_damage, p_damageType=p_damageType, piercing=piercing)

    def display(self):
        print("---Character's sheet:---")
        print("Character name: ", self.name)
        print("Gender: ", self.gender)
        print("Race: ", self.race)
        print("Fighting class: ", self.fightclass)
        print("Skill points: ", self.skillPoint)
        print("Spell points: ", self.spellPoint)
        print("--------Stat---------")
        super().display()
        print("Experience points: ", self.xp)
        self.fightStatus.display()
        print("\n")

    def displayInfo(self):
        list_1 = []
        if len(self.passiveStatusEffects) + len(self.activeStatusEffects) == 0:
            list_1.append("Status effect:    normal")
        else:
            effectlist = ""
            for status in (self.passiveStatusEffects + self.activeStatusEffects):
                if status.element == "fire":
                    effectlist = effectlist + "burned "
                elif status.element == "poison":
                    effectlist = effectlist + "poisoned "
            list_1.append("Status effect:    {}".format(effectlist))
        list_1.append("Health points:     {}/{}".format(self.hpleft, self.maxhp))
        list_1.append("Mana points :      {}/{}".format(self.manaleft, self.maxmana))
        list_1.append("Stamina points :  {}/{}".format(self.stamleft, self.maxstam))
        list_1.append("")
        list_1.append("Base stats")
        list_1.append("Constitution:  {}".format(int(self.const)))
        list_1.append("Strength:         {}".format(int(self.strength)))
        list_1.append("Intelligence:     {}".format(int(self.intel)))
        list_1.append("Agility:             {}".format(int(self.agility)))
        list_1.append("Defense:           {}".format(int(self.defense)))
        list_1.append("Luck:                  {}".format(int(self.luck)))
        list_2 = [
            self.__str__(),
            str(self.fightclass),
            "Level:   {}".format(self.level),
            "Exp.  points:  {}".format(self.xp)
        ]
        return [list_1, list_2]

    def displayLevelUpInfo(self):
        return [
            self.__str__(),
            str(self.fightclass),
            "Level:   {}".format(self.level),
            "Exp.  points:  {}".format(self.xp),
            "Bonus stat points left: {}".format(self.statPoint),
            "Skill points left: {}".format(self.skillPoint),
            "Spell points left: {}".format(self.spellPoint),
            "",
            "Constitution:     {}".format(int(self.const)),
            "Strength:            {}".format(int(self.strength)),
            "Intelligence:       {}".format(int(self.intel)),
            "Agility:                {}".format(int(self.agility))
        ]

    def fight_display(self):
        print("---Character's sheet:---")
        print("Character name: ", self.name)
        print("Gender: ", self.gender)
        print("Race: ", self.race)
        print("Fighting class: ", self.fightclass)
        print("Skill points: ", self.skillPoint)
        print("Spell points: ", self.spellPoint)
        print("--------Stat---------")
        super().fight_display()
        self.fightStatus.display()
        print("\n")

    # return the type as a string
    def search(self):
        return "Player"

    # Add new action ability, need update ^^
    def addAbilitySkill(self, ability):
        self.moveset.append(ability)

    def addPassiveSkill(self, passive):
        self.passives.append(passive)

    def addSpellSkill(self, spell):
        print("Spell addition called")
        self.spellList.append(spell)

    def spellUsed(self, spell):
        self.fightStatus.spellUsed(spell)

    def spellBonus(self, spell):
        return self.fightStatus.spellBonus(spell)

    def weaponKill(self):
        self.fightStatus.weaponKill(self)

    def weaponLevelUp(self):
        return self.fightStatus.weaponLevelUp(self)

    def armorLevelUp(self):
        return self.fightStatus.armorLevelUp(self.tempArmorType, self)

    def spellLevelUp(self):
        return self.fightStatus.spellLevelUp(self)

    def isStatPointLeft(self):
        return self.statPoint > 0

    def useStatPoint(self, stat):
        if stat == "Constitution":
            self.const += 1
        elif stat == "Strength":
            self.strength += 1
        elif stat == "Intelligence":
            self.intel += 1
        elif stat == "Agility":
            self.agility += 1
        self.statPoint -= 1
        self.updateSubStat()
