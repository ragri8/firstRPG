from core.Item import Item_Class


# Weapon class, deal with weapon object type
# stats are used for combat skills power
# speed attribute is multiplicator used for building up the number of strikes performed
#           in a single attack
class Weapon(Item_Class):
    def __init__(self, name, baseattack=0, damagedice=0, nbrdmgdice=0, wpntype=None,
                 lvl=1, gold=0, speed=1, element="None", magic_damage=0, description=""):
        super().__init__(name, description=description)
        self.baseattack = baseattack
        self.damagedice = damagedice
        self.nbrdmgdice = nbrdmgdice
        self.lvl = lvl
        self.price = gold
        self.wpntype = wpntype
        self.bonus = 0
        self.speed = speed
        self.element = element
        self.magicDamage = magic_damage

    def refine(self, player):
        print("Trying to refine {}'s weapon".format(player))
        if self.lvl == 0:
            print("Weapon cannot be more refined \n")
        else:
            self.name = "Refined {}".format(self.name)
            self.baseattack += self.lvl
            print("Refining succeded!")
            print("Weapon gained +{} base attack!\n".format(self.lvl))
            self.bonus = self.lvl
            self.lvl = 0

    def getDescription(self):
        return [self.description,
                self.getAttackDescription(),
                "Weapon type: {}".format(self.wpntype)]

    def getAttackDescription(self):
        return "Attack: {}-{}".format(self.baseattack + self.nbrdmgdice,
                                      self.baseattack + self.damagedice * self.nbrdmgdice)

    def display(self):
        print("Weapon type: ", self.wpntype)
        print("Damage: {}-{} \n".format(self.baseattack + self.nbrdmgdice,
                                        self.baseattack + self.nbrdmgdice * self.damagedice))

    def item_type(self):
        return "Weapon"

    def isMagic(self):
        return False


class MagicWeapon(Weapon):
    def __init__(self, name, baseattack=0, damagedice=0, nbrdmgdice=0, wpntype=None,
                 lvl=1, gold=0, speed=1, element="fire", magic_damage=3, description=""):
        super().__init__(name, baseattack, damagedice, nbrdmgdice, wpntype, lvl, gold,
                         speed, element, magic_damage, description)

    def isMagic(self):
        return True

    def getDescription(self):
        description = super().getDescription()
        description.append("Element: {}".format(self.element))

    def getAttackDescription(self):
        return "Attack: {}-{} + {}".format(self.baseattack + self.nbrdmgdice,
                                           self.baseattack + self.damagedice * self.nbrdmgdice,
                                           self.magicDamage)
