from enum import Enum

MOVETYPES = ["Attack", "Status", "Combo"]
MOVETARGET = ["Party", "Enemy", "Fallen", "Personal"]

AttackTypes = ["Attack", "Offensive spell", "Heal spell", "Status spell", "Buff spell", "Defend"]
moveList = ["Attack", "Cast spell", "Use item", "Defend", "Flee"]

# enum object used to follow the phase of the fight
# "move": selection of a category of ability on player's turn
# "ability": selection of an ability to do on player's turn
# "target": selection of a target
# "resume": only display a resume of the turn
# "enemy turn": only display resume of the enemy turn
selectPhase = ["move", "ability", "target", "resume", "enemy turn", "victory", "defeat", "flee", "intro", "pre turn"]


class Menu_Mode(Enum):
    FIGHTER = 0
    SPELL = 1
    ITEM = 2
    LEVELUP = 3
    OPTION = 4
    CANCEL = 5
    TARGET = 6
    CONFIRM = 7
    ITEMUSE = 8
    SKILLS = 9

    @staticmethod
    def getMode(index):
        if index == 0:
            return Menu_Mode.FIGHTER
        elif index == 1:
            return Menu_Mode.SPELL
        elif index == 2:
            return Menu_Mode.ITEM
        elif index == 3:
            return Menu_Mode.LEVELUP
        elif index == 4:
            return Menu_Mode.OPTION

menu_mode = ["Fighters", "Spells", "Items", "level up", "Options", "Cancel"]

# real mode should include:
# "Fighters" => None
# "Spells" => "select spell"(with index) => "select target" => apply spell and return to "select spell"
# "Items" => "select item"(with index)  =>  "equip" => if equipable and not already equiped
#                                           "unequip" => if already equiped
#                                           "use" => "select target" => apply on target and delete if consummable
#                                           "move" => "select target" => move object and return to "select item"
#                                           "toss" => "confirm" => toss it
# "level up" => "select character" => "select skill" => "confirm" => apply skill
# "Options" =>  "save game" => "select file" => "confirm"
#               "load game" => "select file" => "confirm"
#               "other"
# "Cancel" => quit menu
#
# Use a factory
