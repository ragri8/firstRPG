class Boss:
    def __init__(self, location, sprite, boss_party, boss_tag, size="normal"):
        self.tag = boss_tag
        self.location = location
        self.sprite = sprite
        self.party = boss_party
        self.size = size
        self.display = True

    def beaten(self):
        self.display = False
