

# handling class for skill unlock;
# a skill can either be an active ability, a passive or a spell
# param [in] skill: skill to unlock
# param [in] prerequite_skills: list of skills needed to unlock this one
# param [in] prerequite_levels: list of abilities level needed to unlock the skill
# param [in] cost: skillPoint cost to unlock the skill
class Skill_Unlock:
    def __init__(self, skill, prerequite_skills, prerequite_levels, cost):
        self.skill = skill  # skill to unlock
        self.isUnlocked = False  # if skill is unlocked or not
        self.prerequite_skills = prerequite_skills  # required skills list
        self.prerequite_levels = prerequite_levels  # required levels list
        self.cost = cost  # skill cost to unlock

    def checkPrerequite(self, fighter):
        unlockable = True
        current_skills = []
        print("Trying to unlock {}".format(self.skill.name))
        for skill in fighter.moveset:
            current_skills.append(skill.name)
        for skill in fighter.passives:
            current_skills.append(skill.name)
        for skill in fighter.spellList:
            current_skills.append(skill.name)
        for skill in self.prerequite_skills:  # check required skills
            if skill not in current_skills:
                unlockable = False
                print("{}'s skill required".format(skill.name))
                break
        for stat_key in self.prerequite_levels.keys():  # check required levels
            if stat_key == "level":
                if self.prerequite_levels[stat_key] > fighter.level:
                    unlockable = False
                    print("Level too low")
                    break
            elif self.prerequite_levels[stat_key] > fighter.fightStatus.stat_level(stat_key):
                unlockable = False
                print("{}'s level too low".format(stat_key))
                break
        return unlockable

    # add a new skill to a fighter
    # return: success or failure of setting the new skill
    def setSkill(self, fighter):
        if not self.isUnlocked and self.checkPrerequite(fighter) and fighter.skillPoint >= self.cost:
            fighter.skillPoint -= self.cost
            fighter.addAbilitySkill(self.skill)
            self.isUnlocked = True
            return True
        else:
            return False


class Spell_Unlock(Skill_Unlock):
    def __init__(self, skill, prerequite_skills, prerequite_levels, cost):
        super().__init__(skill, prerequite_skills, prerequite_levels, cost)

    # add a new spell to a fighter
    # return: success or failure of setting the new spell
    def setSkill(self, fighter):
        if not self.isUnlocked and self.checkPrerequite(fighter) and fighter.spellPoint >= self.cost:
            fighter.spellPoint -= self.cost
            fighter.addSpellSkill(self.skill)
            self.isUnlocked = True
            return True
        else:
            print("Failed to add a new spell")
            return False


class Passive_Unlock(Skill_Unlock):
    def __init__(self, skill, prerequite_skills, prerequite_levels, cost):
        super().__init__(skill, prerequite_skills, prerequite_levels, cost)

    # add a new passive skill to a fighter
    # return: success or failure of setting the new passive
    def setSkill(self, fighter):
        if not self.isUnlocked and self.checkPrerequite(fighter) and fighter.skillPoint >= self.cost:
            fighter.skillPoint -= self.cost
            fighter.addPassiveSkill(self.skill)
            self.isUnlocked = True
            return True
        else:
            return False
