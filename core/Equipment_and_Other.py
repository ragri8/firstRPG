import random
from core.Item import Item_Class


class Equipment(Item_Class):
    def __init__(self, name, pdefense=0, mdefense=0, equip_type="None",
                 armorClass="light", lvl=1, gold=0, weight=0, description=""):
        super().__init__(name, description)
        self.pdefense = pdefense
        self.mdefense = mdefense
        self.lvl = lvl
        self.price = gold
        self.defensetype = [self.pdefense, self.mdefense]
        self.armorClass = armorClass
        self.weight = weight

    def gearSet(self):
        pass

    def item_type(self):
        return "Equipment"

    def getDescription(self):
        return [super().getDescription(),
                "Physical resistance: {}".format(self.pdefense),
                "Magical resistance: {}".format(self.mdefense),
                "Armor class type: {}".format(self.armorClass)]

    def getDefenseDescription(self):
        return "Physical resistance: {}, magical resistance: {}".format(self.pdefense, self.mdefense)


class HeadGear(Equipment):
    def __init__(self, name, pdefense, mdefense, equip_type, armorClass, lvl=1, gold=0, weight=0, description=""):
        super().__init__(name=name, pdefense=pdefense, mdefense=mdefense, equip_type=equip_type,
                         armorClass=armorClass, lvl=lvl, gold=gold, weight=weight, description=description)

    def gearSet(self):
        return "Head"


class ChestGear(Equipment):
    def __init__(self, name, pdefense, mdefense, equip_type, armorClass, lvl=1, gold=0, weight=0, description=""):
        super().__init__(name=name, pdefense=pdefense, mdefense=mdefense, equip_type=equip_type,
                         armorClass=armorClass, lvl=lvl, gold=gold, weight=weight, description=description)

    def gearSet(self):
        return "Body"


class ArmGear(Equipment):
    def __init__(self, name, pdefense, mdefense, equip_type, armorClass, lvl=1, gold=0, weight=0, description=""):
        super().__init__(name=name, pdefense=pdefense, mdefense=mdefense, equip_type=equip_type,
                         armorClass=armorClass, lvl=lvl, gold=gold, weight=weight, description=description)

    def gearSet(self):
        return "Hand"


class Shield(Equipment):
    def __init__(self, name, pdefense, mdefense, equip_type, armorClass, lvl=1, gold=0, weight=0, description=""):
        super().__init__(name=name, pdefense=pdefense, mdefense=mdefense, equip_type=equip_type,
                         armorClass=armorClass, lvl=lvl, gold=gold, weight=weight, description=description)

    def gearSet(self):
        return "Shield"

# todo: create boots

def createEquipment(p_equipment):
    gearType = p_equipment[3]
    if gearType == "Head":
        armor = HeadGear(*p_equipment)
    elif gearType == "Body":
        armor = ChestGear(*p_equipment)
    elif gearType == "Hand":
        armor = ArmGear(*p_equipment)
    elif gearType == "Shield":
        armor = Shield(*p_equipment)
    return armor


def dice(dsize):
    if dsize == 0:
        return 0
    dvalue = random.randint(1, dsize)
    return dvalue
