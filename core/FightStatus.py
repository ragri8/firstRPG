

class FighterStatus:
    def __init__(self, p_fighter):
        pass


class PlayerStatus(FighterStatus):
    def __init__(self, p_player):
        super().__init__(p_player)
        self.sword = WeaponStat("sword")
        self.axe = WeaponStat("axe")
        self.rapier = WeaponStat("rapier")
        self.mace = WeaponStat("mace")
        self.spear = WeaponStat("spear")
        self.staff = WeaponStat("staff")
        self.bow = WeaponStat("bow")
        self.fireSpell = SpellStat("fire")
        self.iceSpell = SpellStat("ice")
        self.earthSpell = SpellStat("earth")
        self.electricSpell = SpellStat("electric")
        self.healingSpell = SpellStat("healing")
        self.buffSpell = SpellStat("buff")
        self.lightArmor = ArmorStat("light")
        self.mediumArmor = ArmorStat("medium")
        self.heavyArmor = ArmorStat("heavy")

    def getWeaponStatList(self):
        return [self.sword, self.axe, self.rapier, self.mace, self.spear, self.staff, self.bow]

    def getSpellStatList(self):
        return [self.fireSpell, self.iceSpell, self.earthSpell, self.electricSpell, self.healingSpell, self.buffSpell]

    def spellUsed(self, p_type):
        if p_type == "fire":
            self.fireSpell.spellUsed()
        elif p_type == "ice":
            self.iceSpell.spellUsed()
        elif p_type == "earth":
            self.earthSpell.spellUsed()
        elif p_type == "electric":
            self.electricSpell.spellUsed()
        elif p_type == "healing" or p_type == "heal":
            self.healingSpell.spellUsed()
        elif p_type == "buff":
            self.buffSpell.spellUsed()

    def weaponKill(self, player):
        if player.weapon.wpntype == "sword":
            self.sword.finalBlow()
        elif player.weapon.wpntype == "axe":
            self.axe.finalBlow()
        elif player.weapon.wpntype == "rapier":
            self.rapier.finalBlow()
        elif player.weapon.wpntype == "mace":
            self.mace.finalBlow()
        elif player.weapon.wpntype == "spear":
            self.spear.finalBlow()
        elif player.weapon.wpntype == "staff":
            self.staff.finalBlow()
        elif player.weapon.wpntype == "bow":
            self.bow.finalBlow()

    def weaponUsed(self, p_player):
        if p_player.weapon.wpntype == "sword":
            self.sword.finalBlow()
        elif p_player.weapon.wpntype == "axe":
            self.axe.finalBlow()
        elif p_player.weapon.wpntype == "rapier":
            self.rapier.finalBlow()
        elif p_player.weapon.wpntype == "mace":
            self.mace.finalBlow()
        elif p_player.weapon.wpntype == "spear":
            self.spear.finalBlow()
        elif p_player.weapon.wpntype == "staff":
            self.staff.finalBlow()
        elif p_player.weapon.wpntype == "bow":
            self.bow.finalBlow()

    def armorHit(self, player):
        if player.tempArmorType == "light":
            self.lightArmor.hitTaken()
        elif player.tempArmorType == "medium":
            self.mediumArmor.hitTaken()
        elif player.tempArmorType == "heavy":
            self.heavyArmor.hitTaken()

    def weaponBonus(self, p_player):
        weaponLevel = 0
        if p_player.weapon.wpntype == "sword":
            weaponLevel = self.sword.level
        elif p_player.weapon.wpntype == "axe":
            weaponLevel = self.axe.level
        elif p_player.weapon.wpntype == "rapier":
            weaponLevel = self.rapier.level
        elif p_player.weapon.wpntype == "mace":
            weaponLevel = self.mace.level
        elif p_player.weapon.wpntype == "spear":
            weaponLevel = self.spear.level
        elif p_player.weapon.wpntype == "staff":
            weaponLevel = self.staff.level
        elif p_player.weapon.wpntype == "bow":
            weaponLevel = self.bow.level
        bonus = 1 + weaponLevel * 0.1
        return bonus

    def spellBonus(self, p_spell):
        spellLevel = 0
        if p_spell.typeEffect == "fire":
            spellLevel = self.fireSpell.level
        elif p_spell.typeEffect == "ice":
            spellLevel = self.iceSpell.level
        elif p_spell.typeEffect == "earth":
            spellLevel = self.earthSpell.level
        elif p_spell.typeEffect == "electric":
            spellLevel = self.electricSpell.level
        elif p_spell.typeEffect == "healing":
            spellLevel = self.healingSpell.level
        elif p_spell.typeEffect == "buff":
            spellLevel = self.buffSpell.level
        bonus = 1 + (spellLevel * 0.1)
        return bonus

    def armorBonus(self, p_armorType):
        armorLevel = 0
        if p_armorType == "light":
            armorLevel = self.lightArmor.level
        elif p_armorType == "medium":
            armorLevel = self.mediumArmor.level
        elif p_armorType == "heavy":
            armorLevel = self.heavyArmor.level
        return armorLevel

    def stat_level(self, key):
        max_level = 0
        if key == "weapon":
            max_level = max([self.sword.level, self.axe.level, self.rapier.level, self.mace.level, self.spear.level,
                             self.staff.level, self.bow.level])
        elif key == "thrust":
            max_level = max([self.sword.level, self.rapier.level, self.spear.level])
        elif key == "blunt":
            max_level = max([self.mace.level, self.staff.level])
        elif key == "fire":
            max_level = self.fireSpell.level
        elif key == "ice":
            max_level = self.iceSpell.level
        elif key == "electric":
            max_level = self.electricSpell.level
        elif key == "earth":
            max_level = self.earthSpell.level
        elif key == "healing":
            max_level = self.healingSpell.level
        elif key == "buff":
            max_level = self.buffSpell.level
        return max_level

    def weaponLevelUp(self, p_player):
        dialogList = []
        weaponStatList = [self.sword, self.axe, self.rapier, self.mace, self.spear, self.staff, self.bow]
        weaponList = ["sword", "axe", "rapier", "mace", "spear", "staff", "bow"]
        weaponIndex = weaponList.index(p_player.weapon.wpntype)
        if weaponStatList[weaponIndex].level < 1 and weaponStatList[weaponIndex].weaponKill >= 10:
            weaponStatList[weaponIndex].levelUp()
            dialog = "{} gained a {} handling level!".format(p_player, weaponList[weaponIndex])
            print(dialog)
            dialogList.append(dialog)
        elif weaponStatList[weaponIndex].level < 2 and weaponStatList[weaponIndex].weaponKill >= 30:
            weaponStatList[weaponIndex].levelUp()
            dialog = "{} gained a {} handling level!".format(p_player, weaponList[weaponIndex])
            print(dialog)
            dialogList.append(dialog)
        elif weaponStatList[weaponIndex].level < 3 and weaponStatList[weaponIndex].weaponKill >= 60:
            weaponStatList[weaponIndex].levelUp()
            dialog = "{} gained a {} handling level!".format(p_player, weaponList[weaponIndex])
            print(dialog)
            dialogList.append(dialog)
        elif weaponStatList[weaponIndex].level < 4 and weaponStatList[weaponIndex].weaponKill >= 100:
            weaponStatList[weaponIndex].levelUp()
            dialog = "{} gained a {} handling level!".format(p_player, weaponList[weaponIndex])
            print(dialog)
            dialogList.append(dialog)
        elif weaponStatList[weaponIndex].level < 5 and weaponStatList[weaponIndex].weaponKill >= 150:
            weaponStatList[weaponIndex].levelUp()
            dialog = "{} gained a {} handling level!".format(p_player, weaponList[weaponIndex])
            print(dialog)
            dialogList.append(dialog)
        elif weaponStatList[weaponIndex].level < 6 and weaponStatList[weaponIndex].weaponKill >= 210:
            weaponStatList[weaponIndex].levelUp()
            dialog = "{} gained a {} handling level!".format(p_player, weaponList[weaponIndex])
            print(dialog)
            dialogList.append(dialog)
        elif weaponStatList[weaponIndex].level < 7 and weaponStatList[weaponIndex].weaponKill >= 280:
            weaponStatList[weaponIndex].levelUp()
            dialog = "{} gained a {} handling level!".format(p_player, weaponList[weaponIndex])
            print(dialog)
            dialogList.append(dialog)
        elif weaponStatList[weaponIndex].level < 8 and weaponStatList[weaponIndex].weaponKill >= 360:
            weaponStatList[weaponIndex].levelUp()
            dialog = "{} gained a {} handling level!".format(p_player, weaponList[weaponIndex])
            print(dialog)
            dialogList.append(dialog)
        elif weaponStatList[weaponIndex].level < 9 and weaponStatList[weaponIndex].weaponKill >= 450:
            weaponStatList[weaponIndex].levelUp()
            dialog = "{} gained a {} handling level!".format(p_player, weaponList[weaponIndex])
            print(dialog)
            dialogList.append(dialog)
        elif weaponStatList[weaponIndex].level < 10 and weaponStatList[weaponIndex].weaponKill >= 550:
            weaponStatList[weaponIndex].levelUp()
            dialog = "{} gained a {} handling level!".format(p_player, weaponList[weaponIndex])
            print(dialog)
            dialogList.append(dialog)
        return dialogList

    def spellLevelUp(self, p_player):
        dialogList = []
        spellStatList = [self.fireSpell, self.iceSpell, self.earthSpell,
                         self.electricSpell, self.healingSpell, self.buffSpell]
        for i in spellStatList:
            if i.level < 1 and i.numberUse >= 5:
                i.levelUp()
                dialog = "{} gained a level in {} spellcast!".format(p_player, i.type)
                print(dialog)
                dialogList.append(dialog)
            elif i.level < 2 and i.numberUse >= 15:
                i.levelUp()
                dialog = "{} gained a level in {} spellcast!".format(p_player, i.type)
                print(dialog)
                dialogList.append(dialog)
            elif i.level < 3 and i.numberUse >= 30:
                i.levelUp()
                dialog = "{} gained a level in {} spellcast!".format(p_player, i.type)
                print(dialog)
                dialogList.append(dialog)
            elif i.level < 4 and i.numberUse >= 50:
                i.levelUp()
                dialog = "{} gained a level in {} spellcast!".format(p_player, i.type)
                print(dialog)
                dialogList.append(dialog)
            elif i.level < 5 and i.numberUse >= 75:
                i.levelUp()
                dialog = "{} gained a level in {} spellcast!".format(p_player, i.type)
                print(dialog)
                dialogList.append(dialog)
            elif i.level < 6 and i.numberUse >= 105:
                i.levelUp()
                dialog = "{} gained a level in {} spellcast!".format(p_player, i.type)
                print(dialog)
                dialogList.append(dialog)
            elif i.level < 7 and i.numberUse >= 140:
                i.levelUp()
                dialog = "{} gained a level in {} spellcast!".format(p_player, i.type)
                print(dialog)
                dialogList.append(dialog)
            elif i.level < 8 and i.numberUse >= 180:
                i.levelUp()
                dialog = "{} gained a level in {} spellcast!".format(p_player, i.type)
                print(dialog)
                dialogList.append(dialog)
            elif i.level < 9 and i.numberUse >= 225:
                i.levelUp()
                dialog = "{} gained a level in {} spellcast!".format(p_player, i.type)
                print(dialog)
                dialogList.append(dialog)
            elif i.level < 10 and i.numberUse >= 275:
                i.levelUp()
                dialog = "{} gained a level in {} spellcast!".format(p_player, i.type)
                print(dialog)
                dialogList.append(dialog)
        return dialogList

    def armorLevelUp(self, p_type, p_player):
        dialogList = []
        armorStatList = [self.lightArmor, self.mediumArmor, self.heavyArmor]
        armorList = ["light", "medium", "heavy"]
        if p_type not in armorList:
            return []
        armorIndex = armorList.index(p_type)
        armorLevel = armorStatList[armorIndex].level
        numberHit = armorStatList[armorIndex].numberHit
        isLevelingUp = False
        if armorLevel < 1 and numberHit >= 20:
            isLevelingUp = True
        elif armorLevel < 2 and numberHit >= 60:
            isLevelingUp = True
        elif armorLevel < 3 and numberHit >= 120:
            isLevelingUp = True
        elif armorLevel < 4 and numberHit >= 200:
            isLevelingUp = True
        elif armorLevel < 5 and numberHit >= 300:
            isLevelingUp = True
        elif armorLevel < 6 and numberHit >= 420:
            isLevelingUp = True
        elif armorLevel < 7 and numberHit >= 560:
            isLevelingUp = True
        elif armorLevel < 8 and numberHit >= 720:
            isLevelingUp = True
        elif armorLevel < 9 and numberHit >= 900:
            isLevelingUp = True
        elif armorLevel < 10 and numberHit >= 1100:
            isLevelingUp = True
        if isLevelingUp:
            armorStatList[armorIndex].levelUp()
            dialog = "{} gained a level in {} armor!".format(p_player, armorStatList[armorIndex].type)
            print(dialog)
            dialogList.append(dialog)
        return dialogList

    def display(self):
        print("-----fight stat------")
        print("-----Weapon level----")
        print("Sword: {}".format(self.sword.level))
        print("Axe: {}".format(self.axe.level))
        print("Rapier: {}".format(self.rapier.level))
        print("Mace: {}".format(self.mace.level))
        print("Spear: {}".format(self.spear.level))
        print("Staff: {}".format(self.staff.level))
        print("Bow: {}".format(self.bow.level))
        print("-----Spell level-----")
        print("Fire spell: {}".format(self.fireSpell.level))
        print("Ice spell: {}".format(self.iceSpell.level))
        print("Earth spell: {}".format(self.earthSpell.level))
        print("Electric spell: {}".format(self.electricSpell.level))
        print("Healing spell: {}".format(self.healingSpell.level))
        print("Buff spell {}".format(self.buffSpell.level))
        print("-----Armor level-----")
        print("Light armor: {}".format(self.lightArmor.level))
        print("Medium armor: {}".format(self.mediumArmor.level))
        print("Heavy armor: {}".format(self.heavyArmor.level))

    def fightHistoricDisplay(self, p_player):
        print("\nCharacter: {}".format(p_player))
        print("-----Fighting historic-----")
        print("-------Final blows-------")
        print("Sword: {}".format(self.sword.weaponKill))
        print("Axe: {}".format(self.axe.weaponKill))
        print("Rapier: {}".format(self.rapier.weaponKill))
        print("Mace: {}".format(self.mace.weaponKill))
        print("Spear: {}".format(self.spear.weaponKill))
        print("Staff: {}".format(self.staff.weaponKill))
        print("Bow: {}".format(self.bow.weaponKill))
        print("-------Spell used--------")
        print("Fire spell: {}".format(self.fireSpell.numberUse))
        print("Ice spell: {}".format(self.iceSpell.numberUse))
        print("Earth spell: {}".format(self.earthSpell.numberUse))
        print("Electric spell: {}".format(self.electricSpell.numberUse))
        print("Healing spell: {}".format(self.healingSpell.numberUse))
        print("Buff spell {}".format(self.buffSpell.level))
        print("------Armor block--------")
        print("Light armor: {}".format(self.lightArmor.numberHit))
        print("Medium armor: {}".format(self.mediumArmor.numberHit))
        print("Heavy armor: {}".format(self.heavyArmor.numberHit))


class BaseStat:
    def __init__(self, p_type):
        self.type = p_type
        self.level = 0

    def levelUp(self):
        self.level += 1

    def setLevel(self, p_level):
        self.level = p_level


class WeaponStat(BaseStat):
    def __init__(self, p_type):
        super().__init__(p_type=p_type)
        self.weaponKill = 0

    def finalBlow(self):
        self.weaponKill += 1


class SpellStat(BaseStat):
    def __init__(self, p_type):
        super().__init__(p_type=p_type)
        self.numberUse = 0

    def spellUsed(self):
        self.numberUse += 1


class ArmorStat(BaseStat):
    def __init__(self, p_type):
        super().__init__(p_type=p_type)
        self.numberHit = 0

    def hitTaken(self):
        self.numberHit += 1
