from core.Skill_Unlock import Skill_Unlock, Spell_Unlock
from copy import deepcopy

# The requirement use a specific pattern, each possible requirement is here, but is set to 0 by default
# (level, sword, axe, rapier, mace, spear, staff, bow,
# fire, water/ice, earth, electric, healing, buff, light, medium, heavy)
# (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)


# class for Class specific skill tree
# used for display and access of fighter skill unlock
# uase a tree pattern to build
class Skill_Tree:
    def __init__(self, skills, spells, passives):
        self.skills = skills
        self.spells = spells
        self.passives = passives
        
    #def addNode(self, node):
    #    self.layer1.append(node)

    def copy(self):
        return deepcopy(self)


# specific skill node
# set a skill to unlock, its cost, and its requirements to unlock
# param [in] name: name of the skill
# param [in] ability: An Ability object
# param [in] cost: skill point cost to unlock
# param [in] level_requirements: a dict of requirement before unlock
# param [in] skill_requirement: skill to unlock before this one
class Skill_Node:
    def __init__(self, name, ability, cost, level_requirement, skill_requirement):
        self.name = name
        self.ability = ability
        self.cost = cost
        self.requirements = requirements
        self.isUnlocked = False

    def isRequirementSatisfied(self, fighter):
        requirement = False
        #if self.skill_type == "weapon skill":
        #    for stat in fighter.fightStatus.getWeaponStatList():
        #        if stat.level >= self.level:
        #            requirement = True
        return requirement

    def getSkill(self, fighter):
        if fighter.skillPoint >= self.cost:
            fighter.skillPoint -= self.cost
            self.isUnlocked = True
            return self.ability


class Stat_Node:
    def __init__(self, name, attribute):
        self.name = name
        self.attribute = attribute
        self.cost = 1
        self.level = 0

    def getSkill(self, fighter):
        pass

    def isRequirementSatisfied(self, fighter):
        requirement = False
        if fighter.level >= self.level+1:
            requirement = True
        return requirement


class Constitution_Node(Stat_Node):
    def __init__(self, name):
        super().__init__(name, "constitution")

    def getSkill(self, fighter):
        if fighter.skillPoint >= self.cost:
            fighter.skillPoint -= self.cost
            fighter.const += 1
            self.level += 1


class Ability_Node(Skill_Node):
    def __init__(self, name, ability, cost, level_requirement, skill_requirement=[]):
        super().__init__(name, ability, cost, level_requirement, skill_requirement)


class Spell_Node(Skill_Node):
    def __init__(self, name, spell, cost, level_requirement, skill_requirement):
        super().__init__(name, spell, cost, level_requirement, skill_requirement)


def requirements(level=1, weapon=0, sword=0, axe=0, rapier=0, mace=0, spear=0, staff=0, bow=0,
                 fire=0, water=0, earth=0, electric=0, healing=0, buff=0, light=0, medium=0, heavy=0):
    return {"level": level, "weapon": weapon, "sword": sword, "axe": axe, "rapier": rapier, "mace": mace,
            "spear": spear, "staff": staff, "bow": bow, "fire": fire, "water": water, "earth": earth,
            "electric": electric, "healing": healing, "buff": buff, "light": light, "medium": medium, "heavy": heavy}
