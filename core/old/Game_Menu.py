from core.Equipment_and_Other import HeadGear, ChestGear, ArmGear, Shield
from core.Item import ReviveItem, RecoveryItem
from core.Weapon import Weapon
from core.old.Abilities import healingSpellEffect
from interface.Pygame_Basic import *
from objects.old.Equipment_Sheet import *


class Game_Menu:
    def __init__(self, party, save, p_map, x_location=0, y_location=0):
        self.action = "None"
        self.closeMenu = False
        self.x_select = 0
        self.y_select = 0
        self.temp_select_1 = 0
        self.temp_select_2 = 0
        self.y_limit = 6
        self.x_limit = 0
        self.team = party
        self.party = party.members[:]
        self.save = save
        self.location = (x_location, y_location)
        self.p_map = p_map
        self.disabled = False

    def update(self, party, save, p_map):
        self.action = "None"
        self.closeMenu = False
        self.x_select = 0
        self.y_select = 0
        self.temp_select_1 = 0
        self.temp_select_2 = 0
        self.y_limit = 6
        self.x_limit = 0
        self.team = party
        self.party = party.members[:]
        self.save = save
        self.p_map = p_map
        self.disabled = False

    def menu_loop(self):
        new_map = None
        while not self.closeMenu:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.closeMenu = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_o:
                        for i in self.party:
                            i.organise_items()
                        print("Item reorganised")
                    if event.key == pygame.K_LEFT:
                        if self.x_select > 0:
                            self.x_select -= 1
                            self.y_select = 0
                    if event.key == pygame.K_RIGHT:
                        if self.x_select < self.x_limit:
                            self.x_select += 1
                            self.y_select = 0
                    if event.key == pygame.K_UP:
                        if self.y_select > 0:
                            self.y_select -= 1
                    if event.key == pygame.K_DOWN:
                        if self.y_select < self.y_limit:
                            self.y_select += 1
                    if event.key == pygame.K_m:
                        self.closeMenu = True
                    if event.key == pygame.K_x and self.action != "None":
                        self.action = "None"
                        self.y_select = 0
                    if event.key == pygame.K_SPACE:
                        if self.action == "item":
                            self.disabled = False
                        self.action = self.menu_window_action()
                        if self.action not in ["spell selected", "saving", "loading", "erasing", "item selected"]:
                            self.x_select = 0
                            self.y_select = 0
                        if self.action in ["item selected", "spell selected"] and not self.disabled:
                            self.temp_select_1 = self.x_select
                            self.temp_select_2 = self.y_select
                            self.x_select = 0
                            self.y_select = 0
            if self.action == "stat":
                self.x_limit = len(self.party)-1
                self.y_limit = 0
                self.display_menu_stat()
            elif self.action == "spell":
                self.x_limit = len(self.party)-1
                self.y_limit = len(self.party[self.x_select].spelllist)-1
                self.display_menu_spell(self.x_select, self.y_select)
            elif self.action == "spell selected":
                self.x_limit = 0
                self.y_limit = 1
                self.display_spell_use()
            elif self.action == "spell target":
                self.x_limit = 0
                self.y_limit = len(self.party) - 1
                self.display_spell_target()
            elif self.action == "spell completion":
                self.x_limit = 0
                self.y_limit = -1
                self.display_spell_completion()
            elif self.action == "item":
                self.x_limit = len(self.party) - 1
                self.y_limit = len(self.party[self.x_select].items)
                self.display_menu_items(self.x_select, self.y_select)
            elif self.action == "item selected":
                self.y_limit = 5
                self.display_item_use()
            elif self.action == "give item":
                self.y_limit = len(self.party) - 2
                self.give_item_display()
            elif self.action == "fight historic":
                self.x_limit = len(self.party) - 1
                self.y_limit = 0
                self.display_fight_historic()
            elif self.action == "option":
                self.x_limit = 0
                self.y_limit = 3
                self.menu_option()
            elif self.action == "save option":
                self.save_option()
            elif self.action == "load option":
                self.load_option()
            elif self.action == "erase option":
                self.erase_option()
            elif self.action == "close":
                self.closeMenu = True
            elif self.action == "saving":
                self.action = "close"
                self.save.newSavingChoice(self.y_select, self.p_map)
                self.y_select = 0
            elif self.action == "loading":
                self.action = "close"
                new_map = self.save.newLoadingChoice(self.y_select)
                self.closeMenu = True
            elif self.action == "erasing":
                self.action = "None"
                self.save.erasingFileChoice(self.y_select)
                self.y_select = 0
            else:
                self.display_menu_window(self.y_select)
                self.x_limit = 0
                self.y_limit = 6
            pygame.display.update()
            clock.tick(fps)
        return new_map

    def display_menu_window(self, option=0):
        menu_text_list = ["player stat", "player spell", "player item", "fight historic", "level up", "game option", "close menu", "", "", "", "", "", "", "", "", "", "", "Gold: {}".format(self.team.gold)]
        gameDisplay.fill(menu_blue)
        pygame.draw.rect(gameDisplay, black, (thin_border, thin_border, display_width - thin_border * 2, display_height - thin_border * 2))
        pygame.draw.rect(gameDisplay, menu_blue, (thin_border * 2, thin_border * 2, display_width - thin_border * 4, display_height - thin_border * 4))
        pygame.draw.rect(gameDisplay, pale_blue, (border-2, border+option*(menu_text_size+4)+4, 158, menu_text_size))
        pygame.draw.rect(gameDisplay, black, (border + 180, thin_border, thin_border, display_height - thin_border * 2))
        self.menu_display(menu_text_list)

    def display_menu_stat(self):
        stat_text_list_1 = []
        stat_text_list_2 = []
        player = self.party[self.x_select % len(self.party)]
        if len(player.fightStatus.statusEffect) == 0:
            stat_text_list_1.append("Status effect:    normal")
        else:
            effectlist = ""
            for i in player.fightStatus.statusEffect:
                if i.element == "fire":
                    effectlist = effectlist+"burned "
                elif i.element == "poison":
                    effectlist = effectlist+"poisoned "
            stat_text_list_1.append("Status effect:    {}".format(effectlist))
        stat_text_list_1.append("Health points:     {}/{}".format(player.hpleft, player.maxhp))
        stat_text_list_1.append("Mana points :      {}/{}".format(player.manaleft, player.maxmana))
        stat_text_list_1.append("Stamina points :  {}/{}".format(player.stamleft, player.maxstam))
        stat_text_list_2.append(player.__str__())
        stat_text_list_2.append(str(player.fightclass))
        stat_text_list_2.append("Level:   {}".format(player.level))
        stat_text_list_2.append("Exp.  points:  {}".format(player.xp))
        stat_text_list_1.append("")
        stat_text_list_1.append("Base stats")
        stat_text_list_1.append("Constitution:  {}".format(int(player.const)))
        stat_text_list_1.append("Strength:         {}".format(int(player.strength)))
        stat_text_list_1.append("Intelligence:     {}".format(int(player.intel)))
        stat_text_list_1.append("Agility:             {}".format(int(player.agility)))
        stat_text_list_1.append("Defense:           {}".format(int(player.defense)))
        stat_text_list_1.append("Luck:                  {}".format(int(player.luck)))
        self.display_menu_window(0)
        self.menu_display(stat_text_list_1, border+480, 10)
        self.menu_display(stat_text_list_2, border+190, 4*(menu_text_size+4))
        player.sprite.load()
        gameDisplay.blit(player.sprite.waiting(), (border+250, border+thin_border))

    def display_menu_spell(self, option, spell):
        stat_text_list_1 = [""]
        stat_text_list_2 = [""]
        stat_text_list_3 = [""]
        player = self.party[option % len(self.party)]
        stat_text_list_1.append("{}' spells".format(player.__str__()))
        stat_text_list_2.append("Mana cost:")
        stat_text_list_3.append("Spell type:")
        stat_text_list_1.append("")
        stat_text_list_2.append("")
        stat_text_list_3.append("")
        for i in player.spelllist:
            stat_text_list_1.append("  {}".format(i.__str__()))
            stat_text_list_2.append("  {}".format(i.manacost))
            stat_text_list_3.append("  {}".format(i.element))
        self.disabled = True
        if len(player.spelllist) != 0:
            selected_spell = player.spelllist[spell]
            if selected_spell.typeeffect in ["healing", "recovery"] and selected_spell.manacost <= player.manaleft:
                self.disabled = False
        self.display_menu_window(1)
        if spell <= 7:
            pygame.draw.rect(gameDisplay, pale_blue,
                             (border + 212, border + (spell + 3) * (menu_text_size + 4) + 12, 600, menu_text_size))
        else:
            pygame.draw.rect(gameDisplay, pale_blue,
                             (border + 212, border + 10 * (menu_text_size + 4) + 12, 600, menu_text_size))
        self.special_display(stat_text_list_1, border + 190, 10)
        self.special_display(stat_text_list_2, border + 450, 10)
        self.special_display(stat_text_list_3, border + 620, 10)

    def display_spell_use(self):
        choiceList = ["Do you want to use this spell?", "Yes", "No"]
        self.display_menu_spell(self.temp_select_1, self.temp_select_2)
        player = self.party[self.temp_select_1 % len(self.party)]
        self.dialog_box(self.y_select)
        self.dialog_display(choiceList)

    def display_spell_target(self):
        choiceList = ["Who would you like to use your spell on?"]
        self.display_menu_spell(self.temp_select_1, self.temp_select_2)
        for i in self.party:
            choiceList.append("-{}".format(i.__str__()))
        self.dialog_box(self.y_select)
        self.dialog_display(choiceList)

    def display_spell_completion(self):
        dialog = ["Spell successfully casted!"]
        self.display_menu_spell(self.temp_select_1, self.temp_select_2)
        self.dialog_box()
        self.dialog_display(dialog)

    def apply_spell(self, caster, spell, target):
        caster = self.party[caster]
        spell = caster.spelllist[spell]
        caster.manaleft -= spell.manacost
        if spell.nbrtarget == 1:
            target = self.party[target]
            print("caster: {}, spell: {}, target: {}".format(caster.__str__(), spell.__str__(), target.__str__()))
            if spell.typeeffect == "healing":
                healing = healingSpellEffect(caster, spell)
                target.hpleft += healing
                if target.hpleft >= target.maxhp:
                    target.hpleft = target.maxhp
            elif spell.typeeffect == "recovery":
                for i in target.fightStatus.statusEffect:
                    if i.element in ["poison", "fire", "frost"]:
                        target.fightStatus.statusEffect.remove(i)
        else:
            for i in self.party:
                print("caster: {}, spell: {}, target: {}".format(caster.__str__(), spell.__str__(), i.__str__()))
                if spell.typeeffect == "healing":
                    healing = healingSpellEffect(caster, spell)
                    i.hpleft += healing
                    if i.hpleft >= i.maxhp:
                        i.hpleft = i.maxhp

    def display_menu_items(self, option, item, stat_display=True):
        item_text_List = []
        item_stat_List = []
        player = self.party[option % len(self.party)]
        item_text_List.append("{}'s items:".format(player.__str__()))
        item_stat_List.append("  Item's stat")
        for i in player.items:
            item_text_List.append("  -{}".format(i.__str__()))
            if i.item_type() == "Weapon":
                item_stat_List.append("  Damage: {}-{}".format(i.baseattack+i.nbrdmgdice, i.baseattack+i.nbrdmgdice*i.damagedice))
            elif i.item_type() == "Equipment":
                item_stat_List.append("Pdef: {}   Mdef: {}".format(i.pdefense, i.mdefense))
            else:
                item_stat_List.append("")
        item_text_List.append("  -Cancel")
        item_text_List.append("")
        select_box_size = 260
        if stat_display:
            select_box_size += 260
        self.display_menu_window(2)
        pygame.draw.rect(gameDisplay, pale_blue, (border + 240, border + (item + 2) * (menu_text_size + 4) - 14, select_box_size, menu_text_size))
        self.menu_display(item_text_List, border + 190, 10)
        if stat_display:
            self.menu_display(item_stat_List, border + 580, 10)

    def display_item_use(self):
        choiceList = []
        self.display_menu_items(self.temp_select_1, self.temp_select_2, False)
        player = self.party[self.temp_select_1 % len(self.party)]
        choiceList.append("What do you want to do?")
        pygame.draw.rect(gameDisplay, pale_blue, (border + 500, border + (self.y_select + 2) * (menu_text_size + 4) - 10, 180, menu_text_size))
        self.menu_display(choiceList, border + 460, 10)
        self.display_special_choice(player, self.temp_select_2)

    def display_special_choice(self, player, item):
        choice_list = ["-Use", "-Equip", "-Unequip", "-Move", "-Show", "-Throw away"]
        item = player.items[item]
        for i in range(len(choice_list)):
            color = grey
            if i == 0 and item.item_type() == "Usable item":
                color = black
            elif i == 1:
                if item.item_type() == "Equipment":
                    color = black
                    for j in player.equipment:
                        if item == j:
                            color = grey
                elif item.item_type() == "Weapon":
                    if item != player.weapon and item != player.weaponSecondHand:
                        color = black
            elif i == 2:
                if item.item_type() == "Equipment":
                    color = grey
                    for j in player.equipment:
                        if item == j:
                            color = black
                elif item.item_type() == "Weapon":
                    if item == player.weapon or item == player.weaponSecondHand:
                        color = black
            elif i == 3:
                color = black
                if item.item_type() == "Equipment":
                    for j in player.equipment:
                        if item == j:
                            color = grey
                elif item.item_type() == "Weapon":
                    if item == player.weapon or item == player.weaponSecondHand:
                        color = grey
            elif i > 3:
                color = black
            if self.y_select == i:
                if color == black:
                    self.disabled = False
                else:
                    self.disabled = True
            TextSurf, TextRect = text_objects(choice_list[i], menuText, color)
            TextRect = (border+510, border+thin_border+(i+2)*(menu_text_size+4)-14)
            gameDisplay.blit(TextSurf, TextRect)

    def give_item_display(self):
        self.display_menu_items(self.temp_select_1, self.temp_select_2, False)
        choiceList = ["To who would you like",
                      "to give it?"]
        for i in range(len(self.party)):
            if i != self.temp_select_1:
                choiceList.append("  -{}".format(self.party[i].__str__()))
        pygame.draw.rect(gameDisplay, pale_blue,
                         (border + 500, border + (self.y_select + 3) * (menu_text_size + 4) - 12, 180, menu_text_size))
        self.menu_display(choiceList, border + 460, 10)

    def give_item_completion(self):
        receiver = None
        item = self.party[self.temp_select_1].items[self.temp_select_2]
        player_list = []
        for i in range(len(self.party)):
            if i != self.temp_select_1:
                player_list.append(self.party[i])
        receiver = player_list[self.y_select]
        self.party[self.temp_select_1].giveItem(item, receiver)

    def display_fight_historic(self):
        stat_text_list_1 = []
        stat_text_list_2 = []
        player = self.party[self.x_select % len(self.party)]
        stat_text_list_2.append(player.__str__())
        stat_text_list_2.append(str(player.fightclass))
        stat_text_list_2.append("Level:   {}".format(player.level))
        stat_text_list_2.append("Exp.  points:  {}".format(player.xp))
        stat_text_list_2.append("")
        stat_text_list_2.append("Weaponry level ")
        stat_text_list_2.append("   Sword:   {}".format(int(player.fightStatus.sword.level)))
        stat_text_list_2.append("   Rapier:   {}".format(int(player.fightStatus.rapier.level)))
        stat_text_list_2.append("   Axe:        {}".format(int(player.fightStatus.axe.level)))
        stat_text_list_2.append("   Mace:      {}".format(int(player.fightStatus.mace.level)))
        stat_text_list_2.append("   Staff:       {}".format(int(player.fightStatus.staff.level)))
        stat_text_list_1.append("")
        stat_text_list_1.append("Armor level ")
        stat_text_list_1.append("Light:        {}".format(int(player.fightStatus.lightArmor.level)))
        stat_text_list_1.append("Medium:    {}".format(int(player.fightStatus.mediumArmor.level)))
        stat_text_list_1.append("Heavy:       {}".format(int(player.fightStatus.heavyArmor.level)))
        stat_text_list_1.append("")
        stat_text_list_1.append("")
        stat_text_list_1.append("Spellcast level ")
        stat_text_list_1.append("Fire:        {}".format(int(player.fightStatus.fireSpell.level)))
        stat_text_list_1.append("Ice:          {}".format(int(player.fightStatus.iceSpell.level)))
        stat_text_list_1.append("Earth:      {}".format(int(player.fightStatus.earthSpell.level)))
        stat_text_list_1.append("Electric:  {}".format(int(player.fightStatus.electricSpell.level)))
        stat_text_list_1.append("Healing:  {}".format(int(player.fightStatus.healingSpell.level)))
        self.display_menu_window(3)
        self.menu_display(stat_text_list_1, border + 480, 10)
        self.menu_display(stat_text_list_2, border + 190, 4 * (menu_text_size + 4))
        player.sprite.load()
        gameDisplay.blit(player.sprite.waiting(), (border + 250, border + thin_border))

    def menu_option(self):
        textList = ["What do you want to do?"]
        choiceList = ["Save your file", "Load a file", "Erase a file", "Cancel"]
        self.display_menu_window(5)
        pygame.draw.rect(gameDisplay, pale_blue, (border+210, border+(self.y_select+2)*(menu_text_size+4)-4, 180, menu_text_size))
        self.menu_display(textList, border+180)
        self.menu_display(choiceList, border+180, menu_text_size*2)

    def save_option(self):
        textList = ["Where do you want to save?"]
        choiceList = []
        choiceList.append("-File 1, {}".format(self.save.firstSaveCondition))
        choiceList.append("-File 2, {}".format(self.save.secondSaveCondition))
        choiceList.append("-File 3, {}".format(self.save.thirdSaveCondition))
        choiceList.append("Cancel")
        self.display_menu_window(5)
        pygame.draw.rect(gameDisplay, pale_blue, (border + 210, border + (self.y_select + 2) * (menu_text_size + 4) - 4, 180, menu_text_size))
        self.menu_display(textList, border + 180)
        self.menu_display(choiceList, border + 180, menu_text_size * 2)

    def load_option(self):
        textList = ["Which file do you want to load?"]
        choiceList = []
        choiceList.append("-File 1, {}".format(self.save.firstSaveCondition))
        choiceList.append("-File 2, {}".format(self.save.secondSaveCondition))
        choiceList.append("-File 3, {}".format(self.save.thirdSaveCondition))
        choiceList.append("Cancel")
        self.display_menu_window(5)
        pygame.draw.rect(gameDisplay, pale_blue, (border + 210, border + (self.y_select + 2) * (menu_text_size + 4) - 4, 180, menu_text_size))
        self.menu_display(textList, border + 180)
        self.menu_display(choiceList, border + 180, menu_text_size * 2)

    def erase_option(self):
        textList = ["Which file do you want to permanently delete?"]
        choiceList = []
        choiceList.append("-File 1, {}".format(self.save.firstSaveCondition))
        choiceList.append("-File 2, {}".format(self.save.secondSaveCondition))
        choiceList.append("-File 3, {}".format(self.save.thirdSaveCondition))
        choiceList.append("Cancel")
        self.display_menu_window(5)
        pygame.draw.rect(gameDisplay, pale_blue, (border + 210, border + (self.y_select + 2) * (menu_text_size + 4) - 4, 180, menu_text_size))
        self.menu_display(textList, border + 180)
        self.menu_display(choiceList, border + 180, menu_text_size * 2)

    def menu_window_action(self):
        if self.action == "None":
            if self.y_select == 0:
                return "stat"
            elif self.y_select == 1:
                return "spell"
            elif self.y_select == 2:
                return "item"
            elif self.y_select == 3:
                return "fight historic"
            elif self.y_select == 4:
                for i in self.party:
                    i.givexp(50)
            elif self.y_select == 5:
                return "option"
            elif self.y_select == 6:
                return "close"
        elif self.action == "spell":
            if self.y_select > self.y_limit or self.disabled:
                print("y_select: {}, y_limit: {}".format(self.y_select, self.y_limit))
                if self.disabled:
                    print("Disabled")
                return "spell"
            else:
                return "spell selected"
        elif self.action == "spell selected":
            if self.y_select == 0:
                print("Spellcaster: {}, spell number used: {}".format(self.temp_select_1, self.temp_select_2))
                if self.party[self.temp_select_1].spelllist[self.temp_select_2].nbrtarget == 2:
                    self.apply_spell(self.temp_select_1, self.temp_select_2, self.y_select)
                    return "spell completion"
                else:
                    return "spell target"
            elif self.y_select == 1:
                return "spell"
        elif self.action == "spell target":
            self.apply_spell(self.temp_select_1, self.temp_select_2, self.y_select)
            return "spell completion"
        elif self.action == "spell completion":
            return "spell"
        elif self.action == "item":
            if self.y_select == self.y_limit:
                return "None"
            else:
                return "item selected"
        elif self.action == "item selected":
            if self.y_select == 0 and not self.disabled:
                return "item"
            elif self.y_select == 1 and not self.disabled:
                item = self.party[self.temp_select_1].items[self.temp_select_2]
                if item.item_type() == "Equipment":
                    print("working")
                    self.party[self.temp_select_1].newSetEquipment(item)
                elif item.item_type() == "Weapon":
                    print("working")
                    self.party[self.temp_select_1].newSetWeapon(item)
                return "item"
            elif self.y_select == 2 and not self.disabled:
                item = self.party[self.temp_select_1].items[self.temp_select_2]
                if item.item_type() == "Equipment":
                    self.party[self.temp_select_1].unsetEquipment(item)
                elif item.item_type() == "Weapon":
                    self.party[self.temp_select_1].unsetWeapon()
                return "item"
            elif self.y_select == 3 and not self.disabled:
                return "give item"
            elif self.y_select == 4:
                return "item"
            elif self.y_select == 5:
                return "item"
            elif self.y_select == 6:
                return "item"
        if self.action == "give item":
            self.give_item_completion()
            return "item"
        elif self.action == "option":
            if self.y_select == 0:
                return "save option"
            elif self.y_select == 1:
                return "load option"
            elif self.y_select == 2:
                return "erase option"
            elif self.y_select == 3:
                return "None"
        elif self.action == "save option":
            if self.y_select == 3:
                return "option"
            else:
                return "saving"
        elif self.action == "load option":
            if self.y_select == 3:
                return "option"
            else:
                return "loading"
        elif self.action == "erase option":
            if self.y_select == 3:
                return "option"
            else:
                return "erasing"
        return self.action

    def use_item(self):
        self.display_menu_items(self.temp_select_1, self.temp_select_2)
        player = self.temp_select_1
        item = self.temp_select_2
        if self.party[player].items[item].item_type() == "usable item":
            choiceList = ["On who would you like", "to use it?"]
            for i in range(len(self.party)):
                if i != self.temp_select_1:
                    choiceList.append("  -{}".format(self.party[i].__str__()))
        pygame.draw.rect(gameDisplay, pale_blue,
                         (border + 490, border + (self.y_select + 3) * (menu_text_size + 4) - 12, 180, menu_text_size))
        self.menu_display(choiceList, border + 450, 10)

    def menu_display(self, menu_text_list, x_position=0, y_position=0):
        for i in range(len(menu_text_list)):
            TextSurf, TextRect = text_objects(menu_text_list[i], menuText, black)
            TextRect = (border+x_position, border+thin_border+i*(menu_text_size+4)+y_position)
            gameDisplay.blit(TextSurf, TextRect)

    def special_display(self, display_list, x_position=0, y_position=0):
        if self.y_select <= 7:
            self.menu_display(display_list[0: 11], x_position, y_position)
        else:
            begin = self.y_select - 4
            end = begin + 8
            new_list = display_list[0: 3] + display_list[begin: end]
            self.menu_display(new_list, x_position, y_position)

    def dialog_box(self, option=-1):
        pygame.draw.rect(gameDisplay, black, (border, int(0.65*display_height), display_width-border*2, display_height*0.4-border))
        pygame.draw.rect(gameDisplay, menu_blue, (thin_border+border, int(0.65*display_height+thin_border), display_width-thin_border*2-border*2, display_height*0.4-thin_border*2-border))
        if option >= 0:
            pygame.draw.rect(gameDisplay, pale_blue, (border + 20, border + option * (menu_text_size + 4) + int(0.65*display_height+thin_border) - 6, 80, menu_text_size))

    def dialog_display(self, dialog_list, x_position=0, y_position=0):
        for i in range(len(dialog_list)):
            TextSurf, TextRect = text_objects(dialog_list[i], menuText, black)
            TextRect = (border*1.5+x_position, border+thin_border+i*menu_text_size+int(dialog_height*0.9)+y_position)
            gameDisplay.blit(TextSurf, TextRect)


class Menu_Seller(Game_Menu):
    def __init__(self, party):
        self.action = "None"
        self.active_item = None
        self.closeMenu = False
        self.x_select = 0
        self.y_select = 0
        self.temp_select_1 = 0
        self.temp_select_2 = 0
        self.y_limit = 2
        self.x_limit = 0
        self.party = party
        self.weaponList = []
        for i in [*swords, *rapiers, *axes, *maces, *staffs]:
            self.weaponList.append(Weapon(*i))
        self.equipmentList = []
        for i in [*equipmentT1, *equipmentT2, *equipmentT3]:
            if i[3] == "Head":
                self.equipmentList.append(HeadGear(*i))
            elif i[3] == "Body":
                self.equipmentList.append(ChestGear(*i))
            elif i[3] == "Hand":
                self.equipmentList.append(ArmGear(*i))
            elif i[3] == "Shield":
                self.equipmentList.append(Shield(*i))
        self.itemList = []
        for i in itemList:
            if i[0] == "recovery":
                self.itemList.append(RecoveryItem(*i))
            elif i[0] == "revive":
                self.itemList.append(ReviveItem(*i))
        self.menu_loop()

    def menu_loop(self):
        while not self.closeMenu:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.closeMenu = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_o:
                        print("Action: {}".format(self.action))
                    if event.key == pygame.K_LEFT:
                        if self.x_select > 0:
                            self.x_select -= 1
                            self.y_select = 0
                    if event.key == pygame.K_RIGHT:
                        if self.x_select < self.x_limit:
                            self.x_select += 1
                            self.y_select = 0
                    if event.key == pygame.K_UP:
                        if self.y_select > 0:
                            self.y_select -= 1
                    if event.key == pygame.K_DOWN:
                        if self.y_select < self.y_limit:
                            self.y_select += 1
                    if event.key == pygame.K_m:
                        self.closeMenu = True
                    if event.key == pygame.K_x and self.action != "None":
                        self.action = "None"
                        self.y_select = 0
                    if event.key == pygame.K_SPACE:
                        self.action = self.menu_window_action()
                        if self.action == "sell":
                            self.x_select = 0
                            self.y_select = 0
                        if self.action in ["purchase", "selling"]:
                            self.temp_select_1 = self.x_select
                            self.temp_select_2 = self.y_select
                            self.x_select = 0
                            self.y_select = 0
            if self.action == "buy":
                self.x_limit = 4
                self.y_limit = 10
                self.display_menu_buy(select_1=self.x_select, select_2=self.y_select)
            elif self.action == "sell":
                self.x_limit = len(self.party.members) - 1
                self.y_limit = len(self.party.members[self.x_select].items) - 1
                self.display_menu_sell(select_1=self.x_select, select_2=self.y_select)
            elif self.action == "purchase":
                self.purchase_confirmation()
                self.x_limit = 0
                self.y_limit = 1
            elif self.action == "selling":
                self.selling_confirmation()
                self.x_limit = 0
                self.y_limit = 1
            elif self.action == "close":
                self.closeMenu = True
            else:
                self.display_menu_window(self.y_select)
                self.x_limit = 0
                self.y_limit = 2
            pygame.display.update()
            clock.tick(fps)

    def display_menu_window(self, option=0):
        menu_text_list = ["Buy items", "Sell items", "Leave""", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "Gold: {}".format(self.party.gold)]
        gameDisplay.fill(menu_blue)
        pygame.draw.rect(gameDisplay, black, (thin_border, thin_border, display_width - thin_border * 2, display_height - thin_border * 2))
        pygame.draw.rect(gameDisplay, menu_blue, (thin_border * 2, thin_border * 2, display_width - thin_border * 4, display_height - thin_border * 4))
        pygame.draw.rect(gameDisplay, pale_blue, (border-2, border+option*(menu_text_size+4)+4, 158, menu_text_size))
        pygame.draw.rect(gameDisplay, black, (border + 180, thin_border, thin_border, display_height - thin_border * 2))
        self.menu_display(menu_text_list)

    def display_menu_buy(self, box=True, select_1=0, select_2=0):
        list_item_type = ["Weapon", "Light armor", "Medium armor", "Heavy armor", "Item"]
        item_list = []
        stat_text_list_1 = [list_item_type[select_1], "Name", ""]
        stat_text_list_4 = []
        stat_text_list_2 = ["", "Price", ""]
        stat_text_list_3 = ["", "Level", ""]
        max_level = 3
        limit = 0
        for i in self.party.members:
            if i.level + 2 > max_level:
                max_level = i.level + 2
        if select_1 == 0:
            stat_text_list_4.append("")
            stat_text_list_4.append("Attack")
            stat_text_list_4.append("")
            for i in self.weaponList:
                if i.lvl <= max_level:
                    limit += 1
                    item_list.append(i)
                    stat_text_list_1.append(i.__str__())
                    stat_text_list_4.append("{}-{}".format(i.baseattack+i.nbrdmgdice, i.baseattack+i.nbrdmgdice*i.damagedice))
                    stat_text_list_2.append(" {}".format(i.price))
                    stat_text_list_3.append("    {}".format(i.lvl))
        elif select_1 == 1:
            stat_text_list_4.append("")
            stat_text_list_4.append("Pdef/Mdef")
            stat_text_list_4.append("")
            for i in self.equipmentList:
                if i.lvl <= max_level and i.armorClass == "light":
                    limit += 1
                    item_list.append(i)
                    stat_text_list_1.append(i.__str__())
                    stat_text_list_4.append("{} / {}".format(i.pdefense, i.mdefense))
                    stat_text_list_2.append(" {}".format(i.price))
                    stat_text_list_3.append("    {}".format(i.lvl))
        elif select_1 == 2:
            stat_text_list_4.append("")
            stat_text_list_4.append("Pdef/Mdef")
            stat_text_list_4.append("")
            for i in self.equipmentList:
                if i.lvl <= max_level and i.armorClass == "medium":
                    limit += 1
                    item_list.append(i)
                    stat_text_list_1.append(i.__str__())
                    stat_text_list_4.append("{} / {}".format(i.pdefense, i.mdefense))
                    stat_text_list_2.append(" {}".format(i.price))
                    stat_text_list_3.append("    {}".format(i.lvl))
        elif select_1 == 3:
            stat_text_list_4.append("")
            stat_text_list_4.append("Pdef/Mdef")
            stat_text_list_4.append("")
            for i in self.equipmentList:
                if i.lvl <= max_level and i.armorClass == "heavy":
                    limit += 1
                    item_list.append(i)
                    stat_text_list_1.append(i.__str__())
                    stat_text_list_4.append("{} / {}".format(i.pdefense, i.mdefense))
                    stat_text_list_2.append(" {}".format(i.price))
                    stat_text_list_3.append("    {}".format(i.lvl))
        elif select_1 == 4:
            for i in self.itemList:
                if max_level > 8 or limit < 5:
                    limit += 1
                    item_list.append(i)
                    stat_text_list_1.append(i.__str__())
                    stat_text_list_2.append(" {}".format(i.price))
        self.y_limit = limit - 1
        self.display_menu_window(0)
        if box and len(item_list) != 0:
            self.active_item = item_list[select_2]
            if select_2 <= 7:
                pygame.draw.rect(gameDisplay, pale_blue, (border + 212, border + (select_2+3) * (menu_text_size + 4) + 12, 600, menu_text_size))
            else:
                pygame.draw.rect(gameDisplay, pale_blue, (border + 212, border + 10 * (menu_text_size + 4) + 12, 600, menu_text_size))
        self.special_display(stat_text_list_1, border+190, 10)
        self.special_display(stat_text_list_4, border+480, 10)
        self.special_display(stat_text_list_2, border+630, 10)
        if select_1 != 4:
            self.special_display(stat_text_list_3, border+700, 10)

    def purchase_confirmation(self):
        self.display_menu_buy(box=False, select_1=self.temp_select_1, select_2=self.temp_select_2)
        if self.active_item is None:
            poor_list = ["Sorry, you can't afford this item"]
            self.dialog_box(self.y_select)
            self.dialog_display(poor_list)
        elif self.active_item.price > self.party.gold:
            poor_list = ["Sorry, you can't afford this item"]
            self.dialog_box(self.y_select)
            self.dialog_display(poor_list)
            self.active_item = None
        elif self.active_item.price <= self.party.gold:
            purchase_list = ["Are you sure you want to buy {}?".format(self.active_item.__str__()), "  Yes", "  No"]
            self.dialog_box(self.y_select)
            self.dialog_display(purchase_list)

    def complete_purchase(self):
        self.party.gold -= self.active_item.price
        self.party.members[0].newSetItem(self.active_item)
        self.active_item = None

    def display_menu_sell(self, select_1=0, select_2=0):
        player = self.party.members[select_1]
        display_list_1 = ["{}'s items".format(player.__str__()), "Name:", ""]
        display_list_2 = ["", "Selling price", ""]
        for i in player.items:
            display_list_1.append(i.__str__())
            display_list_2.append("  {}".format(int(i.price/4)))
        self.active_item = player.items[select_2]
        self.display_menu_window(0)
        if select_2 <= 7:
            pygame.draw.rect(gameDisplay, pale_blue,
                             (border + 212, border + (select_2 + 3) * (menu_text_size + 4) + 12, 600, menu_text_size))
        else:
            pygame.draw.rect(gameDisplay, pale_blue,
                             (border + 212, border + 10 * (menu_text_size + 4) + 12, 600, menu_text_size))
        self.special_display(display_list_1, border + 190, 10)
        self.special_display(display_list_2, border + 600, 10)

    def selling_confirmation(self):
        self.display_menu_sell(select_1=self.temp_select_1, select_2=self.temp_select_2)
        equiped_items = [self.party.members[self.temp_select_1].weapon, *self.party.members[self.temp_select_1].equipment]
        if self.active_item is None:
            poor_list = ["Sorry, you can't sell this item"]
            self.dialog_box(self.y_select)
            self.dialog_display(poor_list)
        elif self.active_item.price is None or self.active_item in equiped_items:
            poor_list = ["Sorry, you can't sell this item"]
            self.dialog_box(self.y_select)
            self.dialog_display(poor_list)
            self.active_item = None
        else:
            purchase_list = ["Are you sure you want to sell {}?".format(self.active_item.__str__()), "  Yes", "  No"]
            self.dialog_box(self.y_select)
            self.dialog_display(purchase_list)

    def complete_selling(self):
        self.party.gold += int(self.active_item.price / 4)
        self.party.members[self.temp_select_1].removeItem(self.active_item)
        self.active_item = None

    def menu_window_action(self):
        if self.action == "None":
            if self.y_select == 0:
                return "buy"
            elif self.y_select == 1:
                return "sell"
            elif self.y_select == 2:
                return "close"
        elif self.action == "buy":
            return "purchase"
        elif self.action == "sell":
            return "selling"
        elif self.action == "purchase":
            if self.y_select == 0 and self.active_item is not None:
                self.complete_purchase()
            return "buy"
        elif self.action == "selling":
            if self.y_select == 0 and self.active_item is not None:
                self.complete_selling()
            return "sell"
