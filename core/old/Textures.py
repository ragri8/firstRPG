from core.Boss import *
from core.Monster import Monster
from core.old.NPC_Class import *
from objects.Texture_Sheet import *


class Map_Data:
    def __init__(self, mapcode, ajust_x, ajust_y, team, map_number=1):  # new_attribute added, have to add it when called in pygame_main_game.py
        self.map_number = map_number
        self.size_x = int(mapcode[0: 4])
        self.size_y = int(mapcode[4: 8])
        self.ajust_x = int(ajust_x + (16 - self.size_x) * tile_size / 2)
        self.ajust_y = int(ajust_y + (14 - self.size_y) * tile_size / 2)
        print("Ajustment: ({}, {})".format(self.ajust_x, self.ajust_y))
        self.biomecode = mapcode[8: 10]
        self.biome = self.getBiome(int(mapcode[8: 10]))
        self.minLevel = int(mapcode[10: 12])
        self.maxLevel = int(mapcode[12: 14])
        self.fightBackground = mapcode[14: 16]
        self.start_x = int(mapcode[16: 20])
        self.start_y = int(mapcode[20: 24])
        self.mapcode = mapcode[24:(self.size_x*self.size_y+24)*2]
        self.team = team
        self.mapaccess = self.tile_access()
        self.map_boss = []
        self.object_list = []
        self.special_list = []
        self.chest_list = []
        self.mapinteraction = self.interactive_tile(mapcode[(24 + self.size_x * self.size_y * 2):])
        self.mapnpc = self.npc_location(self.mapinteraction)
        print("mapcode:")
        print("Size: {}x{}".format(self.size_x, self.size_y))
        print("Data:")
        for i in range(self.size_y):
            print(self.mapcode[i * self.size_x*2: (i + 1) * self.size_x*2])
        print("Tile access:")
        for i in range(self.size_y-1, -1, -1):
            print(self.mapaccess[i * self.size_x: (i + 1) * self.size_x])

    def getBiome(self, code):
        if code == 0:
            return "all biome"

    def setMapcode(self, code):
        mapcodeList = []
        for i in range(0, len(code), 4):
            mapcodeList.append(int(code[i: i+4]))
        return mapcodeList

    def getTile(self, x, y, player_x, player_y):
        tile_x_position = x // tile_size + player_x // tile_size
        tile_y_position = y // tile_size - player_y // tile_size
        position = (y // tile_size - player_y // tile_size) * self.size_x + x // tile_size + player_x // tile_size
        if len(self.mapcode) <= position or tile_x_position < 0 or tile_y_position < 0:
            return None, position
        elif tile_x_position >= self.size_x or tile_y_position >= self.size_y:
            return None, position
        return texture_tags.get(self.mapcode[position*2]+self.mapcode[position*2+1]), position

    def getTileFromList(self, x, y, player_x, player_y):
        tile_x_position = x // tile_size + player_x // tile_size
        tile_y_position = y // tile_size - player_y // tile_size
        position = (y // tile_size - player_y // tile_size) * self.size_x + x // tile_size + player_x // tile_size
        if len(self.mapcode) <= position or tile_x_position < 0 or tile_y_position < 0:
            return None, position
        elif tile_x_position >= self.size_x or tile_y_position >= self.size_y:
            return None, position
        return texture_list[self.mapcodeList[position]], position

    def new_terrain_grid(self, player_x, player_y):
        for x in range(-60, display_width + 60, tile_size):
            for y in range(-60, display_height + 60, tile_size):
                tile, position = self.getTile(x, y, player_x, player_y)
                if tile is None:
                    continue
                gameDisplay.blit(tile, (x - (player_x % tile_size), y + (player_y % tile_size)))
        for i in self.mapnpc:
            self.display_npc(i, player_x, player_y)
        for i in self.map_boss:
            self.display_boss(i, player_x, player_y)
        for i in self.chest_list:
            i.display_chest(player_x, player_y, self)

    def player_position(self, x, y):
        x = x + self.size_x * 30 - 20 + self.ajust_x
        y = y + self.size_y * 30 + 100 - self.ajust_y
        return x, y

    def tile_access(self):
        tileList = []
        for i in range(self.size_y):
            sublist = []
            for j in range(self.size_x):
                tile = (j + i * self.size_x) * 2
                if self.mapcode[tile]+self.mapcode[tile+1] in ["aa", "ac", "ay", "az", "aA", "aB", "aC", "aD", "aE", "aF", "aG", "aH", "aI", "aJ", "aK"]:
                    sublist.append(0)
                elif self.mapcode[tile] in ["ag"]:
                    sublist.append(1)
                elif self.mapcode[tile] in ["ah", "ao"]:
                    sublist.append(2)
                elif self.mapcode[tile] in ["ae", "al"]:
                    sublist.append(3)
                elif self.mapcode[tile] in ["aj"]:
                    sublist.append(4)
                elif self.mapcode[tile] in ["af", "aq"]:
                    sublist.append(5)
                elif self.mapcode[tile] in ["ak", "am"]:
                    sublist.append(6)
                elif self.mapcode[tile] in ["ad", "ar"]:
                    sublist.append(7)
                elif self.mapcode[tile] in ["ai", "am"]:
                    sublist.append(8)
                elif self.mapcode[tile] in ["a0"]:
                    sublist.append(9)
                elif self.mapcode[tile] in ["ap"]:
                    sublist.append(10)
                elif self.mapcode[tile] in ["a1"]:
                    sublist.append(11)
                elif self.mapcode[tile] in ["a2"]:
                    sublist.append(12)
                else:
                    sublist.append(9)
            sublist.reverse()
            for k in sublist:
                tileList.append(k)
        tileList.reverse()
        return tileList

    def limits(self, x, y, x_speed, y_speed, ground_level=0):
        x, y = self.player_position(x, y)
        occupied_tiles = []
        for i in self.map_boss:
            if i.display:
                occupied_tiles.append(i.location)
        for i in self.object_list:
            occupied_tiles.append(i.location)
        for i in self.special_list:
            if i.obstacle(ground_level):
                occupied_tiles.append(i.location)
        for i in self.chest_list:
            occupied_tiles.append(i.location)
        for i in self.mapnpc:
            occupied_tiles.append((i[0]))
        tile_left_side = (x + 5) // 60 + ((y + 20) // 60) * self.size_x
        tile_right_side = (x + 30) // 60 + ((y + 20) // 60) * self.size_x
        tile_upper_side = (x + 25) // 60 + ((y + 25) // 60) * self.size_x
        tile_upper_side_2 = (x + 10) // 60 + ((y + 25) // 60) * self.size_x
        tile_lower_side = (x + 25) // 60 + ((y + 15) // 60) * self.size_x
        tile_lower_side_2 = (x + 10) // 60 + ((y + 15) // 60) * self.size_x
        if x <= 0 > x_speed:
            x_speed /= 10
        elif x >= self.size_x * tile_size - 40 and 0 < x_speed:
            x_speed /= 10
        else:
            if tile_left_side < self.size_x * self.size_y and x_speed < 0:
                if self.mapaccess[tile_left_side] in [6, 7, 8, 9, 10] or tile_left_side in occupied_tiles or \
                                        self.mapaccess[tile_left_side] in [11] and ground_level == 0:
                    x_speed /= 10
            elif len(self.mapaccess) > tile_right_side > 0 and x_speed > 0:
                if self.mapaccess[tile_right_side] in [2, 3, 4, 9, 10] or tile_right_side in occupied_tiles or \
                                        self.mapaccess[tile_right_side] in [11] and ground_level == 0:
                    x_speed /= 10
        if y <= 0 > y_speed:
            y_speed /= 10
        elif y >= self.size_y * tile_size - 60 and 0 < y_speed:
            y_speed /= 10
        else:
            if len(self.mapaccess) > tile_upper_side:
                if (self.mapaccess[tile_upper_side] in [4, 5, 6, 9, 10] or self.mapaccess[tile_upper_side_2] in [4, 5, 6, 9, 10] or tile_upper_side in occupied_tiles or tile_upper_side_2 in occupied_tiles) and y_speed > 0:
                    y_speed /= 10
                elif self.mapaccess[tile_upper_side] in [12] and ground_level == 1 and y_speed > 0:
                    y_speed /= 10
            if len(self.mapaccess) > tile_lower_side > 0:
                if (self.mapaccess[tile_lower_side] in [1, 2, 8, 9] or self.mapaccess[tile_lower_side_2] in [1, 2, 8, 9] or tile_lower_side in occupied_tiles or tile_lower_side_2 in occupied_tiles) and y_speed < 0:
                    y_speed /= 10
                elif (self.mapaccess[tile_lower_side] in [12]) and ground_level == 1 and y_speed < 0:
                    y_speed /= 10
        return x_speed, y_speed

    def left_tile(self, x, y):
        x, y = self.player_position(x, y)
        return (x + 5) // 60 + ((y + 20) // 60) * self.size_x

    def right_tile(self, x, y):
        x, y = self.player_position(x, y)
        return (x + 30) // 60 + ((y + 20) // 60) * self.size_x

    def upper_tile(self, x, y):
        x, y = self.player_position(x, y)
        return (x + 25) // 60 + ((y + 25) // 60) * self.size_x

    def lower_tile(self, x, y):
        x, y = self.player_position(x, y)
        return (x + 25) // 60 + ((y + 15) // 60) * self.size_x

    def interactive_tile(self, mapcode):
        interactionList = []
        if 0 < len(mapcode):
            for i in range(0, len(mapcode), 8):
                interactionList.append([mapcode[i: i+4], mapcode[i+4: i+8]])
                if 4200 <= int(mapcode[i + 4: i + 8]) < 5000:
                    print(str(int(mapcode[i + 4: i + 8])))
                    self.create_mimic(int(mapcode[i+4: i+8]), int(mapcode[i: i+4]))
                elif 6000 <= int(mapcode[i+4: i+8]) < 6100:
                    print(str(int(mapcode[i+4: i+8])))
                    self.create_tree(int(mapcode[i+4: i+8]), int(mapcode[i: i+4]))
                elif 6200 <= int(mapcode[i+4: i+8]) < 7000:
                    self.create_chest(int(mapcode[i+4: i+8]), int(mapcode[i: i+4]))
                elif 7000 <= int(mapcode[i+4: i+8]) < 8000:
                    self.create_boss(int(mapcode[i+4: i+8]), int(mapcode[i: i+4]))
                elif 5500 <= int(mapcode[i+4: i+8]) < 5600:
                    self.create_bridge(int(mapcode[i+4: i+8]), int(mapcode[i: i+4]))
        return interactionList

    def interaction(self, x, y, orientation, condition):
        action = 1
        position = 0
        if orientation == 1:
            for i in range(len(self.mapinteraction)):
                if self.lower_tile(x, y-20) == int(self.mapinteraction[i][0]):
                    print("down interaction!!!")
                    action = int(self.mapinteraction[i][1])
                    position = i
        elif orientation == 3:
            for i in range(len(self.mapinteraction)):
                if self.right_tile(x+20, y) == int(self.mapinteraction[i][0]):
                    print("right interaction!!!")
                    action = int(self.mapinteraction[i][1])
                    position = i
        elif orientation == 5:
            for i in range(len(self.mapinteraction)):
                if self.upper_tile(x, y+20) == int(self.mapinteraction[i][0]):
                    print("up interaction!!!")
                    action = int(self.mapinteraction[i][1])
                    position = i
        elif orientation == 7:
            for i in range(len(self.mapinteraction)):
                if self.left_tile(x-10, y) == int(self.mapinteraction[i][0]):
                    print("left interaction!!!")
                    action = int(self.mapinteraction[i][1])
                    position = i
        print("Action: {}".format(action))
        if action == 100 and condition is None:
            action = 1
        return action

    def passive_interaction(self, position, ground_level):
        for i in range(len(self.mapinteraction)):
            if position == int(self.mapinteraction[i][0]):
                if int(self.mapinteraction[i][1]) == 8500:
                    return 0
                elif int(self.mapinteraction[i][1]) == 8501:
                    return 1
        return ground_level

    def npc_location(self, code):
        npc_list = []
        for i in code:
            if int(i[1]) >= 9000:
                if int(i[1]) == 9000:
                    npc_list.append([int(i[0]), sprite2])
                elif int(i[1]) == 9001:
                    npc_list.append([int(i[0]), sprite3])
        return npc_list

    def remove_npc(self, position):
        for i in self.mapnpc:
            if int(i[0]) == position:
                self.mapnpc.remove(i)
                break

    def display_npc(self, npc_code, player_x, player_y):
        x, y = npc_code[0] % self.size_x * 60 + 10, npc_code[0] // self.size_x * 60 - self.size_y * 60 + 80
        sprite = npc_code[1]
        gameDisplay.blit(sprite, (x-player_x, -y+player_y))

    def create_boss(self, code, location):
        enemy_party = []
        size = "normal"
        if code == 7001:
            enemy_1 = Monster(*wolf2)
            enemy_2 = Monster(*wolf3)
            enemy_3 = Monster(*wolf2)
            enemy_1.elite()
            enemy_2.boss(blife=5)
            enemy_3.elite()
            enemy_party = [enemy_1, enemy_2, enemy_3]
            sprite = boss_sprite_1
        elif code == 7002:
            enemy_1 = Monster(*wolf2)
            enemy_2 = Monster(*bear3)
            enemy_3 = Monster(*wolf2)
            enemy_1.elite()
            enemy_2.boss(blife=3)
            enemy_3.elite()
            enemy_party = [enemy_1, enemy_2, enemy_3]
            sprite = default_map_sprite
        elif code == 7003:
            enemy_1 = Monster(*wolf3)
            enemy_2 = Monster(*bear3)
            enemy_3 = Monster(*wolf3)
            enemy_1.elite()
            enemy_2.boss(blife=5)
            enemy_3.elite()
            enemy_party = [enemy_1, enemy_2, enemy_3]
            sprite = boss_sprite_2
        elif code == 7004:
            enemy_1 = Monster(*bandit1)
            enemy_2 = Monster(*bandit4)
            enemy_3 = Monster(*bandit1)
            enemy_1.elite()
            enemy_2.boss(blife=3)
            enemy_3.elite()
            enemy_party = [enemy_1, enemy_2, enemy_3]
            sprite = boss_sprite_3
        elif code == 7005:
            enemy = Monster(*dragon1)
            enemy_party = [enemy]
            sprite = boss_sprite_4
            size = "big"
        else:
            enemy = Monster(*wolf3)
            enemy.boss(blife=4)
            enemy_party = [enemy]
            sprite = default_map_sprite
        self.map_boss.append(Boss(location, sprite, enemy_party, code, size))

    def display_boss(self, boss, player_x, player_y):
        if boss.display:
            x, y = boss.location % self.size_x * 60 + 0, boss.location // self.size_x * 60 - self.size_y * 60 + 80
            if boss.size == "big":
                x -= 40
                y += 60
            gameDisplay.blit(boss.sprite, (x - player_x, -y + player_y))

    def create_bridge(self, code, location):
        if 5500 <= int(code) < 5600:
            self.special_list.append(Bridge(code, location))

    def create_tree(self, code, location):
        if int(code) == 6001:
            trunk = trunk_1
            leaves = leaves_1
        else:
            return None
        self.object_list.append(Tree(code, location))

    def create_chest(self, code, location):
        close_sprite = chest_1
        open_sprite = chest_2
        self.chest_list.append(Chest(close_sprite, open_sprite, location, code, self.team))

    def create_mimic(self, code, location):
        close_sprite = chest_1
        open_sprite = chest_2
        self.chest_list.append(Mimic(close_sprite, open_sprite, location, code, self.team))


class Chest:
    def __init__(self, sprite1, sprite2, location, reward, party):
        self.sprite1 = sprite1
        self.sprite2 = sprite2
        self.opened = party.check_chest(reward)
        self.location = location
        self.reward = reward

    def display_chest(self, player_x, player_y, p_map):
        if self.opened:
            sprite = self.sprite2
        else:
            sprite = self.sprite1
        x, y = self.location % p_map.size_x * 60, self.location // p_map.size_x * 60 - p_map.size_y * 60 + 60
        gameDisplay.blit(sprite, (x - player_x, -y + player_y))

    def chest_reward(self, team):
        if not self.opened:
            self.opened = True
            team.chest_opening(self.reward)
            if 6500 <= self.reward < 6600:
                team.gold += (self.reward - 6500) * 10
                return None
            elif 6600 <= self.reward < 6700:
                team.gold += (self.reward - 6600) * 50
                return None
            elif 6700 <= self.reward < 7000:
                team.gold += (self.reward - 6700) * 200
                return None
            elif 6200 <= self.reward < 6300:
                item = (self.reward-6200) % len(itemList)
                if item == 0:
                    item = ReviveItem(*(itemList[item]))
                else:
                    item = RecoveryItem(*(itemList[item]))
                team.members[0].newSetItem(item)
            elif 6300 <= self.reward < 6400:
                item = Weapon(*(complete_weapon_list[self.reward-6300]))
                team.members[0].newSetItem(item)
            elif 6400 <= self.reward < 6500:
                item = complete_armor_list[self.reward-6400]
                if item[3] == "Head":
                    item = HeadEquipment(*item)
                if item[3] == "Body":
                    item = BodyEquipment(*item)
                if item[3] == "Hand":
                    item = HandEquipment(*item)
                if item[3] == "Shield":
                    item = ShieldEquipment(*item)
                team.members[0].newSetItem(item)
            return item


class Mimic(Chest):
    def __init__(self, sprite1, sprite2, location, reward, party):
        super().__init__(sprite1, sprite2, location, reward+2000, party)

    def enemy_party(self):
        if self.reward == 6312:
            enemy = Monster(*mimic2)
            enemy.boss(blife=3)
            return [enemy]


class Texture_Object:
    def __init__(self, code, location):
        self.code = code
        self.location = location

    def display_level_0(self, player_x, player_y, p_map):
        pass

    def display_level_1(self, player_x, player_y, p_map):
        pass

    def display_level_2(self, player_x, player_y, p_map):
        pass

    def obstacle(self, ground_level):
        return True


class Tree(Texture_Object):
    def __init__(self, code, location):
        super().__init__(code, location)
        self.trunk = trunk_1
        self.leaves = leaves_1

    def display_level_0(self, player_x, player_y, p_map):
        x, y = self.location % p_map.size_x * 60, self.location // p_map.size_x * 60 - p_map.size_y * 60 + 80
        gameDisplay.blit(self.trunk, (x - player_x, -y + player_y))

    def display_level_2(self, player_x, player_y, p_map):
        x, y = (self.location-1) % p_map.size_x * 60, self.location // p_map.size_x * 60 - p_map.size_y * 60 + 200
        gameDisplay.blit(self.leaves, (x - player_x, -y + player_y))


class Bridge(Texture_Object):
    def __init__(self, code, location):
        super().__init__(code, location)
        self.tile_sprite = object_tags.get(str(code))
        self.type = self.border_type()

    def border_type(self):
        if self.code in [5500, 5502, 5503, 5505, 5506, 5508, 5509, 5511]:
            return 1
        elif self.code in [5501, 5510]:
            return 0
        else:
            return -1

    def display_level_1(self, player_x, player_y, p_map):
        x, y = self.location % p_map.size_x * 60, self.location // p_map.size_x * 60 - p_map.size_y * 60 + 60
        gameDisplay.blit(self.tile_sprite, (x - player_x, -y + player_y))

    def obstacle(self, ground_level):
        if ground_level == self.type:
            return True
        else:
            return False
