from core.old.Game_Menu import *


class NPC:
    def __init__(self, script, dialog):
        self.script = script
        self.dialog = dialog
        self.npc_dialog_loop()

    def get_dialog(self, line):
        return self.dialog[line]

    def get_choice(self, line):
        pass

    def npc_dialog_loop(self):
        end_dialog = False
        x_select = 0
        y_select = 0
        x_limit = 0
        y_limit = 0
        line = 0
        while not end_dialog:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    end_dialog = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        if x_select > 0:
                            x_select -= 1
                    if event.key == pygame.K_RIGHT:
                        if x_select < x_limit:
                            x_select += 1
                    if event.key == pygame.K_UP:
                        if y_select > 0:
                            y_select -= 1
                    if event.key == pygame.K_DOWN:
                        if y_select < y_limit:
                            y_select += 1
                    if event.key == pygame.K_RETURN or event.key == pygame.K_SPACE:
                        if line+1 == len(self.dialog):
                            end_dialog = True
                        else:
                            line += 1
            self.dialog_box()
            self.dialog_display(self.get_dialog(line))
            pygame.display.update()
            clock.tick(fps)
        return None

    def dialog_box(self, option=-1):
        pygame.draw.rect(gameDisplay, black, (border, int(0.65*display_height), display_width-border*2, display_height*0.4-border))
        pygame.draw.rect(gameDisplay, menu_blue, (thin_border+border, int(0.65*display_height+thin_border), display_width-thin_border*2-border*2, display_height*0.4-thin_border*2-border))
        if option >= 0:
            pygame.draw.rect(gameDisplay, pale_blue, (border - 2, border + option * (menu_text_size + 4) + 4, 158, menu_text_size))

    def dialog_display(self, dialog_list, x_position=0, y_position=0):
        for i in range(len(dialog_list)):
            TextSurf, TextRect = text_objects(dialog_list[i], menuText, black)
            TextRect = (border*1.5+x_position, border+thin_border+i*menu_text_size+int(dialog_height*0.9)+y_position)
            gameDisplay.blit(TextSurf, TextRect)

    def choice_display(self, choice_list, choice, y_position=0):
        pass


class NPC_Coliseum(NPC):
    def __init__(self, script):
        self.x_limit = 0
        self.y_limit = 0
        self.x_select = 0
        self.y_select = 0
        self.selection = None
        self.condition = None
        dialog = [[["Welcome to the coliseum!"], []], [["How can I help you?"], ["Participate", "Information about the rules", "Leave"]]]
        super().__init__(script=script, dialog=dialog)

    def dialog_intro(self):
        self.dialog_display(["Welcome to the coliseum!"])
        return "next"

    def first_choice_dialog(self):
        self.dialog_display(["How can I help you?"])
        self.choice_display(["Participate", "Information about the rules", "Leave"], self.y_select, menu_text_size)
        return "choice1"

    def coliseum_inscription_dialog(self):
        self.dialog_display(["What level of difficulty of enemies would you like to fight?"])
        self.choice_display(["First level", "2nd level", "3rd level", "4th level", "Back"], self.y_select, menu_text_size)
        return "choice2"


    def get_dialog(self, line):
        return self.dialog[line][0]

    def get_choice(self, line):
        return self.dialog[line][1]

    def get_choice_shift(self, line):
        return len(self.dialog[line][0]) * menu_text_size

    def choice_display(self, choice_list, choice, y_position=0):
        self.y_limit = len(choice_list)
        pygame.draw.rect(gameDisplay, pale_blue, (border*1.5+10, border+choice*menu_text_size+int(dialog_height*0.9)+y_position+4, 180, menu_text_size))
        self.dialog_display(choice_list, x_position=10, y_position=y_position)

    def choice_linker(self, dialog_part):
        if dialog_part == "choice1":
            if self.selection == 0:
                return self.coliseum_inscription_dialog()
            elif self.selection == 2:
                return "end"
        elif dialog_part == "choice2":
            if self.selection == 4:
                return "next"
            else:
                self.condition = "F{}".format(self.selection+1)
                return "end"

    def dialog_link(self, dialog_part):
        if dialog_part == "intro":
            return self.dialog_intro()
        elif dialog_part == "end":
            return "end"
        elif dialog_part == "next":
            return self.first_choice_dialog()
        elif dialog_part in ["choice1", "choice2"]:
            return self.choice_linker(dialog_part)

    def npc_dialog_loop_old(self):
        end_dialog = False
        dialog_part = "intro"
        next_dialog = None
        while not end_dialog:
            for event in pygame.event.get():
                if next_dialog == "end":
                    end_dialog = True
                if event.type == pygame.QUIT:
                    end_dialog = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        if self.x_select > 0:
                            self.x_select -= 1
                    if event.key == pygame.K_RIGHT:
                        if self.x_select < self.x_limit:
                            self.x_select += 1
                    if event.key == pygame.K_UP:
                        if self.y_select > 0:
                            self.y_select -= 1
                    if event.key == pygame.K_DOWN:
                        if self.y_select + 1 < self.y_limit:
                            self.y_select += 1
                    if event.key == pygame.K_RETURN or event.key == pygame.K_SPACE:
                        print(dialog_part)
                        dialog_part = next_dialog
                        print(dialog_part)
                        self.selection = self.y_select
            self.dialog_box()
            next_dialog = self.dialog_link(dialog_part)
            pygame.display.update()
            clock.tick(fps)
        return self.condition

    def function_linker(self, next_function):
        dialogList = []
        choiceList = []
        if next_function == "Coliseum selection":
            dialogList = ["How can I help you?"]
            choiceList = ["Participate", "Information about the rules", "Leave"]
            next_function = "Coliseum select"
        elif next_function == "Coliseum select":
            if self.y_select == 0:
                dialogList = ["What level of difficulty of enemies would you like to fight?"]
                choiceList = ["First level", "2nd level", "3rd level", "4th level", "5th level", "Back"]
                next_function = "Enemy select"
            elif self.y_select == 1:
                return self.function_linker("Coliseum selection")
            elif self.y_select == 2:
                next_function = "end"
        elif next_function == "Enemy select":
            print(self.y_select)
            if self.y_select == 5:
                return self.function_linker("Coliseum selection")
            else:
                print("Supposed to work")
                self.condition = str("F{}".format(str(self.y_select + 1)))
                next_function = "end"
        return dialogList, choiceList, next_function

    def npc_dialog_loop(self):
        end_dialog = False
        dialogList, choiceList, next_function = ["Welcome to the coliseum!"], [], "Coliseum selection"
        while not end_dialog:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    end_dialog = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        if self.x_select > 0:
                            self.x_select -= 1
                    if event.key == pygame.K_RIGHT:
                        if self.x_select < self.x_limit:
                            self.x_select += 1
                    if event.key == pygame.K_UP:
                        if self.y_select > 0:
                            self.y_select -= 1
                    if event.key == pygame.K_DOWN:
                        if self.y_select + 1 < self.y_limit:
                            self.y_select += 1
                    if event.key == pygame.K_RETURN or event.key == pygame.K_SPACE:
                        dialogList, choiceList, next_function = self.function_linker(next_function)
                        if next_function == "end":
                            end_dialog = True
                        self.y_select = 0
            self.dialog_box()
            self.dialog_display(dialogList)
            if len(choiceList) > 0:
                self.choice_display(choiceList, self.y_select, menu_text_size)
            pygame.display.update()
            clock.tick(fps)
        return self.condition


class NPC_Merchant(NPC):
    def __init__(self, script, party):
        menu = Menu_Seller(party)
        self.x_limit = 0
        self.y_limit = 0
        self.x_select = 0
        self.y_select = 0
        self.selection = None
        self.condition = None
        dialog = [[["My humble shop is at your service!!"], []], [["What do you need?"], ["Buy items", "Sell items", "Leave"]]]
        super().__init__(script=script, dialog=dialog)

    def function_linker(self, next_function):
        dialogList = []
        choiceList = []
        if next_function == "shop option":
            dialogList = ["What can I do for you?"]
            choiceList = ["Buy items", "Sell items", "Leave"]
            next_function = "shop select"
        elif next_function == "shop select":
            if self.y_select == 0:
                pass
            elif self.y_select == 1:
                pass
            elif self.y_select == 2:
                next_function = "end"
        return dialogList, choiceList, next_function

    def npc_dialog_loop(self):
        end_dialog = False
        dialogList, choiceList, next_function = ["My humble shop is at your service!"], [], "shop option"
        while not end_dialog:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    end_dialog = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        if self.x_select > 0:
                            self.x_select -= 1
                    if event.key == pygame.K_RIGHT:
                        if self.x_select < self.x_limit:
                            self.x_select += 1
                    if event.key == pygame.K_UP:
                        if self.y_select > 0:
                            self.y_select -= 1
                    if event.key == pygame.K_DOWN:
                        if self.y_select + 1 < self.y_limit:
                            self.y_select += 1
                    if event.key == pygame.K_RETURN or event.key == pygame.K_SPACE:
                        dialogList, choiceList, next_function = self.function_linker(next_function)
                        if next_function == "end":
                            end_dialog = True
                        self.y_select = 0
            self.dialog_box()
            self.dialog_display(dialogList)
            if len(choiceList) > 0:
                self.choice_display(choiceList, self.y_select, menu_text_size)
            pygame.display.update()
            clock.tick(fps)
