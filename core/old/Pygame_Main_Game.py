from Tests.character_sheet_test import Team
from core.Saving_Process import SaveFile
from core.old.Fight_System_Graphical_Display import DisplayFight
from core.old.Pygame_Display_Fight import *
from core.old.Textures import *
from interface.Map_Read import *

saveProcess = SaveFile(Team)
closegame = False
fightwindow = False
ajustement_x = int((display_width - 960) / 2)
ajustement_y = int((display_height - 540) / 2)

mapcode1 = "00180014" \
           "000102zz" \
           "00000000" \
           "bbbbbbbbbbbbbbbbbb" \
           "bbbbbbbbbbbbbbbbbb" \
           "bbbbmfffffffflbbbb" \
           "bbbbdaaacaaaaebbbb" \
           "bbbbdaaaaaaaaebbbb" \
           "bbbbniaaaaaahobbbb" \
           "bbbbbdcaaaacebbbbb" \
           "bbbbmkaaaaaajlbbbb" \
           "bbbbdaaaaaaaaebbbb" \
           "bbbbdaaaaaaaaebbbb" \
           "bbbbdaaacaaaaebbbb" \
           "bbbbnggggggggobbbb" \
           "bbbbbbbbbbbbbbbbbb" \
           "cbbbbbbbbbbbbbbbbb" \
           "0206000000440100" \
           "0113000001200000" \
           "01009000"

mapcode2 = "00140013" \
           "000102aa" \
           "04000000" \
           "000pppstppp000" \
           "000cxccccwc000" \
           "000cxccccwc000" \
           "0ppuuccccuupp0" \
           "0cccccccccccc0" \
           "0cc0cccccc0cc0" \
           "0ccpccccccpcc0" \
           "0cccccccccccc0" \
           "0cccccccccccc0" \
           "0cc0cccccc0cc0" \
           "0ccpccccccpcc0" \
           "0cccccccccccc0" \
           "000000cc000000" \
           "0174010001750100" \
           "0144000001439000" \
           "0129000001509001"


def display_player_position(x, y, orientation, ground_level, p_map):
    x, y = p_map.player_position(x, y)
    position_text = "( {} ,  {} , {})".format(x, y, orientation)
    TextSurf, TextRect = text_objects(position_text, largeText, white)
    TextRect = (10, 20)
    gameDisplay.blit(TextSurf, TextRect)
    standing_tile = (x + 15) // 60 + ((y+20) // 60) * p_map.size_x
    position_text2 = "(standing tile: {})".format(standing_tile)
    TextSurf, TextRect = text_objects(position_text2, largeText, white)
    TextRect = (10, 40)
    gameDisplay.blit(TextSurf, TextRect)
    position_text3 = "(ground level: {})".format(ground_level)
    TextSurf, TextRect = text_objects(position_text3, largeText, white)
    TextRect = (10, 60)
    gameDisplay.blit(TextSurf, TextRect)
    return standing_tile


def layer_display(sprite, player_x, player_y, player_level, p_map):
    for i in p_map.object_list:
        i.display_level_0(player_x, player_y, p_map)
    for i in p_map.special_list:
        i.display_level_0(player_x, player_y, p_map)
    if player_level == 0:
        gameDisplay.blit(sprite, (int(display_width / 2) - 18, int(display_height / 2) - 30))
    for i in p_map.object_list:
        i.display_level_1(player_x, player_y, p_map)
    for i in p_map.special_list:
        i.display_level_1(player_x, player_y, p_map)
    if player_level == 1:
        gameDisplay.blit(sprite, (int(display_width / 2) - 18, int(display_height / 2) - 30))
    for i in p_map.object_list:
        i.display_level_2(player_x, player_y, p_map)
    for i in p_map.special_list:
        i.display_level_2(player_x, player_y, p_map)


def generate_sprite(orientation, x, y, iterator):
    if x == 0 and y < 0:
        orientation = 1
    elif x > 0 > y:
        orientation = 2
    elif x > 0 and y == 0:
        orientation = 3
    elif x > 0 < y:
        orientation = 4
    elif x == 0 and y > 0:
        orientation = 5
    elif x < 0 < y:
        orientation = 6
    elif x < 0 and y == 0:
        orientation = 7
    elif x < 0 > y:
        orientation = 8
    if orientation in [1, 2, 8]:
        if iterator == 0:
            return orientation, sprite1_2
        elif iterator <= 5:
            return orientation, sprite1_1
        elif iterator <= 10:
            return orientation, sprite1_2
        elif iterator <= 15:
            return orientation, sprite1_3
        elif iterator <= 20:
            return orientation, sprite1_2
    elif orientation in [3]:
        if iterator == 0:
            return orientation, sprite1_8
        elif iterator <= 5:
            return orientation, sprite1_7
        elif iterator <= 10:
            return orientation, sprite1_8
        elif iterator <= 15:
            return orientation, sprite1_9
        elif iterator <= 20:
            return orientation, sprite1_8
    elif orientation == 3:
        return orientation,
    elif orientation in [4, 5, 6]:
        if iterator == 0:
            return orientation, sprite1_11
        elif iterator <= 5:
            return orientation, sprite1_10
        elif iterator <= 10:
            return orientation, sprite1_11
        elif iterator <= 15:
            return orientation, sprite1_12
        elif iterator <= 20:
            return orientation, sprite1_11
    elif orientation in [7]:
        if iterator == 0:
            return orientation, sprite1_5
        elif iterator <= 5:
            return orientation, sprite1_4
        elif iterator <= 10:
            return orientation, sprite1_5
        elif iterator <= 15:
            return orientation, sprite1_6
        elif iterator <= 20:
            return orientation, sprite1_5
    elif orientation == 7:
        return orientation, sprite1
    elif orientation == 8:
        return orientation, sprite1


def action_linker(code, party, condition, p_map):
    if code == 0:
        interaction = NPC_Coliseum(code)
        condition = interaction.condition
        del interaction
    elif code == 50:
        interaction = Menu_Seller(party)
        print(code)
        del interaction
    elif code == 100:
        fight_encounter(party, condition, p_map.fightBackground)
        condition = None
    elif 4100 <= code < 5000:
        mimic_fight(code, party, p_map)
        code += 2000
    if 6100 <= code < 7000:
        for i in p_map.chest_list:
            if i.reward == code:
                item = i.chest_reward(party)
                if item is not None:
                    if item.__str__()[0] in ["a", "e", "i", "o", "u"]:
                        object = "an"
                    else:
                        object = "a"
                    NPC(0, [["You found {} {}!".format(object, item.__str__())]])
    elif 7000 <= code < 8000:
        special_fight(code, party, p_map)
    elif 8000 <= code < 8500:
        condition = "change map"
    return condition


def special_fight(code, party, p_map):
    enemy_party = []
    for i in p_map.map_boss:
        if code == i.tag:
            if i.display:
                enemy_party = i.party
                boss = i
                break
    if len(enemy_party) != 0:
        fight = DisplayFight(party, enemy_party)
        background = map_tags.get(p_map.fightBackground)
        battle_system_loop(fight, background)
        boss.display = False
        for i in enemy_party:
            del i
        del fight


def mimic_fight(code, party, p_map):
    enemy_party = []
    for i in p_map.chest_list:
        if code+2000 == i.reward:
            if not i.opened:
                enemy_party = i.enemy_party()
            break
    if len(enemy_party) != 0:
        fight = DisplayFight(party, enemy_party)
        background = map_tags.get(p_map.fightBackground)
        battle_system_loop(fight, background)
        for i in enemy_party:
            del i
        del fight


def fight_encounter(party, condition, background):
    print("Fight choosen: {}".format(condition))
    enemy_party = []
    nbr = 3
    lvlmin = 1
    lvlmax = 2
    if condition == "F1":
        nbr = 3
        lvlmin = 1
        lvlmax = 2
    elif condition == "F2":
        nbr = 4
        lvlmin = 1.5
        lvlmax = 2.5
    elif condition == "F3":
        nbr = 5
        lvlmin = 2
        lvlmax = 4
    elif condition == "F4":
        boss = Monster(*wolf3)
        boss.boss(blife=5)
        monster1 = Monster(*wolf2)
        monster1.elite()
        monster2 = Monster(*wolf2)
        monster2.elite()
        enemy_party = [monster1, boss, monster2]
    elif condition == "F5":
        nbr = 3
        lvlmin = 3.5
        lvlmax = 5.5
    if len(enemy_party) == 0:
        enemy_party = enemyrdmnbr(nbr, lvlmin, lvlmax, biome=allb)
    fight = DisplayFight(party, enemy_party)
    background = map_tags.get(background)
    battle_system_loop(fight, background)
    for i in enemy_party:
        del i
    del fight


def random_fight_encounter(p_map, party):
    enemy_party = enemyrdmnbr(3, p_map.minLevel, p_map.maxLevel, biome=allb)
    fight = DisplayFight(party, enemy_party)
    background = map_tags.get(p_map.fightBackground)
    battle_system_loop(fight, background)
    for i in enemy_party:
        del i


def map_spawn(p_map, start, team, p_number):
    gameMap = Map_Data(p_map, ajustement_x, ajustement_y, team, p_number)
    if start is None:
        x_position = gameMap.start_x
        y_position = gameMap.start_y
        temp_x, temp_y = gameMap.player_position(0, 0)
        x_position -= temp_x
        y_position -= temp_y
    else:
        x_position, y_position = 0, 0
    return gameMap, x_position, y_position


def map_warp(warp_code, team):
    x, y = 0, 0
    if warp_code == 8000:
        x, y = 400, 900
        new_map = Map_Data(file_read(4), ajustement_x, ajustement_y, team, 4)
    elif warp_code == 8001:
        x, y = 400, 0
        new_map = Map_Data(file_read(3), ajustement_x, ajustement_y, team, 3)
    elif warp_code == 8002:
        x, y = 940, 20
        new_map = Map_Data(file_read(1), ajustement_x, ajustement_y, team, 1)
    elif warp_code == 8003:
        x, y = 940, 900
        new_map = Map_Data(file_read(4), ajustement_x, ajustement_y, team, 4)
    elif warp_code == 8004:
        x, y = 280, 20
        new_map = Map_Data(file_read(2), ajustement_x, ajustement_y, team, 2)
    elif warp_code == 8005:
        x, y = 280, 1250
        new_map = Map_Data(file_read(1), ajustement_x, ajustement_y, team, 1)
    elif warp_code == 8006:
        x, y = 400, 60
        new_map = Map_Data(file_read(6), ajustement_x, ajustement_y, team, 6)
    elif warp_code == 8007:
        x, y = 1030, 440
        new_map = Map_Data(file_read(5), ajustement_x, ajustement_y, team, 5)
    elif warp_code == 8008:
        x, y = 280, 800
        new_map = Map_Data(file_read(2), ajustement_x, ajustement_y, team, 2)
    elif warp_code == 8009:
        x, y = 560, 20
        new_map = Map_Data(file_read(5), ajustement_x, ajustement_y, team, 5)
    elif warp_code == 8010:
        x, y = 820, 20
        new_map = Map_Data(file_read(7), ajustement_x, ajustement_y, team, 7)
    elif warp_code == 8011:
        x, y = 130, 540
        new_map = Map_Data(file_read(8), ajustement_x, ajustement_y, team, 8)
    elif warp_code == 8012:
        x, y = 820, 1160
        new_map = Map_Data(file_read(5), ajustement_x, ajustement_y, team, 5)
    elif warp_code == 8013:
        x, y = 130, 20
        new_map = Map_Data(file_read(8), ajustement_x, ajustement_y, team, 8)
    elif warp_code == 8014:
        x, y = 540, 60
        new_map = Map_Data(file_read(9), ajustement_x, ajustement_y, team, 5)
    elif warp_code == 8015:
        x, y = 20, 620
        new_map = Map_Data(file_read(7), ajustement_x, ajustement_y, team, 8)
    else:
        x, y = 400, 0
        new_map = Map_Data(file_read(3), ajustement_x, ajustement_y, team, 3)
    ajust_x, ajust_y = new_map.player_position(0, 0)
    x -= ajust_x
    y -= ajust_y
    return new_map, x, y


def overworld(p_map, p_party, p_map_number, start=None):
    condition = None
    game_end = False
    ground_level = 0
    closeOverworld = False
    gameMap, x_position, y_position = map_spawn(p_map, start, p_party, p_map_number)
    menu = Game_Menu(p_party, saveProcess, gameMap)
    x_left = 0
    x_right = 0
    x_speed = 0
    y_speed = 0
    y_up = 0
    y_down = 0
    movement_iterator = 0
    orientation = 5
    cooldown = 100
    while not closeOverworld:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_end = True
                closeOverworld = True
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    x_left = 0
                if event.key == pygame.K_RIGHT:
                    x_right = 0
                if event.key == pygame.K_UP:
                    y_up = 0
                if event.key == pygame.K_DOWN:
                    y_down = 0
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_o:
                    print("position: ({},{})".format(x_position, y_position))
                    print("Player position: ({}, {})".format(x_position, y_position))
                    x, y = gameMap.player_position(x_position, y_position)
                    tile = (x+30) // 60 + (gameMap.size_y - (y+30) // 60) * gameMap.size_x
                    print("Tile: {}, {}".format(tile, gameMap.mapaccess[tile]))
                if event.key == pygame.K_LEFT:
                    x_left = -5
                if event.key == pygame.K_RIGHT:
                    x_right = 5
                if event.key == pygame.K_UP:
                    y_up = 5
                if event.key == pygame.K_DOWN:
                    y_down = -5
                if event.key == pygame.K_SPACE:
                    action = gameMap.interaction(x_position, y_position, orientation, condition)
                    condition = action_linker(action, p_party, condition, gameMap)
                    print("Condition: {}".format(condition))
                    if condition == "change map":
                        gameMap, x_position, y_position = map_warp(int(action), p_party)
                        orientation = 5
                        cooldown = 50
                        condition = None
                        action = 0
                    x_left = x_right = y_up = y_down = 0
                if event.key == pygame.K_m:
                    menu.update(p_party, saveProcess, gameMap)
                    new_map_number = menu.menu_loop()
                    if new_map_number is not None:
                        print("Map number: ".format(new_map_number))
                        new_map = file_read(new_map_number)
                        gameMap, x_position, y_position = map_spawn(new_map, start, p_party, new_map_number)
        x_speed = x_left + x_right
        y_speed = y_up + y_down
        #if x_speed != 0 and y_speed != 0:
            #x_speed = int(x_speed * 4/5)
            #y_speed = int(y_speed * 4/5)
        if movement_iterator == 20:
            movement_iterator = 0
        if x_speed != 0 or y_speed != 0:
            movement_iterator += 1
        else:
            movement_iterator = 0
        x_speed, y_speed = gameMap.limits(x_position, y_position, x_speed, y_speed, ground_level)
        x_position += int(x_speed)
        y_position += int(y_speed)
        if not (x_speed == y_speed == 0):
            if cooldown != 0:
                cooldown -= 1
            else:
                if dice(100) > 97 and gameMap.maxLevel > 0:
                    random_fight_encounter(gameMap, p_party)
                    cooldown = 100
                    x_left = 0
                    x_right = 0
                    y_up = 0
                    y_down = 0
        gameDisplay.fill(black)
        gameMap.new_terrain_grid(x_position, y_position)
        orientation, sprite = generate_sprite(orientation, x_speed, y_speed, movement_iterator)
        layer_display(sprite, x_position, y_position, ground_level, gameMap)
        standing_tile = display_player_position(x_position, y_position, orientation, ground_level, gameMap)
        ground_level = gameMap.passive_interaction(standing_tile, ground_level)
        pygame.display.update()
        clock.tick(fps)


map_number = 3
overworld(file_read(map_number), Team, map_number)

pygame.quit()
quit()
