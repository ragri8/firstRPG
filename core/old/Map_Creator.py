from core.old.Textures import *
from interface.Map_Read import *

ajustement_x = int((display_width - 960) / 2)
ajustement_y = int((display_height - 540) / 2)
cross = pygame.image.load("../texture_file/cross.png")
from Tests.character_sheet_test import Team

fps = 40


def display_cross_position(x, y, p_map, mode):
    x, y = p_map.player_position(x, y)
    position_text = "( {} ,  {})".format(x, y)
    TextSurf, TextRect = text_objects(position_text, largeText, white)
    TextRect = (10, 20)
    gameDisplay.blit(TextSurf, TextRect)
    TextSurf, TextRect = text_objects("mode: {}".format(mode), largeText, white)
    TextRect = (10, 40)
    gameDisplay.blit(TextSurf, TextRect)
    position_text2 = "(standing tile: {})".format((x + 0) // 60 + ((y+0) // 60) * p_map.size_x)
    TextSurf, TextRect = text_objects(position_text2, largeText, white)
    TextRect = (10, 60)
    gameDisplay.blit(TextSurf, TextRect)


def display_used_tile(tile):
    pygame.draw.rect(gameDisplay, black, (25, 100, 70, 70))
    tile_sprite = texture_tags.get(tile)
    gameDisplay.blit(tile_sprite, (30, 105))


def display_used_tree(tile):
    pygame.draw.rect(gameDisplay, black, (25, 100, 70, 70))
    tile_sprite = object_tags.get(tile)
    gameDisplay.blit(tile_sprite, (30, 105))


def display_used_boss(tile):
    pygame.draw.rect(gameDisplay, black, (25, 100, 70, 70))
    tile_sprite = boss_tags.get(tile)
    gameDisplay.blit(tile_sprite, (30, 105))


def display_erase():
    pygame.draw.rect(gameDisplay, black, (25, 100, 70, 70))


def creation_loop(p_map, map_number):
    close_creation = False
    gameMap = Edit_Map(p_map, ajustement_x, ajustement_y, Team, map_number)
    x_position = y_position = 0
    x_left = 0
    x_right = 0
    x_speed = 0
    y_speed = 0
    y_up = 0
    y_down = 0
    key_list = ["aa", "a0", "ab", "ac", "ad", "ae", "af", "ag", "ah", "ai", "aj", "ak", "al", "am", "an", "ao", "ap",
                "aq", "ar", "as", "at", "au", "av", "aw", "ax", "ay", "az", "aA", "aB", "aC", "aD", "aE", "aF", "aG",
                "aH", "aI", "aJ", "aK", "aL", "aM", "aN", "aO", "aP", "aQ", "aR", "aS", "aT", "aU", "aV", "aW", "aX",
                "a1", "a2"]
    object_list = ["6001", "5500", "5501", "5502", "5503", "5504", "5505", "5506", "5507", "5508", "5509", "5510",
                   "5511", "delete"]
    boss_list = ["7001", "7002", "7003", "7004", "7005", "delete"]
    mode = "tile"
    tile_used = 0
    while not close_creation:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                close_creation = True
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    x_left = 0
                if event.key == pygame.K_RIGHT:
                    x_right = 0
                if event.key == pygame.K_UP:
                    y_up = 0
                if event.key == pygame.K_DOWN:
                    y_down = 0
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x_left = -5
                if event.key == pygame.K_RIGHT:
                    x_right = 5
                if event.key == pygame.K_UP:
                    y_up = 5
                if event.key == pygame.K_DOWN:
                    y_down = -5
                if event.key == pygame.mouse:
                    mouse_position_x = x_position  # Need to add real mouse position
                    mouse_position_y = y_position  # Need to add real mouse position
                    print("mouse left click")
                if event.key == pygame.K_1:
                    if mode != "tile":
                        mode = "tile"
                        tile_used = 0
                    elif tile_used > 0:
                        tile_used -= 1
                    else:
                        tile_used = len(key_list) - 1
                if event.key == pygame.K_2:
                    if mode != "tile":
                        mode = "tile"
                        tile_used = 0
                    elif tile_used < len(key_list) - 1:
                        tile_used += 1
                    else:
                        tile_used = 0
                if event.key == pygame.K_3:
                    if mode != "object":
                        mode = "object"
                        tile_used = 0
                    elif tile_used > 0:
                        tile_used -= 1
                    else:
                        tile_used = len(object_list) - 1
                if event.key == pygame.K_4:
                    if mode != "object":
                        mode = "object"
                        tile_used = 0
                    elif tile_used < len(object_list) - 1:
                        tile_used += 1
                    else:
                        tile_used = 0
                if event.key == pygame.K_5:
                    if mode != "boss":
                        mode = "boss"
                        tile_used = 0
                    elif tile_used > 0:
                        tile_used -= 1
                    else:
                        tile_used = len(boss_list) - 1
                if event.key == pygame.K_6:
                    if mode != "boss":
                        mode = "boss"
                        tile_used = 0
                    elif tile_used < len(boss_list) - 1:
                        tile_used += 1
                    else:
                        tile_used = 0
                if event.key == pygame.K_SPACE:
                    if mode == "tile":
                        gameMap.change_tile(key_list[tile_used], x_position+int(display_width / 2) - 35, -y_position+int(display_height / 2) + 30)
                    elif mode == "object":
                        gameMap.change_tree(object_list[tile_used], x_position, y_position)
                    elif mode == "boss":
                        gameMap.change_boss(boss_list[tile_used], x_position, y_position)
                if event.key == pygame.K_s:
                    mapfile_save(gameMap, map_number)
        x_speed = x_left + x_right
        y_speed = y_up + y_down
        x_position += int(x_speed)
        y_position += int(y_speed)
        gameDisplay.fill(black)
        gameMap.new_terrain_grid(x_position, y_position)
        for i in gameMap.object_list:
            i.display_level_0(x_position, y_position, gameMap)
            i.display_level_1(x_position, y_position, gameMap)
            i.display_level_2(x_position, y_position, gameMap)
        for i in gameMap.special_list:
            i.display_level_0(x_position, y_position, gameMap)
            i.display_level_1(x_position, y_position, gameMap)
            i.display_level_2(x_position, y_position, gameMap)
        gameDisplay.blit(cross, (int(display_width / 2) - 35, int(display_height / 2) + 35))
        display_cross_position(x_position, y_position, gameMap, mode)
        if mode == "tile":
            display_used_tile(key_list[tile_used])
        elif mode == "object" and tile_used != len(object_list) - 1:
            display_used_tree(object_list[tile_used])
        elif mode == "boss" and tile_used != len(boss_list) - 1:
            display_used_boss(boss_list[tile_used])
        else:
            display_erase()
        pygame.display.update()
        clock.tick(fps)


class Edit_Map(Map_Data):
    def __init__(self, mapcode, ajust_x, ajust_y, team, map_number):
        super().__init__(mapcode, ajust_x, ajust_y, team, map_number)

    def new_terrain_grid(self, player_x, player_y):
        for x in range(-60, display_width + 60, tile_size):
            for y in range(-60, display_height + 60, tile_size):
                tile, position = self.getTile(x, y, player_x, player_y)
                if tile is None:
                    continue
                gameDisplay.blit(tile, (x - (player_x % tile_size), y + (player_y % tile_size)))
                gameDisplay.blit(frame, (x - (player_x % tile_size), y + (player_y % tile_size)))
        for i in self.mapnpc:
            self.display_npc(i, player_x, player_y)
        for i in self.map_boss:
            self.display_boss(i, player_x, player_y)
        for i in self.object_list:
            i.display_level_0(player_x, player_y, self)
        for i in self.chest_list:
            i.display_chest(player_x, player_y, self)

    def change_tile(self, tile, x, y):
        print("x position: {}, y position: {}".format(x, y))
        position = (x + 15) // 60 + ((y+20) // 60) * self.size_x
        if position < 0:
            return None
        print(position)
        new_mapcode = ""
        for i in range(len(self.mapcode)):
            if i == position:
                new_mapcode += tile
            else:
                new_mapcode += self.mapcode[i]
        self.mapcode = new_mapcode

    def change_tree(self, tree, x, y):
        x, y = self.player_position(x, y)
        position = (x + 0) // 60 + ((y + 0) // 60) * self.size_x
        if position < 0:
            return None
        new_interaction = ""
        tree_placed = True
        if tree != "delete":
            tree_placed = False
        for i in self.mapinteraction:
            if position == int(i[0]):
                if tree == "delete":
                    continue
                position = (4 - len(str(position)))*"0" + str(position)
                new_interaction += position + tree
                tree_placed = True
            else:
                new_interaction += str(i[0]) + str(i[1])
        if not tree_placed:
            position = (4 - len(str(position))) * "0" + str(position)
            new_interaction += position + tree
        self.object_list = []
        self.special_list = []
        self.chest_list = []
        self.map_boss = []
        self.mapinteraction = self.interactive_tile(new_interaction)
        self.mapnpc = self.npc_location(self.mapinteraction)

    def change_boss(self, boss, x, y):
        x, y = self.player_position(x, y)
        position = (x + 0) // 60 + ((y + 0) // 60) * self.size_x
        if position < 0:
            return None
        new_interaction = ""
        boss_placed = True
        if boss != "delete":
            boss_placed = False
        for i in self.mapinteraction:
            if position == int(i[0]):
                if boss == "delete":
                    continue
                position = (4 - len(str(position))) * "0" + str(position)
                new_interaction += position + boss
                boss_placed = True
            else:
                new_interaction += str(i[0]) + str(i[1])
        if not boss_placed:
            position = (4 - len(str(position))) * "0" + str(position)
            new_interaction += position + boss
        self.object_list = []
        self.chest_list = []
        self.special_list = []
        self.map_boss = []
        self.mapinteraction = self.interactive_tile(new_interaction)
        self.mapnpc = self.npc_location(self.mapinteraction)


creation_loop(file_read(7), 7)
pygame.quit()
quit()
