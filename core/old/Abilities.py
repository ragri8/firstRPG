from core.Equipment_and_Other import dice


def attackAbilityEffect(p_attack, p_player):
    if p_attack == "Basic attack":
        (damage, numberTarget, damageType) = basicAttack(p_player)
        return damage, numberTarget, damageType
    elif p_attack == "Power attack":
        (damage, numberTarget, damageType) = powerAttack(p_player)
        return damage, numberTarget, damageType


def basicAttack(p_player):
    if p_player.weapon.wpntype in ["sword", "rapier", "axe", "mace", "spear", "staff", "bow"]:
        damageType = "pattack"
        damage = basicAttackDamageAlgorithm(p_player, damageType)
        return damage, 1, damageType
    elif p_player.weapon.wpntype in ["magic bow"]:
        damageType = "mattack"
        damage = basicAttackDamageAlgorithm(p_player, damageType)
        return damage, 1, damageType


def powerAttack(p_player):
    (damage, numberTarget, damageType) = basicAttack(p_player)
    damage = (damage * 1.5) // 1
    return damage, numberTarget, damageType


def basicAttackDamageAlgorithm(p_player, p_damageType):
    damage = p_player.weapon.baseattack
    for i in range(p_player.weapon.nbrdmgdice):
        damage += dice(p_player.weapon.damagedice)
    if p_damageType == "pattack":
        damage += p_player.tempStrength // 2
    elif p_damageType == "mattack":
        damage += p_player.tempIntel // 2
    return damage


def spellEffect(p_player, p_spell, p_target, numberstrike=1):
    if p_spell.search() == "OffensiveSpell":
        return offensiveSpellDamage(p_player, p_spell, numberstrike)
    elif p_spell.search() == "StatusSpell":
        return statusSpellEffect(p_player, p_spell, p_target)
    elif p_spell.search() == "HealingSpell":
        return healingSpellEffect(p_player, p_spell)


def offensiveSpellDamage(p_player, p_spell, numberstrike):  # To erase after changing dependancies
    damage = 0
    damage += p_player.intel // 2
    for i in range(numberstrike):
        partialDamage = 0
        if p_player.weapon.wpntype == "staff":
            partialDamage += p_player.weapon.baseattack
        partialDamage += p_spell.beffect
        for j in range(p_spell.nbrdice):
            partialDamage += dice(p_spell.effectdice)
        partialDamage = int(partialDamage)
        print("Partial damage{}: {}".format(i, partialDamage))
        damage += partialDamage
    return int(damage)


def offensiveSpellEffect(p_user, p_spell, p_target):
    dialogList = []
    duration = 2 + dice(4)
    if p_spell.element == "fire":
        effectProbability = dice(100)
        if 100 - p_user.tempLuck > effectProbability:
            return dialogList
        for i in p_target.fightStatus.statusEffect:
            if i.element == "fire":
                i.duration += 2
                return dialogList
        dialog = "{} got burned!".format(p_target)
        print(dialog)
        dialogList.append(dialog)
        p_target.fightStatus.addMagicEffect(p_spell.element, p_spell.beffect, duration)
    elif p_spell.element == "ice":
        return dialogList
    elif p_spell.element == "poison":
        for i in p_target.fightStatus.statusEffect:
            if i.element == "poison":
                return dialogList
        effectProbability = dice(100)
        if 100 - (20 + p_user.tempLuck * 2) > effectProbability:
            return dialogList
        duration = 999
        dialog = "{} got poisoned!".format(p_target)
        print(dialog)
        dialogList.append(dialog)
        p_target.fightStatus.addMagicEffect(p_spell.element, p_spell.beffect, duration)
        return dialogList
    else:
        return dialogList
    return dialogList


def statusSpellEffect(p_user, p_spell, p_target):
    dialogList = []
    if p_spell.typeeffect == "strength buff":
        maxStrengthBonus = p_target.fightStatus.strengthLimit - p_target.tempStrength
        strengthBonus = p_spell.beffect + int(p_user.tempIntel // p_spell.effectdice)
        if strengthBonus < maxStrengthBonus:
            p_target.tempStrength += strengthBonus
            dialog = "{} gained {} strength".format(p_target, strengthBonus)
            print(dialog)
            dialogList.append(dialog)
        elif strengthBonus > maxStrengthBonus > 0:
            p_target.tempStrength += int(maxStrengthBonus)
            dialog = "{} gained {} strength".format(p_target, int(maxStrengthBonus))
            print(dialog)
            dialogList.append(dialog)
        elif maxStrengthBonus == 0:
            dialog = "{} strength is already maxed out".format(p_target)
            print(dialog)
            dialogList.append(dialog)
    elif p_spell.typeeffect == "defense buff":
        maxDefenseBonus = p_target.fightStatus.defenseLimit - p_target.tempPDefense
        defenseBonus = p_spell.beffect + int(p_user.tempIntel // p_spell.effectdice)
        if defenseBonus < maxDefenseBonus:
            p_target.tempPDefense += defenseBonus
            p_target.tempMDefense += defenseBonus
            dialog = "{} gained {} defense!".format(p_target, defenseBonus)
            print(dialog)
            dialogList.append(dialog)
        elif defenseBonus > maxDefenseBonus > 0:
            p_target.tempPDefense += maxDefenseBonus
            p_target.tempMDefense += maxDefenseBonus
            dialog = "{} gained {} defense!".format(p_target, maxDefenseBonus)
            print(dialog)
            dialogList.append(dialog)
        elif maxDefenseBonus == 0:
            dialog = "{} defense is already maxed out!".format(p_target)
            print(dialog)
            dialogList.append(dialog)
    elif p_spell.typeeffect == "agility buff":
        maxAgilityBonus = p_target.fightStatus.agilityLimit - p_target.tempAgility
        agilityBonus = int(p_user.tempIntel // 6)
        if agilityBonus < maxAgilityBonus:
            p_target.tempPDefense += agilityBonus
            p_target.tempMDefense += agilityBonus
            dialog = "{} gained {} of agility!".format(p_target, agilityBonus)
            print(dialog)
            dialogList.append(dialog)
        elif agilityBonus > maxAgilityBonus > 0:
            p_target.tempPDefense += maxAgilityBonus
            p_target.tempMDefense += maxAgilityBonus
            dialog = "{} gained {} of agility!".format(p_target, maxAgilityBonus)
            print(dialog)
            dialogList.append(dialog)
        elif maxAgilityBonus == 0:
            dialog = "{}'s agility is already maxed out!".format(p_target)
            print(dialog)
            dialogList.append(dialog)
    elif p_spell.typeeffect == "luck buff":
        print("Player luck: {}".format(p_target.luck))
        print("Player temp luck: {}".format(p_target.tempLuck))
        if p_target.tempLuck == int(p_target.luck):
            p_target.tempLuck += p_spell.beffect + int(p_user.tempIntel//p_spell.effectdice)
            if p_target.tempLuck > 60:
                p_target.tempLuck = 60
            dialog = "{}'s critical chance raised!".format(p_target)
            duration = 2+dice(3)
            p_target.fightStatus.addMagicEffect(p_spell.typeeffect, p_spell.beffect, duration)
            print(dialog)
            dialogList.append(dialog)
        else:
            dialog = "It has no effect"
            print(dialog)
            dialogList.append(dialog)
    elif p_spell.typeeffect == "recovery":
        if len(p_target.fightStatus.statusEffect) == 0:
            dialog = "Nothing happened!"
            print(dialog)
            dialogList.append(dialog)
        else:
            for i in p_target.fightStatus.statusEffect:
                if i.element == "poison":
                    p_target.fightStatus.statusEffect.remove(i)
                    dialog = "{} is no longer poisonned!".format(p_target)
                    print(dialog)
                    dialogList.append(dialog)
                elif i.element == "fire":
                    p_target.fightStatus.statusEffect.remove(i)
                    dialog = "{} isn't burned anymore!".format(p_target)
                    print(dialog)
                    dialogList.append(dialog)
    return dialogList


def healingSpellEffect(p_player, p_spell):  # To erase after changing dependancies
    healingPoints = p_spell.beffect
    healingPoints += (p_player.intel - 10) // 2
    for i in range(p_spell.nbrdice):
        healingPoints += dice(p_spell.effectdice)
    return int(healingPoints)
