from math import tanh

from core.old.Abilities import *
from objects.Monsters_and_Characters_Sheets import intelligentCreatureList


# Class to manage a fight between the player party and an enemy party
# A new object is created after each fight
class DisplayFight:
    # intitiate the class object
    # param [in] p_playerParty: party of the user, as a PlayerParty object type
    # param [in] p_enemyParty: list with the enemy party
    # param [in] test: bool object, if True add bonus stats to enemies, for fast testing
    def __init__(self, p_playerParty, p_enemyParty, test=True):
        if test is True:
            for i in p_enemyParty:
                i.const *= 1.2
                i.maxhp *= 1.2
                i.maxhp = int(i.maxhp)
                i.hpleft = i.maxhp
                i.pdefense += 6
                i.mdefense += 4
        self.team = p_playerParty
        self.playerParty = p_playerParty.members
        self.standingPartyMembers = p_playerParty.members[:]
        self.faintedpartymembers = []
        self.startingEnemyParty = p_enemyParty
        self.enemyParty = p_enemyParty[:]
        self.faintedEnemyParty = []
        self.skipTurn = []
        self.fightType = None
        self.reward = 0
        self.groupxp = 0
        for i in self.playerParty:
            i.initTempStats()
        for i in self.enemyParty:
            i.initTempStats()
            i.newTargetPriorityInit(self.playerParty)
        self.enemyLeader = self.enemyParty[0]
        self.turnNbr = 0
        self.tempFightOrder = []
        self.tempDamage = 0
        self.tempNumberTarget = 0
        self.tempDamageType = ""
        self.tempCost = 0
        self.tempPage = 0
        self.tempSpell = None
        self.tempTargetGroup = None
        self.tempItem = None
        self.tempChoiceListLength = 0
        self.tempDodge = False
        self.attack = None
        self.escape = False

    # manage any duplicated enemy names by adding a suffix number after them
    def nameIssues(self):
        enemyNbr = len(self.enemyParty)
        for i in range(enemyNbr):
            nameDuplicate = [i]
            for j in range(enemyNbr):
                if i == j:
                    continue
                if self.enemyParty[i].__str__() == self.enemyParty[j].__str__():
                    nameDuplicate.append(j)
            if len(nameDuplicate) > 1:
                iterator = 0
                for j in nameDuplicate:
                    iterator += 1
                    newName = self.enemyParty[j].__str__() + " " + str(iterator)
                    self.enemyParty[j].nameset(newName)
                return self.nameIssues()

    # first method called when fight begin and display first dialogs
    # set last issues before the fight
    # return dialog to display
    def fightEncounter(self):
        for i in self.enemyParty:
            if i.level > self.enemyLeader.level:
                self.enemyLeader = i
        dialog1 = "{} party encountered".format(self.enemyLeader.__str__())
        dialog2 = "Begin fight!"
        print(dialog1)
        print(dialog2)
        self.fightType = self.enemyLeader.enemytype
        self.nameIssues()
        dialogList = [dialog1, dialog2]
        choiceList = []
        return dialogList, choiceList, "First turn"

    def sortTurnOrder(self):
        self.turnNbr += 1
        self.tempFightOrder = []
        if len(self.skipTurn) > 0:
            self.ready()
        tempFighters = [*self.standingPartyMembers, *self.enemyParty]
        tempFighterAgility = []
        for i in tempFighters:
            tempFighterAgility.append(i.tempAgility)
        while len(tempFighters) != 0:
            order = tempFighterAgility.index(sorted(tempFighterAgility)[-1])
            self.tempFightOrder.append(tempFighters.pop(order))
            tempFighterAgility.pop(order)

    def ready(self):
        for i in self.skipTurn:
            if i in self.playerParty:
                self.skipTurn.remove(i)
                return self.ready()
            elif i in self.enemyParty:
                self.skipTurn.remove(i)
                return self.ready()
            else:
                self.skipTurn.remove(i)
                return self.ready()

    def mainFightIterative(self):
        self.tempDodge =False
        for i in [*self.playerParty]:
            i.spriteCondition = "waiting"
        for i in [*self.enemyParty]:
            i.spriteCondition = "waiting"
        if len(self.tempFightOrder) == 0:
            for i in self.enemyParty:
                i.targetIncrement(self.playerParty)
            for i in self.standingPartyMembers:
                i.fightclass.turnRecovery(i)
            self.sortTurnOrder()
        fighter = self.tempFightOrder[0]
        if fighter.search() == "Player":
            if fighter.hpleft <= 0 or fighter in self.skipTurn:
                del self.tempFightOrder[0]
                if fighter.hpleft > 0:
                    self.settingStatus(fighter)
                return self.mainFightIterative()
            else:
                return self.playerTurnDialog(fighter)
        elif fighter.search() == "Monster":
            del self.tempFightOrder[0]
            if fighter.hpleft <= 0 or fighter in self.skipTurn:
                return self.mainFightIterative()
            else:
                print("Enemy turn!!!!")
                return self.enemyTurn(fighter)
        else:
            fighter = self.tempFightOrder.pop(0)
            return self.mainFightIterative()

    def playerTurnDialog(self, p_player):
        dialog = "{}'s turn, what do you want to do?".format(p_player)
        dialogList = [dialog]
        choiceList = ["-Attack", "-Cast spell", "-Use item", "-Defend", "-Flee"]
        print(dialog)
        for i in choiceList:
            print(i)
        return dialogList, choiceList, "Player choice"

    def playerTurn(self, p_player, action):
        dialogList = []
        if action == "1":
            return self.playerAttackDialog(p_player)
        elif action == "2":
            if len(p_player.spelllist) == 0:
                dialog = "You have no spell"
                print(dialog)
                dialogList.append(dialog)
                return dialogList, [], "Return main"
            return self.playerSpellCastDialog(p_player)
        elif action == "3":
            if len(p_player.items) == 0:
                dialog = "You have no item"
                print(dialog)
                dialogList.append(dialog)
                return dialogList, [], "Return main"
            return self.playerUseItemDialog(p_player)
        elif action == "4":
            dialogList.append(self.fighterDefend(p_player))
            for i in self.settingStatus(p_player):
                dialogList.append(i)
            del self.tempFightOrder[0]
            return dialogList, [], "Main fight"
        elif action == "5":
            result, dialog = self.playerFlee(p_player)
            dialogList.append(dialog)
            if result == "no escape":
                return dialogList, [], "Return main"
            elif result == "Failure":
                del self.tempFightOrder[0]
                for i in self.settingStatus(p_player):
                    dialogList.append(i)
                return dialogList, [], "Main fight"
            elif result == "Success":
                return dialogList, [], "escape"

    def playerAttackDialog(self, p_player):
        dialog = "Which attack:"
        dialogList = [dialog]
        choiceList = []
        attackChoice = p_player.attackAbility()
        attackCost = p_player.attackAbilityCost()
        print(dialog)
        for i in range(len(attackChoice)):
            choice = "-{}: cost {} SP".format(attackChoice[i], attackCost[i])
            choiceList.append(choice)
            print(choice)
        choice = "-cancel"
        print(choice)
        choiceList.append(choice)
        return dialogList, choiceList, "Player attack"

    def playerAttack(self, p_player, attack):
        attackChoice = p_player.attackAbility()
        attackCost = p_player.attackAbilityCost()
        if attack.isdigit():
            if 0 <= int(attack) - 1 < len(attackChoice):
                attack = attackChoice[int(attack) - 1]
            elif int(attack) - 1 == len(attackChoice):
                return self.playerTurnDialog(p_player)
        if attack in attackChoice:
            (damage, self.tempNumberTarget, self.tempDamageType) = attackAbilityEffect(attack, p_player)
            self.attack = attack
            damage *= p_player.tempWeaponBonus
            self.tempDamage = int(damage)
            self.tempCost = attackCost[attackChoice.index(attack)]
            return self.playerTargetEnemyDialog(p_player, self.tempNumberTarget, self.tempDamageType, self.tempCost)

    def playerSpellCastDialog(self, p_player, page=0):
        dialogList = []
        choiceList = []
        dialog = "Choose a spell:"
        print(dialog)
        print("Number of spell: {}".format(len(p_player.spelllist)))
        dialogList.append(dialog)
        if page > 0:
            choice = "-previous page"
            print(choice)
            choiceList.append(choice)
        if len(p_player.spelllist) > 4+page*4:
            for i in p_player.spelllist[page*4:4+page*4]:
                choice = "-{}, cost {} MP".format(i, i.manacost)
                print(choice)
                choiceList.append(choice)
            choice = "-next page"
            print(choice)
            choiceList.append(choice)
        else:
            for i in p_player.spelllist[page*4:]:
                choice = "-{}, cost {} MP".format(i, i.manacost)
                print(choice)
                choiceList.append(choice)
        choice = "-cancel"
        print(choice)
        choiceList.append(choice)
        self.tempPage = page
        self.tempChoiceListLength = len(choiceList)
        return dialogList, choiceList, "Player spell"

    def playerSpellCast(self, p_player, spell):
        dialogList = []
        base = 0
        page = self.tempPage
        spellList = []
        if page > 0:
            base = 1
        if len(p_player.spelllist) > 4+page*4:
            for i in p_player.spelllist[page*4:4+page*4]:
                spellList.append(i.__str__())
        else:
            for i in p_player.spelllist[page*4:]:
                spellList.append(i.__str__())
        if spell.isdigit():
            print(spell)
            if int(spell) == self.tempChoiceListLength:
                return self.playerTurnDialog(p_player)
            elif base <= (int(spell) - 1) <= len(p_player.spelllist)-page*4 and int(spell)-1 < 4+base:
                spell = spellList[int(spell)-1-base]  # bug à régler, semble réglé
                print("Spell choosed: {}".format(spell))
            elif int(spell) == base:
                page -= 1
                dialogList, choiceList, nextFunction = self.playerSpellCastDialog(p_player, page)
                return dialogList, choiceList, nextFunction
            elif int(spell) == self.tempChoiceListLength - 1:
                page += 1
                dialogList, choiceList, nextFunction = self.playerSpellCastDialog(p_player, page)
                return dialogList, choiceList, nextFunction
            elif int(spell) == self.tempChoiceListLength:
                return self.playerTurnDialog(p_player)
        if spell in spellList:
            spellIndex = spellList.index(spell) + page * 4
            spell = p_player.spelllist[spellIndex]
            if p_player.manaleft < spell.manacost:
                dialog = "Not enought mana point left"
                print(dialog)
                dialogList.append(dialog)
                return dialogList, [], "Main fight"
            if spell.search() == "OffensiveSpell":
                if spell.nbrtarget == 1:
                    self.tempSpell = spell
                    self.tempTargetGroup = self.enemyParty
                    return self.spellTargetDialog()
                elif spell.nbrtarget == 2:
                    p_player.manaleft -= spell.manacost
                    p_player.spellUsed(spell.typeeffect)
                    dialog = "{} use {}".format(p_player, spell)
                    print(dialog)
                    dialogList.append(dialog)
                    critical = self.criticalBool(p_player)
                    if critical:
                        dialog = "Critical strike!!!"
                        print(dialog)
                        dialogList.append(dialog)
                    for i in self.enemyParty:
                        dialogs = self.spellAttack(p_player, i, spell, True, critical)
                        for j in dialogs:
                            dialogList.append(j)
                    del self.tempFightOrder[0]
                    for i in self.settingStatus(p_player):
                        dialogList.append(i)
                    return dialogList, [], "Main fight"
            elif spell.search() in ["StatusSpell", "HealingSpell"]:
                if spell.nbrtarget == 1:
                    self.tempSpell = spell
                    self.tempTargetGroup = self.standingPartyMembers
                    return self.spellTargetDialog()
                elif spell.nbrtarget == 2:
                    dialog = "{} use {}".format(p_player, spell)
                    print(dialog)
                    dialogList.append(dialog)
                    critical = self.criticalBool(p_player)
                    if critical:
                        dialog = "Critical strike!!!"
                        print(dialog)
                        dialogList.append(dialog)
                    p_player.manaleft -= spell.manacost
                    if spell.typeeffect == "healing":
                        p_player.spellUsed(spell.typeeffect)
                        for i in self.enemyParty:
                            if i.race in intelligentCreatureList:
                                i.undirectRageBuild(self.playerParty, p_player)
                    for i in self.standingPartyMembers:
                        dialogs = self.spellAttack(p_player, i, spell, True, critical)
                        for j in dialogs:
                            dialogList.append(j)
                    del self.tempFightOrder[0]
                    for i in self.settingStatus(p_player):
                        dialogList.append(i)
                    return dialogList, [], "Main fight"
        elif spell in ["Cancel", "cancel", "c"]:
            return self.playerTurnDialog(p_player)

    def playerTargetEnemyDialog(self, p_player, p_numberTarget, p_damageType, p_cost):
        dialogList = []
        choiceList = []
        if p_numberTarget == 1:
            dialog = "Choose your target:"
            print(dialog)
            dialogList.append(dialog)
            for i in self.enemyParty:
                choice = "-{}".format(i)
                print(choice)
                choiceList.append(choice)
            choice = "-cancel"
            print(choice)
            choiceList.append(choice)
            return dialogList, choiceList, "Player target"
        else:
            if p_player.stamleft < self.tempCost:  # Implantation future: contournement de la condition si le joueur possède une abileté spéciale
                dialog = "Attack failed, not enought stamina"
                del self.tempFightOrder[0]
                for i in self.settingStatus(p_player):
                    dialogList.append(i)
                return [dialog], [], "Main fight"
            p_player.stamleft -= p_cost
            damage, criticalConfirm = self.criticalStrike(p_player, self.tempDamage)
            for i in self.enemyParty:
                finalDamage = i.newDamageResist(damage, p_damageType)
                dialogs = self.playerDamageDealt(p_player, i, finalDamage, criticalStrike=criticalConfirm)
                dialogList.append(*dialogs)
            return dialogList, choiceList, "Main fight"

    def playerTargetEnemy(self, p_fighter, p_target):
        targetList = []
        dialogList = []
        print(p_target)
        for i in self.enemyParty:
            targetList.append(i.__str__())
        if p_target.isdigit():
            if 0 <= (int(p_target) - 1) < len(targetList):
                p_target = targetList[int(p_target) - 1]
            elif int(p_target) == len(targetList) + 1:
                return self.playerAttackDialog(p_fighter)
        if p_fighter.stamleft < self.tempCost:  # Implantation future: contournement de la condition si le joueur possède une abileté spéciale
            dialog = "Attack failed, not enought stamina"
            del self.tempFightOrder[0]
            for i in self.settingStatus(p_fighter):
                dialogList.append(i)
            return [dialog], [], "Main fight"
        if p_target in targetList:
            p_fighter.stamleft -= self.tempCost
            targetIndex = targetList.index(p_target)
            targetRange = (self.tempNumberTarget - 1) // 2
            for i in range(targetIndex - targetRange, targetIndex + targetRange + 1):
                if 0 <= i <= len(self.enemyParty):
                    damage = []
                    numberStrike = int(self.strike(p_fighter, self.enemyParty[i]))
                    for j in range(numberStrike):
                        (partialdamage, self.tempNumberTarget, self.tempDamageType) = attackAbilityEffect(self.attack, p_fighter)
                        partialdamage *= p_fighter.tempWeaponBonus
                        partialdamage = int(partialdamage)
                        print("Partial damage{}: {}".format(j, partialdamage))
                        damage.append(partialdamage)
                    criticalConfirm = self.criticalBool(p_fighter)
                    if criticalConfirm:
                        dialog = "Critical strike on the target!"
                        print(dialog)
                        dialogList.append(dialog)
                        damage = self.criticalStrikeConfirmed(damage, p_fighter)
                        for d in damage:
                            print("Critical damage: ", d)
                    finalDamage = 0
                    for k in damage:
                        partialDamage = self.enemyParty[i].newDamageResist(k, self.tempDamageType)
                        finalDamage += partialDamage
                    dialogs = self.playerDamageDealt(p_fighter, self.enemyParty[i], finalDamage,
                                           criticalStrike=criticalConfirm)
                    for i in dialogs:
                        dialogList.append(i)
            del self.tempFightOrder[0]
            for i in self.settingStatus(p_fighter):
                dialogList.append(i)
            return dialogList, [], "Main fight"

    def spellTargetDialog(self):
        dialogList = []
        dialog = "Choose your target:"
        print(dialog)
        dialogList.append(dialog)
        targetList = []
        for i in self.tempTargetGroup:
            choice = "-{}".format(i.__str__())
            print(choice)
            targetList.append(choice)
        choice = "-cancel"
        print(choice)
        targetList.append(choice)
        return dialogList, targetList, "Spell target"

    def spellTarget(self, p_player, p_target):
        dialogList = []
        spell = self.tempSpell
        targetList = []
        for i in self.tempTargetGroup:
            targetList.append(i.__str__())
            print("-{}".format(i))
        if p_target.isdigit():
            if 0 <= (int(p_target) - 1) < len(targetList):
                p_target = targetList[int(p_target) - 1]
            elif int(p_target) == len(targetList) + 1:
                return self.playerSpellCastDialog(p_player)
        if p_target in targetList:
            p_player.manaleft -= spell.manacost
            p_player.spellUsed(spell.typeeffect)
            if spell.typeeffect == "healing":
                for i in self.enemyParty:
                    if i.race in intelligentCreatureList:
                        i.undirectRageBuild(self.playerParty, p_player)
            targetIndex = targetList.index(p_target)
            target = self.tempTargetGroup[targetIndex]
            critical = self.criticalBool(p_player)
            if critical:
                dialog = "Critical strike!!!"
                print(dialog)
                dialogList.append(dialog)
            dialogs = self.spellAttack(p_player, target, spell, p_critical=critical)
            for i in dialogs:
                dialogList.append(i)
            del self.tempFightOrder[0]
            for i in self.settingStatus(p_player):
                dialogList.append(i)
            return dialogList, [], "Main fight"

    def spellAttack(self, p_player, p_target, p_spell, p_multiTarget=False, p_critical=False):
        if p_spell.search() == "OffensiveSpell":
            numberStrike = int(self.strike(p_player, p_target))
            damage = spellEffect(p_player, p_spell, p_target, numberStrike)
            damage *= p_player.spellBonus(p_spell)
            if p_critical:
                damage += damage
            print("Total damage before def: {}".format(damage))
            finalDamage = p_target.newDamageResist(damage, p_spell.typeeffect)
            dialogs = self.playerDamageDealt(p_player, p_target, finalDamage, p_spell.__str__(),
                                             p_multiTarget, criticalStrike=p_critical)
            if p_target.hpleft > 0 and self.tempDodge is True:
                effectDialog = offensiveSpellEffect(p_player, p_spell, p_target)
                for i in effectDialog:
                    dialogs.append(i)
            return dialogs
        elif p_spell.search() == "HealingSpell":
            healingPoints = spellEffect(p_player, p_spell, p_target)
            healingPoints = int(healingPoints * p_player.spellBonus(p_spell))
            if p_critical:
                healingPoints *= 2
            dialogs = self.playerHealing(p_player, p_target, healingPoints)
            return dialogs
        elif p_spell.search() == "StatusSpell":
            dialogs = spellEffect(p_player, p_spell, p_target)
            return dialogs

    def criticalStrike(self, p_player, p_damage):
        if p_player.tempLuck >= dice(100):
            if p_player.weapon.wpntype == "axe":
                return p_damage * 2, True
            return int(p_damage * 1.5), True
        return p_damage, False

    def criticalStrikeConfirmed(self, damage, p_fighter):
        criticalDamage = []
        for i in damage:
            if p_fighter.weapon.wpntype == "axe":
                criticalDamage.append(i * 2)
            else:
                criticalDamage.append(int(i * 1.5))
        return criticalDamage

    def criticalBool(self, p_player):
        return p_player.tempLuck >= dice(100)

    def dodge(self, p_attacker, p_target):
        diceResult = dice(100)
        dodgingProb = (90 + 10 * tanh(1.5 * (p_attacker.tempAgility - p_target.tempAgility) / p_attacker.tempAgility))
        if dodgingProb < diceResult:
            print(dodgingProb, "<", diceResult)
            return True
        else:
            print(dodgingProb, ">", diceResult)
            return False

    def settingStatus(self, p_fighter):
        p_fighter.tempDefendingStatus = False
        if len(p_fighter.statusEffect()) > 0:
            return self.applyStatusEffect(p_fighter)
        return []

    def playerDamageDealt(self, p_player, p_target, p_damage, p_spell="none", p_multiTarget=False, criticalStrike=False, counter=False):
        dialogList = []
        p_player.spriteCondition = "attacking"
        if self.dodge(p_player, p_target) and not criticalStrike and not counter:
            dialog = "{} dodged the attack!".format(p_target)
            print(dialog)
            dialogList.append(dialog)
            self.tempDodge = True
        elif p_damage >= 1:
            p_target.spriteCondition = "takingDamage"
            if p_spell != "none":
                if p_multiTarget:
                    dialog = "{} received {} damage".format(p_target, p_damage)
                    print(dialog)
                    dialogList.append(dialog)
                else:
                    dialog = "{} dealt {} damage with {} ".format(p_player.name, int(p_damage), p_spell)
                    print(dialog)
                    dialogList.append(dialog)
            else:
                dialog = "{} dealt {} damage with {} {}".format(p_player.name, int(p_damage),
                                                             p_player.article(), p_player.weapon)
                print(dialog)
                dialogList.append(dialog)
            for i in self.enemyParty:
                if i == p_target:
                    i.attackRageBuild(self.playerParty, p_player)
                else:
                    i.undirectRageBuild(self.playerParty, p_player)
            p_target.hpleft -= p_damage
            if p_target.hpleft <= 0:
                dialog = "{} fainted!".format(p_target)
                print(dialog)
                dialogList.append(dialog)
                if p_spell == "none":
                    p_player.weaponKill()
        elif p_damage <= 0:
            dialog = "{} blocked the attack!".format(p_target)
            print(dialog)
            dialogList.append(dialog)
        return dialogList

    def playerHealing(self, p_player, p_target, p_healingPoints):
        dialogList = []
        target_display = p_target.__str__()
        if p_target == p_player:
            if p_player.gender == "male":
                target_display = "himself"
            elif p_player.gender == "female":
                target_display = "herself"
        dialog = "{} healed {} by {} healt points".format(p_player, target_display, p_healingPoints)
        print(dialog)
        dialogList.append(dialog)
        p_target.hpleft += p_healingPoints
        if p_target.hpleft >= p_target.maxhp:
            p_target.hpleft = p_target.maxhp
            dialog = "{} is fully restored!".format(p_target)
            print(dialog)
            dialogList.append(dialog)
        return dialogList

    def playerUseItemDialog(self, p_player, page=0):
        dialogList = []
        itemList = []
        dialog = "Which item do you want to use?"
        print(dialog)
        dialogList.append(dialog)
        if page > 0:
            choice = "-previous page"
            print(choice)
            itemList.append(choice)
        if len(p_player.items) > 4+page*4:
            for i in p_player.items[page*4:4+page*4]:
                choice = "-{}".format(i)
                print(choice)
                itemList.append(choice)
            choice = "-next page"
            print(choice)
            itemList.append(choice)
        else:
            for i in p_player.items[page*4:]:
                choice = "-{}".format(i)
                print(choice)
                itemList.append(choice)
        self.tempPage = page
        choice = "-cancel"
        print(choice)
        itemList.append(choice)
        self.tempChoiceListLength = len(itemList)
        return dialogList, itemList, "Player item"

    def playerUseItem(self, p_player, p_item):
        dialogList = []
        itemList = []
        base = 0
        page = self.tempPage
        if page > 0:
            base = 1
        if len(p_player.items) > 4+page*4:
            for i in p_player.items[page*4:4+page*4]:
                itemList.append(i.__str__())
        else:
            for i in p_player.items[page*4:]:
                itemList.append(i.__str__())
        print("item value: {}".format(p_item))
        if p_item.isdigit():
            if int(p_item) == self.tempChoiceListLength:
                dialogList, choiceList, nextFunction = self.playerTurnDialog(p_player)
                return dialogList, choiceList, nextFunction
            elif base <= (int(p_item) - 1) <= len(p_player.items)-page*4 and int(p_item)-1 < 4+base:  # Modif has been made, seems to work, but why (compare with spell choice)
                p_item = itemList[int(p_item)-1-base]
            elif int(p_item) == base:
                page -= 1
                dialogList, choiceList, nextFunction = self.playerUseItemDialog(p_player, page)
                return dialogList, choiceList, nextFunction
            elif int(p_item) == self.tempChoiceListLength - 1:
                page += 1
                dialogList, choiceList, nextFunction = self.playerUseItemDialog(p_player, page)
                return dialogList, choiceList, nextFunction
            else:
                return self.playerUseItemDialog(p_player, page)
        if p_item in itemList:
            itemIndex = itemList.index(p_item) + page * 4
            self.tempItem = p_player.items[itemIndex]
            if self.tempItem.item_type() in ["Weapon", "Equipment"]:
                dialog = "Cannot use this item"
                print(dialog)
                dialogList.append(dialog)
                targetList = []
                nextFunction = "Player item"
                return dialogList, targetList, nextFunction
            return self.playerItemTargetDialog()
        elif p_item in ["Cancel", "cancel", "c"]:
            return self.playerTurn(p_player)
        else:
            return self.playerUseItem(p_player)

    def playerItemTargetDialog(self):
        dialogList = []
        targetList = []
        targetGroup = []
        nextFunction = "Player item target"
        p_item = self.tempItem
        if p_item.itemType == "revive":
            targetGroup = self.faintedpartymembers
            for i in targetGroup:
                choice = "-{}".format(i.__str__())
                print(choice)
                targetList.append(choice)
        elif p_item.itemType == "recovery":
            targetGroup = self.standingPartyMembers
            for i in targetGroup:
                choice = "-{}".format(i.__str__())
                print(choice)
                targetList.append(choice)
        if len(targetList) == 0:
            dialog = "Cannot use this item"
            print(dialog)
            dialogList.append(dialog)
            targetList = []
            nextFunction = "Player item"
            return dialogList, targetList, nextFunction
        else:
            dialog = "Which target?"
            print(dialog)
            dialogList.append(dialog)
        self.tempTargetGroup = targetGroup
        choice = "-cancel"
        print(choice)
        targetList.append(choice)
        return dialogList, targetList, nextFunction

    def playerTargetPlayer(self, p_fighter, p_target):
        dialogList = []
        p_item = self.tempItem
        targetList = self.tempTargetGroup
        targetListNames = []
        for i in targetList:
            targetListNames.append(i.__str__())
        if p_target.isdigit():
            if 0 <= (int(p_target) - 1) < len(targetListNames):
                p_target = targetListNames[int(p_target) - 1].__str__()
            elif int(p_target) == len(targetList) + 1:
                return self.playerUseItemDialog(p_fighter)
        if p_target in targetListNames:
            p_target = targetList[targetListNames.index(p_target)]
            dialogs = p_fighter.useItem(p_target, p_item)
            for i in dialogs:
                dialogList.append(i)
            del self.tempFightOrder[0]
            for i in self.settingStatus(p_fighter):
                dialogList.append(i)
            return dialogList, [], "Main fight"
        elif p_target in ["Cancel", "cancel", "c"]:
            return self.playerUseItemDialog(p_fighter)

    def fighterDefend(self, p_fighter):
        p_fighter.tempDefendingStatus = True
        dialog = "{} is defending".format(p_fighter)
        print(dialog)
        return dialog

    def playerFlee(self, p_player):
        diff = self.enemyLeader.level - p_player.level + 10
        if self.fightType == "boss":
            result = "no escape"
            dialog = "Can't escape!"
            return result, dialog
        elif diff <= dice(20):
            result = "Success"
            dialog = "Escape succeded"
            self.escape = True
            return result, dialog
        else:
            result = "Failure"
            dialog = "Escape failed"
            return result, dialog

    def enemyTurn(self, p_enemy):
        dialogList = []
        dialogs = self.settingStatus(p_enemy)
        for i in dialogs:
            dialogList.append(i)
        if p_enemy.hpleft >= 1:
            dialogs = self.enemyMonsterAttack(p_enemy)
            for i in dialogs:
                dialogList.append(i)
        return dialogList, [], "Main fight"

    def enemyMonsterAttack(self, p_enemy):
        dialogList = []
        attackProbabilityList = [0]
        for i in p_enemy.weapon:
            attackProbabilityList.append(i.frequency + attackProbabilityList[-1])
        diceThrow = dice(attackProbabilityList[-1])
        for i in range(len(p_enemy.weapon)):
            if diceThrow <= attackProbabilityList[i + 1]:
                attack = p_enemy.weapon[i]
                damage = attack.baseattack
                for j in range(attack.nbrdmgdice):
                    damage += dice(attack.damagedice)
                if attack.attacktype() == "mattack":
                    damage += (p_enemy.tempIntel) // 2
                elif attack.attacktype() == "pattack":
                    damage += (p_enemy.tempStrength) // 2
                print("Enemy damage before def resist:  {}".format(damage))
                if attack.nbrtarget == 1:
                    target = p_enemy.targetChoice(self.playerParty)
                    dialog = "{} use {} on {}".format(p_enemy, attack, target)
                    print(dialog)
                    dialogList.append(dialog)
                    finalDamage = target.newDamageResist(damage, attack.attacktype())
                    dialogs = self.enemyDamageDealt(p_enemy, target, finalDamage)
                    if attack.wpntype == "poison":
                        effectDialog = offensiveSpellEffect(p_enemy, attack, target)
                        for k in effectDialog:
                            dialogList.append(k)
                    for j in dialogs:
                        dialogList.append(j)
                elif attack.nbrtarget == 2:
                    dialog = "{} use {}".format(p_enemy, attack)
                    print(dialog)
                    dialogList.append(dialog)
                    for j in self.standingPartyMembers:
                        finalDamage = int(j.newDamageResist(damage, attack.attacktype()))
                        dialogs = self.enemyDamageDealt(p_enemy, j, finalDamage)
                        for k in dialogs:
                            dialogList.append(k)
                return dialogList

    def enemyDamageDealt(self, p_enemy, p_target, p_damage):
        p_enemy.spriteCondition = "attacking"
        dialogList = []
        if self.dodge(p_enemy, p_target):
            if p_target.weapon.wpntype == "rapier":
                dialog = "Counter attack!"
                print(dialog)
                dialogList.append(dialog)
                counterDamage, nbrTarget, damageType = basicAttack(p_target)
                damageType = "cattack"
                counterDamage = p_enemy.newDamageResist(counterDamage, damageType)
                dialogs = self.playerDamageDealt(p_target, p_enemy, counterDamage, counter=True)
                for i in dialogs:
                    dialogList.append(i)
                return dialogList
            else:
                dialog = "{} dodged the attack!".format(p_target)
            print(dialog)
            dialogList.append(dialog)
        elif p_damage >= 1:
            p_target.spriteCondition = "takingDamage"
            dialog = "{} received {} damage".format(p_target, int(p_damage))
            print(dialog)
            dialogList.append(dialog)
            p_target.hpleft -= p_damage
            if p_target.hpleft <= 0:
                p_target.hpleft = 0
                dialog = "{} fainted!".format(p_target)
                print(dialog)
                dialogList.append(dialog)
        elif p_damage <= 0:
            dialog = "{} blocked the attack!".format(p_target)
            print(dialog)
            dialogList.append(dialog)
        return dialogList

    def applyStatusEffect(self, p_fighter):
        statusEnding = 0
        dialogList = []
        for i in p_fighter.statusEffect():
            if i.element == "fire" and i.duration == 0:
                dialog = "{} isn't burned anymore".format(p_fighter)
                print(dialog)
                dialogList.append(dialog)
                statusEnding += 1
            elif i.element == "luck buff" and i.duration == 0:
                dialog = "{} critical chance returned to normal".format(p_fighter)
                p_fighter.tempLuck = p_fighter.luck
                print(dialog)
                dialogList.append(dialog)
                statusEnding += 1
            else:
                dialogs = self.magicDamageDealt(p_fighter, i)
                for j in dialogs:
                    dialogList.append(j)
            i.duration -= 1
            if p_fighter.hpleft == 0:
                return dialogList
        while statusEnding > 0:
            for i in p_fighter.statusEffect():
                if i.duration < 0:
                    p_fighter.removeStatusEffect(i)
                    statusEnding -= 1
        return dialogList

    def magicDamageDealt(self, p_fighter, p_statusEffect):
        dialogList = []
        if p_statusEffect.element == "fire":
            damage = int(p_statusEffect.effect * (1 - (p_fighter.tempFireResist / 100)))
            p_fighter.hpleft -= damage
            dialog = "{} takes {} points of fire damage!".format(p_fighter, damage)
            print(dialog)
            dialogList.append(dialog)
        elif p_statusEffect.element == "poison":
            damage = int(p_statusEffect.effect)
            p_fighter.hpleft -= damage
            dialog = "{} takes {} points of poison damage!".format(p_fighter, damage)
            print(dialog)
            dialogList.append(dialog)
        if p_fighter.hpleft <= 0:
            p_fighter.hpleft = 0
            dialog = "{} fainted!".format(p_fighter)
            print(dialog)
            dialogList.append(dialog)
        return dialogList

    def endCondition(self):
        loseCondition = 1
        if self.escape:
            return "escape"
        for i in self.faintedpartymembers:
            if i.hpleft > 0:
                self.skipTurn.append(i)
                self.standingPartyMembers.append(i)
                self.faintedpartymembers.remove(i)
                return self.endCondition()
        for i in self.standingPartyMembers:
            if i.hpleft > 0:
                loseCondition = 0
            else:
                self.faintedpartymembers.append(i)
                self.standingPartyMembers.remove(i)
                return self.endCondition()
        if loseCondition == 1:
            return "defeat"
        winCondition = 1
        for i in self.enemyParty:
            if i.hpleft > 0:
                winCondition = 0
            else:
                self.faintedEnemyParty.append(i)
                self.enemyParty.remove(i)
                i.spriteCondition = "dead"
                if i in self.tempFightOrder:
                    self.tempFightOrder.remove(i)
                return self.endCondition()
        if winCondition == 1:
            return "victory"
        else:
            return "none"

    def fightEnd(self, p_condition):
        dialogList = []
        nextFunction = "End"
        if p_condition == "defeat":
            dialog = "Game over"
            print(dialog)
            dialogList.append(dialog)
        elif p_condition == "escape":
            dialog = "Escape has succeeded!"
            print(dialog)
            dialogList.append(dialog)
        elif p_condition == "victory":
            dialog = "Victory!"
            print(dialog)
            dialogList.append(dialog)
            for i in self.faintedEnemyParty:
                self.reward += i.gold
                self.groupxp += i.xp
            dialog = "Party received {} experience points".format(self.groupxp)
            print(dialog)
            dialogList.append(dialog)
            for i in self.standingPartyMembers:
                i.fightclass.turnRecovery(i, 3, 6)
                dialogs = i.givexpGraphic(self.groupxp)
                for j in dialogs:
                    print(j)
                    dialogList.append(j)
                dialogs1 = i.weaponLevelUp()
                dialogs2 = i.armorLevelUp()
                dialogs3 = i.spellLevelUp()
                for j in dialogs1:
                    dialogList.append(j)
                for k in dialogs2:
                    dialogList.append(k)
                for l in dialogs3:
                    dialogList.append(l)
                #  i.gold += ((self.reward / len(self.standingPartyMembers)) // 1 + (((self.reward / len(self.standingPartyMembers)) % 1) // 0.01) / 100)
                #  i.gold = i.gold // 1 + (((i.gold % 1) // 0.01) / 100)
            self.team.gold += int(self.reward)
            dialog = "Party received {} gold".format(self.reward)
            print(dialog)
            dialogList.append(dialog)
        return dialogList, [], nextFunction

    def givexpDialog(self):
        nextFunction = "End"
        dialogList = []
        for i in self.standingPartyMembers:
            dialogs, nextFunction = i.givexpGraphic(0)
            for j in dialogs:
                print(j)
                dialogList.append(j)
        return dialogList, [], nextFunction

    def strike(self, fighter, target):
        numberStrike = 1 + fighter.tempAgility // target.tempAgility
        if ((fighter.tempAgility % target.tempAgility) / target.tempAgility) * 100 > dice(100):
            numberStrike += 1
        print("Number of strike: ", numberStrike)
        return numberStrike
