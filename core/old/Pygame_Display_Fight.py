from interface.Pygame_Basic import *


def stat_bar(p_fighter, stat, x, y, size=1):
    if stat == "hp":
        pointLeft = int((p_fighter.hpleft / p_fighter.maxhp) * 50 * size)
        if p_fighter.hpleft <= 0:
            pointLeft = -1
    elif stat == "sp":
        pointLeft = int((p_fighter.stamleft / p_fighter.maxstam) * 50 * size)
    else:
        pointLeft = int((p_fighter.manaleft / p_fighter.maxmana) * 50 * size)
    if pointLeft != -1:
        pygame.draw.rect(gameDisplay, black, (x, y, 50*size, 5))
    if pointLeft >= 30 * size:
        color = green
    elif 30 * size > pointLeft >= 18 * size:
        color = yellow
    elif 18 * size > pointLeft >= 8:
        pigment = int(pointLeft/18*200/size)
        color = (240, pigment, 0)
    elif 8 * size > pointLeft >= 0:
        color = red
    if pointLeft >= 0:
        pygame.draw.rect(gameDisplay, black, (x, y, 50*size, 5))
        pygame.draw.rect(gameDisplay, color, (x, y, pointLeft, 5))


def functionChoiceLinker(p_fight, nextFunction, choice):
    dialog = []
    choiceList = []
    p_fighter = p_fight.tempFightOrder[0]
    if nextFunction == "Player choice":
        dialog, choiceList, nextFunction = p_fight.playerTurn(p_fighter, str(choice))
    elif nextFunction == "Player attack":
        dialog, choiceList, nextFunction = p_fight.playerAttack(p_fighter, str(choice))
    elif nextFunction == "Player target":
        dialog, choiceList, nextFunction = p_fight.playerTargetEnemy(p_fighter, str(choice))
    elif nextFunction == "Player spell":
        dialog, choiceList, nextFunction = p_fight.playerSpellCast(p_fighter, str(choice))
    elif nextFunction == "Spell target":
        dialog, choiceList, nextFunction = p_fight.spellTarget(p_fighter, str(choice))
    elif nextFunction == "Player item":
        dialog, choiceList, nextFunction = p_fight.playerUseItem(p_fighter, str(choice))
    elif nextFunction == "Player item target":
        dialog, choiceList, nextFunction = p_fight.playerTargetPlayer(p_fighter, str(choice))
    return dialog, choiceList, nextFunction


def functionLinker(p_fight, nextFunction):
    if nextFunction in ["Main fight", "Skip", "First turn"]:
        dialogList, choiceList, nextFunction = p_fight.mainFightIterative()
    elif nextFunction == "End fight":
        for i in p_fight.playerParty:
            i.spriteCondition = "waiting"
        endCondition = p_fight.endCondition()
        dialogList, choiceList, nextFunction = p_fight.fightEnd(endCondition)
        if endCondition == "victory":
            pygame.mixer.music.load("../music/victory_theme.wav")
        pygame.mixer.music.play(-1)
    elif nextFunction == "Next level":
        dialogList, choiceList, nextFunction = p_fight.givexpDialog()
    elif nextFunction == "Player item":
        dialogList, choiceList, nextFunction = p_fight.playerUseItemDialog(p_fight.tempFightOrder[0])
    elif nextFunction == "Return main":
        dialogList, choiceList, nextFunction = p_fight.playerTurnDialog(p_fight.tempFightOrder[0])
    elif nextFunction == "Monster attack":
        dialogList, choiceList, nextFunction = p_fight.playerTurnDialog(p_fight.tempFightOrder[0])
        print("lolololol")
    else:
        dialogList, choiceList, nextFunction = [], [], "End"
    return dialogList, choiceList, nextFunction


def party_stat_display(party):
    positionList = playerPosition(party)
    for i in range(len(party)):
        statList = []
        stat = party[i].__str__()
        statList.append(stat)
        stat = "Hp {}/{}".format(party[i].hpleft, party[i].maxhp)
        statList.append(stat)
        stat = "Sp {}/{}".format(party[i].stamleft, party[i].maxstam)
        statList.append(stat)
        stat = "Mp {}/{}".format(party[i].manaleft, party[i].maxmana)
        statList.append(stat)
        iterator = 0
        pygame.draw.rect(gameDisplay, black, (border+i*(stat_width+thin_border)-thin_border, 15, stat_width, 85))
        pygame.draw.rect(gameDisplay, white, (border+i*(stat_width+thin_border), 15+thin_border, stat_width-thin_border*2, 85-thin_border*2))
        stat_bar(party[i], "hp", border+i*(stat_width+thin_border)+thin_border, 45, 2)
        stat_bar(party[i], "sp", border+i*(stat_width+thin_border)+thin_border, 64, 2)
        stat_bar(party[i], "mp", border+i*(stat_width+thin_border)+thin_border, 83, 2)
        position = positionList[i]
        gameDisplay.blit(playerSprite(party[i]), (position[0], position[1]))
        for j in range(len(statList)):
            if j == 0:
                TextSurf, TextRect = text_objects(statList[j], largeText)
                side = 0
            else:
                TextSurf, TextRect = text_objects(statList[j], smallText)
                side = 110
            TextRect = (border+i*(stat_width+thin_border)+thin_border+side, 20+thin_border+iterator)
            iterator += text_size + 1
            gameDisplay.blit(TextSurf, TextRect)


def enemy_stat_display(enemyParty, positionList):
    for i in range(len(enemyParty)):
        position = positionList[i]
        stat_bar(enemyParty[i], "hp", position[0]+(enemyParty[i].size-50)/2, position[1]-6)
        gameDisplay.blit(enemySprite(enemyParty[i]), (position[0], position[1]))


def enemyPosition(enemyParty):
    totalSize = -12
    partySize = len(enemyParty)
    positionList = []
    for i in enemyParty:
        totalSize += i.size + 12
    x = 3*display_width/4
    y = (display_height - totalSize)/2 - 60
    for i in range(partySize):
        x_position = x + abs((partySize-1)/2 - i) * 80
        print(x_position)
        positionList.append((x_position, y))
        y += 12 + enemyParty[i].size
    return positionList


def enemySprite(p_fighter):
    if p_fighter.spriteCondition == "waiting":
        return p_fighter.sprite.waiting()
    elif p_fighter.spriteCondition == "attacking":
        return p_fighter.sprite.attacking()
    elif p_fighter.spriteCondition == "takingDamage":
        return p_fighter.sprite.takingDamage()
    elif p_fighter.spriteCondition == "dead":
        return blank


def playerPosition(playerParty):
    totalSize = -12
    partySize = len(playerParty)
    positionList = []
    for i in playerParty:
        totalSize += 70
    x = int(display_width/8)
    y = (display_height - totalSize)/2 - 60
    for i in range(partySize):
        x_position = x + abs((partySize-1)/2 - i) * 80
        positionList.append((x_position, y))
        y += 80
    return positionList


def playerSprite(p_fighter):
    if p_fighter.spriteCondition == "waiting":
        return p_fighter.sprite.waiting()
    elif p_fighter.spriteCondition == "attacking":
        return p_fighter.sprite.attacking()
    elif p_fighter.spriteCondition == "takingDamage":
        return p_fighter.sprite.takingDamage()
    elif p_fighter.spriteCondition == "dead":
        return blank


def battle_system_loop(p_fight, background):
    endingFight = 0
    gameExit = False
    lineDisplay = 1
    dialogList, choiceList, nextFunction = p_fight.fightEncounter()
    print(p_fight.fightType)
    if p_fight.fightType == "boss":
        pygame.mixer.music.load("../music/boss_theme.wav")
    else:
        pygame.mixer.music.load("../music/battle_theme.wav")
    pygame.mixer.music.play(-1)
    p_fight.sortTurnOrder()
    focusBox = 0
    for i in p_fight.playerParty:
        i.sprite.load()
    for i in p_fight.enemyParty:
        i.sprite.load()
    positionList = enemyPosition(p_fight.enemyParty)
    while not gameExit:
        if p_fight.endCondition() != "none":
            if nextFunction == "Main fight":
                nextFunction = "End fight"
        if len(choiceList) > 0:
            displayMode = "Big display"
        else:
            displayMode = "Small display"
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_o:
                    for i in p_fight.tempFightOrder:
                        print(i)
                    for i in p_fight.enemyParty:
                        print("{} rage:".format(i.__str__()))
                        for j in range(len(i.targetingList)):
                            print("Rage against {}: {}".format(p_fight.playerParty[j], i.targetingList[j]))
                if displayMode == "Big display":
                    if event.key == pygame.K_UP:
                        if focusBox > 0:
                            focusBox -= 1
                    if event.key == pygame.K_DOWN:
                        if focusBox < len(choiceList) - 1:
                            focusBox += 1
                if event.key in [pygame.K_RETURN, pygame.K_SPACE]:
                    if nextFunction == "End" and lineDisplay == len(dialogList):
                        print("Thank you for playing!")
                        pygame.mixer.music.stop()
                        gameExit = True
                    elif nextFunction == "escape":
                        pygame.mixer.music.stop()
                        gameExit = True
                    elif displayMode == "Big display":
                        dialogList, choiceList, nextFunction = functionChoiceLinker(p_fight, nextFunction, focusBox+1)
                        focusBox = 0
                    elif displayMode == "Small display":
                        if lineDisplay == len(dialogList):
                            dialogList, choiceList, nextFunction = functionLinker(p_fight, nextFunction)
                            lineDisplay = 1
                        else:
                            lineDisplay += 1
        dialogSize = len(dialogList) * text_size
        gameDisplay.blit(background, (0, 0))
        party_stat_display(p_fight.playerParty)
        enemy_stat_display(p_fight.startingEnemyParty, positionList)
        if displayMode == "Big display":
            big_display(dialogList, choiceList, focusBox, dialogSize)
        elif displayMode == "Small display":
            small_display(dialogList, lineDisplay)
        pygame.display.update()
        clock.tick(fps)
