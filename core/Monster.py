from core.Equipment_and_Other import dice
from core.FightStatus import FighterStatus
from core.Life_Entities import LifeEntity
from core.Sprite_Class import EnemySprite
from objects.Monsters_Sheet import wildCreatureList, intelligentCreatureList


class Monster(LifeEntity):
    # initiate method
    # call the parent class with super(), and need spec. stats like name, attacks and spritelist
    # all parameter are included when initiated with a tuple from Monster_and_Characters_Sheets.py
    # monsters are created before a fight and deleted after it
    def __init__(self, level=1, const=10, strength=0, intel=0, agility=10, luck=0, gold=0, xp=0,
                 pdefense=0, mdefense=0, fireresist=0, iceresist=0, earthresist=0, electricresist=0,
                 poisonresist=0, race="None", name="None", moveset=None, spawnfreq=10, spriteList=list,
                 size=100, weapon=None, enemytype="normal"):
        super().__init__(level=level, const=const, strength=strength, intel=intel, agility=agility,
                         fireresist=fireresist, iceresist=iceresist, earthresist=earthresist,
                         electricresist=electricresist, poisonresist=poisonresist, luck=luck, gold=gold)
        self.targetingList = []
        self.pdefense = pdefense
        self.mdefense = mdefense
        self.name = name
        self.moveset = moveset
        self.newSetWeapon(weapon)
        self.xp = xp
        self.race = race
        self.enemytype = enemytype
        self.spawnfreq = spawnfreq
        self.fightStatus = FighterStatus(self)
        self.sprite = EnemySprite(spriteList)
        self.sprite.load()
        self.spriteCondition = "waiting"
        self.nextSpriteCondition = "waiting"
        self.size = size

    def initTempStats(self):
        super().initTempStats()

    def display(self):
        print("---Monster's sheet:---")
        print("Monster name: ", self.name)
        print("Race: ", self.race)
        print("--------Stat---------")
        super().display()
        print("Xp value: ", self.xp)
        print("\n")

    def fight_display(self):
        print("---Monster's sheet:---")
        print("Monster name: ", self.name)
        print("Race: ", self.race)
        print("--------Stat---------")
        super().fight_display()
        print("Xp value: ", self.xp)
        print("\n")

    # return the type as a string
    def search(self):
        return "Monster"

    # method to upgrade enemy stats to make a superior enemy called elite
    def elite(self):
        self.name += " (elite)"
        self.level += 1
        self.const *= 2
        self.maxhp = int(self.const * 2)
        self.hpleft = self.maxhp
        self.strength *= 1.1
        self.maxstam = int(10 + 5 * (self.const / 15) * (self.strength / 10))
        self.stamleft = self.maxstam
        self.intel *= 1.4
        self.maxmana = int(self.intel * 2)
        self.manaleft = self.maxmana
        self.pdefense = int(self.pdefense * 1.1) + 2
        self.mdefense = int(self.mdefense * 1.1) + 2
        self.agility *= 1.1
        self.luck += 0
        self.gold *= 1.5
        self.xp *= 2

    # method to upgrade enemy stats to make a superior enemy called boss
    # param [in] blife: Max hp multipicator
    # param [in] resistance: Additional defense bonus
    def boss(self, blife=3, resistance=4):
        self.enemytype = "boss"
        self.name += " (boss)"
        self.level += blife / 2
        self.const *= blife
        self.maxhp = int(self.const * 2)
        self.hpleft = self.maxhp
        self.strength = self.strength*1.1 + 4
        self.maxstam = int(10 + 5 * (self.const / 15) * (self.strength / 10))
        self.stamleft = self.maxstam
        self.intel *= 1.2
        self.maxmana = int(self.intel * 2)
        self.manaleft = self.maxmana
        self.pdefense = int(self.pdefense * 1.1 + resistance) + 2
        self.mdefense = int(self.mdefense * 1.1 + resistance) + 2
        self.agility *= 1.1
        self.luck += 0
        self.gold *= (1.5+blife/2)
        self.xp = int(self.xp*(2.5+blife/2))

    # todo: check if this method will still be used, or else replace it
    # Initiate rage against every opponent
    # param [in] party: list of opposite party members
    # Always intitiate at 20 pts
    def newTargetPriorityInit(self, party):
        for i in range(len(party)):
            self.targetingList.append(20)

    # Change rage level against each character
    # Naturally tend to 12 when unattacked
    # param [in] party: list of opposite party members
    def targetIncrement(self, party):
        newlist = []
        increment = 0
        for i in range(len(party)):
            if party[i].hpleft == 0:
                newlist.append(0)
            elif party[i].hpleft > 0 and self.targetingList[i] == 0:
                newlist.append(10)
            elif self.targetingList[i] < 8:
                increment = 2
            elif self.targetingList[i] < 12:
                increment = 1
            elif self.targetingList[i] > 24:
                increment = -5
            elif self.targetingList[i] > 20:
                increment = -3
            elif self.targetingList[i] > 16:
                increment = -2
            elif self.targetingList[i] > 12:
                increment = -1
            newlist.append(self.targetingList[i]+increment)
        self.targetingList = newlist

    # Increment by 3 rage after a player attack
    # param [in] party: list of opposite party members
    # param [in] player: opponent attacking the monster
    def attackRageBuild(self, party, player):
        newlist = []
        for i in range(len(party)):
            if party[i].hpleft == 0:
                newlist.append(0)
            elif party[i] == player:
                print("{}'s rage against {}: {} + 3".format(self.__str__(), party[i], self.targetingList[i]))
                newlist.append(self.targetingList[i]+3)
            else:
                newlist.append(self.targetingList[i])
        self.targetingList = newlist

    # Rage increment by 1 for any attack on a party member
    # param [in] party: list of opposite party members
    # param [in] player: opponent attacking a monster party member
    def undirectRageBuild(self, party, player):
        newlist = []
        for i in range(len(party)):
            if party[i].hpleft == 0:
                newlist.append(0)
            elif party[i] == player:
                print("{}'s rage against {}: {} + 1".format(self.__str__(), party[i], self.targetingList[i]))
                newlist.append(self.targetingList[i]+1)
            else:
                newlist.append(self.targetingList[i])
        self.targetingList = newlist

    # Targeting algorithm for enemies
    # Use the ragebuild list to choose with probability each target
    # Use specialTarget() method for further details
    # param [in] party: list of opposite party members
    # return the chosen target
    def targetChoice(self, party):
        probList = [0]
        for i in range(len(party)):
            if party[i].hpleft == 0:
                probList.append(probList[-1])
            else:  # new indent, check if good
                addedRage = self.specialTarget(party[i], party)
                print("Rage against {}: {} + {}".format(party[i], self.targetingList[i], addedRage))
                probList.append(self.targetingList[i]+addedRage+probList[-1])
        dicerslt = dice(probList[len(party)])  # problem over here when someone is dead, have to modify something
        print("Dice result: " + str(dicerslt))
        for i in range(len(party)):
            if dicerslt <= probList[i + 1]:
                return party[i]

    # Add bonus rage against special target, depending the type of enemy
    # param [in] party: list of opposite party members
    # param [in] player: particular member of the opposite party
    # return rage bonus if condition apply
    def specialTarget(self, player, party):
        bonus = 0
        if self.race in wildCreatureList:
            bonus = 5
            for i in party:
                if i.const + i.strength + i.defense > player.const + player.strength + player.defense:
                    bonus = 0
                    break
        elif self.race in intelligentCreatureList:
            bonus = 4
            for i in party:
                if i.hpleft / i.maxhp < player.hpleft / player.maxhp:
                    bonus = 0
                    break
        return bonus


