import os.path

from core.Equipment_and_Other import Weapon, createEquipment
from core.Playable_Character import PlayableCharacter
from objects.Spell_Sheet import *
from objects.old.Equipment_Sheet import *


class SaveFile:
    def __init__(self, p_party):
        self.party = p_party
        self.firstSave = "../save_files/Save_1.txt"
        self.firstSaveCondition = self.verifySavingFiles(self.firstSave)
        self.secondSave = "../save_files/Save_2.txt"
        self.secondSaveCondition = self.verifySavingFiles(self.secondSave)
        self.thirdSave = "../save_files/Save_3.txt"
        self.thirdSaveCondition = self.verifySavingFiles(self.thirdSave)

    def __str__(self):
        return "Save file"

    def verifySavingFiles(self, p_fileName):
        if os.path.exists(p_fileName):
            return "Used"
        else:
            return "New game"

    def fileDialog(self):
        for i in [self.firstSave, self.secondSave, self.thirdSave]:
            self.verifySavingFiles(i)
        print("\nWhat do you want to do?")
        print("-Save your file")
        print("-Load a file")
        print("-Erase a file")
        print("-Cancel")
        choice = input()
        if choice in ["Save your file", "Save", "S", "s", "1"]:
            self.savingDialog()
        elif choice in ["Load a file", "Load", "L", "l", "2"]:
            self.loadingDialog()
        elif choice in ["Erase a file", "Erase", "E", "e", "3"]:
            input("Erasing option not supported")
            self.fileDialog()
        elif choice in ["Cancel", "C", "c", "4"]:
            pass
        else:
            input("Invalid argument")
            self.fileDialog()

    def fileDialogChoice(self, choice):
        for i in [self.firstSave, self.secondSave, self.thirdSave]:
            self.verifySavingFiles(i)
        if choice == 0:
            self.savingDialog()
        elif choice == 1:
            self.loadingDialog()
        elif choice == 2:
            input("Erasing option not supported")
            self.fileDialog()
        elif choice == 3:
            return "stat"

    def savingDialog(self):
        print("\nWhere do you want to save?")
        print("-File 1, {}".format(self.firstSaveCondition))
        print("-File 2, {}".format(self.secondSaveCondition))
        print("-File 3, {}".format(self.thirdSaveCondition))
        choice = input()
        if choice in ["File 1", "1"]:
            if self.firstSaveCondition == "New game":
                self.createNewGame(1)
            else:
                input("Overwriting files not supported right now")
                self.savingDialog()
        elif choice in ["File 2", "2"]:
            if self.secondSaveCondition == "New game":
                self.createNewGame(2)
            else:
                input("Overwriting files not supported right now")
                self.savingDialog()
        elif choice in ["File 3", "3"]:
            if self.thirdSaveCondition == "New game":
                self.createNewGame(3)
            else:
                input("Overwriting files not supported right now")
                self.savingDialog()
        elif choice in ["Cancel", "C", "c", "4"]:
            self.fileDialog()
        else:
            input("Invalid argument")
            self.savingDialog()

    def newSavingChoice(self, choice, p_map):
        print("Choice: {}".format(str(choice)))
        if choice == 0:
            if self.firstSaveCondition == "New game":
                self.createNewGame(1, p_map)
            else:
                input("Overwriting files not supported right now")
                self.savingDialog()
        elif choice == 1:
            if self.secondSaveCondition == "New game":
                self.createNewGame(2, p_map)
            else:
                input("Overwriting files not supported right now")
                self.savingDialog()
        elif choice == 2:
            if self.thirdSaveCondition == "New game":
                self.createNewGame(3, p_map)
            else:
                input("Overwriting files not supported right now")
                self.savingDialog()
        elif choice in ["Cancel", "C", "c", "4"]:
            self.fileDialog()
        else:
            input("Invalid argument")
            self.savingDialog()

    def loadingDialog(self):
        print("Which file do you want to load?:")
        if self.firstSaveCondition == self.secondSaveCondition == self.thirdSaveCondition == "New game":
            input("There is no game to load")
            return self.savingDialog()
        if self.firstSaveCondition != "New game":
            print("-File 1")
        if self.secondSaveCondition != "New game":
            print("-File 2")
        if self.thirdSaveCondition != "New game":
            print("-File 3")
        choice = input()
        if choice in ["File 1", "1"] and self.firstSaveCondition != "New game":
            loadingFile = open(self.firstSave, "r")
            self.readingSavedFiles(loadingFile)
        elif choice in ["File 2", "2"] and self.secondSaveCondition != "New game":
            loadingFile = open(self.secondSave, "r")
            self.readingSavedFiles(loadingFile)
        elif choice in ["File 3", "3"] and self.thirdSaveCondition != "New game":
            loadingFile = open(self.thirdSave, "r")
            self.readingSavedFiles(loadingFile)
        elif choice in ["Cancel", "C", "c", "4"]:
            self.fileDialog()
        else:
            input("Invalid argument")
            self.loadingDialog()

    def newLoadingChoice(self, choice):
        new_map = None
        if choice == 0 and self.firstSaveCondition != "New game":
            loadingFile = open(self.firstSave, "r")
            new_map = self.readingSavedFiles(loadingFile)
        elif choice == 1 and self.secondSaveCondition != "New game":
            loadingFile = open(self.secondSave, "r")
            new_map = self.readingSavedFiles(loadingFile)
        elif choice == 2 and self.thirdSaveCondition != "New game":
            loadingFile = open(self.thirdSave, "r")
            new_map = self.readingSavedFiles(loadingFile)
        elif choice in ["Cancel", "C", "c", "4"]:
            self.fileDialog()
        else:
            input("Invalid argument")
            self.loadingDialog()
        return new_map

    def erasingFileChoice(self, choice):
        if choice == 0:
            file = self.firstSave
            condition = self.firstSaveCondition
            self.firstSaveCondition = "New game"
        elif choice == 1:
            file = self.secondSave
            condition = self.secondSaveCondition
            self.secondSaveCondition = "New game"
        elif choice == 2:
            file = self.thirdSave
            condition = self.thirdSaveCondition
            self.thirdSaveCondition = "New game"
        if condition == "Used":
            os.remove(file)
            if self.verifySavingFiles(file) == "New game":
                print("File deleted properly")
            else:
                print("Error, file not erased")
        else:
            print("Error, there is no file to delete")

    def createNewGame(self, p_saveNumber, p_map):
        if p_saveNumber == 1:
            saveFile = self.firstSave
            self.firstSaveCondition = "Used"
        elif p_saveNumber == 2:
            saveFile = self.secondSave
            self.secondSaveCondition = "Used"
        elif p_saveNumber == 3:
            saveFile = self.thirdSave
            self.thirdSaveCondition = "Used"
        newFile = open(saveFile, "w")
        self.savingInformation(newFile, p_map)

    def savingInformation(self, p_file, p_map):
        print("Begin saving process")
        p_file.write("{}\n".format(len(self.party.members)))
        p_file.write("{}\n".format(self.party.gold))
        p_file.write("{}\n".format(p_map.map_number))
        chestList = ""
        for i in self.party.opened_chest:
            chestList += str(i)
        p_file.write("{}\n".format(chestList))
        for i in self.party.members:
            i.organise_items()
            print("Saving character's stat")
            p_file.write("{}\n".format(i.level))
            p_file.write("{}\n".format(i.const))
            p_file.write("{}\n".format(i.strength))
            p_file.write("{}\n".format(i.intel))
            p_file.write("{}\n".format(i.agility))
            p_file.write("{}\n".format(i.defense))
            p_file.write("{}\n".format(i.luck))
            p_file.write("{}\n".format(i.hpleft))
            p_file.write("{}\n".format(i.stamleft))
            p_file.write("{}\n".format(i.manaleft))
            p_file.write("{}\n".format(i.xp))
            p_file.write("{}\n".format(i.spriteTag))
            p_file.write("{}\n".format(i.race))
            p_file.write("{}\n".format(i.gender))
            p_file.write("{}\n".format(i.name))
            p_file.write("{}\n".format(i.fightclass.name))
            print("Saving weapon")
            if i.weapon is not None:
                p_file.write("{}\n".format(i.weapon.name))
            else:
                p_file.write("None\n")
            print("Saving equipments")
            p_file.write("{}\n".format(len(i.equipment)))
            for j in i.equipment:
                p_file.write("{}\n".format(j.name))
            print("Saving spells")
            p_file.write("{}\n".format(len(i.spelllist)))
            for j in i.spelllist:
                p_file.write("{}\n".format(j.name))
            print("Saving skills")
            p_file.write("{}\n".format(len(i.attackmoveset)))
            for j in range(len(i.attackmoveset)):
                p_file.write("{}\n".format(i.attackmoveset[j]))
                p_file.write("{}\n".format(i.attackmovecost[j]))
            print("Saving items")
            p_file.write("{}\n".format(len(i.items)))
            for j in i.items:
                p_file.write("{}\n".format(j.name))
            print("Saving fight statistics")
            p_file.write("{}\n".format(i.fightStatus.sword.weaponKill))
            p_file.write("{}\n".format(i.fightStatus.sword.level))
            p_file.write("{}\n".format(i.fightStatus.axe.weaponKill))
            p_file.write("{}\n".format(i.fightStatus.axe.level))
            p_file.write("{}\n".format(i.fightStatus.rapier.weaponKill))
            p_file.write("{}\n".format(i.fightStatus.rapier.level))
            p_file.write("{}\n".format(i.fightStatus.mace.weaponKill))
            p_file.write("{}\n".format(i.fightStatus.mace.level))
            p_file.write("{}\n".format(i.fightStatus.spear.weaponKill))
            p_file.write("{}\n".format(i.fightStatus.spear.level))
            p_file.write("{}\n".format(i.fightStatus.staff.weaponKill))
            p_file.write("{}\n".format(i.fightStatus.staff.level))
            p_file.write("{}\n".format(i.fightStatus.bow.weaponKill))
            p_file.write("{}\n".format(i.fightStatus.bow.level))
            p_file.write("{}\n".format(i.fightStatus.fireSpell.numberUse))
            p_file.write("{}\n".format(i.fightStatus.fireSpell.level))
            p_file.write("{}\n".format(i.fightStatus.iceSpell.numberUse))
            p_file.write("{}\n".format(i.fightStatus.iceSpell.level))
            p_file.write("{}\n".format(i.fightStatus.earthSpell.numberUse))
            p_file.write("{}\n".format(i.fightStatus.earthSpell.level))
            p_file.write("{}\n".format(i.fightStatus.electricSpell.numberUse))
            p_file.write("{}\n".format(i.fightStatus.electricSpell.level))
            p_file.write("{}\n".format(i.fightStatus.healingSpell.numberUse))
            p_file.write("{}\n".format(i.fightStatus.healingSpell.level))
            p_file.write("{}\n".format(i.fightStatus.lightArmor.numberHit))
            p_file.write("{}\n".format(i.fightStatus.lightArmor.level))
            p_file.write("{}\n".format(i.fightStatus.mediumArmor.numberHit))
            p_file.write("{}\n".format(i.fightStatus.mediumArmor.level))
            p_file.write("{}\n".format(i.fightStatus.heavyArmor.numberHit))
            p_file.write("{}\n".format(i.fightStatus.heavyArmor.level))
            p_file.write("Character end\n")
        p_file.write("File end\n")
        p_file.close()
        print("Saving process finished")

    def readingSavedFiles(self, p_file):
        self.party.clearMembers()
        print("Loading file")
        numberOfPlayers = int(p_file.readline()[:-1])
        partyGold = int(p_file.readline()[:-1])
        print("Loading map")
        new_map = int(p_file.readline()[:-1])
        self.party.gold = partyGold
        openedChest = str(p_file.readline()[:-1])
        for i in range(0, len(openedChest), 4):
            self.party.opened_chest.append(int(openedChest[i: i+4]))
        for i in range(numberOfPlayers):
            print("Loading new player")
            print("Loading basic stats")
            playerLevel = int(p_file.readline()[:-1])
            playerConst = float(p_file.readline()[:-1])
            playerStrength = float(p_file.readline()[:-1])
            playerIntel = float(p_file.readline()[:-1])
            playerAgility = float(p_file.readline()[:-1])
            playerDefense = float(p_file.readline()[:-1])
            playerLuck = float(p_file.readline()[:-1])
            playerHp = int(p_file.readline()[:-1])
            playerStam = int(p_file.readline()[:-1])
            playerMana = int(p_file.readline()[:-1])
            playerXp = int(p_file.readline()[:-1])
            print("Loading character's spec")
            playerSprites = (p_file.readline()[:-1])
            playerRace = (p_file.readline()[:-1])
            playerGender = (p_file.readline()[:-1])
            playerName = (p_file.readline()[:-1])
            playerFightClass = (p_file.readline()[:-1])
            print("Loading weapon")
            playerWeapon = (p_file.readline()[:-1])
            weaponRefined = False
            if len(playerWeapon) > 7:
                if playerWeapon[:7] == "Refined":
                    weaponRefined = True
                    playerWeapon = playerWeapon[8:]
            print("Loading equipment")
            numberOfGearPart = int(p_file.readline()[:-1])
            equipmentList = []
            for j in range(numberOfGearPart):
                equipmentList.append(p_file.readline()[:-1])
            print("Loading spells")
            numberOfSpell = int(p_file.readline()[:-1])
            spellList = []
            for j in range(numberOfSpell):
                spellList.append(p_file.readline()[:-1])
            print("Loading skills")
            numberOfSkill = int(p_file.readline()[:-1])
            skillList = []
            skillCostList = []
            for j in range(numberOfSkill):
                skillList.append(p_file.readline()[:-1])
                skillCostList.append(int(p_file.readline()[:-1]))
            print("Loading items")
            numberOfItem = int(p_file.readline()[:-1])
            playerItemList = []
            for j in range(numberOfItem):
                playerItemList.append(p_file.readline()[:-1])
            print("Loading weapon stats")
            swordKill = int(p_file.readline()[:-1])
            swordLevel = int(p_file.readline()[:-1])
            axeKill = int(p_file.readline()[:-1])
            axeLevel = int(p_file.readline()[:-1])
            rapierKill = int(p_file.readline()[:-1])
            rapierLevel = int(p_file.readline()[:-1])
            maceKill = int(p_file.readline()[:-1])
            maceLevel = int(p_file.readline()[:-1])
            spearKill = int(p_file.readline()[:-1])
            spearLevel = int(p_file.readline()[:-1])
            staffKill = int(p_file.readline()[:-1])
            staffLevel = int(p_file.readline()[:-1])
            bowKill = int(p_file.readline()[:-1])
            bowLevel = int(p_file.readline()[:-1])
            print("Loading spell stats")
            fireSpellUsed = int(p_file.readline()[:-1])
            fireSpellLevel = int(p_file.readline()[:-1])
            iceSpellUsed = int(p_file.readline()[:-1])
            iceSpellLevel = int(p_file.readline()[:-1])
            earthSpellUsed = int(p_file.readline()[:-1])
            earthSpellLevel = int(p_file.readline()[:-1])
            electricSpellUsed = int(p_file.readline()[:-1])
            electricSpellLevel = int(p_file.readline()[:-1])
            healingSpellUsed = int(p_file.readline()[:-1])
            healingSpellLevel = int(p_file.readline()[:-1])
            print("Loading armor stats")
            lightArmorHit = int(p_file.readline()[:-1])
            lightArmorLevel = int(p_file.readline()[:-1])
            mediumArmorHit = int(p_file.readline()[:-1])
            mediumArmorLevel = int(p_file.readline()[:-1])
            heavyArmorHit = int(p_file.readline()[:-1])
            heavyArmorLevel = int(p_file.readline()[:-1])
            if p_file.readline()[:-1] != "Character end":
                input("Error!!!")
                p_file.close()
                return None
            else:
                player = PlayableCharacter(level=1, gold=0, xp=playerXp, spritetag=playerSprites)
                player.personnalID(playerRace, playerGender, playerName, playerFightClass, loaded=True)
                player.modifstat(level=playerLevel, const=playerConst, strength=playerStrength, intel=playerIntel,
                                 agility=playerAgility, defense=playerDefense, luck=playerLuck)
                player.hpleft = playerHp
                player.stamleft = playerStam
                player.manaleft = playerMana
                for j in complete_weapon_list:
                    if playerWeapon == j[0]:
                        player.setweapon(j)
                if weaponRefined:
                    player.weapon.refine(player)
                for j in equipmentList:
                    for k in complete_armor_list:
                        if j == k[0]:
                            player.setequipment(k)
                for j in spellList:
                    for k in offensiveSpellList:
                        if j == k[0]:
                            player.setspell(OffensiveSpell(*k))
                    for k in healingSpellList:
                        if j == k[0]:
                            player.setspell(HealingSpell(*k))
                    for k in statusSpellList:
                        if j == k[0]:
                            player.setspell(StatusSpell(*k))
                for j in range(1, len(skillList)):
                    player.addAttackAbility(skillList[j], skillCostList[j])
                for j in playerItemList:
                    for k in itemList:
                        if j == k[1]:
                            player.setitems(k)
                            break
                    for k in complete_weapon_list:
                        if j == k[0] and j != playerWeapon:
                            player.newSetItem(Weapon(*k))
                            break
                    for k in complete_armor_list:
                        if j == k[0] and j not in equipmentList:
                            player.newSetItem(createEquipment(k))
                player.fightStatus.sword.weaponKill = swordKill
                player.fightStatus.sword.level = swordLevel
                player.fightStatus.axe.weaponKill = axeKill
                player.fightStatus.axe.level = axeLevel
                player.fightStatus.rapier.weaponKill = rapierKill
                player.fightStatus.rapier.level = rapierLevel
                player.fightStatus.mace.weaponKill = maceKill
                player.fightStatus.mace.level = maceLevel
                player.fightStatus.spear.weaponKill = spearKill
                player.fightStatus.spear.level = spearLevel
                player.fightStatus.staff.weaponKill = staffKill
                player.fightStatus.staff.level = staffLevel
                player.fightStatus.bow.weaponKill = bowKill
                player.fightStatus.bow.level = bowLevel
                player.fightStatus.fireSpell.numberUse = fireSpellUsed
                player.fightStatus.fireSpell.level = fireSpellLevel
                player.fightStatus.iceSpell.numberUse = iceSpellUsed
                player.fightStatus.iceSpell.level = iceSpellLevel
                player.fightStatus.earthSpell.numberUse = earthSpellUsed
                player.fightStatus.earthSpell.level = earthSpellLevel
                player.fightStatus.electricSpell.numberUse = electricSpellUsed
                player.fightStatus.electricSpell.level = electricSpellLevel
                player.fightStatus.healingSpell.numberUse = healingSpellUsed
                player.fightStatus.healingSpell.level = healingSpellLevel
                player.fightStatus.lightArmor.numberHit = lightArmorHit
                player.fightStatus.lightArmor.level = lightArmorLevel
                player.fightStatus.mediumArmor.numberHit = mediumArmorHit
                player.fightStatus.mediumArmor.level = mediumArmorLevel
                player.fightStatus.heavyArmor.numberHit = heavyArmorHit
                player.fightStatus.heavyArmor.level = heavyArmorLevel
                self.party.addMember(player)
        if p_file.readline()[:-1] != "File end":
            input("Error!!!")
            p_file.close()
            return None
        p_file.close()
        print("File loaded")
        return new_map
