from core.Game_Map import GameMap
from core.Handlers.Fight_Handler import FightHandler
from core.Handlers.Menu_Handler import MenuHandler
from core.Handlers.World_Handler import WorldHandler
from core.Life_Entities import PlayerParty
from core.Saving_Process import SaveFile
from interface.Pygame_Basic import *

gameMode = ["world", "menu", "fight"]


# need a complete restructuration, will need to be declared as main
# and never run directly the interface loop, but rather delegate it to Handlers instances
class Game:
    def __init__(self):
        self.mode = gameMode[0]
        self.map = GameMap()
        self.team = PlayerParty()
        self.save = SaveFile(self.team)
        self.world_handler = WorldHandler()
        self.menu_handler = MenuHandler()
        self.fight_handler = FightHandler()
        self.quit = False

    def changeMode(self, mode):
        if mode == gameMode[0]:
            self.mode = gameMode[0]
        elif mode == gameMode[1]:
            self.mode = gameMode[1]
        elif mode == gameMode[2]:
            self.mode = gameMode[2]
        else:
            raise ValueError

    def loadFile(self, file_nbr):
        self.map = self.save.newLoadingChoice(file_nbr)

    def saveFile(self, file_nbr):
        pass

    def mainLoop(self):
        while not self.quit:
            if self.mode == gameMode[0]:
                event = self.fight_handler.getEvent()
                if event != "quit":
                    self.fight_handler.setAction(event)
                    self.fight_handler.display()
                else:
                    self.quit = True
            elif self.mode == gameMode[1]:
                event = self.fight_handler.getEvent()
                if event != "quit":
                    self.fight_handler.setAction(event)
                    self.fight_handler.display()
                else:
                    self.quit = True
            elif self.mode == gameMode[2]:
                event = self.fight_handler.getEvent()
                if event != "quit":
                    self.fight_handler.setAction(event)
                    self.fight_handler.display()
                else:
                    self.quit = True
            else:
                raise ValueError
            pygame.display.update()
            clock.tick(fps)
