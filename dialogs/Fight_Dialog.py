from core.Global import moveList


def intro_dialog(enemy_party):
    text = ["{}'s party encountered".format(enemy_party.leader), "Begin fight!"]
    return text


# todo: add proper dialog to handle active status, and add p dialog
def preturn_dialog(fighter, action):
    text = []
    for index in range(len(fighter.activeStatusEffects)):
        print("Status turn left: {}".format(fighter.activeStatusEffects[index].duration))
        if fighter.activeStatusEffects[index].effect_type == "hp damage":
            text = status_effect_damage_dialog(text, fighter, action.effect_points[index],
                                               fighter.activeStatusEffects[index])
    if action.isParalyzed:
        text.append("{} is stunned and cannot move!".format(fighter))
    for status in fighter.activeStatusEffects:
        if status.duration == 0:
            text.append("{} is no longer affected by {}".format(fighter, status.name))
    for status in fighter.passiveStatusEffects:
        if status.duration == 0:
            text.append("{} is no longer affected by {}".format(fighter, status.name))
    return text


def status_effect_damage_dialog(text, fighter, damage, statusEffect):
    if damage > 0:
        if statusEffect.name == "leech life":
            text.append("{} steal {} health point to {} from leech life".format(statusEffect.caster, damage, fighter))
        else:
            text.append("{} suffer {} {} damage".format(fighter, damage, statusEffect.name))
        if fighter.hpleft - damage <= 0:
            text.append("{} fainted".format(fighter))
    return text


def player_header_dialog(fighter):
    text = ["{}'s turn, what do you want to do?".format(fighter)]
    return text


def player_choice_dialog():
    text = ["-Attack", "-Cast spell", "-Use item", "-Defend", "-Flee"]
    return text


def player_header_move_dialog(fighter, action):
    text = []
    if action == moveList[0]:
        text = ["Which ability do you want to use?"]
    elif action == moveList[1]:
        text = ["Which spell do you want to use?"]
    elif action == moveList[2]:
        text = ["Which item do you want to use?"]
    elif action == moveList[3]:
        text = ["{} is defending".format(fighter)]
    elif action == moveList[4]:
        text = ["Trying to flee"]
    return text


def player_header_no_move_dialog(action):
    text = []
    if action == moveList[0]:
        text = ["No attack available"]
    elif action == moveList[1]:
        text = ["No spell available"]
    elif action == moveList[2]:
        text = ["No item available"]
    return text


def player_choice_move_dialog(fighter, action):
    text = []
    if action == moveList[0]:
        for move in fighter.moveset:
            text.append("-{}, cost: {}{}".format(move, move.cost, move.cost_type))
    elif action == moveList[1]:
        for spell in fighter.spellList:
            text.append("-{}, cost: {} MP".format(spell, spell.cost))
    elif action == moveList[2]:
        for item in fighter.usableItems:
            text.append("-{}".format(item))
    return text


def player_header_target_dialog():
    return ["Choose your target:"]


def player_choice_target_dialog(targetList, size, group_type):
    text = []
    if size == "1":
        for target in targetList:
            text.append("-{}".format(target))
    else:
        if group_type == "Enemy":
            text.append("-All enemies")
        elif group_type in ["Party", "Fallen"]:
            text.append("-Party")
    return text


def player_header_no_target_dialog():
    return ["No target available"]


def no_escape_dialog():
    return ["You can't flee this fight!"]


def action_cast_dialog(fighter, targets, action):
    text = []
    if action.damages[0] == -1:
        text = ["Action failed, not enough {}".format(action.ability.cost_type)]
    else:
        if action.moveType == moveList[0]:
            if action.ability.ability_type == "recovery ability":
                text = recovery_ability_dialog(fighter, action)
            else:
                text = attack_dialog(fighter, targets, action)
        elif action.moveType == moveList[1]:
            text = spell_cast_dialog(fighter, targets, action)
        elif action.moveType == moveList[2]:
            text = item_use_dialog(fighter, targets, action)
        elif action.moveType == moveList[3]:
            text = defend_dialog(fighter)
        elif action.moveType == moveList[4]:
            text = escape_dialog(action.ability.isSuccessful)
    return text


def recovery_ability_dialog(fighter, action):
    text = []
    text.append("{} used {}".format(fighter, action.ability))
    text.append("{} recovered {} {} points".format(fighter, action.damages[0], action.ability.damage_type))
    return text


def attack_dialog(fighter, targets, action):
    text = []
    if str(action.ability) == "attack":
        text.append("{} used {} {} on {}".format(fighter, fighter.article(), fighter.weapon, targets[0]))
        if action.critical:
            text.append("Critical strike!")
        text = target_attack_dialog(text, 0, targets, action)
    else:
        if action.ability.targetGroupSize == "1":
            if action.ability.name == "charge strike":
                text = charging_dialog(text, fighter)
            else:
                text.append("{} used {} on {}".format(fighter, action.ability, targets[0]))
                if action.critical:
                    text.append("Critical strike!")
                text = target_attack_dialog(text, 0, targets, action)
        else:
            text.append("{} used {}".format(fighter, action.ability))
            if action.critical:
                text.append("Critical strike!")
            for index in range(len(targets)):
                text = target_attack_dialog(text, index, targets, action)
    return text


def spell_cast_dialog(fighter, targets, action):
    text = []
    if action.ability.search() == "OffensiveSpell":
        if action.ability.targetGroupSize == "1":
            text.append("{} casted {} on {}".format(fighter, action.ability, targets[0]))
        else:
            text.append("{} casted {}".format(fighter, action.ability))
        if action.critical:
            text.append("Critical strike!")
        for index in range(len(targets)):
            text = target_attack_dialog(text, index, targets, action)
    elif action.ability.search() == "HealingSpell":
        if action.ability.targetGroupSize == "1":
            text.append("{} casted {} on {}".format(fighter, action.ability, targets[0]))
        else:
            text.append("{} casted {}".format(fighter, action.ability))
        if action.critical:
            text.append("Critical strike!")
        for index in range(len(targets)):
            text.append("{} is healed by {} points".format(targets[index], action.damages[index]))
            for status in targets[index].activeStatusEffects:
                if str(status) == "bleeding" and status.spell_cured:
                    text.append("{} no longer bleed".format(targets[index]))
                    status.spell_cured = False
    elif action.ability.search() == "RecoverySpell":
        if action.ability.targetGroupSize == "1":
            text.append("{} casted {} on {}".format(fighter, action.ability, targets[0]))
        else:
            text.append("{} casted {}".format(fighter, action.ability))
            for status in fighter.activeStatusEffects:
                if status.duration == 0 and status.spell_cured:
                    text.append("{} is no longer affected by {}".format(fighter, status.name))
    elif action.ability.search() == "StatusSpell":
        if action.ability.targetGroupSize == "1":
            text.append("{} casted {} on {}".format(fighter, action.ability, targets[0]))
        else:
            text.append("{} casted {}".format(fighter, action.ability))
        for index in range(len(targets)):
            text = extra_dialog(text, targets[index], action.extra[index])
    return text


def item_use_dialog(fighter, targets, action):
    text = []
    if action.ability.targetGroupSize == "1":
        text.append("{} used {} on {}".format(fighter, action.ability, targets[0]))
        if action.ability.ability_type == "Recovery item":
            if action.damages[0] > 0:
                text.append("{} recovered {} {}".format(targets[0], abs(action.damages[0]),
                                                        action.ability.recovery_type))
            else:
                text.append("It has no effect")
        elif action.ability.ability_type == "Revive item":
            text.append("{} is back!".format(targets[0]))
    else:
        text.append("{} used {}".format(fighter, action.ability))
        for index in range(len(targets)):
            if action.damages[0] > 0:
                text.append("{} recovered {} {}".format(targets[0], abs(action.damages[0]),
                                                        action.ability.recovery_type))
            else:
                text.append("It has no effect")
    return text


def defend_dialog(fighter):
    return ["{} is defending".format(fighter)]


def target_attack_dialog(text, index, targets, action):
    if action.damages[index] == 0:
        text.append("{} dodged the attack".format(targets[index]))
    else:
        text.append("{} suffered {} damage".format(targets[index], action.damages[index]))
        if targets[index].hpleft - action.damages[index] <= 0:
            text.append("{} fainted".format(targets[index]))
        else:
            text = extra_dialog(text, targets[index], action.extra[index])
    return text


def charging_dialog(text, fighter):
    text.append("{} is charging".format(fighter))
    return text


def extra_dialog(text, target, status):
    if status is not None:
        if status.search() == "active":
            text.append("{} got {}".format(target, status.afflict_text))
        elif status.search() == "passive":
            if status.name in ["critical boost", "freezing"]:
                text.append("{} got {}".format(target, status.afflict_text))
            else:
                text.append("{} got {} by {}".format(target, status.afflict_text, status.effect_point))
    return text


def win_dialog(reward_text, enemy_party):
    text = []
    text.append("{}'s party is defeated".format(enemy_party.leader))
    text.append("Victory!")
    for line in reward_text:
        text.append(line)
    return text


def lose_dialog():
    text = ["You have been defeated", "Game over"]
    return text


def escape_dialog(escaped):
    if escaped:
        text = ["Party successfully escaped"]
    else:
        text = ["Escape failed"]
    return text


def display_test():
    return ["this is a test"]


def display_test_2():
    return ["testing"]
